import sys

import torch
from torch.utils.data import DataLoader, Dataset, TensorDataset
from torchvision.transforms import ToTensor

import config
from load_data import *
from model import *
from train import _train_ae, _train_nn, _train_dagger
from test import _test_ae, _test_nn

filename = sys.argv[1] #name for logging and visualization purposes

if config.TRAIN_AE:
    #autoencoder training
    
    print("Load and preprocess ae data...")
    x_v, y = load_and_preprocess_ae()

    #convert arrays to torch tensors
    x_v = torch.Tensor(x_v)
    y = torch.Tensor(y)

    #prepare dataset and dataloader
    print("Set up data loader...")
    dataset = TensorDataset(x_v, y)
    num_workers = 0
    if torch.cuda.is_available(): num_workers = 4
    dataloader = DataLoader(dataset, batch_size = config.BATCH_SIZE, shuffle=True, num_workers=num_workers)

    print("Train autoencoder...")
    model = AE(skip_concat=True)
    _train_ae(model, dataset, dataloader, filename)
    print("Done.")

    dir_ = 'models'
    if os.path.exists(dir_): 
        pass
    else: 
        os.makedirs(dir_)

elif config.TRAIN_NN:
    #time integration network training
    
    print("Load encodings...")
    x, _, y, _ = load_encodings_nn()

    #convert arrays to torch tensors
    x = torch.Tensor(x)
    y = torch.Tensor(y)

    #prepare the dataset and dataloader
    print("Set up data loader...")
    dataset = TensorDataset(x, y)
    num_workers = 0
    if torch.cuda.is_available(): num_workers = 4
    
    print('Train Time integration network...')
    if config.USE_LSTM:
        dataloader = DataLoader(dataset, batch_size = config.BATCH_SIZE_LSTM, shuffle=True, num_workers=num_workers)
        net = TimeIntegration2DLSTM()
    else:
        dataloader = DataLoader(dataset, batch_size = config.BATCH_SIZE_NN, shuffle=True, num_workers=num_workers)
        net = TimeIntegration2D()
        
    _train_nn(net, dataloader, filename)
    print('Done.')

elif config.TRAIN_DAGGER:
    #dagger training
    _train_dagger(filename)
    print('Done.')
    
#Test model
else:
    ''' print("Test autoencoder...")
    print("Load and preprocess data...")
    x_v, y = load_and_preprocess_ae()

    #convert arrays to torch tensors
    x_v = torch.Tensor(x_v)
    y = torch.Tensor(y)

    #prepare the dataset and dataloader
    print("Set up data loader...")
    dataset = TensorDataset(x_v, y)
    num_workers = 0
    if torch.cuda.is_available(): num_workers = 4
    test_dataloader = DataLoader(dataset, batch_size = config.BATCH_SIZE, shuffle=True, num_workers=num_workers)
    _test_ae(test_dataloader, filename) '''
    
    ''' print('Encode data for Dagger learning')
    load_and_preprocess_dagger() '''
    
    print('Test Time integration...')
    x, _, y, sc, fr = load_encodings_nn()
    x = torch.Tensor(x)
    y = torch.Tensor(y)
    sc = torch.Tensor(sc)
    fr = torch.Tensor(fr)
    
    dataset = TensorDataset(x, y, sc, fr)
    num_workers = 0
    if torch.cuda.is_available(): num_workers = 4
    test_dataloader = DataLoader(dataset, batch_size = 1, shuffle=True, num_workers=num_workers)
    
    ''' autoencoder = AE()
    autoencoder.load_state_dict(torch.load('models/ae/model_new.pt', map_location='cpu'))
    x_samples_batch, label, cnt = next(iter(test_dataloader))
    gt_samples = np.empty((config.W_NUM, config.Y_RES, config.X_RES, 2))
    for i in range(config.W_NUM):
        gt_dec = autoencoder.decoder(x_samples_batch[:,i,:])
        gt_dec = gt_dec.detach().numpy()
        gt_samples[i] = np.array(gt_dec[0]).transpose(1,2,0)
        
    gt_im = save_img(gt_samples, filename="abc.png")
    save_image_batch(gt_batch=gt_im, pred_batch=gt_im, filename="vis/test/vis_"+filename+"/"+str(0)+"nn_test.png", batch_size=config.W_NUM) '''
    _test_nn(test_dataloader, filename)
    print("Done")
