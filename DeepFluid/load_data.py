import glob
import numpy as np
import os
import config
import torch
from collections import deque

import sys
from torch.utils.data import DataLoader, Dataset, TensorDataset
from torchvision.transforms import ToTensor
from utils import *
from model import *
if not torch.cuda.is_available():
    sys.path.insert(1, '/cluster/home/jherter/DeepFluid/deepfluid_rl/Scene generation') #For Euler cluster with gpu
    sys.path.insert(1, 'C:\\Users\\98her\\Desktop\\ETH\\BA- cgl\\deepfluid-rl\\DeepFluid_pytorch\\Scene generation')
else:
    sys.path.insert(1, '/cluster/home/jherter/DeepFluid/deepfluid_rl/Scene generation') #For Euler cluster with gpu
    
from differentiable_fluid_functions.utils.io import save_img, save_img_help
from differentiable_fluid_functions.utils.plot import _process_img_2d, show_2d_image
from smoke2d_integration import advect_vel

from tqdm import tqdm
from datetime import datetime


def load_and_preprocess_ae():
    """load and normalize data for autoencoder training or testing

    Returns:
        list, list: velocity fields and corresponding supervised parameters
    """
        
    #get velocity and density data
    if not torch.cuda.is_available():
        directory = "C:\\Users\\98her\\Desktop\\ETH\\BA- cgl\\deepfluid-rl\\DeepFluid_pytorch\\Scene generation\\data\\" + config.DATASET 
        filelist_v = glob.glob("C:\\Users\\98her\\Desktop\\ETH\\BA- cgl\\deepfluid-rl\\DeepFluid_pytorch\\Scene generation\\data\\smoke_mov200_f400\\v\\*.npz") 
        directory = "/cluster/scratch/jherter/data/" + config.DATASET + '/dagger'#For Euler cluster
        filelist_v = glob.glob("/cluster/scratch/jherter/data/smoke_mov200_f400/dagger/v/*.npz") #For Euler cluster
    else:
        directory = "/cluster/scratch/jherter/data/" + config.DATASET + '/dagger'#For Euler cluster
        filelist_v = glob.glob("/cluster/scratch/jherter/data/smoke_mov200_f400/dagger/v/*.npz") #For Euler cluster
        
    #get arguments from scene generation
    args = {}
    with open(os.path.join(directory,'args.txt'), 'r') as f:
        while True:
            line = f.readline()
            if not line:
                break
            arg, arg_value = line[:-1].split(': ')
            args[arg] = arg_value
            print(args[arg])
    
    #d_range = np.max(np.abs(d_range_bounds))
    d_range = 1.0
    print(d_range)
    #v_range_bounds = np.loadtxt(os.path.join(directory, 'v_range.txt'))
    #v_range = np.max(np.abs(v_range_bounds))
    v_range = 11.338
    print(v_range)

    xs_v = []
    #xs_d = []
    ys = []
    
    
    if not config.TRAIN_AE:
        num_sim = int(args['num_simulations_test'])
        start_idx = int(args['num_simulations']) - int(args['num_simulations_test'])
        num_sim = 3_000
    else:
        num_sim = int(args['num_simulations']) - int(args['num_simulations_test'])
        start_idx = 0
        
    for cnt in tqdm(range(num_sim)):
        with np.load(filelist_v[cnt+start_idx]) as data_v:
            x_v = data_v['x']
            x_v = np.array(x_v[0]).transpose(2,0,1) #2x128x96
            
            y = data_v['y'][0]
            #normalize x_v data 
            x_v /= v_range
            #normalize y data (y[0] = src_x_pos, y[1] = timeframenr) between [-1,1]
            y[0] = (y[0] - (-1)) / (1 - (-1)) * 2 - 1
            y[1] = (y[1] - 0) / (399 - 0) * 2 - 1
        ''' with np.load(filelist_d[cnt+start_idx]) as data_d:
            x_d = data_d['x']
            x_d = np.array(x_d[0]).transpose(2,0,1) #1x128x96
            x_d /= d_range '''
        
        #add density scalar field to third channel -> x.shape = 3x128x96
        #xs_d.append(x_d)
        xs_v.append(x_v)
        ys.append(y)

    return xs_v, ys


def load_and_preprocess_nn():
    """load and normalize velocity fields and use pretrained encoder to project them to
       latent space. Store encodings as sliding windows of size config.W_NUM for each scene
    """
    
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    
    if not torch.cuda.is_available():
        autoencoder = AE().to(device)
        autoencoder.load_state_dict(torch.load('models/ae/model_new.pt', map_location=device))
    else:
        autoencoder = AE().to(device)
        autoencoder.load_state_dict(torch.load('models/ae/model_new.pt', map_location=device))
    
    autoencoder.eval()
   
    #get velocity data    
    if not torch.cuda.is_available():
        directory = "/cluster/scratch/jherter/data/" + config.DATASET #For Euler cluster
        filelist_v = sorted(glob.glob("/cluster/scratch/jherter/data/smoke_mov200_f400/v/*.npz"), key=os.path.getctime) #For Euler cluster
    else:
        directory = "/cluster/scratch/jherter/data/" + config.DATASET #For Euler cluster
        filelist_v = sorted(glob.glob("/cluster/scratch/jherter/data/smoke_mov200_f400/v/*.npz"), key=os.path.getctime) #For Euler cluster
    
    #get arguments from scene generation
    args = {}
    with open(os.path.join(directory,'args.txt'), 'r') as f:
        while True:
            line = f.readline()
            if not line:
                break
            arg, arg_value = line[:-1].split(': ')
            args[arg] = arg_value
            print(args[arg])
    
    v_range_bounds = np.loadtxt(os.path.join(directory, 'v_range.txt'))
    v_range = np.max(np.abs(v_range_bounds))
    print(v_range)
    
    #xs = []
    #ys = []
    x_w = deque()
    y_w = deque()
    w_cnt = 0
    enc_cnt = 0
    for cnt in tqdm(range(int(args['num_simulations']))):
        #print(int(args['num_frames']))
        if not config.TRAIN_NN and not config.TRAIN_DAGGER:
            if cnt == config.DATACOUNT_TEST_NN: break
            
        with np.load(filelist_v[cnt]) as data:
            
            if cnt % int(args['num_frames']) == 0:
                x_w = deque()
                y_w = deque()
                w_cnt = 0
            
            x = data['x']
            x = np.array(x[0]).transpose(2,0,1)
            y = data['y'][0]
            x /= v_range
            
            #normalize y data (y[0] = src_x_pos, y[1] = timeframenr) between [-1,1]
            y[0] = (y[0] - (-0.75)) / (0.75 - (-0.75)) * 2 - 1
            y[1] = (y[1] - 0) / (399 - 0) * 2 - 1
            
            x_w.append(x)
            y_w.append(y)
            w_cnt += 1
            
            if w_cnt >= config.W_NUM:
                with torch.no_grad():
                    x_w_tens = torch.Tensor(x_w) 
                    x_w_tens = x_w_tens.to(device)
                    enc_xw = autoencoder.encoder(x_w_tens)
                    enc_xw = enc_xw.to('cpu')
                    
                    np.savez('encodings/enc_'+ str(enc_cnt) +'.npz', x=np.array(enc_xw), y=np.array(y_w))
                    enc_cnt += 1
                
                _ = x_w.popleft()
                _ = y_w.popleft()
                    
    #return xs, ys
    
def load_encodings_nn(dagger_tr=False, dir=''):
    """load encodings for training or testing the time integration network

    Args:
        dagger_tr (bool, optional): Indicates whether method call is instance of a dagger training or NN training. Defaults to False.
        dir (str, optional): directory of encodings. Defaults to ''.

    Returns:
        list, list, list, list, list: encoded velocity field windows, corresponding density fields, 
                                      encoded supervised parameters, scene numbers, frame numbers of starting encoding
    """
    
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
   
    #get velocity data    
    if not torch.cuda.is_available():
        filelist = sorted(glob.glob('C:/Users/98her/Desktop/ETH/BA- cgl/deepfluid-rl/DeepFluid_pytorch/DeepFluid/encodings_dagger/*.npz'), key=os.path.getctime)
        filelist_d = sorted(glob.glob("C:\\Users\\98her\\Desktop\\ETH\\BA- cgl\\deepfluid-rl\\DeepFluid_pytorch\\Scene generation\\data\\dagger\\d\\*.npz"))

        filelist = sorted(glob.glob("/cluster/home/jherter/DeepFluid/deepfluid_rl/DeepFluid/encodings_dagger/*.npz"), key=os.path.getctime)
        filelist_d = sorted(glob.glob("/cluster/scratch/jherter/data/smoke_mov200_f400/dagger/d/*.npz")) #For Euler cluster
        if dagger_tr:
            filelist = sorted(glob.glob("/cluster/scratch/jherter/data/smoke_mov200_f400/dagger/"+ dir +"/encodings/*.npz"), key=os.path.getctime) #For Euler cluster cpu
            filelist_d = sorted(glob.glob("/cluster/scratch/jherter/data/smoke_mov200_f400/dagger/"+ dir +"/d/*.npz"), key=os.path.getctime)
    else:
        filelist = sorted(glob.glob("/cluster/home/jherter/DeepFluid/deepfluid_rl/DeepFluid/encodings_dagger/*.npz"), key=os.path.getctime) #For Euler cluster    
        filelist_d = sorted(glob.glob("/cluster/scratch/jherter/data/smoke_mov200_f400/dagger/d/*.npz"))
        if dagger_tr:
            filelist = sorted(glob.glob("/cluster/scratch/jherter/data/smoke_mov200_f400/dagger/"+ dir +"/encodings/*.npz")) #For Euler cluster cpu
            filelist_d = sorted(glob.glob("/cluster/scratch/jherter/data/smoke_mov200_f400/dagger/"+ dir +"/d/*.npz"), key=os.path.getctime)
        
    xs = []
    xs_d = []
    ys = []
    scenes_list = []
    frames_list = []
    cnt_tmp = 0
    curr_scene = 0
    cnt_d = 0
    n_iter = config.DATACOUNT_TRAIN_NN
    n_iter = len(filelist)
    nframes = config.NUM_FRAMES
    if dagger_tr: 
        n_iter = len(filelist)
        print(n_iter)
        nframes = config.SAMPLE_FRAMES_PER_TRAJECTORY
    for cnt in tqdm(range(n_iter)):
        if cnt_tmp == nframes - (config.W_NUM - 1):
            curr_scene += 1
            cnt_tmp = 0
        if not config.TRAIN_NN and not config.TRAIN_DAGGER:
            if cnt == config.DATACOUNT_TEST_NN: break
        with np.load(filelist[cnt]) as data:
            x = data['x']
            y = data['y']
            sc = data['s']
            fr = data['f']
            scenes_list.append(int(sc))
            frames_list.append(int(fr+1))
            xs.append(list(x))
            ys.append(list(y))
            cnt_d = cnt + curr_scene*(config.W_NUM-1)
        with np.load(filelist_d[cnt_d]) as data_d:
            #print(filelist_d[cnt_d])
            #print(filelist[cnt])
            x_d = data_d['x']
            x_d = np.array(x_d[0]).transpose(2,0,1) #1x128x96
            xs_d.append(x_d)
            
        cnt_tmp += 1
    
    return xs, xs_d, ys, scenes_list, frames_list

def load_and_preprocess_dagger(dagger_tr=False, dir=''):
    """encode velocity fields for dagger training

    Args:
        dagger_tr (bool, optional): Is set to false for training of first policy on initial dataset. Set to True afterwards. Defaults to False.
        dir (str, optional): directory for dagger data. Defaults to ''.
    """
    
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    
    if not torch.cuda.is_available():
        autoencoder = AE().to(device)
        autoencoder.load_state_dict(torch.load('models/ae/model_new.pt', map_location=device))
    else:
        autoencoder = AE().to(device)
        autoencoder.load_state_dict(torch.load('models/ae/model_new.pt', map_location=device))
    
    #autoencoder = autoencoder.to('cpu')
    autoencoder.eval()
   
    #get velocity data    
    if not torch.cuda.is_available():
        filelist_v = sorted(glob.glob("/cluster/scratch/jherter/data/smoke_mov200_f400/dagger/v/*.npz")) #For Euler cluster cpu
        if dagger_tr:
            filelist_v = sorted(glob.glob("/cluster/scratch/jherter/data/smoke_mov200_f400/dagger"+ dir +"/v/*.npz")) #For Euler cluster cpu
    else:
        filelist_v = sorted(glob.glob("/cluster/scratch/jherter/data/smoke_mov200_f400/dagger/v/*.npz"))
        if dagger_tr:
            filelist_v = sorted(glob.glob("/cluster/scratch/jherter/data/smoke_mov200_f400/dagger/"+ dir +"/v/*.npz"))
            
    if dagger_tr:    
        enc_dir = "/cluster/scratch/jherter/data/smoke_mov200_f400/dagger/"+ dir +"/encodings"
        if not os.path.exists(enc_dir):
            os.makedirs(enc_dir)

    #get arguments from scene generation
    args = {}
    with open(os.path.join('/cluster/scratch/jherter/data/' + config.DATASET, 'args.txt'), 'r') as f:
        while True:
            line = f.readline()
            if not line:
                break
            arg, arg_value = line[:-1].split(': ')
            args[arg] = arg_value
            print(args[arg])
    
    #v_range_bounds = np.loadtxt(os.path.join(directory, 'v_range.txt'))
    v_range = 11.338
    d_range = 1.0
    
    #xs = []
    #ys = []
    x_w = deque()
    y_w = deque()
    w_cnt = 0
    enc_cnt = 0
    niters = int(args['num_simulations'])
    nframes = int(args['num_frames']) 
    if dagger_tr: 
        niters = len(filelist_v)
        nframes = config.SAMPLE_FRAMES_PER_TRAJECTORY
        
    for cnt in tqdm(range(niters)):
        #print(int(args['num_frames']))
        ''' if not config.TRAIN_NN:
            if cnt == config.DATACOUNT_TEST_NN: break '''
            
        with np.load(filelist_v[cnt]) as data_v:
            
            if cnt % nframes == 0:
                x_w = deque()
                y_w = deque()
                w_cnt = 0
            
            x_v = data_v['x']
            x_v = np.array(x_v[0]).transpose(2,0,1)
            y = data_v['y'][0]
            x_v /= v_range
            
            x = np.copy(x_v)
                        
            #normalize y data (y[0] = src_x_pos, y[1] = timeframenr) between [-1,1]
            # -0.6553226517752255 to 0.7031151854433841
            y[0] = (y[0] - (-0.75)) / (0.75 - (-0.75)) * 2 - 1
            y[1] = (y[1] - 0) / (399 - 0) * 2 - 1
            
            x_w.append(x)
            y_w.append(y)
        
            w_cnt += 1
            
            if w_cnt >= config.W_NUM:
                with torch.no_grad():
                    x_w_tens = torch.Tensor(x_w) 
                    x_w_tens = x_w_tens.to(device)
                    enc_xw = autoencoder.encoder(x_w_tens)
                    enc_xw = enc_xw.to('cpu')
                    sc = int(data_v['s'])
                    fr = int(data_v['f'] - config.W_NUM)
                    
                    np_path = "/cluster/scratch/jherter/data/smoke_mov200_f400/dagger/"+ dir +"/encodings/enc_"+ str(enc_cnt) +'.npz'  
                    np.savez(np_path, x=np.array(enc_xw), y=np.array(y_w), s=sc, f=fr)
                    enc_cnt += 1
                    
                _ = x_w.popleft()
                _ = y_w.popleft()
  
if __name__ == "__main__":
    filelist_v = sorted(glob.glob("C:\\Users\\98her\\Desktop\\ETH\\BA- cgl\\deepfluid-rl\\DeepFluid_pytorch\\Scene generation\\data\\dagger\\v\\*.npz"))

    ''' x, x_d, y, sc, fr = load_encodings_nn()
    v_range = 11.338
    x = torch.Tensor(x)
    x_d = torch.Tensor(x_d)
    y = torch.Tensor(y)
    sc = torch.Tensor(sc)
    fr = torch.Tensor(fr)
    print(x.shape)
    print(x_d.shape)
    print(y.shape)
    print(sc.shape)
    print(fr.shape)
    dataset = TensorDataset(x, x_d, y, sc, fr)
    num_workers = 0
    if torch.cuda.is_available(): num_workers = 4
    test_dataloader = DataLoader(dataset, batch_size = 1, shuffle=True, num_workers=num_workers)
        
    autoencoder = AE()
    autoencoder.load_state_dict(torch.load('models/ae/model_new.pt', map_location='cpu'))
    autoencoder.eval()
    cnt = 0
    for (input_v, input_d, label, sc, fr) in test_dataloader:
        if cnt == 1: break
        rec_v = autoencoder.decoder(input_v)
        rec_v = rec_v.detach().numpy()
        
        advect_vel(rec_v[0]*v_range, np.array(input_d), (label[0][0][0]+ 1) * 0.75 - 0.75, int(sc), int(fr), 30)
        cnt += 1
        
        out_tr = np.array(rec_v).transpose(0,2,3,1)
        out_batch = save_img(out_tr, filename="abc.png", chan=3)
        x_batch = np.empty((30,128,96,2))
        for i in range(30):
            with np.load(filelist_v[int(fr + i) + int(sc)*(400)]) as data_v:
                x_batch[i] = data_v['x']
        x_batch = save_img(x_batch, filename="abc.png", chan=3)
        save_image_batch(gt_batch=x_batch, pred_batch=out_batch, filename="data/test_adv_extrpol/vis_test_v.png", chan=3, batch_size=30) '''
        
    filelist_d = sorted(glob.glob("C:\\Users\\98her\\Desktop\\ETH\\BA- cgl\\deepfluid-rl\\DeepFluid_pytorch\\Scene generation\\data\\dagger\\epoch_1\\d\\*.npz"), key=os.path.getctime)
    filelist_v = sorted(glob.glob("C:\\Users\\98her\\Desktop\\ETH\\BA- cgl\\deepfluid-rl\\DeepFluid_pytorch\\Scene generation\\data\\dagger\\epoch_1\\v\\*.npz"), key=os.path.getctime)
    filelist_gt = sorted(glob.glob("C:\\Users\\98her\\Desktop\\ETH\\BA- cgl\\deepfluid-rl\\DeepFluid_pytorch\\Scene generation\\data\\dagger\\v\\*.npz"), key=os.path.getctime)
    ''' v_range=11.338
    with np.load(filelist_d[12]) as data_d:
        print(filelist_d[12])
        x = data_d['x']
        save_img_help(x, 'd1_test.png', chan=1)
    with np.load(filelist_v[12]) as data_v:
        x = data_v['x']
        save_img_help(x, 'v1_test.png', chan=3)
        print(x.shape)
        x = torch.Tensor(x.transpose(0,3,2,1) / v_range)
        autoencoder = AE()
        autoencoder.load_state_dict(torch.load('models/ae/model_new.pt', map_location='cpu'))
        autoencoder.eval()
        out, z = autoencoder(x)
        out = out.detach().numpy().transpose(0,2,3,1)
        save_img_help(out, 'vae_test.png', chan=3) '''