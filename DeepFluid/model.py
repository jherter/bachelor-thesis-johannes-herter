from tokenize import Double
from typing import OrderedDict
import torch
import torch.nn as nn 
import torch.nn.functional as F
import torch.optim as optim
import numpy as np
import math
import functools
import operator

import config
from utils import *


def curl2d(x):
    
    u = x[:, :, 1:, :] - x[: , :, :-1, :] # -ds/dy,
    v = x[: , :, :, :-1] - x[: , :, :, 1:] # ds/dx
    u = torch.cat([u, torch.unsqueeze(u[:, : , -1, :], axis=2)], axis=2)
    v = torch.cat([v, torch.unsqueeze(v[:, :, :, -1], axis=3)], axis=3)
    c = torch.cat([u,v], axis=1)

    return c

def weights_init(m):
    """Weights initialization for the models

    Args:
        m (nn.Module): linear layer from torch.nn.module
    """
    
    if isinstance(m, nn.Linear):
        torch.nn.init.xavier_uniform(m.weight)
        m.bias.data.fill_(0.01)

class BigBlock(nn.Module):

    def __init__(self, in_channels, conv_layers_per_bb=4):
        """Big block as defined in the Deepfluids paper (SB block in my thesis): A block with convolutional layers and skip connections

        Args:
            in_channels (int): number of in channels
            conv_layers_per_bb (int): number of convolutional blocks per big block. Defaults to 4.
        """

        super(BigBlock, self).__init__()
        
        self.conv_layers = nn.ModuleList([])
        self.leaky = nn.LeakyReLU(0.2)

        for idx in range(conv_layers_per_bb):
            if idx==0: in_ch = in_channels
            else: in_ch = config.NUM_CHANNELS
            self.conv_layers.append(nn.Conv2d(in_ch, config.NUM_CHANNELS, 3, padding=1).apply(weights_init))

    def forward(self, x, skip_concat=True, conv_layers_per_bb=3):
        x0 = x
        for i in range(conv_layers_per_bb):
            x = self.leaky(self.conv_layers[i](x))
        
        #skip concat connection
        return torch.cat((x, x0), axis=1) if skip_concat else x


class Encoder2D(nn.Module):

    def __init__(self, input_dim, z_num, skip_concat):
        """Encoder model mapping from [N,C,H,W] to [N,z_num]

        Args:
            input_dim (tuple): dimension of the input, should be (C,H,W)
            z_num (int): dimension of latent space
            skip_concat (bool): use skip connections if set true
        """
        super(Encoder2D, self).__init__()

        self.act = nn.LeakyReLU(0.2)
        self.include_density_field = False
        input_ch = 2
        if self.include_density_field: input_ch = 3
        filters = 128
        ch = filters

        pre_layers = OrderedDict()
        pre_layers[str(0)] = nn.Conv2d(input_ch, ch, kernel_size=3, padding=1)
        idx_2 = 1
        repeat = 5
        for idx in range(repeat):
            pre_layers[str(idx_2)] = BigBlock(ch,conv_layers_per_bb=config.CONV_LAYERS_PER_BB-1)
            idx_2 += 1
            if idx < repeat-1:
                ch += filters
                in_channels_conv = ch if skip_concat else filters
                pre_layers[str(idx_2)] = nn.Conv2d(in_channels_conv, ch, kernel_size=3, padding=1, stride=2)
                idx_2 += 1
                pre_layers[str(idx_2)] = self.act
                idx_2 += 1
        
        self.pre_network = nn.Sequential(pre_layers)
        self.pre_network.apply(weights_init)
        
        #given pre_network out_channels, compute in_channels for linear layers
        dim = functools.reduce(operator.mul, list(self.pre_network(torch.rand(1, *input_dim)).shape))
        self.Linear_Layer = nn.Linear(dim, z_num).apply(weights_init)

    def forward(self, x):

        x = torch.tensor(x)
        out = self.pre_network(x)
        out = out.view(x.shape[0], -1)
        
        return self.Linear_Layer(out)
    
    
class Decoder2D(nn.Module):
    
    def __init__(self, z_num):
        """Decoder model mapping from [N,z_num] to [N,C,H,W]

        Args:
            z_num (int): dimension of latent space
        """
        super(Decoder2D, self).__init__()

        self.upsampler = nn.Upsample(scale_factor=2, mode='nearest')
        
        self.pre_reshape_dim = 128 * 8 * 6 # (n_channels, height, width) of the first feature map: 8=128/16, 6=96/16
        self.Linear_Layer = nn.Linear(z_num, self.pre_reshape_dim).apply(weights_init)
        
        self.big_blocks = nn.ModuleList([])

        for i in range(config.NUM_BIG_BLOCKS+1):
            self.big_blocks.append(BigBlock(config.NUM_CHANNELS, config.CONV_LAYERS_PER_BB))

        self.to_RGB = nn.Conv2d(config.NUM_CHANNELS, 1, 3, padding=1).apply(weights_init)

    def forward(self, x):

        x = torch.tensor(x)

        #Project the input array to the first feature map
        out = self.Linear_Layer(x)
        out = torch.reshape(out, (-1, 128, 8, 6))
        out_0 = out #used for skip connection
        p = config.NUM_BIG_BLOCKS - 1

        for i in range(p + 1):
            #Additive skip connection (therefore set skip_concat to false)
            out = self.big_blocks[i].forward(out, skip_concat=False, conv_layers_per_bb=config.CONV_LAYERS_PER_BB)

            #We only upsample for the right amount of times, until the desired resolution is attained
            if i < math.log2(config.Y_RES / 8):
                out += out_0
                out = self.upsampler(out)
                out_0 = out
            else:
                out += out_0
        
        return curl2d(self.to_RGB(out))

class AE(nn.Module):
    
    def __init__(self, skip_concat=True):
        """Full autoencoder model combining Encoder and Decoder function

        Args:
            skip_concat (bool, optional): use skip connections if set true. Defaults to True.
        """

        super(AE, self).__init__()
        self.z_num = config.Z_NUM
        self.skip_concat = skip_concat
        self.encoder = Encoder2D(z_num=self.z_num, input_dim=tuple((2,128,96)), skip_concat = self.skip_concat)
        self.decoder = Decoder2D(z_num=self.z_num)
        self.parameters = list(self.encoder.parameters()) + list(self.decoder.parameters())
        self.optimizer = optim.Adam(self.parameters, betas=(config.BETA_1, config.BETA_2))

    def forward(self, x):

        z = self.encoder.forward(x)
        out_v = self.decoder.forward(z)

        return out_v, z



class EncoderSimple(nn.Module):
    
    def __init__(self, encoded_space_dim):
        """Simple Encoder architecture for testing purposes

        Args:
            encoded_space_dim (int): dimension of latent space
        """
        super().__init__()
        
        ### Convolutional section
        self.encoder_cnn = nn.Sequential(
            nn.Conv2d(2, 8, 4, stride=2, padding=1), #out: 8x64x48
            nn.ReLU(True),
            nn.Conv2d(8, 16, 4, stride=2, padding=1), #out: 16x32x24
            nn.BatchNorm2d(16),
            nn.ReLU(True),
            nn.Conv2d(16, 32, 4, stride=2, padding=1), #out: 32x16x12
            nn.ReLU(True)
        )
        
        ### Flatten layer
        self.flatten = nn.Flatten(start_dim=1)
        ### Linear section
        self.encoder_lin = nn.Sequential(
            nn.Linear(32*16*12, 128),
            nn.ReLU(True),
            nn.Linear(128, encoded_space_dim)
        )
        
    def forward(self, x):
        x = self.encoder_cnn(x)
        x = self.flatten(x)
        x = self.encoder_lin(x)
        return x

class DecoderSimple(nn.Module):
    
    def __init__(self, encoded_space_dim):
        """Simple Decoder architecture for testing purposes

        Args:
            encoded_space_dim (int): dimension of the latent space
        """
        
        super().__init__()
        self.decoder_lin = nn.Sequential(
            nn.Linear(encoded_space_dim, 128),
            nn.ReLU(True),
            nn.Linear(128, 32*16*12),
            nn.ReLU(True)
        )

        self.unflatten = nn.Unflatten(dim=1, 
        unflattened_size=(32, 16, 12))

        self.decoder_conv = nn.Sequential(
            nn.ConvTranspose2d(32, 16, 4, stride=2, padding=1), #out: 16x32x24
            nn.BatchNorm2d(16),
            nn.ReLU(True),
            nn.ConvTranspose2d(16, 8, 4, stride=2, padding=1), #out: 8x64x48
            nn.ReLU(True),
            nn.ConvTranspose2d(8, 1, 4, stride=2, padding=1) #out: RGB 128x96
        )
        
    def forward(self, x):
        x = self.decoder_lin(x)
        x = self.unflatten(x)
        x = self.decoder_conv(x)
        #x = torch.sigmoid(x)
        return curl2d(x)

class AESimple(nn.Module):

    def __init__(self):
        """Simple autoencoder model for teting purposes
        """

        super(AESimple, self).__init__()
        self.z_num = config.Z_NUM
        self.encoder = EncoderSimple(encoded_space_dim=16, fc2_input_dim=128)
        self.decoder = DecoderSimple(encoded_space_dim=16, fc2_input_dim=128)
        self.parameters = params_to_optimize = [
                        {'params': self.encoder.parameters()},
                        {'params': self.decoder.parameters()}
                        ]
        #self.optimizer = optim.Adam(self.parameters, lr=0.001, weight_decay=1e-05)
        self.optimizer = optim.Adam(self.parameters(), betas=(config.BETA_1, config.BETA_2))

    def forward(self, x):

        z = self.encoder.forward(x)
        out = self.decoder.forward(z)

        return out, z
    
class TimeIntegration2D(nn.Module):
    
    def __init__(self):
        """MLP model for latent space time integration
        """

        super(TimeIntegration2D, self).__init__()
        self.dropout = config.DROPOUT
        self.onum = config.O_NUM
        self.pnum = config.P_NUM
        self.filters = config.NUM_FILTERS_NN
        self.act = nn.ELU(alpha=1.0, inplace=False)
        self.nnet = nn.Sequential(
            nn.Linear(self.onum, self.filters*2),
            nn.BatchNorm1d(self.filters*2), #requires NxC format
            self.act,
            nn.Dropout(self.dropout),
            nn.Linear(self.filters*2, self.filters),
            nn.BatchNorm1d(self.filters),
            self.act,
            nn.Dropout(self.dropout),
            nn.Linear(self.filters, self.onum-2*self.pnum)
        )
        self.optimizer = optim.Adam(self.nnet.parameters(), betas=(config.BETA_1, config.BETA_2))
        
    def forward(self, x):
        x = self.nnet(x)
        
        return x

class TimeIntegration2DLSTM(nn.Module):
    
    def __init__(self):
        """LSTM model as proposed in my thesis for latent space time integration
        """
        
        super(TimeIntegration2DLSTM, self).__init__()
        self.dropout = config.DROPOUT_LSTM
        self.onum = config.O_NUM
        self.pnum = config.P_NUM
        self.filters = config.NUM_FILTERS_CONV1D
        self.act = nn.LeakyReLU(negative_slope=0.3)
        self.nnet = nn.Sequential(
            nn.LSTM(self.onum, 2*self.onum, batch_first=False),
            nn.LSTM(2*self.onum, 2*self.onum, batch_first=False),
            nn.Conv1d(2*self.onum, self.filters, kernel_size=1),
            self.act,
            nn.Conv1d(self.filters, self.onum-2*self.pnum, kernel_size=1)
        )
        self.optimizer = optim.Adam(self.nnet.parameters(), betas=(config.BETA_1, config.BETA_2))
        
    def forward(self, x):
        x = torch.unsqueeze(x, axis=0)
        lstm1, lstm2, conv = self.nnet[0], self.nnet[1], self.nnet[2:]
        out, (_, _) = lstm1(x)
        out, (_, _) = lstm2(out)
        out = conv(torch.unsqueeze(torch.squeeze(out, axis=0), axis=2))
        return torch.squeeze(out, axis=2)
    