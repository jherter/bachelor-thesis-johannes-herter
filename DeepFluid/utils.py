import torch
import numpy as np
from PIL import Image
import imageio
from tqdm import tqdm
import math

import torch
from torch.utils.data import TensorDataset, DataLoader, Dataset

from load_data import *
from model import *
import config    

def curl2d(x):
    """Computes curl of discrete velocity field using forward differences with first order truncation error

    Args:
        x (torch.Tensor): discrete velocity field

    Returns:
        np.ndarray: curl of input field
    """
    
    u = x[:, :, 1:, :] - x[: , :, :-1, :] # -ds/dy,
    v = x[: , :, :, :-1] - x[: , :, :, 1:] # ds/dx
    u = torch.cat([u, torch.unsqueeze(u[:, : , -1, :], axis=2)], axis=2)
    v = torch.cat([v, torch.unsqueeze(v[:, :, :, -1], axis=3)], axis=3)
    c = torch.cat([u,v], axis=1)

    return c



def jacobian2d(x):
    """Computes jacobian of discrete velocity field using forward differences with first order truncation error

    Args:
        x (torch.Tensor): discrete velocity field

    Returns:
        torch.Tensor: jacobian of input field
    """

    dudy = x[:, 0, 1:, :] - x[:, 0, :-1, :]
    dudy = torch.cat((dudy, torch.unsqueeze(dudy[:, -1, :], 1)), dim=1)
    
    dudx = x[:, 0, :, 1:] - x[:, 0, :, :-1]
    dudx = torch.cat((dudx, torch.unsqueeze(dudx[:, :, -1], 2)), dim=2)

    dvdy = x[:, 1, 1:, :] - x[:, 1, :-1, :]
    dvdy = torch.cat((dvdy, torch.unsqueeze(dvdy[:, -1, :], 1)), dim=1)
    
    dvdx = x[:, 1, :, 1:] - x[:, 1, :, :-1]
    dvdx = torch.cat((dvdx, torch.unsqueeze(dvdx[:, :, -1], 2)), dim=2)

    j = torch.cat((dudx, dudy, dvdx, dvdy))

    return j

def vorticity(x):
    """Computes vorticity of discrete input field

    Args:
        x (torch.Tensor): input velocity field

    Returns:
        torch.Tensor: vorticity of input field
    """

    dudy = x[:, 0, 1:, :] - x[:, 0, :-1, :]
    dudy = torch.cat((dudy, torch.unsqueeze(dudy[:, -1, :], 1)), dim=1)
    
    dvdx = x[:, 1, :, 1:] - x[:, 1, :, :-1]
    dvdx = torch.cat((dvdx, torch.unsqueeze(dvdx[:, :, -1], 2)), dim=2)

    w = dvdx - dudy
    w = torch.unsqueeze(w, dim=1)

    return w

def save_image_batch(gt_batch, pred_batch, filename, padding=2, batch_size = config.BATCH_SIZE, chan=3):
    """Save image batch to memory

    Args:
        gt_batch (np.ndarray): ground truth data
        pred_batch (np.ndarray): prediction data; should have same dimension than gt_batch
        filename (str): full path for storing output
        padding (int, optional): padds images next to each other. Defaults to 2.
        batch_size (_type_, optional): batch size of input groundtruth and prediction. Defaults to config.BATCH_SIZE.
        chan (int, optional): 3 dor velocity fields (RGB) and 1 for density fields (scalar array). Defaults to 3.
    """
    if chan == 1:
        grid = np.empty((batch_size*2,config.Y_RES, config.X_RES))
    else:
        grid = np.empty((batch_size*2,config.Y_RES, config.X_RES, chan))
    for i in range(batch_size):
        grid[i] = pred_batch[i]
        grid[i+batch_size] = gt_batch[i]
        
    grid = torch.Tensor(grid)
    nmaps = grid.shape[0]
    xmaps = min(batch_size, nmaps)
    ymaps = int(math.ceil(float(nmaps) / xmaps))
    height, width = int(grid.shape[1] + padding), int(grid.shape[2] + padding)
    if chan == 1:
        imgrid = np.zeros([height * ymaps + 1 + padding // 2, width * xmaps + 1 + padding // 2], dtype=np.uint8)
    else:
        imgrid = np.zeros([height * ymaps + 1 + padding // 2, width * xmaps + 1 + padding // 2, chan], dtype=np.uint8)
    k = 0
    for y in range(ymaps):
        for x in range(xmaps):
            if k >= nmaps:
                break
            h, h_width = y * height + 1 + padding // 2, height - padding
            w, w_width = x * width + 1 + padding // 2, width - padding

            imgrid[h:h+h_width, w:w+w_width] = grid[k]
            k += 1
    im = Image.fromarray(imgrid)
    im.save(filename)
    
def save_image_batch_vort(gt_batch, pred_batch, filename, padding=2, batch_size = config.BATCH_SIZE):
    """Save vorticity plot to memory

    Args:
        gt_batch (np.ndarray): ground truth data
        pred_batch (np.ndarray): prediction data; should have same dimension than gt_batch
        filename (str): full path for storing output
        padding (int, optional): padds images next to each other. Defaults to 2.
        batch_size (_type_, optional): batch size of input groundtruth and prediction. Defaults to config.BATCH_SIZE.
    """
    grid = np.empty((batch_size*2,config.Y_RES, config.X_RES, 3))
    for i in range(batch_size):
        grid[i] = vorticity(torch.unsqueeze(torch.Tensor(pred_batch[i]), 0))
        grid[i+batch_size] = vorticity(torch.unsqueeze(torch.Tensor(gt_batch[i]), 0))
        
    grid = torch.Tensor(grid)
    nmaps = grid.shape[0]
    xmaps = min(batch_size, nmaps)
    ymaps = int(math.ceil(float(nmaps) / xmaps))
    height, width = int(grid.shape[1] + padding), int(grid.shape[2] + padding)
    imgrid = np.zeros([height * ymaps + 1 + padding // 2, width * xmaps + 1 + padding // 2, 3], dtype=np.uint8)
    k = 0
    for y in range(ymaps):
        for x in range(xmaps):
            if k >= nmaps:
                break
            h, h_width = y * height + 1 + padding // 2, height - padding
            w, w_width = x * width + 1 + padding // 2, width - padding

            imgrid[h:h+h_width, w:w+w_width] = grid[k]
            k += 1
    im = Image.fromarray(imgrid)
    im.save(filename)
    
def images_to_gif():
    """Convert multiple images in a directory to a gif file
    """
    import imageio
    import glob
    filelist_v = glob.glob("C:\\Users\\98her\\Desktop\\ETH\\BA- cgl\\deepfluid-rl\\DeepFluid_pytorch\\DeepFluid\\vis\\train_ae\\vis_ae_final\\v\\*.png") 
    images = []
    for filename in filelist_v:
        images.append(imageio.imread(filename))
    imageio.mimsave('vis/train_ae/ae_learning.gif', images, duration=0.005)
    
def color_space_viz(filename):
    """Visualize color space of an image in a 3D plot

    Args:
        filename (str): full path of image
    """
    import matplotlib.pyplot as plt
    from PIL import Image
    
    im = Image.open(filename)
    px = im.load()
    ax = plt.axes(projection='3d')
    x, y, z, c = [], [], [], []
    
    for row in range(im.height):
        for col in range(0, im.width):
            pix = px[col, row]
            newCol = (pix[0] / 255, pix[1] / 255, pix[2] / 255)
            
            if not newCol in c:
                x.append(pix[0])
                y.append(pix[1])
                z.append(pix[2])
                c.append(newCol)
    
    ax.scatter(x, y, z, c=c)
    plt.show()
            
def convergence_plot_from_event_file(experiment_id_bl):
    """Plot tensorboard data
        To get experiment id run tensorboard dev upload --logdir=name

    Args:
        experiment_id_bl (str): experiment id from tensorboard.dev

    """
    from packaging import version

    import pandas as pd
    from matplotlib import pyplot as plt
    import seaborn as sns
    from scipy import stats
    import tensorboard as tb
    from scipy.stats import sem  
    
    def sliding_mean(data_array, window=5):
        """Computes sliding mean over array of values

        Args:
            data_array (np.ndarray): linear input vector
            window (int, optional): specifies over how many values to compute the sliding mean. Defaults to 5.

        Returns:
            np.ndarray: vector with averaged values, same size as input vector
        """
        new_list = []  
        for i in range(len(data_array)):  
            indices = range(max(i - window + 1, 0),  
                            min(i + window + 1, len(data_array)))  
            avg = 0  
            for j in indices:  
                avg += data_array[j]  
            avg /= float(len(indices))  
            new_list.append(avg)  
            
        return new_list
    
    experiment_id_bl = experiment_id_bl
    experiment = tb.data.experimental.ExperimentFromDev(experiment_id_bl)
    
    
    dfw = experiment.get_scalars(pivot = True)
    sldm = np.array(sliding_mean(dfw['Loss/batch'], window=10))
    sldm_small = np.array(sliding_mean(dfw['Loss/batch'], window=4))
    sldm_small = np.array([sldm_small[v]*(1-0.0022*v if v>155 else 1) for v in range(len(sldm_small))])
    ax = plt.subplot(111)
    print(len(sldm))
    ax.set_yscale('log')
    ax.spines["top"].set_visible(False)  
    ax.spines["right"].set_visible(False) 
    ax.get_xaxis().tick_bottom()  
    ax.get_yaxis().tick_left()
    #plt.ylim(0, 0.05)
    plt.xticks(np.arange(0, len(sldm)+1, step= len(sldm) // 5), ('0', '15k', '30k', '45k', '60k', '75k'), fontsize=14)
    #plt.yticks(np.arange(0, 0.05+0.0001, step=0.01), fontsize=14)
    iters = range(len(sldm))
    plt.fill_between(iters, sldm - sldm_small*0.4, sldm + sldm_small*0.4, color="#FFCDC5", alpha=0.5)
    plt.plot(iters, sldm, color='#FF3D1D', lw=1)
    
    plt.grid(axis='y')
    plt.show()
    
def greyscale_histogram(filename):
    """Get color distribution for greyscale image

    Args:
        filename (str): full path to image
    """
    import numpy as np
    import skimage.color
    import skimage.io
    import matplotlib.pyplot as plt
    
    image = skimage.io.imread(fname=filename)

    # display the image
    fig, ax = plt.subplots()
    histogram, bin_edges = np.histogram(image, bins=6000, range=(0, 1))
    plt.figure()
    plt.title("Grayscale Histogram")
    plt.xlabel("grayscale value")
    plt.ylabel("pixel count")
    plt.xlim([0.0, 1.0])

    plt.plot(bin_edges[0:-1], histogram)
    plt.show()
    
if __name__ == "__main__":
    ''' filelist_enc = sorted(glob.glob('C:/Users/98her/Desktop/ETH/BA- cgl/deepfluid-rl/DeepFluid_pytorch/DeepFluid/encodings/*.npz'), key=os.path.getctime)
    filelist_gt = sorted(glob.glob('C:/Users/98her/Desktop/ETH/BA- cgl/deepfluid-rl/DeepFluid_pytorch/Scene generation/data/smoke_mov200_f400/v/*.npz'), key=os.path.getctime)
    
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    #load_and_preprocess_nn()
    #convert arrays to torch tensors
    
    x, y = load_encodings_nn()
    x = torch.Tensor(x)
    y = torch.Tensor(y)
    
    #prepare the dataset and dataloader
    print("Set up data loader...")
    dataset = TensorDataset(x, y)
    num_workers = 0
    if torch.cuda.is_available(): num_workers = 4
    #dataloader = DataLoader(dataset, batch_size = config.BATCH_SIZE, shuffle=True, num_workers=num_workers)
    
    #x_samples, _ = next(iter(dataloader))
    idxset = [123, 193, 2, 84, 125, 159, 172, 195]
    idxset = [100,111,120,130,140,150,160,170]
    x_samples = torch.stack([x[123],x[193], x[2], x[84], x[125], x[159], x[172], x[195]], axis=0)
    x_samples = torch.stack([x[100],x[110], x[120], x[130], x[140], x[150], x[160], x[170]], axis=0)
    autoencoder = AE().to(device)
    autoencoder.load_state_dict(torch.load('models/ae/ae_sup_smalllr.pt', map_location=device))
    
    for i in range(len(idxset)):
        
        x_samples_recon = autoencoder.decoder(x_samples[i,:,:])
        #x_samples_recon = x_samples_recon.detach().numpy()
        x_samples_recon = x_samples_recon.to(device)
        x_samples_recon = x_samples_recon.detach().numpy()
        out_tr = np.array(x_samples_recon).transpose(0,2,3,1)
        out_batch = save_img(out_tr, filename="abc.png")
        gt_samples = np.empty(x_samples_recon.shape)
        print(filelist_gt[idxset[i]])
        for j in range(config.W_NUM):
            gt_samples[j] = np.squeeze(np.load(filelist_gt[idxset[i]+j])['x'], axis=0).transpose(2,0,1)
            
        x_samples_np_array = np.array(gt_samples).transpose(0,2,3,1)
        x_batch = save_img(x_samples_np_array, filename="abc.png")
        save_image_batch_vort(gt_batch=x_batch, pred_batch=out_batch, filename="vis_test_nnloader_vort/"+str(i)+"_nn.png", batch_size=config.W_NUM) '''
        
    greyscale_histogram('dens_gray.png')