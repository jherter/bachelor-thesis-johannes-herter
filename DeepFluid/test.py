import sys
import os

import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn.functional as F
import torch.optim as optim
from PIL import Image
from torch import linalg as la
from utils import *
import glob

device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
print(f'Selected device: {device}')

if not torch.cuda.is_available():
    sys.path.insert(1, 'C:\\Users\\98her\\Desktop\\ETH\\BA- cgl\\deepfluid-rl\\DeepFluid_pytorch\\Scene generation')
else:
    sys.path.insert(1, '/cluster/home/jherter/DeepFluid/deepfluid_rl/Scene generation') #For Euler cluster
    
from differentiable_fluid_functions.utils.io import save_img
from differentiable_fluid_functions.utils.plot import (_process_img_2d,
                                                       show_2d_image)

import config
from model import *
from utils import *


def _test_ae(test_dataloader, filename):
    """main testing loop for the autoencoder

    Args:
        test_dataloader (torch.utils.data.DataLoader ): dataloader with testing data
        filename (str): name for visualization purposes
    """
        
    if not torch.cuda.is_available():
        autoencoder = AE().to(device)
        autoencoder.load_state_dict(torch.load('models/ae/model_new.pt', map_location=device))
    else:
        autoencoder = AE().to(device)
        autoencoder.load_state_dict(torch.load('models/ae/model_new.pt', map_location=device))
        #autoencoder.to(device)
        
    # Move both the encoder and the decoder to the selected device and put in eval mode
    autoencoder.eval()

    #set loss functions
    L1loss = nn.L1Loss()
    MSELoss = nn.MSELoss()

    
    test_loss_avg, test_loss_avg_u, test_loss_avg_u_grad, test_loss_avg_p, num_batches, img_cnt = 0, 0, 0, 0, 0, 0
    
    #Compute test loss
    for (image_batch, label) in tqdm(test_dataloader):
        
        image_batch = image_batch.to(device)
        label = label.to(device)
        
        with torch.no_grad():
        
            # autoencoder reconstruction
            out_v, z = autoencoder(image_batch)
            #reconstruction error
            loss_u = L1loss(out_v, image_batch) #L1 loss of velocity field
            loss_grad_u = L1loss(jacobian2d(out_v), jacobian2d(image_batch)) #L1 loss of gradients of velocity field
            loss_p = MSELoss(z[:,-config.P_NUM:], label) #L2 loss of supervised (control) parameters (like src x position and time frame)
            
            #loss scaling
            loss = config.LAMBDA_U * loss_u + \
                   config.LAMBDA_GU * loss_grad_u + \
                   config.LAMBDA_P * loss_p
                    
            test_loss_avg += loss.item()
            test_loss_avg_u += loss_u.item()
            test_loss_avg_u_grad += loss_grad_u.item()
            test_loss_avg_p += loss_p.item()
            num_batches += 1
        
    test_loss_avg /= num_batches
    test_loss_avg_u /= num_batches
    test_loss_avg_u_grad /= num_batches
    test_loss_avg_p /= num_batches
    print('Average total reconstruction error: %f' % (test_loss_avg))
    print('Average l_u reconstruction error: %f' % (test_loss_avg_u))  
    print('Average l_grad_u reconstruction error: %f' % (test_loss_avg_u_grad))  
    print('Average l_p reconstruction error: %f' % (test_loss_avg_p))           
    
    visdir_ = 'vis/test/vis_'+filename
    if os.path.exists(visdir_): 
        pass
    else: 
        os.makedirs(visdir_)
    visdir_ = 'vis/test/vis_'+filename+'/v'
    if os.path.exists(visdir_): 
        pass
    else: 
        os.makedirs(visdir_)

    dec_cnt = 0
    #visualize ranom test batches
    for i in range(config.VISCOUNT_TEST):
        x_samples_v, _ = iter(test_dataloader).next()
        x_samples = x_samples_v.to(device)
        x_samples_np_array_v = np.array(x_samples_v).transpose(0,2,3,1)

        with torch.no_grad():
            out_sample_v, z = autoencoder(x_samples)
            out_sample_v = out_sample_v.to('cpu')
            z = z.to('cpu')
            
            #np.savez('reconstructions/dec_'+ str(dec_cnt) +'v.npz', x=np.array(out_sample_v[0]), y=np.array(z[0,-config.P_NUM:]))
            dec_cnt += 1
                    
            out_sample_v = out_sample_v.detach().numpy()
            out_tr_v = np.array(out_sample_v).transpose(0,2,3,1)
            x_batch_v = save_img(x_samples_np_array_v, filename="abc.png", chan=3)
            out_batch_v = save_img(out_tr_v, filename="abc.png", chan=3)
            save_image_batch(gt_batch=x_batch_v, pred_batch=out_batch_v, filename="vis/test/vis_"+filename+"/v/"+str(i)+"_"+str(img_cnt)+"ae_test_v.png", chan=3)
            
            img_cnt += 1
            
def _test_nn(test_dataloader, filename):
    """main testing loop for time integration network

    Args:
        test_dataloader (torch.utils.data.DataLoader ): dataloader with testing data
        filename (str): name for visualization purposes
    """
    #only run on local cpu or on Euler cpu
    if not torch.cuda.is_available():
        filelist_gt = sorted(glob.glob("/cluster/scratch/jherter/data/smoke_mov200_f400/dagger/v/*.npz")) #For Euler cluster cpu
    else:
        filelist_gt = sorted(glob.glob("/cluster/scratch/jherter/data/smoke_mov200_f400/dagger/v/*.npz")) #For Euler cluster gpu
        
    if not torch.cuda.is_available():
        if config.USE_LSTM:
            net = TimeIntegration2DLSTM()
            net.load_state_dict(torch.load('models/nn/lstm//model_new.pt', map_location='cpu'))
        else:
            net = TimeIntegration2D()
            net.load_state_dict(torch.load('models/nn/model_new.pt', map_location='cpu'))
            
        autoencoder = AE()
        autoencoder.load_state_dict(torch.load('models/ae/model_new.pt', map_location='cpu'))
    else:
        if config.USE_LSTM:
            net.load_state_dict(torch.load('models/nn/lstm/model_new.pt', map_location='cpu'))
        else: 
            net.load_state_dict(torch.load('models/nn/model_new.pt', map_location='cpu'))
            
        autoencoder.load_state_dict(torch.load('models/ae/model_new.pt', map_location='cpu'))
            
    # Move model to device
    net_LSTM = TimeIntegration2DLSTM()
    net_LSTM.load_state_dict(torch.load('models/nn/lstm/model_new.pt', map_location='cpu'))
    net_LSTM.eval()
    net.eval()
    autoencoder.eval()

    #set loss function
    MSELoss = nn.MSELoss()
    
    test_loss_avg, num_batches, img_cnt = 0, 0, 0
    
    for input, label in tqdm(test_dataloader):
        
        with torch.no_grad():
                            
                #Move data to device
                input = input.to(device)
                label = label.to(device)
                
                loss_tw = 0.0
                ct = input[:,0,:]
                zt_ = input[:,0,:-config.P_NUM]
                
                for t in range(config.W_NUM-1):
                    delta_pt = label[:,t+1,:] - label[:,t,:] #sup part
                    x_t = torch.cat([ct, delta_pt], axis=-1)
                    T_t = net(x_t)
                    zt_p_1_ = torch.add(zt_, T_t)
                    
                    #TODO: Check whether to put label or to put learned supervised control parameter
                    ct_p_1 = torch.cat([zt_p_1_, label[:,t+1,:]], axis=-1)
                    
                    zt = input[:,0,:-config.P_NUM]
                    zt_p_1 = input[:,t+1,:-config.P_NUM]
                    delta_zt = zt_p_1 - zt
                    loss_tw += MSELoss(delta_zt, T_t)
                    
                    ct = ct_p_1
                    zt_ = zt_p_1_
                    
                loss = loss_tw / config.W_NUM
                test_loss_avg += loss
                num_batches += 1
                        
    test_loss_avg /= num_batches
    print('Average integration error: %f' % (test_loss_avg))           
    
    visdir_ = 'vis/test/vis_'+ filename
    if os.path.exists(visdir_): 
        pass
    else: 
        os.makedirs(visdir_)
    
    for i in range(config.VISCOUNT_TEST):
        x_samples_batch, label, sc, fr = next(iter(test_dataloader))

        #x_samples = x_samples.to(device)
        with torch.no_grad():
            out_window = np.empty((config.W_NUM, config.Y_RES, config.X_RES, 2))
            gt_samples = np.empty((config.W_NUM, config.Y_RES, config.X_RES, 2))
            ct = x_samples_batch[:,0,:]
            zt = x_samples_batch[:,0,:-config.P_NUM]
            zt_ = x_samples_batch[:,0,:-config.P_NUM]
            G_ct_p_1 = autoencoder.decoder(ct)
            G_ct_p_1 = G_ct_p_1.detach().numpy()
            gt_dec = autoencoder.decoder(x_samples_batch[:,0,:])
            gt_dec = gt_dec.detach().numpy()
            
            for t in range(config.W_NUM-1):
                
                out_window[t] = np.array(G_ct_p_1[0]).transpose(1,2,0)
                gt_samples[t] = np.array(gt_dec[0]).transpose(1,2,0)
                
                delta_pt = label[:,t+1,:] - label[:,t,:] #sup part
                x_t = torch.cat([ct, delta_pt], axis=-1)
                T_t = net(x_t)
                zt_p_1_ = torch.add(zt_, T_t)
                
                #Stack z together with supervised part of next frame
                ct_p_1 = torch.cat([zt_p_1_, x_samples_batch[:,t+1,-config.P_NUM:]], axis=-1)
                
                #decoding of ct_p_1
                G_ct_p_1 = autoencoder.decoder(ct_p_1)
                gt_dec = autoencoder.decoder(x_samples_batch[:,t+1,:])
                #G_ct_p_1 = G_ct_p_1.to(device)
                G_ct_p_1 = G_ct_p_1.detach().numpy()
                gt_dec = gt_dec.detach().numpy()
                
                ct = ct_p_1
                zt_ = zt_p_1_
                
            out_window[config.W_NUM-1] = np.array(G_ct_p_1[0]).transpose(1,2,0)
            gt_samples[config.W_NUM-1] = np.array(gt_dec[0]).transpose(1,2,0)
            
            #gt_samples = np.empty(out_window.shape)
            sc = int(sc)
            fr = int(fr)
            for j in range(config.W_NUM):
                gt_samples[j] = np.expand_dims(np.load(filelist_gt[sc*config.NUM_FRAMES + fr + j])['x'], axis=0)
            gt_samples = save_img(np.array(gt_samples), filename="abc.png")  
            gt_im = save_img(gt_samples, filename="abc.png")
            out_im = save_img(out_window, filename="abc.png")
            save_image_batch(gt_batch=gt_samples, pred_batch=out_im, filename="vis/test/vis_"+filename+"/"+str(img_cnt)+"nn_test.png", batch_size=config.W_NUM)
            
    
        with torch.no_grad():
            out_window = np.empty((config.W_NUM, config.Y_RES, config.X_RES, 2))
            gt_samples = np.empty((config.W_NUM, config.Y_RES, config.X_RES, 2))
            ct = x_samples_batch[:,0,:]
            zt = x_samples_batch[:,0,:-config.P_NUM]
            zt_ = x_samples_batch[:,0,:-config.P_NUM]
            G_ct_p_1 = autoencoder.decoder(ct)
            G_ct_p_1 = G_ct_p_1.detach().numpy()
            gt_dec = autoencoder.decoder(x_samples_batch[:,0,:])
            gt_dec = gt_dec.detach().numpy()
            
            for t in range(config.W_NUM-1):
                
                out_window[t] = np.array(G_ct_p_1[0]).transpose(1,2,0)
                gt_samples[t] = np.array(gt_dec[0]).transpose(1,2,0)
                
                delta_pt = label[:,t+1,:] - label[:,t,:] #sup part
                x_t = torch.cat([ct, delta_pt], axis=-1)
                T_t = net_LSTM(x_t)
                zt_p_1_ = torch.add(zt_, T_t)
                
                #Stack z together with supervised part of next frame
                ct_p_1 = torch.cat([zt_p_1_, x_samples_batch[:,t+1,-config.P_NUM:]], axis=-1)
                
                #decoding of ct_p_1
                G_ct_p_1 = autoencoder.decoder(ct_p_1)
                gt_dec = autoencoder.decoder(x_samples_batch[:,t+1,:])
                #G_ct_p_1 = G_ct_p_1.to(device)
                G_ct_p_1 = G_ct_p_1.detach().numpy()
                gt_dec = gt_dec.detach().numpy()
                
                ct = ct_p_1
                zt_ = zt_p_1_
                
            out_window[config.W_NUM-1] = np.array(G_ct_p_1[0]).transpose(1,2,0)
            gt_samples[config.W_NUM-1] = np.array(gt_dec[0]).transpose(1,2,0)
            
            #gt_samples = np.empty(out_window.shape)
            sc = int(sc)
            fr = int(fr)
            for j in range(config.W_NUM):
                gt_samples[j] = np.expand_dims(np.load(filelist_gt[sc*config.NUM_FRAMES + fr + j])['x'], axis=0)
            gt_samples = save_img(np.array(gt_samples), filename="abc.png")  
            gt_im = save_img(gt_samples, filename="abc.png")
            out_im = save_img(out_window, filename="abc.png")
            save_image_batch(gt_batch=gt_samples, pred_batch=out_im, filename="vis/test/vis_"+filename+"/"+str(img_cnt)+"lstm_test.png", batch_size=config.W_NUM)
            
            img_cnt += 1
            