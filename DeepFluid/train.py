from http.client import NOT_IMPLEMENTED
from logging import NullHandler
from posixpath import normpath
import numpy as np
import tqdm as tqdm
import os
import torch
import torchvision
from torch.utils.tensorboard import SummaryWriter
from torchvision import datasets, transforms
from torch.utils.data import DataLoader, Dataset, TensorDataset, ConcatDataset
import torch.nn as nn 
import torch.nn.functional as F
import torch.optim as optim
from torch import linalg as la

from load_data import *

import matplotlib.pyplot as plt
from PIL import Image
from scipy.stats import truncnorm


import sys
device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
print(f'Selected device: {device}')

if not torch.cuda.is_available():
    sys.path.insert(1, 'C:\\Users\\98her\\Desktop\\ETH\\BA- cgl\\deepfluid-rl\\DeepFluid_pytorch\\Scene generation')
else:
    sys.path.insert(1, '/cluster/home/jherter/DeepFluid/deepfluid_rl/Scene generation') #For Euler cluster
    
from differentiable_fluid_functions.utils.io import save_img
from differentiable_fluid_functions.utils.plot import _process_img_2d, show_2d_image
from smoke2d_integration import advect_vel


import config

from model import *
from utils import *

def _train_ae(autoencoder, dataloader, filename):
    """Main training loop for training a AE(nn.Module) model

    Args:
        autoencoder (AE): autoencoder model
        dataloader (torch.utils.data.DataLoader ): data loader with training data
        filename (str): name for logging purposes
    """

    writer = SummaryWriter('runs/ae/' + filename)
    #load pretrained model if config.LOAD_MODEL is set to True
    if config.LOAD_MODEL:
        if not torch.cuda.is_available():autoencoder.load_state_dict(torch.load('models/ae/model_new.pt', map_location=device))
            
        else:
            autoencoder = AE().to(device)
            autoencoder.load_state_dict(torch.load('models/ae/model_new.pt', map_location=device))
    #move both the encoder and the decoder to the selected device and put in train mode
    else: 
        autoencoder = autoencoder.to(device)
    autoencoder.train()
    
    #create logging and vis directories
    dir_ = 'models/ae'
    if os.path.exists(dir_): 
        pass
    else: 
        os.makedirs(dir_)
        
    visdir_ = 'vis/train_ae/vis_'+filename
    if os.path.exists(visdir_): 
        pass
    else: 
        os.makedirs(visdir_)
    visdir_ = 'vis/train_ae/vis_'+filename+'/v'
    if os.path.exists(visdir_): 
        pass
    else: 
        os.makedirs(visdir_)
    visdir_ = 'vis/train_ae/vis_'+filename+'/d'
    if os.path.exists(visdir_): 
        pass
    else: 
        os.makedirs(visdir_)

    #set loss functions
    L1loss = nn.L1Loss()
    MSELoss = nn.MSELoss() 
    
    #get random batch from dataloader for constant visual inspectations
    x_samples, _ = next(iter(dataloader))
    x_samples_np_array = np.array(x_samples).transpose(0,2,3,1)
    x_samples = x_samples.to(device)
    
    vis_cnt = 0
    img_cnt = 0
    n_iter = 0
    
    #use cosine annealing as learning rate schedule and set lr to config.LR_MAX
    lr = config.LR_MAX
    for param_group in autoencoder.optimizer.param_groups:
        param_group['lr'] = lr
    scheduler = optim.lr_scheduler.CosineAnnealingLR(autoencoder.optimizer, T_max=config.EPOCHS, eta_min=config.LR_MIN)
    
    #training loop
    for i in tqdm(range(config.EPOCHS)):

        with open("control_epoch2.txt", "w") as output:
            output.write(str(i))

        autoencoder.epochs = i
        
        for (input, label) in dataloader:
            
            n_iter += 1
            autoencoder.optimizer.zero_grad()
            
            #move data to device
            input = input.to(device)
            #input_d = input_d.to(device)
            input = input.to(device)
            label = label.to(device)
            
            #forward pass
            out, z = autoencoder(input)
            
            #calculate loss(es)
            loss_u = L1loss(out, input) #L1 loss of velocity field
            loss_grad_u = L1loss(jacobian2d(out), jacobian2d(input)) #L1 loss of gradients of velocity field
            loss_p = MSELoss(z[:,-config.P_NUM:], label) #L2 loss of supervised (control) parameters (like src x position and time frame)
            #loss_d = MSELoss(out_d, input_d) #L2 loss of density scalar field
            
            #loss scaling
            loss = config.LAMBDA_U * loss_u + \
                   config.LAMBDA_P * loss_p + \
                   config.LAMBDA_GU * loss_grad_u
                   #config.LAMBDA_D * loss_d

            #loss reporting
            if n_iter % 250 == 0:
                writer.add_scalar('Loss/Loss_total', loss, n_iter)
                writer.add_scalar('Loss/Loss_u', loss_u, n_iter)
                writer.add_scalar('Loss/Loss_grad_u', loss_grad_u, n_iter)
                writer.add_scalar('Loss/Loss_p', loss_p, n_iter)
                #writer.add_scalar('Loss/Loss_d', loss_d, n_iter)
                writer.add_scalar('Misc/Learning rate', autoencoder.optimizer.param_groups[0]["lr"], n_iter)
                writer.add_text('Supervised params', "Label: "+str(label)+", z: "+str(z[:,-2:]))
            
            #backward pass
            loss.backward()
            autoencoder.optimizer.step()

            #show frames from random batch every 2_000 image batches (16_000 frames)
            if vis_cnt % 2_000 == 0:
                with torch.no_grad():
                    out_sample, _ = autoencoder(x_samples)
                    out_sample = out_sample.to('cpu')
                
                out_sample = out_sample.detach().numpy()
                out_tr = np.array(out_sample).transpose(0,2,3,1)
                x_batch = save_img(x_samples_np_array, filename="abc.png", chan=3)
                out_batch = save_img(out_tr, filename="abc.png", chan=3)
                
                save_image_batch(gt_batch=x_batch, pred_batch=out_batch, filename="vis/train_ae/vis_"+filename+"/v/"+str(i)+"_"+str(img_cnt)+"v.png", chan=3)
                
                img_cnt += 1

                ''' img_batch = np.zeros((2*config.BATCH_SIZE,3,128,96))
                for q in range(2*config.BATCH_SIZE):
                    if q < config.BATCH_SIZE: 
                        x_sample_np_array_temp = x_samples_np_array[q][::-1]
                        x_sample_np_array_temp = _process_img_2d(x_sample_np_array_temp, "RGB")
                        img_batch[q,:] = x_sample_np_array_temp.transpose((2,0,1))
                    else:
                        out_tr_sample = out_tr[q-config.BATCH_SIZE][::-1]
                        out_tr_sample = _process_img_2d(out_tr_sample, "RGB")
                        img_batch[q,:] = out_tr_sample.transpose((2,0,1))
                #NOTE:Color does not really map to RGB in tensorboard...
                writer.add_images("Random image batch", img_batch) '''
            vis_cnt += 1
            
        #update learning rate every epoch according to scheduler
        scheduler.step()
        
        #save model checkpoint after each epoch
        torch.save(autoencoder.state_dict(), 'models/ae/model_new_abl.pt')
    
    #save final model
    torch.save(autoencoder.state_dict(), 'models/ae/' + filename + '.pt')
    writer.close()
        
def _train_nn(net, dataloader, filename):
    """"Main training loop for training a TimeIntegration2D(nn.Module) or TimeIntegration2DLSTM(nn.Module) model

    Args:
        net (TimeIntegration2D or TimeIntegration2DLSTM): time integration model
        dataloader (torch.utils.data.DataLoader ): dataloader with training data
        filename (str): name for logging purposes
    """
    
    writer = SummaryWriter('runs/nn/lstm/' + filename)
    if config.LOAD_MODEL:
            if not torch.cuda.is_available():
                if config.USE_LSTM:
                    net.load_state_dict(torch.load('models/nn/lstm/model_new.pt', map_location=device))
                else:
                    net.load_state_dict(torch.load('models/nn/model_new.pt', map_location=device))
            else:
                if config.USE_LSTM:
                    net = TimeIntegration2DLSTM().to(device)
                    net.load_state_dict(torch.load('models/nn/lstm/model_new.pt', map_location=device))
                else:
                    net = TimeIntegration2D().to(device)
                    net.load_state_dict(torch.load('models/nn/model_new.pt', map_location=device))
    else:
        net = net.to(device)
    net.train()
    
    #create logging directory
    if config.USE_LSTM:
        dir_ = 'models/nn/lstm'
    else:
        dir_ = 'models/nn'
    if os.path.exists(dir_): 
        pass
    else: 
        os.makedirs(dir_)
    
    #set loss function
    MSELoss = nn.MSELoss()
    
    #Cosine annealing scheduler for adam optimizer
    lr = config.LR_MAX_NN
    for param_group in net.optimizer.param_groups:
        param_group['lr'] = lr
    scheduler = optim.lr_scheduler.CosineAnnealingLR(net.optimizer, T_max=config.EPOCHS_NN, eta_min=config.LR_MIN)
    
    n_iter = 0
    
    for i in tqdm(range(config.EPOCHS_NN)):
        
        with open("control_epoch_nn.txt", "w") as output:
                output.write(str(i))
        
        for (input, label) in dataloader:
                n_iter += 1
                
                net.optimizer.zero_grad()
                
                #Move data to device
                input = input.to(device)
                label = label.to(device)
                
                #TODO after thesis deadline: Increase number of frames to feed into the Time Integration network
                #per iteration (currently 1)
                
                loss_tw = 0.0
                ct = input[:,0,:]
                zt_ = input[:,0,:-config.P_NUM]
                for t in range(config.W_NUM-1):
                    delta_pt = label[:,t+1,:] - label[:,t,:] #sup part
                    x_t = torch.cat([ct, delta_pt], axis=-1)
                    #renormalize output to the scale of x (currently not implemented)
                    T_t = net(x_t)
                    T_t_norm = T_t #* (y_std / x_std)
                    zt_p_1_ = torch.add(zt_, T_t_norm)
                    
                    #TODO: Check whether to put label or to put learned supervised control parameter
                    ct_p_1 = torch.cat([zt_p_1_, label[:,t+1,:]], axis=-1)
                    
                    zt = input[:,t,:-config.P_NUM]
                    zt_p_1 = input[:,t+1,:-config.P_NUM]
                    delta_zt = zt_p_1 - zt
                    loss_tw += MSELoss(delta_zt, T_t)
                    
                    ct = ct_p_1
                    zt_ = zt_p_1_
                
                #loss reporting
                if n_iter % 250 == 0:
                    writer.add_scalar('Learning rate', net.optimizer.param_groups[0]["lr"], n_iter)
                    writer.add_scalar('Loss/batch', loss_tw, n_iter)
                    
                loss = loss_tw / config.W_NUM
                loss.backward()
                net.optimizer.step()
                
        scheduler.step()
        #save model checkpoint after each epoch
        if config.USE_LSTM:
            torch.save(net.state_dict(), 'models/nn/lstm/model_new.pt')
        else:
            torch.save(net.state_dict(), 'models/nn//model_new.pt')
        
    #save final model
    if config.USE_LSTM:
        torch.save(net.state_dict(), 'models/nn/lstm/' + filename + '.pt')
    else:
        torch.save(net.state_dict(), 'models/nn/' + filename + '.pt')
    writer.close()   
             

def _train_dagger(filename):
    """Main training loop for dagger training, training policy for N_d iterations

    Args:
        filename (str): name for logging purposes
    """
    
    #create logging directory
    if config.USE_LSTM:
        dir_ = 'models/nn/lstm'
    else:
        dir_ = 'models/nn'
    if os.path.exists(dir_): 
        pass
    else: 
        os.makedirs(dir_)
    
    #set loss function
    MSELoss = nn.MSELoss()
    
    load_model = config.LOAD_MODEL
    datasets = []
    print("Load encodings...")
    x, x_d, y, sc, fr = load_encodings_nn()
    x = torch.Tensor(x)
    y = torch.Tensor(y)
    x_d = torch.Tensor(x_d)
    sc = torch.Tensor(sc)
    fr = torch.Tensor(fr)
    
    print("Set up data loader...")
    datasets.append(TensorDataset(x, x_d, y, sc, fr))
    
    if load_model:
        if not torch.cuda.is_available():
            if config.USE_LSTM:
                net_train.load_state_dict(torch.load('models/nn/lstm/model_new.pt', map_location=device))
                epochs_dagger = config.EPOCHS_DAGGER_LSTM
                epochs_per_dagger_it = config.EPOCHS_PER_DAGGER_IT_LSTM
            else:
                net_train.load_state_dict(torch.load('models/nn/model_new.pt', map_location=device))
                epochs_dagger = config.EPOCHS_DAGGER_NN
                epochs_per_dagger_it = config.EPOCHS_PER_DAGGER_IT_NN
        else:
            if config.USE_LSTM:
                net_train = TimeIntegration2DLSTM().to(device)
                net_train.load_state_dict(torch.load('models/nn/lstm/model_new.pt', map_location=device))
                epochs_dagger = config.EPOCHS_DAGGER_LSTM
                epochs_per_dagger_it = config.EPOCHS_PER_DAGGER_IT_LSTM
            else:
                net_train = TimeIntegration2D().to(device)
                net_train.load_state_dict(torch.load('models/nn/model_new.pt', map_location=device))
                epochs_dagger = config.EPOCHS_DAGGER_NN
                epochs_per_dagger_it = config.EPOCHS_PER_DAGGER_IT_NN
    else:
        if config.USE_LSTM:
            net_train = TimeIntegration2DLSTM().to(device)
            epochs_dagger = config.EPOCHS_DAGGER_LSTM
            epochs_per_dagger_it = config.EPOCHS_PER_DAGGER_IT_LSTM
        else:
            net_train = TimeIntegration2D().to(device)
            epochs_dagger = config.EPOCHS_DAGGER_NN
            epochs_per_dagger_it = config.EPOCHS_PER_DAGGER_IT_NN
            
        net_train = net_train.to(device)
        load_model = True
    
    autoencoder = AE().to(device)
    autoencoder.load_state_dict(torch.load('models/ae/model_new.pt', map_location=device))
    autoencoder.eval()
    net_train.train()

    
    #set global learning rate for whole dagger training
    lr = config.LR_MAX_NN
    for param_group in net_train.optimizer.param_groups:
        param_group['lr'] = lr
    scheduler = optim.lr_scheduler.CosineAnnealingLR(net_train.optimizer, T_max=epochs_dagger*epochs_per_dagger_it, 
                                                    eta_min=config.LR_MIN)
        
    ###################################################################################################################
    #Main taining loop
    for iter in tqdm(range(epochs_dagger)):
                
        #create new writer every dagger epoch in the same directory
        if config.USE_LSTM: writer = SummaryWriter('runs/nn/lstm/' + filename)
        else: writer = SummaryWriter('runs/nn/' + filename)
        
        dataset = ConcatDataset(datasets)
        dataset_len = len(dataset)
        num_workers = 0
        if torch.cuda.is_available(): num_workers = 4
        
        if config.USE_LSTM:
            dataloader = DataLoader(dataset, batch_size = config.BATCH_SIZE_LSTM, shuffle=True, num_workers=num_workers)
        else:
            dataloader = DataLoader(dataset, batch_size = config.BATCH_SIZE_NN, shuffle=True, num_workers=num_workers)
                            
        n_iter = 0
        
        ###################################################################################################################
        #train current policy for epochs_per_dagger_it iterations
        for i in tqdm(range(epochs_per_dagger_it)):
            for (input, _, label, _, _) in dataloader:
                    n_iter += 1
                    
                    net_train.optimizer.zero_grad()
                    
                    #Move data to device
                    input = input.to(device)
                    label = label.to(device)
                    
                    loss_tw = 0.0
                    ct = input[:,0,:]
                    zt_ = input[:,0,:-config.P_NUM]
                    
                    for t in range(config.W_NUM-1):
                        delta_pt = label[:,t+1,:] - label[:,t,:] #sup part
                        x_t = torch.cat([ct, delta_pt], axis=-1)
                        T_t = net_train(x_t)
                        T_t_norm = T_t 
                        zt_p_1_ = torch.add(zt_, T_t_norm)
                        
                        ct_p_1 = torch.cat([zt_p_1_, label[:,t+1,:]], axis=-1)
                        
                        zt = input[:,t,:-config.P_NUM]
                        zt_p_1 = input[:,t+1,:-config.P_NUM]
                        delta_zt = zt_p_1 - zt
                        loss_tw += MSELoss(delta_zt, T_t)
                        
                        ct = ct_p_1
                        zt_ = zt_p_1_
                    
                    #loss reporting
                    if n_iter % 250 == 0:
                        writer.add_scalar('Misc/Learning rate', net_train.optimizer.param_groups[0]["lr"], n_iter)
                        writer.add_scalar('Misc/Length of dataset', dataset_len, n_iter)
                        writer.add_scalar('Misc/Epoch dagger', iter, n_iter)
                        writer.add_scalar('Loss/batch', loss_tw, n_iter)
                                                
                    loss = loss_tw / config.W_NUM
                    loss.backward()
                    net_train.optimizer.step()
                    
            scheduler.step()
            #save model checkpoint after each epoch
            if config.USE_LSTM:
                torch.save(net_train.state_dict(), 'models/nn/lstm/model_new.pt')
            else:
                torch.save(net_train.state_dict(), 'models/nn//model_new.pt')
        
        ###################################################################################################################
        #Now that policy ist trained, run policy to gather new observations and then simulate new data; add new data to dataset
        if not torch.cuda.is_available():
                if config.USE_LSTM:
                    net_eval = TimeIntegration2D().to(device)
                    net_eval.load_state_dict(torch.load('models/nn/lstm/model_new.pt', map_location=device))
                else:
                    net_eval = TimeIntegration2D().to(device)
                    net_eval.load_state_dict(torch.load('models/nn/model_new.pt', map_location=device))
        else:
            if config.USE_LSTM:
                net_eval = TimeIntegration2DLSTM().to(device)
                net_eval.load_state_dict(torch.load('models/nn/lstm/model_new.pt', map_location=device))
            else:
                net_eval = TimeIntegration2D().to(device)
                net_eval.load_state_dict(torch.load('models/nn/model_new.pt', map_location=device))
        
        net_eval.eval()
                    
        observations = _run_policy(net_eval, autoencoder, dataloader)
        datasets.append(_simulate_new_data(observations, iter))
        
        
def _run_policy(net, autoencoder, dataloader):
    """run policy at iteration i by running integrating random samples in latent space using the trained model of iteration i

    Args:
        net (TimeIntegration2D or TimeIntegration2DLSTM): current trained model
        autoencoder (AE): pretrained autoencoder
        dataloader (torch.utils.data.DataLoader ): dataloader containing training data

    Returns:
        list: list of observation tuples of the form (velocity:np.ndarray, density:np.ndarray, label:np.ndarray, scene:int, frame:int)
    """
    #--> returns o_1(v,d),...,o_M(v,d)
    autoencoder.eval()
    net.eval()
    v_range = 11.338
    
    obs_list = []
    
    for i in tqdm(range(config.SAMPLE_TRAJECTORIES)):
        x_samples_batch_v, x_samples_batch_d ,label, sc, fr = next(iter(dataloader))
        v_start = sample_from_window() #sample random index v_g from trajectory
        print(v_start)
        x_samples_batch_v = x_samples_batch_v.to(device)
        x_samples_batch_d = x_samples_batch_d.to(device)
        label = label.to(device)
        sc = sc.to(device)
        fr = fr.to(device)
        
        with torch.no_grad():
            ct = x_samples_batch_v[:,0,:]
            ct = ct.to(device)
            G_ct_0 =  autoencoder.decoder(ct)
            G_ct_0 = G_ct_0.to('cpu')
            G_ct_0 = G_ct_0.detach().numpy()
            G_ct_p_1 = G_ct_0
            d_t = x_samples_batch_d[0]
            d_t = d_t.to('cpu')
            zt_ = x_samples_batch_v[:, 0, :-config.P_NUM]
            
            #simulate trajectory up to v_start to generate density field d(v_start) that fits to (errorneous) velocity field v(v_start)
            for t in range(v_start - 1):
                sc = sc.to('cpu')
                d_t = advect_vel(G_ct_p_1[0] * v_range, np.array(d_t), (label[0,t,0] + 1) * 0.75 - 0.75, int(sc[0]), int(fr[0] + t), 1, store_data=False, log_dir = 'C:\\Users\\98her\\Desktop\\ETH\\BA- cgl\\deepfluid-rl\\DeepFluid_pytorch\\DeepFluid\\test_run_pol')
                delta_pt = label[:,t+1, :] - label[:,t,:]
                x_t = torch.cat([ct, delta_pt], axis=-1)
                T_t = net(x_t)
                zt_p_1_ = torch.add(zt_, T_t)
                
                #Stack z together with supervised part of next frame
                ct_p_1 = torch.cat([zt_p_1_, x_samples_batch_v[:,t+1,-config.P_NUM:]], axis=-1)
                ct_p_1 = ct_p_1.to(device)
                #decoding of ct_p_1
                G_ct_p_1 = autoencoder.decoder(ct_p_1)
                #G_ct_p_1 = G_ct_p_1.to(device)
                G_ct_p_1  = G_ct_p_1.to('cpu')
                G_ct_p_1 = G_ct_p_1.detach().numpy()
                
                ct = ct_p_1
                zt_ = zt_p_1_
                
        obs_list.append((G_ct_p_1[0], np.array(d_t), label[0,v_start,0], int(sc[0]), int(fr[0] + v_start)))
        
    return obs_list

def _simulate_new_data(observations, iter):
    """Use expert OPT to get matching actions to the sampled observations

    Args:
        observations (list): list of observations, sampled by running policy
        iter (int): current dagger iteration in [0,N_d-1]

    Returns:
        _type_: _description_
    """
    #store new data in logdir
    v_range = 11.338 #currently hard coded
    scenesize = config.SAMPLE_FRAMES_PER_TRAJECTORY
    dir='epoch_'+str(iter)
    for i in range(len(observations)):
        vel = observations[i][0]
        den = observations[i][1]
        label = observations[i][2]
        sc = observations[i][3]
        fr = observations[i][4]
        advect_vel(vel*v_range, np.array(den), (label + 1) * 0.75 - 0.75, sc, fr, 
                scenesize, log_dir='/cluster/scratch/jherter/data/smoke_mov200_f400/yldagger/'+str(dir),
                store_data=True)
    
    #Encode new data and store in logdir
    load_and_preprocess_dagger(dagger_tr=True, dir=str(dir))
    
    print("Load encodings...")
    x, x_d, y, sc, fr = load_encodings_nn(dagger_tr=True, dir=str(dir))
    x = torch.Tensor(x)
    y = torch.Tensor(y)
    x_d = torch.Tensor(x_d)
    sc = torch.Tensor(sc)
    fr = torch.Tensor(fr)
    
    dataset = TensorDataset(x, x_d, y, sc, fr)
    
    return dataset

def sample_from_window(mean=config.W_NUM // 2 - 3, sd = 6, low = 1, upp = config.W_NUM-1):
    """sample along the trajectory with accumulated error

    Args:
        mean (int, optional): See thesis in results section. Will be the mode of the resulting truncated Gaussian.
                                         Defaults to config.W_NUM//2-3.
        sd (int, optional): See thesis in results section. Basically scaling of truncated Gaussian Defaults to 6.
        low (int, optional): value to truncate Gaussian from below. Defaults to 1.
        upp (_type_, optional): value to truncate Gaussian from above. Defaults to config.W_NUM-1.

    Returns:
        int: index for where to sample in the integration window
    """
    
    x_tnorm = get_truncated_normal(mean = mean, sd = sd, low = low, upp = upp)
    return max(0, min(np.rint(x_tnorm.rvs()).astype(int), config.W_NUM - 1))

def get_truncated_normal(mean=config.W_NUM // 2, sd=6, low=1, upp=config.W_NUM):
    """Proposed sampling distribution as described in the results section of the thesis

    Args:
        mean (int, optional): See thesis in results section. Defaults to config.W_NUM//2.
        sd (int, optional): See thesis in results section. Basically scaling of truncated Gaussian Defaults to 6.
        low (int, optional): value to truncate Gaussian from below. Defaults to 1.
        upp (int, optional): value to truncate Gaussian from above. Defaults to 1.

    Returns:
        scipy.stat.truncnorm: truncated normal distribution function that can be used for sampling
    """
    return truncnorm((low - mean) / sd, (upp - mean) / sd, loc=mean, scale=sd)


if __name__ == "__main__":
    
    filelist_d = sorted(glob.glob("C:\\Users\\98her\\Desktop\\ETH\\BA- cgl\\deepfluid-rl\\DeepFluid_pytorch\\Scene generation\\data\\dagger\\d\\*.npz"))
    filelist_v = sorted(glob.glob("C:\\Users\\98her\\Desktop\\ETH\\BA- cgl\\deepfluid-rl\\DeepFluid_pytorch\\Scene generation\\data\\dagger\\v\\*.npz"))
    net = TimeIntegration2DLSTM()
    net.load_state_dict(torch.load('models/nn/lstm/model_new.pt', map_location=device))
    autoencoder = AE()
    autoencoder.load_state_dict(torch.load('models/ae/model_new.pt', map_location=device))
    autoencoder.eval()
    net.eval()
    
    x, x_d, y, sc, fr = load_encodings_nn()
        
    x = torch.Tensor(x)
    y = torch.Tensor(y)
    x_d = torch.Tensor(x_d)
    sc = torch.Tensor(sc)
    fr = torch.Tensor(fr)
    print(x.shape)
    print(y.shape)
    print(x_d.shape)
    
    dataset = TensorDataset(x, x_d, y, sc, fr)
    num_workers = 0
    if torch.cuda.is_available(): num_workers = 4
    if config.USE_LSTM:
        dataloader = DataLoader(dataset, batch_size = 1, shuffle=True, num_workers=num_workers)
    else:
        dataloader = DataLoader(dataset, batch_size = 1, shuffle=True, num_workers=num_workers)
    
    obs = _run_policy(net, autoencoder, dataloader)
    
