from ast import parse
from asyncio import StreamReaderProtocol
from re import T
import sys
import torch
import numpy as np
import matplotlib.pyplot as plt
import os
import gc
from datetime import datetime
import argparse
from typing import List
from random import randrange
from perlin import TileableNoise
from collections import deque
from time import time

sys.path.append("../")

from differentiable_fluid_functions.simulation import SimulationParameters
from differentiable_fluid_functions.types import GridType
from differentiable_fluid_functions.simulation import SimulationRunner
from differentiable_fluid_functions.utils import mkdir, save_img, show_2d_image, save_img_help
from differentiable_fluid_functions.pressure import PressureProjectionType
from differentiable_fluid_functions.types import (
    SolverType,
    ShapeType,
    PreconditionerType,
)
from differentiable_fluid_functions.grid import Staggered2dGrid, CellType, Flag2dGrid, Real2dGrid
from tqdm import tqdm

preconditioner_types = {
    PreconditionerType.NONE.value: PreconditionerType.NONE,
    PreconditionerType.INCOMPLETE_POISSON.value: PreconditionerType.INCOMPLETE_POISSON,
    PreconditionerType.MULTIGRID.value: PreconditionerType.MULTIGRID,
}


def main(
    log_dir: str             = 'data/test_vel_advection',
    path_format: str         = '%d_%d.npz',
    res: List[int]           = [128, 96],
    resolution_x:int         = 96,
    resolution_y:int         = 128,
    num_scenes: int          = 1,
    num_frames: int          = 30,
    num_simulations:int      = 30,
    min_frames:int           = 0,
    max_frames:int           = 29,
    min_scenes:int           = 0,
    max_scenes:int           = 1,
    min_src_pos:float        = -1,
    max_src_pos:float        = 1,
    src_radius:float         = 0.075,
    dt: float                = 1.0,
    rk_order: int            = 1,
    with_obstacle: bool      = False,
    advection_order: int     = 1,
    wind_x: float            = 0.0,
    wind_y: float            = 0.0,
    preconditioner_type: str = "NONE",
    x_pos_curr: float        = 0.0,
):
    """main function for testing purposes. Ignore me
    """
    dim = 2
    batch_size = 1

    # use cuda if exists
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    dtype = torch.float32

    #save args in args.txt
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)
    args = parser.parse_args()
    args_file = os.path.join(log_dir, 'args.txt')
    with open(args_file, 'w') as f:
        print('%s: arguments' % datetime.now())
        for k, v in vars(args).items():
            print(' %s: %s' % (k,v))
            f.write('%s: %s\n' % (k,v))

    # set up the size of the simulation
    simulation_size = (batch_size, 1, *res)

    # set up the simulation parameters
    simulationParameters = SimulationParameters(
        dim                         = dim,
        dtype                       = dtype,
        device                      = device,
        simulation_size             = simulation_size,
        dt                          = dt,
        rk_order                    = rk_order,
        advection_order             = advection_order,
        conjugate_gradient_accuracy = 1e-3,
    )

    # create a simulation runner
    simulationRunner = SimulationRunner(parameters=simulationParameters)


    #file storing preparation
    den_dir = os.path.join(log_dir, 'd_adv')
    if not os.path.exists(den_dir):
        os.makedirs(den_dir)
    vel_dir = os.path.join(log_dir, 'v')
    if not os.path.exists(vel_dir):
        os.makedirs(vel_dir)
    v_range = [np.finfo(float).max, np.finfo(float).min]
    v_ = np.zeros([res[0],res[1],3], dtype=np.float32)
    d_ = np.zeros([res[0], res[1]], dtype=np.float32)
    nx_list = []

    for scene in tqdm(range(num_scenes)):

        # prepare 'perlin noise like' moving of smoke source for the next scene
        noise = TileableNoise(seed=randrange(424242))
        noise.randomize()
        x_pos_curr = (np.load('C:\\Users\\98her\\Desktop\\ETH\\BA- cgl\\deepfluid-rl\\DeepFluid_pytorch\\DeepFluid\\reconstructions\\v\\dec3_v.npz')['y'][0]+ 1) * 0.75 - 0.75
        nx_mov = np.load('C:\\Users\\98her\\Desktop\\ETH\\BA- cgl\\deepfluid-rl\\DeepFluid_pytorch\\DeepFluid\\reconstructions\\v\\dec3_v.npz')['nx']
        print(nx_mov.shape)
        print(x_pos_curr)
        #print(x_pos_curr)
        ny_ = x_pos_curr
        nz_ = noise.rng.randint(200)*0.001
        nqx = deque([-1] * num_frames, num_frames)

        # initialize all the required grids
        flags   = simulationRunner.create_grid(GridType.FLAG)
        vel     = simulationRunner.create_grid(GridType.STAGGERED)
        #temp for testing
        vvv = Real2dGrid.from_npz(filename='C:\\Users\\98her\\Desktop\\ETH\\BA- cgl\\deepfluid-rl\\DeepFluid_pytorch\\DeepFluid\\reconstructions\\v\\dec3_v.npz', key='x').grid2array().transpose(0,3,1,2)
        vv = Real2dGrid.from_npz(filename='C:\\Users\\98her\\Desktop\\ETH\\BA- cgl\\deepfluid-rl\\DeepFluid_pytorch\\DeepFluid\\reconstructions\\v\\dec3_v.npz', key='x').from_array(vvv, dtype=torch.float64, device=device)
        vel = vel.from_real_2d_grid(vv)
        #print(vel.shape)
        density = simulationRunner.create_grid(GridType.REAL)
        dd = Real2dGrid.from_npz(filename='C:\\Users\\98her\\Desktop\\ETH\\BA- cgl\\deepfluid-rl\\DeepFluid_pytorch\\DeepFluid\\reconstructions\\d\\dec3_d.npz', key='x').grid2array().transpose(0,3,1,2)
        dd = (dd - np.min(dd)) / (np.max(dd) - np.min(dd))
        density = Real2dGrid.from_npz(filename='C:\\Users\\98her\\Desktop\\ETH\\BA- cgl\\deepfluid-rl\\DeepFluid_pytorch\\DeepFluid\\reconstructions\\d\\dec3_d.npz', key='x').from_array(dd, dtype=torch.float64, device=device)
        # create external force, advection and pressure projection
        ext = simulationRunner.create_extforces()
        adv = simulationRunner.create_advection()
        adv.setup_external_forces(
            device=device,
            dtype=dtype,
            simulation_size=simulation_size,
        )
        pre = simulationRunner.create_pressure_projection(
            pressure_projection_type=PressureProjectionType.VOXELIZED,
            solver_type=SolverType.PCG,
            preconditioner_type=preconditioner_types[preconditioner_type.upper()],
        )

        # initialize the domain
        flags, phi_obs = ext.init_flags(phi=True, boundary_width=[0], wall=["  y "])

        # create an obstacle and add it to the domain
        if with_obstacle:
            obs_size   = simulationRunner.create_tensor([[0.1]])
            obs_center = simulationRunner.create_tensor([[-0.1, -0.4]])
            sphere     = simulationRunner.create_shape(ShapeType.SPHERE)
            phi_sphere = sphere.compute_level_set(center=obs_center, size=obs_size)
            phi_obs.union(phi_sphere)
            flags      = phi_obs.get_flags()
        
        # create a source
        source = simulationRunner.create_shape(ShapeType.SPHERE)
        source_center    = simulationRunner.create_tensor([[0.0, -0.8]])
        source_size      = simulationRunner.create_tensor([[2*src_radius]])
        source_value     = simulationRunner.create_tensor(1.0)
        gravity          = [0, -4e-3]

        # external wind force
        external_force = Staggered2dGrid.get_constant_grid(
            value_x         = torch.tensor(wind_x, device=device, dtype=dtype),
            value_y         = torch.tensor(wind_y, device=device, dtype=dtype),
            simulation_size = simulation_size,
        )


        for frame in tqdm(range(num_frames), leave = False):

            #perlin noise for next frame
            nx = noise.noise3(x=frame*0.01, y=ny_, z=nz_, repeat=1000)
            px = (nx+1)*0.5 * (max_src_pos - min_src_pos) + min_src_pos
            nqx.append(px)

            #for now only store source position at current frame, without history
            param_ = np.array([list([px, frame])])

            #set source center to next perlin noise entry --> For now no moving!
            source_center[0,0] = float(nx_mov[frame])
            if frame > 0 :
                density = source.apply_to_grid(
                    grid=density, value=source_value, center=source_center, size=source_size
                )
            density = adv.advect(
                dt       = dt,
                phi_obs  = phi_obs,
                velocity = vel,
                rho      = density,
                order    = simulationParameters.advection_order,
                rk_order = simulationParameters.rk_order,
            )
            vel = adv.advect(
                dt       = dt,
                phi_obs  = phi_obs,
                velocity=vel,
                rho=vel,
                order=simulationParameters.advection_order,
                rk_order=simulationParameters.rk_order,
            )
            #print(vel.shape)

            vel = ext.add_buoyancy(
                phi_obs=phi_obs, velocity=vel, density=density, gravity=gravity
            )

            vel = ext.add_external_forces(
                phi_obs=phi_obs, velocity=vel, force=external_force, dt=dt
            )

            vel, pressure = pre.solve_pressure(flags, phi_obs, vel)
            simulationRunner.step()

            d_         = density.grid2array()
            #convert staggerd2d grid to real2d grid and then get array from it
            vel_real2d = vel.to_real_2d_grid()
            v_         = vel_real2d.grid2array()
            v_range    = [np.minimum( v_range[0], np.amin(v_) ), 
                          np.maximum( v_range[1], np.amax(v_) )]

            #prepare data storing of  velocity fields (as npz)
            ''' if scene == 0:
                img_path = os.path.join(den_dir, '%03d_%04d.png' % (scene, frame))
                save_img(d_, filename=img_path) '''
                
            v_file_path = os.path.join(vel_dir, path_format % (scene, frame))
            save_img_help(v_, filename=log_dir+'/v/%03d_%04d.png' % (scene, frame))
            d_file_path = os.path.join(den_dir, path_format % (scene, frame))
            save_img_help(d_, filename=log_dir+'/d_adv/%03d_%04d.png' % (scene, frame))
          
            np.savez_compressed(v_file_path, x=v_, y=param_)
        
        nx_list.append(list(nqx))
        gc.collect()
    
    #plot smoke source movement over all scenes
    n_path = os.path.join(log_dir, 'n.npz')
    np.savez_compressed(n_path, nx=nx_list)
    
    with np.load(n_path) as data:
        nx_list = data['nx']    
    t = range(num_frames)
    fig = plt.figure()

    plt.ylim((-1,1))
    for i in range(num_scenes):
        plt.plot(t, nx_list[i,:])
    
    n_fig_path = os.path.join(log_dir, 'n.png')
    fig.savefig(n_fig_path)

    #save range of velocity fields over all scenes
    vrange_file = os.path.join(log_dir, 'v_range.txt')
    with open(vrange_file, 'w') as f:
        print('%s: velocity min %.3f max %.3f' % (datetime.now(), v_range[0], v_range[1]))
        f.write('%.3f\n' % v_range[0])
        f.write('%.3f' % v_range[1])

def test_npz():
    """testing grid creation from npz files
    """
    strpath = 'C:\\Users\\98her\\Desktop\\ETH\\BA- cgl\\deepfluid-rl\\DeepFluid_pytorch\\Scene generation\\data\\test_vel_advection\\smoke_mov200_f400\\v'
    v_path_ = np.load('C:\\Users\\98her\\Desktop\\ETH\\BA- cgl\\deepfluid-rl\\DeepFluid_pytorch\\Scene generation\\data\\test_vel_advection\\smoke_mov200_f400\\v\\0_99.npz')
    
    v_ = Real2dGrid.from_npz(filename='C:\\Users\\98her\\Desktop\\ETH\\BA- cgl\\deepfluid-rl\\DeepFluid_pytorch\\Scene generation\\data\\test_vel_advection\\smoke_mov200_f400\\v\\0_99.npz', key='x').grid2array().transpose(0,3,1,2)
    save_img(v_, filename='C:\\Users\\98her\\Desktop\\ETH\\BA- cgl\\deepfluid-rl\\DeepFluid_pytorch\\Scene generation\\data\\test_vel_advection\\smoke_mov200_f400\\v\\0_99.png')







def advect_vel(
    v_t:np.ndarray,
    d_t:np.ndarray,
    px,
    sc:int,
    fr:int,
    num_frames:int,
    log_dir: str             = '/cluster/scratch/jherter/data/smoke_mov200_f400/dagger',
    store_data:bool          = False,
    path_format: str         = '%d_%d.npz',
    res: List[int]           = [128, 96],
    src_radius:float         = 0.075,
    dt: float                = 1.0,
    rk_order: int            = 1,
    with_obstacle: bool      = False,
    advection_order: int     = 1,
    wind_x: float            = 0.0,
    wind_y: float            = 0.0,
    preconditioner_type: str = "NONE",
):
    """Simulation policy of expert OPT: optimal policy pi*

    Args:
        v_t (np.ndarray): sampled observation (velocity fields)
        d_t (np.ndarray): matching density field that was computed with the surrogate algorithm described in the thesis
        px (_type_): smoke source position of at time point t
        sc (int): scene number
        fr (int): frame number
        num_frames (int): number of frames to simulate new scenes
        log_dir (str, optional): log directory. Defaults to '/cluster/scratch/jherter/data/smoke_mov200_f400/dagger'.
        store_data (bool, optional): Only store data when true (Set to false when computing mathcing density fields). Defaults to False.
        path_format (str, optional): path format. Defaults to '%d_%d.npz'.
        res (List[int], optional): resolution of simulation; [H, W]. Defaults to [128, 96].
        src_radius (float, optional): radius of smoke source. Defaults to 0.075.
        dt (float, optional): time step size (note that our algorithm is unconditionally stable). Defaults to 1.0.
        rk_order (int, optional): Runge Kutta order. Defaults to 1.
        with_obstacle (bool, optional): Set to True if scen contains obstacles. Defaults to False.
        advection_order (int, optional): ="Advection scheme that should be used SemiLagrangian=1, McCormack=2". Defaults to 1.
        wind_x (float, optional): Uniform wind at x direction. Defaults to 0.0.
        wind_y (float, optional): Uniform wind at y direction. Defaults to 0.0.
        preconditioner_type (str, optional): "Preconditioner type for linear system solver can be NONE, MULTIGRID and INCOMPLETE_POISSON". Defaults to "NONE".

    Returns:
        np.ndarray: density field (only used when computing matching density field for time integrated velocity field)
    """
    dim = 2
    batch_size = 1

    # use cuda if exists
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    dtype = torch.float32

    # set up the size of the simulation
    simulation_size = (batch_size, 1, *res)

    # set up the simulation parameters
    simulationParameters = SimulationParameters(
        dim                         = dim,
        dtype                       = dtype,
        device                      = device,
        simulation_size             = simulation_size,
        dt                          = dt,
        rk_order                    = rk_order,
        advection_order             = advection_order,
        conjugate_gradient_accuracy = 1e-3,
    )

    # create a simulation runner
    simulationRunner = SimulationRunner(parameters=simulationParameters)


    #file storing preparation
    #if store_data:
    den_dir = os.path.join(log_dir, 'd')
    if not os.path.exists(den_dir):
        os.makedirs(den_dir)
    vel_dir = os.path.join(log_dir, 'v')
    if not os.path.exists(vel_dir):
        os.makedirs(vel_dir)
    

    # initialize all the required grids
    flags   = simulationRunner.create_grid(GridType.FLAG)
    vel     = simulationRunner.create_grid(GridType.STAGGERED)
    density = simulationRunner.create_grid(GridType.REAL)
    vel_real_temp = simulationRunner.create_grid(GridType.REAL)
    
    vv = vel_real_temp.from_array(v_t, dtype=torch.float64, device=device)
    vel = vel.from_real_2d_grid(vv)
    density = density.from_array(d_t, dtype=torch.float64, device=device)
    
    # create external force, advection and pressure projection
    ext = simulationRunner.create_extforces()
    adv = simulationRunner.create_advection()
    adv.setup_external_forces(
        device=device,
        dtype=dtype,
        simulation_size=simulation_size,
    )
    pre = simulationRunner.create_pressure_projection(
        pressure_projection_type=PressureProjectionType.VOXELIZED,
        solver_type=SolverType.PCG,
        preconditioner_type=preconditioner_types[preconditioner_type.upper()],
    )

    # initialize the domain
    flags, phi_obs = ext.init_flags(phi=True, boundary_width=[0], wall=["  y "])

    # create an obstacle and add it to the domain
    if with_obstacle:
        obs_size   = simulationRunner.create_tensor([[0.1]])
        obs_center = simulationRunner.create_tensor([[-0.1, -0.4]])
        sphere     = simulationRunner.create_shape(ShapeType.SPHERE)
        phi_sphere = sphere.compute_level_set(center=obs_center, size=obs_size)
        phi_obs.union(phi_sphere)
        flags      = phi_obs.get_flags()
    
    # create a source
    source = simulationRunner.create_shape(ShapeType.SPHERE)
    source_center    = simulationRunner.create_tensor([[0.0, -0.8]])
    source_size      = simulationRunner.create_tensor([[2*src_radius]])
    source_value     = simulationRunner.create_tensor(1.0)
    gravity          = [0, -4e-3]

    # external wind force
    external_force = Staggered2dGrid.get_constant_grid(
        value_x         = torch.tensor(wind_x, device=device, dtype=dtype),
        value_y         = torch.tensor(wind_y, device=device, dtype=dtype),
        simulation_size = simulation_size,
    )
    
    if not torch.cuda.is_available():
        nx = np.load('C:\\Users\\98her\\Desktop\\ETH\\BA- cgl\\deepfluid-rl\\DeepFluid_pytorch\\Scene generation\\data\\dagger\\n.npz')['nx']
    else:
        nx = np.load('/cluster/scratch/jherter/data/smoke_mov200_f400/dagger/n.npz')['nx']
        
    for frame in tqdm(range(num_frames), leave = False):

        source_center[0,0] = nx[sc][fr+frame+1]
        
        param_ = np.array([list([nx[sc][fr+frame+1], fr+frame+1])])
        
        if frame > 0 or num_frames == 1: 
            density = source.apply_to_grid(grid=density, value=source_value, center=source_center, size=source_size)
        
        density = adv.advect(
            dt       = dt,
            phi_obs  = phi_obs,
            velocity = vel,
            rho      = density,
            order    = simulationParameters.advection_order,
            rk_order = simulationParameters.rk_order,
        )
        vel = adv.advect(
            dt       = dt,
            phi_obs  = phi_obs,
            velocity=vel,
            rho=vel,
            order=simulationParameters.advection_order,
            rk_order=simulationParameters.rk_order,
        )
        #print(vel.shape)

        vel = ext.add_buoyancy(
            phi_obs=phi_obs, velocity=vel, density=density, gravity=gravity
        )

        vel = ext.add_external_forces(
            phi_obs=phi_obs, velocity=vel, force=external_force, dt=dt
        )

        vel, pressure = pre.solve_pressure(flags, phi_obs, vel)
        simulationRunner.step()

        d_         = density.grid2array()
        #convert staggerd2d grid to real2d grid and then get array from it
        vel_real2d = vel.to_real_2d_grid()
        v_         = vel_real2d.grid2array()
        
        if store_data:
            milis = int(time() * 1000)
            d_file_path = os.path.join(den_dir, '%03d_%04d_%013d.npz' % (sc, fr+frame+1, milis))
            np.savez_compressed(d_file_path, x=d_, s=sc, f=fr+frame+1)
            v_file_path = os.path.join(vel_dir, '%03d_%04d_%013d.npz' % (sc, fr+frame+1, milis))            
            np.savez_compressed(v_file_path, x=v_, y=param_, s=sc, f=fr+frame+1)
            
        save_img_help(v_, filename=log_dir+'/v/%03d_%04d.png' % (sc, fr+frame+1))
        save_img_help(d_, filename=log_dir+'/d/%03d_%04d.png' % (sc, fr+frame+1))
    
    gc.collect()
    
    return d_.transpose(0,3,1,2)

if __name__ == "__main__":
    torch.set_printoptions(precision=3, linewidth=1000, profile="full", sci_mode=False)
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter, allow_abbrev=False
    )
    parser.add_argument("--log_dir", type=str, default='/cluster/scratch/jherter/data/smoke_mov200_f400/dagger', help="Directory to store density and velocity fields")
    parser.add_argument("--path_format", type=str, default='%d_%d_%d.npz', help="Path format for storing velocity fields")
    parser.add_argument("--res", type=int, nargs="+", default=[128, 96], help="Simulation size of the current simulation in sequence of res_y, res_x")
    parser.add_argument("--resolution_x", type=int, default=96, help="Resolution of x dimension")
    parser.add_argument("--resolution_y", type=int, default=128, help="Resolution of y dimension")
    parser.add_argument("--num_frames", type=int, default=30, help="For how many step to run the simulation per scene")
    parser.add_argument("--num_scenes", type=int, default=1, help="For how many scenes to run the simulation")
    parser.add_argument("--num_simulations", type=int, default=30, help="Total number of frames")
    parser.add_argument("--min_frames", type=int, default=0, help="First frame number in each scene")
    parser.add_argument("--max_frames", type=int, default=99, help="Last frame number in each scene")
    parser.add_argument("--min_scenes", type=int, default=0, help="First scene number")
    parser.add_argument("--max_scenes", type=int, default=1, help="Last scene number")
    parser.add_argument("--min_src_pos", type=float, default=-1, help="Min x position of smoke source")
    parser.add_argument("--max_src_pos", type=float, default=1, help="Max x position of smoke source")
    parser.add_argument("--src_radius", type=float, default=0.075, help="Radius of smoke source")
    parser.add_argument("--dt", type=float, default=1.0, help="Delta t of the simulation")
    parser.add_argument("--with_obstacle", type=eval, choices=[True, False], default="False", help="Include an obstacle in the scene")
    parser.add_argument("--advection_order", type=int, default=1, choices=[1, 2], help="Advection scheme that should be used SemiLagrangian=1, McCormack=2")
    parser.add_argument("--rk_order", type=int, default=3, choices=[1, 2, 3], help="The Runge-Kutta order for the advection")
    parser.add_argument("--wind_x", type=float, default=0.0, help="Uniform wind at x direction")
    parser.add_argument("--wind_y", type=float, default=0.0, help="Uniform wind at y direction")
    parser.add_argument("--preconditioner_type", type=str, default="NONE", help="Preconditioner type for linear system solver can be NONE, MULTIGRID and INCOMPLETE_POISSON")

    opt = vars(parser.parse_args())
    print(opt)
    main(**opt)
    
