from enum import Enum


class ShapeType(Enum):
    BLOB = "BLOB"
    BOX = "BOX"
    SPHERE = "SPHERE"
    BOXSET = "BOXSET"
