from enum import Enum


class GridType(Enum):
    FLAG = "FLAG"
    LEVEL = "LEVEL"
    REAL = "REAL"
    STAGGERED = "STAGGERED"
    NODE = "NODE"
