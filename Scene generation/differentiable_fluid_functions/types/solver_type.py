from enum import Enum


class SolverType(Enum):
    PCG = "PCG"
    DIRECT = "DIRECT"
    JACOBI = "JACOBI"
    MULTIGRID = "MULTIGRID"


class PreconditionerType(Enum):
    NONE = "NONE"
    INCOMPLETE_POISSON = "INCOMPLETE_POISSON"
    MULTIGRID = "MULTIGRID"
