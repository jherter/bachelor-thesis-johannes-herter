# Copyright © 2021 Disney | Copyright © 2021 ETH Zurich.  All rights reserved

from .abstract_kernel import AbstractKernel
import torch


# NOTE inherit from torch.nn.Module to potentially allow scripting/tracing
class CubicDyadic(AbstractKernel):
    """This kernel always splats to one cell and a neighborhood of 2 around it"""

    dyadic = True

    def __init__(self, support_radius: torch.tensor):
        super().__init__()
        # always use 2 cell to splat the values to
        self.max_distance = 2.0
        # divided by two to match the function in p2gg2p common process
        self.support_radius = support_radius

        # normalization factor
        h = self.support_radius / self.max_distance
        self.normalization_factor = 1 / h

    def weight(self, distance: torch.Tensor) -> torch.Tensor:
        """Compute the weight of a distance on a per channel bases
                https://www.seas.upenn.edu/~cffjiang/research/mpmcourse/mpmcourse.pdf
                Page: 32

        Args:
            distance (torch.Tensor): B x nPoints x 1/2/3

        Returns:
            torch.Tensor: Either the weight of the distance per channel
        """
        normalized_distance = distance * self.normalization_factor
        x_abs = torch.abs(normalized_distance)

        # run per channel version
        return torch.where(
            x_abs >= self.max_distance,
            torch.zeros_like(normalized_distance),
            torch.where(
                x_abs >= self.max_distance / 2,
                self.normalization_factor / 6.0 * (2.0 - x_abs) ** 3,
                self.normalization_factor * (0.5 * x_abs ** 3 - x_abs ** 2 + 2.0 / 3.0),
            ),
        )

    __call__ = weight
