from .abstract_kernel import AbstractKernel
import torch
import numpy as np


class CubicSPH(AbstractKernel):
    dyadic = False

    def __init__(self, support_radius: torch.tensor):
        # max_distance where kernel value is 0, depending on kernel.
        self.max_distance = 2.0

        # support_radius to normalize distance
        self.support_radius = support_radius

        # normalization factors for each dim
        # https://pysph.readthedocs.io/en/latest/reference/kernels.html
        h = self.support_radius / self.max_distance
        self.normalization_factor = [
            2 / (3 * h),
            10 / (7 * np.pi * h ** 2),
            1 / (np.pi * h ** 3),
        ]

    def weight(self, distance: torch.Tensor) -> torch.Tensor:
        """Compute the weight of a distance.
            We expect that weight(distance) = 0 for distance > support_radius
                https://interactivecomputergraphics.github.io/SPH-Tutorial/slides/01_intro_foundations_neighborhood.pdf
        Args:
            distance (torch.Tensor): B x nPoints x 1, 2/3.

        Returns:
            torch.Tensor: Either the weight of the distance.
        """
        if len(distance.shape) != 0:
            distance_dimension = distance.shape[-1]
        else:
            distance_dimension = 1
            normalization_factor = self.normalization_factor[distance_dimension - 1]

        if distance_dimension == 1:
            distance = torch.abs(distance)
            normalization_factor = self.normalization_factor[distance_dimension - 1]
        elif distance_dimension == 2 or distance_dimension == 3:
            distance = torch.norm(distance, dim=-1, keepdim=True)  # B x nPoints x 1
            normalization_factor = self.normalization_factor[distance_dimension - 1]
        else:
            raise ValueError("distance should be either 1/2/3D vector")

        normalized_distance = distance / self.support_radius * self.max_distance

        # run per channel version
        return torch.where(
            normalized_distance >= self.max_distance,  # == self.max_distance
            torch.zeros_like(normalized_distance),
            torch.where(
                normalized_distance >= self.max_distance / 2,
                normalization_factor / 4 * (2 - normalized_distance) ** 3,
                normalization_factor
                * (
                    1 - 3 / 2 * normalized_distance ** 2 * (1 - normalized_distance / 2)
                ),
            ),
        )

    __call__ = weight
