from abc import ABC, abstractmethod
import torch


class AbstractKernel(ABC):
    @property
    def dyadic(self):
        raise NotImplementedError

    @abstractmethod
    def weight(self, normalized_distance: torch.Tensor):
        ...
