from .abstract_shape import AbstractShape
import numpy as np
from typing import TypeVar, List

T = TypeVar("T", bound="Abstract3dShape")


class Abstract3dShape(AbstractShape):
    rank = 3

    def __init__(self, simulation_size: List[int], **kwargs):  # size of the simulation
        super(Abstract3dShape, self).__init__(simulation_size, **kwargs)
        grid_size = np.copy(simulation_size)
        grid_size[-3:] += 1
        self._grid_size = grid_size
