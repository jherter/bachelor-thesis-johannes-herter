from abc import ABC, abstractmethod
import torch
import numpy as np
from typing import List, TypeVar

T = TypeVar("T", bound="AbstractShape")


class AbstractShape(ABC):
    """
    Abstract class defining all the methods required for shape

    """

    def __init__(
        self,
        simulation_size: List[int],  # size of the simulation
        dtype,
        device: torch.device = torch.device("cpu"),
    ):
        # on which device is the data stored
        self._device = device
        self._dtype = dtype
        self._simulation_size = simulation_size
        grid_size = np.copy(simulation_size)
        grid_size[-2:] += 1
        self._grid_size = grid_size

    @property
    @abstractmethod
    def rank(self) -> int:
        ...

    # public methods
    @abstractmethod
    def apply_to_grid(self):
        ...

    @abstractmethod
    def compute_level_set(self, center, size):
        ...

    def _batch_diagonal(self, input: torch.tensor) -> torch.tensor:
        """Batches a stack of vectors (batch x N) -> a stack of diagonal matrices (batch x N x N)
            works in  2D -> 3D, should also work in higher dimensions
            make a zero matrix, which duplicates the last dim of input
            idea from here: https://discuss.pytorch.org/t/batch-of-diagonal-matrix/13560
        Args:
            input (torch.tensor): The batched vectors which should be transformed
            into a batched diagonal matrices

        Returns:
            torch.tensor: batched diagonal matrices
        """
        dims = [input.size(i) for i in torch.arange(input.dim())]
        dims.append(dims[-1])
        output = torch.zeros(dims, dtype=input.dtype, device=input.device)
        # stride across the first dimensions, add one to get the diagonal of the last dimension
        strides = [output.stride(i) for i in torch.arange(input.dim() - 1)]
        strides.append(output.size(-1) + 1)
        # stride and copy the input to the diagonal
        output.as_strided(input.size(), strides).copy_(input)
        return output
