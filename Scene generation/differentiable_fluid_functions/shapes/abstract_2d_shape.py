from .abstract_shape import AbstractShape

from typing import TypeVar

T = TypeVar("T", bound="Abstract2dShape")


class Abstract2dShape(AbstractShape):
    rank = 2
