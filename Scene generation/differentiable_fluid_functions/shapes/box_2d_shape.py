from .abstract_2d_shape import Abstract2dShape
import torch
import torch.nn.functional as F
import numpy as np
from typing import List, TypeVar, Union
from differentiable_fluid_functions.grid import Level2dGrid, Real2dGrid, Flag2dGrid
from differentiable_fluid_functions.utils import (
    create_2d_meshgrid_tensor,
    normalize_2d_meshgrid_tensor,
)

T = TypeVar("T", bound="Box2dShape")


class Box2dShape(Abstract2dShape):
    def __init__(self, simulation_size: List[int], **kwargs):
        super(Box2dShape, self).__init__(simulation_size, **kwargs)
        if len(simulation_size) != 4:
            raise ValueError(
                "For 2d array the 4 dim data is required eg. batch x channel x Height x Width"
            )
        batch_size, _, height, width = self._grid_size

        fake_sdf = torch.ones(
            (batch_size, _, height, width), device=self._device, dtype=torch.float32
        )
        # where do these sizes come from? Default values?
        fake_sdf[
            :,
            :,
            int(height / 4.0) : int(3.0 * height / 4.0) + 1,
            int(width / 4.0) : int(3.0 * width / 4.0) + 1,
        ].fill_(-1.0)
        phi_standard = Level2dGrid(fake_sdf)

        # flag grid indicating what is inside the box
        is_inside_standard_sphere = phi_standard.get_obstacle()
        self.grid_ref = Real2dGrid.from_flag_2d_grid(
            flag2dGrid=is_inside_standard_sphere,
            value=torch.tensor(1.0, device=self._device, dtype=torch.float32),
        )

    def apply_to_grid(
        self,
        grid: Union[Real2dGrid, Flag2dGrid],
        value: torch.Tensor,
        center: torch.Tensor,
        size: torch.Tensor,
    ) -> Union[Real2dGrid, Flag2dGrid]:
        """apply value (density) at the area of the box to a grid

        Args:
            grid (Real2dGrid): source grid with the current value
            value (torch.Tensor): how much to add to the grid
            center (torch.Tensor):Center of the box
            size (torch.Tensor): Size of the box

        Raises:
            ValueError: Center/size have wrong dim.
            ValueError: Center/size batch dim do not match

        Returns:
            Real2dGrid: Resulting grid with the value added at the positions of the box
        """
        if len(center.shape) != 2 or center.shape[1] != 2 or len(size.shape) != 2:
            raise ValueError(
                "For 2d box shape the center should have the following dim batch x 2 \n"
                "and the size should be batch x 1\n"
                f"currently: {center.shape} and {size.shape}"
            )
        if center.shape[0] != size.shape[0] or center.shape[0] != grid.size()[0]:
            raise ValueError(
                "Size and center must have the same amount of samples in the batch dimension"
                f"currently {center.shape[0]} != {size.shape[0]} != {grid.size()[0]}"
            )

        if isinstance(grid, Real2dGrid):
            return self._apply_to_real_grid(grid, value, center, size)
        elif isinstance(grid, Flag2dGrid):
            return self._apply_to_flag_grid(grid, value, center, size)
        else:
            raise TypeError(
                f"Only Flag or Real 2d grid are supported. current: {type(grid)}"
            )

    def _apply_to_flag_grid(
        self,
        grid: Flag2dGrid,
        value: torch.Tensor,
        center: torch.Tensor,
        size: torch.Tensor,
    ) -> Flag2dGrid:
        """apply value (density) at the position of the box to a flag grid

        Args:
            grid (Flag2dGrid): source grid with the current value
            value (torch.Tensor): how much to add to the grid
            center (torch.Tensor): The Center of the box in normalized coordinates
            size (torch.Tensor): The size of the sphere in normalized coordinates

        Raises:
            ValueError: Center/size have wrong dim.
            ValueError: Center/size batch dim do not match

        Returns:
            Flag2dGrid: Resulting grid with the value added at the positions of the box
        """
        batch_size, _, height, width = self._grid_size
        grid_size = torch.tensor(
            data=np.array([[width, height]]), device=self._device, dtype=torch.long
        ).repeat(batch_size, 1)

        # add batch and channel dim to size and center

        center_on_grid = (center + 1.0) / 2.0 * grid_size
        size_on_grid = size / 2.0 * grid_size

        p1 = (center_on_grid - size_on_grid).to(torch.long)
        p2 = (center_on_grid + size_on_grid).to(torch.long)

        flag_data = grid.get_data()

        for i in range(batch_size):
            flag_data[i, :, p1[i, 1] : p2[i, 1], p1[i, 0] : p2[i, 0]] = value

        return Flag2dGrid(flag_data)

    def _apply_to_real_grid(
        self,
        grid: Real2dGrid,
        value: torch.Tensor,
        center: torch.Tensor,
        size: torch.Tensor,
    ) -> Real2dGrid:
        """apply value (density) at the position of the box to a real grid

        Args:
            grid (Real2dGrid): source grid with the current value
            value (torch.Tensor): how much to add to the grid
            center (torch.Tensor): The Center of the box in normalized coordinates
            size (torch.Tensor): The size of the sphere in normalized coordinates

        Raises:
            ValueError: Center/size have wrong dim.
            ValueError: Center/size batch dim do not match

        Returns:
            Real2dGrid: Resulting grid with the value added at the positions of the box
        """
        grid_ref = value * self.grid_ref.get_data()

        size = 0.5 * 1.0 / size.repeat(1, self.rank)
        scale = self._batch_diagonal(size)

        translation = -torch.matmul(scale, center.unsqueeze(2))
        theta = torch.cat((scale, translation), dim=2)
        align_corners = False
        flow = F.affine_grid(
            theta=theta, size=grid_ref.size(), align_corners=align_corners
        )
        grid_src = F.grid_sample(grid_ref, flow, align_corners=align_corners)

        grid_new = torch.clamp(grid.get_data() + grid_src, min=0.0, max=1.0)
        return Real2dGrid(grid_new)

    def compute_level_set(
        self, center: torch.Tensor, size: torch.Tensor
    ) -> Level2dGrid:
        """Create a level set grid for a 2d box

        Args:
            center (torch.Tensor): Center coordinates of the box B X 2
            size (torch.Tensor): size of the box B X 1

        Raises:
            ValueError: Wrong center / size dimensions
            ValueError: different batch count size/center

        Returns:
            Level2dGrid: signed distance fields for the required boxes
        """
        # check if the dims are correct
        if len(center.shape) != 2 or center.shape[1] != 2 or len(size.shape) != 2:
            raise ValueError(
                "For 2d sphere shape the center should have the following dim batch x XY \n"
                "and the size should be batch x size\n"
                f"currently: {center.shape} and {size.shape}"
            )
        if center.shape[0] != size.shape[0]:
            raise ValueError(
                "Size and center must have the same amount of samples in the batch dimension"
                f"currently {center.shape[0]} != {size.shape[0]}"
            )

        # create a grid for every batch dim
        mgrid = create_2d_meshgrid_tensor(
            size=self._grid_size, device=self._device, dtype=self._dtype
        )
        # normalized form
        mgrid = normalize_2d_meshgrid_tensor(
            mgrid,
            normalize_x=self._grid_size[-1] - 1,
            normalize_y=self._grid_size[-2] - 1,
        )

        # add batch and channel dim to size and center
        center = center.unsqueeze(2).unsqueeze(3)
        size = size.unsqueeze(2).unsqueeze(3)

        dist_to_edge = torch.abs(mgrid - center) - size
        zero_tensor = torch.tensor([0.0], device=self._device, dtype=self._dtype)
        dist_outside = torch.norm(
            torch.max(dist_to_edge, zero_tensor), dim=1, keepdim=True
        )
        dist_inside = torch.min(
            torch.max(dist_to_edge, dim=1, keepdim=True)[0], zero_tensor
        )
        dist = dist_outside + dist_inside
        return Level2dGrid(dist)

    def compute_volume(self, size: torch.Tensor) -> torch.Tensor:
        """Computes the analytic volume (area) of the box.

        Args:
            size (torch.Tensor): Tensor with all the sizes (side length) shape b X 1

        Returns:
            torch.Tensor: The volume (area) of the box
        """

        return size ** 2
