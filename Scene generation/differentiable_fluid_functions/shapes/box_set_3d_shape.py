from .abstract_3d_shape import Abstract3dShape
import torch
from typing import List, TypeVar, Union
from differentiable_fluid_functions.grid import Level3dGrid, Real3dGrid, Flag3dGrid
from differentiable_fluid_functions.shapes import Box3dShape

T = TypeVar("T", bound="BoxSet3dShape")


class BoxSet3dShape(Abstract3dShape):
    def __init__(self, simulation_size: List[int], **kwargs):
        super(BoxSet3dShape, self).__init__(simulation_size, **kwargs)
        if len(simulation_size) != 5:
            raise ValueError(
                "For 3d array the 5 dim data is required eg. batch x channel x Depth x Height x Width"
            )
        self.box = Box3dShape(
            simulation_size=simulation_size, device=self._device, dtype=self._dtype
        )

    def compute_level_set(
        self, center: torch.Tensor, size: torch.Tensor
    ) -> Level3dGrid:
        """Create a level set grid for the union of all batch elements joint onto single grid

        Args:
            center (torch.Tensor): Center coordinates of the box B x n_boxes x 3
            size (torch.Tensor): size of the box B x n_boxes x 1

        Raises:
            ValueError: Wrong center / size dimensions
            ValueError: different batch count size/center

        Returns:
            Level3dGrid: signed distance fields for the required boxes
        """
        # check if the dims are correct
        if len(center.shape) != 3 or center.shape[2] != 3 or len(size.shape) != 3:
            raise ValueError(
                "For 3d box set shape the center should have the following dim batch x n_boxes x 3 \n"
                "and the size should be batch x n_boxes x 1\n"
                f"currently: {center.shape} and {size.shape}"
            )
        if center.shape[0] != size.shape[0] and size.shape[0] > 0:
            raise ValueError(
                "Size and center must have the same amount of samples in the batch dimension (> 0)"
                f"currently {center.shape[0]} != {size.shape[0]}"
            )
        batch_size = center.shape[0]
        level_grid_tensor = torch.zeros(
            tuple(self._grid_size), dtype=self._dtype, device=self._device
        )
        for sample in range(batch_size):
            phi = self.box.compute_level_set(
                center=center[sample, 0:1], size=size[sample, 0:1]
            )

            # for every element
            for i in range(1, center.shape[1]):
                phi_box = self.box.compute_level_set(
                    center=center[sample, i : i + 1],
                    size=size[sample, i : i + 1],
                )
                phi.union(phi_box)
            level_grid_tensor[sample] = phi.get_data()[0]
        return Level3dGrid(level_grid_tensor)

    def apply_to_grid(
        self,
        grid: Union[Real3dGrid, Flag3dGrid],
        value: torch.Tensor,
        center: torch.Tensor,
        size: torch.Tensor,
    ) -> Union[Real3dGrid, Flag3dGrid]:
        raise NotImplementedError("This method is not implemented yet")
