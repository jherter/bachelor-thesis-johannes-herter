from .abstract_3d_shape import Abstract3dShape
import torch
import torch.nn.functional as F
import numpy as np
from typing import List, TypeVar
from differentiable_fluid_functions.grid import Level3dGrid, Real3dGrid
from differentiable_fluid_functions.utils import (
    create_3d_meshgrid_tensor,
    normalize_3d_meshgrid_tensor,
)

T = TypeVar("T", bound="Sphere3dShape")


class Sphere3dShape(Abstract3dShape):
    def __init__(self, simulation_size: List[int], **kwargs):
        super(Sphere3dShape, self).__init__(simulation_size, **kwargs)
        if len(simulation_size) != 5:
            raise ValueError(
                "For 3d array the 5 dim data is required eg. batch x channel x Depth x Height x Width"
            )
        batch_size = simulation_size[0]
        standard_center = torch.zeros(
            (batch_size, 3), dtype=self._dtype, device=self._device
        )

        # rescale size s.t. sphere is not distorted in non-cubic scenes
        # use only height and width
        shortest = min(self._grid_size[2:])
        ratio = (
            torch.tensor(self._grid_size[2:], device=self._device, dtype=self._dtype)
            / shortest
        )

        # flip to have the order correct
        # transform to BCDHW format
        self.ratio = torch.flip(ratio, dims=[0]).view(1, 3, 1, 1, 1)

        standard_size = (
            torch.ones((batch_size, 1), dtype=self._dtype, device=self._device) * 0.5
        )

        phi_standard = self.compute_level_set(standard_center, standard_size)

        # flag grid indicating what is inside the sphere
        is_inside_standard_sphere = phi_standard.get_obstacle()
        self.grid_ref = Real3dGrid.from_flag_3d_grid(
            flag3dGrid=is_inside_standard_sphere,
            value=torch.tensor(1.0, device=self._device, dtype=self._dtype),
        )

    def apply_to_grid(
        self,
        grid: Real3dGrid,
        value: torch.Tensor,
        center: torch.Tensor,
        size: torch.Tensor,
    ) -> Real3dGrid:
        """apply value (density) at the position of the sphere to a grid

        Args:
            grid (Real3dGrid): source grid with the current value
            value (torch.Tensor): how much to add to the grid
            center (torch.Tensor): The Center of the sphere in normalized coordinates
            size (torch.Tensor): The radius of the sphere in normalized coordinates

        Raises:
            ValueError: Center/size have wrong dim.
            ValueError: Center/size batch dim do not match

        Returns:
            Real3dGrid: Resulting grid with the value added at the positions of the sphere
        """
        if len(center.shape) != 2 or center.shape[1] != 3 or len(size.shape) != 2:
            raise ValueError(
                "For 3d sphere shape the center should have the following dim batch x 3 \n"
                "and the size should be batch x 1\n"
                f"currently: {center.shape} and {size.shape}"
            )
        if center.shape[0] != size.shape[0] or center.shape[0] != grid.size()[0]:
            raise ValueError(
                "Size and center must have the same amount of samples in the batch dimension"
                f"currently {center.shape[0]} != {size.shape[0]} != {grid.size()[0]}"
            )

        grid_ref = value * self.grid_ref.get_data()

        size = 0.5 * 1.0 / size.repeat(1, self.rank)

        scale = self._batch_diagonal(size)
        translation = -torch.matmul(scale, center.unsqueeze(2))
        theta = torch.cat((scale, translation), dim=2)

        flow = F.affine_grid(theta=theta, size=grid_ref.size(), align_corners=True)

        grid_src = F.grid_sample(grid_ref, flow, align_corners=True)
        grid_new = torch.clamp(grid.get_data() + grid_src, min=0.0, max=1.0)
        return Real3dGrid(grid_new)

    def compute_level_set(
        self, center: torch.Tensor, size: torch.Tensor
    ) -> Level3dGrid:
        """Create a level set grid for a 3d sphere

        Args:
            center (torch.Tensor): Center coordinates of the sphere B X 3
            size (torch.Tensor): size(radius) of the spheres B X 1

        Raises:
            ValueError: Wrong center/ size dimensions
            ValueError: different batch count size/center

        Returns:
            Level3dGrid: signed distance fields for the required circles
        """
        # check if the dims are correct
        if len(center.shape) != 2 or center.shape[1] != 3 or len(size.shape) != 2:
            raise ValueError(
                "For 3d sphere shape the center should have the following dim batch x XY \n"
                "and the size should be batch x size\n"
                f"currently: {center.shape} and {size.shape}"
            )
        if center.shape[0] != size.shape[0]:
            raise ValueError(
                "Size and center must have the same amount of samples in the batch dimension"
                f"currently {center.shape[0]} != {size.shape[0]}"
            )
        # get the batch dimensions
        batch_dim = center.shape[0]

        # create a grid for every batch dim
        mgrid = create_3d_meshgrid_tensor(
            size=self._grid_size, device=self._device, dtype=self._dtype
        )
        # normalize from
        mgrid = normalize_3d_meshgrid_tensor(
            mgrid,
            normalize_x=self._grid_size[-1] - 1,
            normalize_y=self._grid_size[-2] - 1,
            normalize_z=self._grid_size[-3] - 1,
        )

        # add channel and batch dim and rescale grid to avoid ellipsis for no rectangular grids
        ratio = self.ratio.repeat(batch_dim, 1, 1, 1, 1)
        mgrid = mgrid * ratio

        # add batch and channel dim to size and center
        center = center.unsqueeze(2).unsqueeze(3).unsqueeze(4)
        size = size.unsqueeze(2).unsqueeze(3).unsqueeze(4)
        dist = torch.norm(mgrid - center, dim=1, keepdim=True) - size

        return Level3dGrid(data=dist)

    def compute_volume(self, size: torch.Tensor) -> torch.Tensor:
        """Computes the analytic volume (area) of the sphere.

        Args:
            size (torch.Tensor): Tensor with all the sizes shape b X 1

        Returns:
            torch.Tensor: The volume of the sphere
        """

        return 0.75 * np.pi * size ** 3
