from .abstract_3d_shape import Abstract3dShape
import torch
import numpy as np
from typing import List, TypeVar
from differentiable_fluid_functions.grid import Level3dGrid, Real3dGrid
from differentiable_fluid_functions.shapes import Sphere3dShape

T = TypeVar("T", bound="Blob3dShape")


class Blob3dShape(Abstract3dShape):
    def __init__(self, simulation_size: List[int], n_sphere: int = 8, **kwargs):
        super(Blob3dShape, self).__init__(simulation_size, **kwargs)
        if len(simulation_size) != 5:
            raise ValueError(
                "For 3d array the 5 dim data is required eg. batch x channel x Depth x Height x Width"
            )

        self.n_sphere = n_sphere
        batch_size = simulation_size[0]
        # compute the positions of the spheres
        self.theta_set = torch.tensor(
            [[2 * np.pi / n_sphere * x for x in range(self.n_sphere)]],
            dtype=self._dtype,
            device=self._device,
        ).repeat(batch_size, 1)

        self.center_sphere = Sphere3dShape(
            simulation_size=simulation_size, dtype=self._dtype, device=self._device
        )

    def apply_to_grid(
        self,
        grid: Real3dGrid,
        value: torch.Tensor,
        center: torch.Tensor,
        size: torch.Tensor,
        outer_size: torch.Tensor,
    ) -> Real3dGrid:
        raise NotImplementedError("Blob3dShape.apply_to_grid")

    def compute_level_set(
        self, center: torch.Tensor, size: torch.Tensor, outer_size: torch.Tensor
    ) -> Level3dGrid:
        """Compute the level set of a blob

        Args:
            center (torch.Tensor): Position of the center sphere shape B x 3 (in relative coordinates -1/1)
            size (torch.Tensor): Radius of the center sphere B x 1 (in relative coordinates -1/1)
            outer_size (torch.Tensor): The size of the outer spheres shape B x n_spheres. A positive
            radius is adding the outer sphere, while a negative radius is removing it.

        Raises:
            ValueError: Wrong input shape
            ValueError: Parameters batch dimension does not match
            ValueError: outer_size has more values than spheres

        Returns:
            Level3dGrid: the Sdf of the sphere set
        """
        if (
            len(center.shape) != 2
            or center.shape[1] != 3
            or len(size.shape) != 2
            or len(outer_size.shape) != 2
        ):
            raise ValueError(
                "For 3d blob shape the center should have the following dim batch x 3 \n"
                "and the size should be batch x 1\n"
                "and the outer_size should should be batch x n_spheres\n"
                f"currently: {center.shape}, {outer_size.shape} and {size.shape}"
            )
        if center.shape[0] != size.shape[0] or outer_size.shape[0] != size.shape[0]:
            raise ValueError(
                "Size, outer_size and center must have the same amount of samples in the batch dimension"
                f"currently {center.shape[0]} != {size.shape[0]}"
            )
        if outer_size.size(1) != self.n_sphere:
            raise ValueError(
                "Size of outer_size should be equal " "to number of outer spheres."
            )
        batch_size = center.shape[0]

        # compute the positions of the sphere on the shell of the center sphere
        dx_set = size * torch.cos(self.theta_set)
        dy_set = size * torch.sin(self.theta_set)
        dz_set = torch.zeros_like(self.theta_set)

        # stack to the shape B x n_spheres x 3
        outer_center_offset = torch.stack((dx_set, dy_set, dz_set), dim=2)
        outer_center_set = center.unsqueeze(1) + outer_center_offset

        level_grid_tensor = torch.zeros(
            tuple(self._grid_size), dtype=self._dtype, device=self._device
        )
        for sample in range(batch_size):
            # slice to keep the underlying dim
            cur_center = center[sample : sample + 1]
            cur_size = size[sample : sample + 1]

            # create a sphere with a single sample
            cur_phi = self.center_sphere.compute_level_set(cur_center, cur_size)

            for i in range(self.n_sphere):
                # get the center position
                # slice to keep the underlying batch dim
                cur_outer_center = outer_center_set[sample, i : i + 1]
                cur_outer_size = outer_size[sample : sample + 1, i : i + 1]

                if cur_outer_size[0, 0] >= 0:
                    phi_i = self.center_sphere.compute_level_set(
                        cur_outer_center, cur_outer_size
                    )
                    cur_phi.union(phi_i)
                else:
                    phi_i = self.center_sphere.compute_level_set(
                        cur_outer_center, -cur_outer_size
                    )
                    cur_phi.subtraction(phi_i)
            level_grid_tensor[sample] = cur_phi.get_data()[0]
        return Level3dGrid(level_grid_tensor)

    def _intersection_volume(
        self, size: torch.Tensor, outer_size: torch.Tensor
    ) -> torch.Tensor:
        """Compute the intersection volume (area) between two circles, assuming they do overlap!
            result
            from: https://mathworld.wolfram.com/Sphere-SphereIntersection.html
        Args:
            size (torch.Tensor): Size of the center sphere shape b x 1
            outer_size (torch.Tensor): size of the outer spheres shape b x n_sphere

        Returns:
            torch.Tensor: resulting overlaps
        """

        # since d = R the formula simplify to
        b, n_spheres = outer_size.shape
        r = outer_size
        R = size.repeat(1, n_spheres)

        factor = np.pi * r * r
        brackets = 8.0 * r * R - 3.0 * r * r
        result = factor * brackets
        result /= 12.0 * R

        return result

    def compute_volume(
        self, size: torch.Tensor, outer_size: torch.Tensor
    ) -> torch.Tensor:
        """Computes the analytic volume of the blob.

        Args:
            size (torch.Tensor): Size of the center sphere shape b x 1
            outer_size (torch.Tensor): size of the outer sphere shape b x n_sphere

        Returns:
            torch.Tensor: The volume for every element in the batch shape bx1
        """
        if len(size.shape) != 2 or len(outer_size.shape) != 2:
            raise ValueError(
                "For 3d blob shape \n"
                "the size should be batch x 1\n"
                "and the outer_size should should be batch x n_sphere\n"
                f"currently: {outer_size.shape} and {size.shape}"
            )
        if outer_size.shape[0] != size.shape[0]:
            raise ValueError(
                "Size and outer_size must have the same amount of samples in the batch dimension"
                f"currently {outer_size.shape[0]} != {size.shape[0]}"
            )
        if outer_size.size(1) != self.n_sphere:
            raise ValueError(
                "Size of outer_size should be equal to number of outer spheres."
            )
        # compute the volume of the center sphere
        center_volume = self.center_sphere.compute_volume(size)

        # compute the overlaps
        overlaps = self._intersection_volume(size, outer_size)

        # compute the area of the outer spheres
        outer_volume = self.center_sphere.compute_volume(torch.abs(outer_size))

        # check if the spheres are added or subtracted
        outer_volume = torch.where(
            outer_size < 0, torch.zeros_like(outer_volume), outer_volume
        )

        # remove the summed overlaps
        outer_volume = torch.sum(outer_volume - overlaps, dim=1, keepdim=True)
        volume = center_volume + outer_volume
        return volume
