from .abstract_2d_shape import Abstract2dShape
import torch
import numpy as np
from typing import List, TypeVar
from differentiable_fluid_functions.grid import Level2dGrid, Real2dGrid
from differentiable_fluid_functions.shapes import Sphere2dShape

T = TypeVar("T", bound="Blob2dShape")


class Blob2dShape(Abstract2dShape):
    def __init__(self, simulation_size: List[int], n_sphere: int = 8, **kwargs):
        super(Blob2dShape, self).__init__(simulation_size, **kwargs)
        if len(simulation_size) != 4:
            raise ValueError(
                "For 2d array the 4 dim data is required eg. batch x channel x Height x Width"
            )

        self.n_sphere = n_sphere
        batch_size = simulation_size[0]
        # compute the positions of the spheres
        self.theta_set = torch.tensor(
            [[2 * np.pi / n_sphere * x for x in range(self.n_sphere)]],
            dtype=self._dtype,
            device=self._device,
        ).repeat(batch_size, 1)

        self.center_sphere = Sphere2dShape(
            simulation_size=simulation_size, dtype=self._dtype, device=self._device
        )

    def apply_to_grid(
        self,
        grid: Real2dGrid,
        value: torch.Tensor,
        center: torch.Tensor,
        size: torch.Tensor,
        outer_size: torch.Tensor,
    ) -> Real2dGrid:
        raise NotImplementedError()

    def compute_level_set(
        self, center: torch.Tensor, size: torch.Tensor, outer_size: torch.Tensor
    ) -> Level2dGrid:
        """Compute the level set of a blob

        Args:
            center (torch.Tensor): Position of the center sphere shape B x 2 (in relative coordinates -1/1)
            size (torch.Tensor): Radius of the center sphere B x 1 (in relative coordinates -1/1)
            outer_size (torch.Tensor): The size of the outer spheres shape B x n_spheres. A positive
            radius is adding the outer sphere, while a negative radius is removing it.

        Raises:
            ValueError: Wrong input shape
            ValueError: Parameters batch dimension does not match
            ValueError: outer_size has more values than spheres

        Returns:
            Level2dGrid: the Sdf of the sphere set
        """
        if (
            len(center.shape) != 2
            or center.shape[1] != 2
            or len(size.shape) != 2
            or len(outer_size.shape) != 2
        ):
            raise ValueError(
                "For 2d blob shape the center should have the following dim batch x 2 \n"
                "and the size should be batch x 1\n"
                "and the outer_size should should be batch x n_spheres\n"
                f"currently: {center.shape}, {outer_size.shape} and {size.shape}"
            )
        if center.shape[0] != size.shape[0] or outer_size.shape[0] != size.shape[0]:
            raise ValueError(
                "Size, outer_size and center must have the same amount of samples in the batch dimension"
                f"currently {center.shape[0]} != {size.shape[0]}"
            )
        if outer_size.size(1) != self.n_sphere:
            raise ValueError(
                "Size of outer_size should be equal " "to number of outer spheres."
            )
        batch_size = center.shape[0]

        # compute the positions of the sphere on the shell of the center sphere
        dx_set = size * torch.cos(self.theta_set)
        dy_set = size * torch.sin(self.theta_set)

        # stack to the shape B x n_spheres x 2
        outer_center_offset = torch.stack((dx_set, dy_set), dim=2)
        outer_center_set = center.unsqueeze(1) + outer_center_offset

        level_grid_tensor = torch.zeros(
            tuple(self._grid_size), dtype=self._dtype, device=self._device
        )
        for sample in range(batch_size):
            # slice to keep the underlying dim
            cur_center = center[sample : sample + 1]
            cur_size = size[sample : sample + 1]

            # create a sphere with a single sample
            cur_phi = self.center_sphere.compute_level_set(cur_center, cur_size)

            for i in range(self.n_sphere):
                # get the center position
                # slice to keep the underlying batch dim
                cur_outer_center = outer_center_set[sample, i : i + 1]
                cur_outer_size = outer_size[sample : sample + 1, i : i + 1]

                if cur_outer_size[0, 0] >= 0:
                    phi_i = self.center_sphere.compute_level_set(
                        cur_outer_center, cur_outer_size
                    )
                    cur_phi.union(phi_i)
                else:
                    phi_i = self.center_sphere.compute_level_set(
                        cur_outer_center, -cur_outer_size
                    )
                    cur_phi.subtraction(phi_i)
            level_grid_tensor[sample] = cur_phi.get_data()[0]
        return Level2dGrid(level_grid_tensor)

    def _intersection_volume(
        self, size: torch.Tensor, outer_size: torch.Tensor
    ) -> torch.Tensor:
        """Compute the intersection volume (area) between two circles, assuming they do overlap!
            https://www.xarg.org/2016/07/calculate-the-intersection-area-of-two-circles/
        Args:
            size (torch.Tensor): Size of the center sphere shape b x 1
            outer_size (torch.Tensor): size of the outer spheres shape b x n_sphere

        Returns:
            torch.Tensor: resulting overlaps
        """
        dx_set = size * torch.cos(self.theta_set)
        dy_set = size * torch.sin(self.theta_set)

        d = torch.sqrt(dx_set ** 2 + dy_set ** 2)
        a = size ** 2
        b = outer_size ** 2

        x = (a - b + d * d) / (2 * d)
        z = x * x
        y = torch.sqrt(a - z)

        return (
            a * torch.asin(y / size)
            + b * torch.asin(y / torch.abs(outer_size))
            - y * (x + torch.sqrt(z + b - a))
        )

    def compute_volume(
        self, size: torch.Tensor, outer_size: torch.Tensor
    ) -> torch.Tensor:
        """Computes the analytic volume of the blob.

        Args:
            size (torch.Tensor): Size of the center sphere shape b x 1
            outer_size (torch.Tensor): size of the outer sphere shape b x n_sphere

        Returns:
            torch.Tensor: The volume for every element in the batch shape bx1
        """

        if len(size.shape) != 2 or len(outer_size.shape) != 2:
            raise ValueError(
                "For 2d blob shape \n"
                "the size should be batch x 1\n"
                "and the outer_size should should be batch x n_sphere\n"
                f"currently: {outer_size.shape} and {size.shape}"
            )
        if outer_size.shape[0] != size.shape[0]:
            raise ValueError(
                "Size and outer_size must have the same amount of samples in the batch dimension"
                f"currently {outer_size.shape[0]} != {size.shape[0]}"
            )
        if outer_size.size(1) != self.n_sphere:
            raise ValueError(
                "Size of outer_size should be equal to number of outer spheres."
            )
        # compute the volume of the center sphere
        center_volume = self.center_sphere.compute_volume(size)

        # compute the overlaps
        overlaps = self._intersection_volume(size, outer_size)

        # compute the area of the outer spheres
        outer_volume = self.center_sphere.compute_volume(torch.abs(outer_size))
        # check if the spheres are added or subtracted
        outer_volume = torch.where(
            outer_size < 0, torch.zeros_like(outer_volume), outer_volume
        )
        # remove the summed overlaps
        outer_volume = torch.sum(outer_volume - overlaps, dim=1, keepdim=True)
        volume = center_volume + outer_volume
        return volume
