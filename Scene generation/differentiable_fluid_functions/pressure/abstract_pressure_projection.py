from abc import ABC, abstractmethod


class AbstractPressureProjection(ABC):
    @property
    @abstractmethod
    def rank(self) -> int:
        ...

    @abstractmethod
    def solve_pressure(self):
        ...
