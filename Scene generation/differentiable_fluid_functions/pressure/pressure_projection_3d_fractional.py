from .abstract_pressure_projection import AbstractPressureProjection
import torch
from differentiable_fluid_functions.grid import (
    Real3dGrid,
    Level3dGrid,
    Staggered3dGrid,
    CellType,
    Flag3dGrid,
    Abstract3dGrid,
)
from differentiable_fluid_functions.solver import AbstractSolver
from differentiable_fluid_functions.external_forces import ExternalForces3d
from typing import List, TypeVar
import warnings

T = TypeVar("T", bound="PressureProjection3dFractional")


class PressureProjection3dFractional(AbstractPressureProjection):
    rank = 3

    def __init__(
        self,
        solver: AbstractSolver,
        dtype,
        simulation_size: List[int],
        conjugate_gradient_accuracy: float = 1e-6,
        system_perturb: float = 0.0,
        device: torch.device = torch.device("cpu"),
    ):
        super(PressureProjection3dFractional, self).__init__()
        self._device = device
        self._system_perturb = system_perturb
        self._conjugate_gradient_accuracy = conjugate_gradient_accuracy
        self._solver = solver
        # initialize external forces
        self.ext = ExternalForces3d(
            simulation_size=simulation_size, device=device, dtype=dtype
        )

    def solve_pressure(
        self, flags: Flag3dGrid, phi_obs: Level3dGrid, velocity: Staggered3dGrid
    ) -> List[Abstract3dGrid]:
        """Solve position equation for pressure.

        Args:
            flags (Flag3dGrid): Flags defining the type of each cell
            phi_obs (Level3dGrid): Domain levelset used for boundary condition corrections
            velocity (Staggered3dGrid): Current velocity of the simulation

        Returns:
            List[Abstract3dGrid]: New velocity as a staggered grid as well as resulting pressure grid as RealGrid
        """
        warnings.warn(
            "PressureProjection3dFractional::solve_pressure is not finished and does not work correctly in all cases"
        )
        # Ap = Nabla dot u*
        # compute the right hand side of the linear system
        right_hand_side = self._compute_pressure_right_hand_side(
            flags, phi_obs, velocity
        )
        pressure = self._solve_poisson_system(flags, phi_obs, right_hand_side)
        velocity = self._correct_velocity(velocity, pressure)

        velocity = self.ext.set_wall_boundary_conditions(phi_obs, velocity)
        return velocity, pressure

    @staticmethod
    def _compute_pressure_right_hand_side(
        flags: Flag3dGrid, phi_obs: Level3dGrid, velocity: Staggered3dGrid
    ) -> torch.Tensor:
        """Compute the right hand site of the pressure equation

        Args:
            flags (Flag3dGrid):  Flags defining the type of each cell
            phi_obs (Level3dGrid): Domain levelset used for boundary condition corrections
            velocity (Staggered3dGrid): Current velocity of the simulation

        Returns:
            torch.Tensor: rhs of the equation
        """
        fractions = phi_obs.get_fractions()  # Staggered3dGrid
        (
            fractions_data_x_tensor,
            fractions_data_y_tensor,
            fractions_data_z_tensor,
        ) = fractions.get_tensor()

        (
            velocity_data_x_tensor,
            velocity_data_y_tensor,
            velocity_data_z_tensor,
        ) = velocity.get_tensor()  # Staggered3dGrid

        # nabla u right hand side
        # delta x and delta y is assumed to be one
        dx = 1.0  # / max(velocity.size())
        rhs = -(
            # x comp
            (
                fractions_data_x_tensor[..., 1:] * velocity_data_x_tensor[..., 1:]
                - fractions_data_x_tensor[..., :-1] * velocity_data_x_tensor[..., :-1]
            )
            / dx
            # y comp
            + (
                fractions_data_y_tensor[..., 1:, :] * velocity_data_y_tensor[..., 1:, :]
                - fractions_data_y_tensor[..., :-1, :]
                * velocity_data_y_tensor[..., :-1, :]
            )
            / dx
            # z comp
            + (
                fractions_data_z_tensor[..., 1:, :, :]
                * velocity_data_z_tensor[..., 1:, :, :]
                - fractions_data_z_tensor[..., :-1, :, :]
                * velocity_data_z_tensor[..., :-1, :, :]
            )
            / dx
        )

        rhs = torch.where(
            flags.get_data() == int(CellType.FLUID), rhs, torch.zeros_like(rhs)
        )
        # remove unused channel dim
        rhs = rhs[:, 0, ...]
        return rhs

    def _solve_poisson_system(
        self, flags: Flag3dGrid, phi_obs: Level3dGrid, rhs: torch.Tensor
    ) -> Real3dGrid:
        """Solve the Poisson system

        Args:
            flags (torch.Tensor): flags (Flag3dGrid):  Flags defining the type of each cell
            phi_obs (Level3dGrid): Domain levelset used for boundary condition corrections
            rhs (torch.Tensor): The rhs of the equation

        Returns:
            Real3dGrid: Result
        """
        if not self._solver.pressure_matrix_built():
            self._solver.build_pressure_matrix(
                flags=flags,
                fractions=phi_obs.get_fractions(),
                build_pressure_matrix_fcn=self._build_pressure_matrix,
            )

        if self._solver.precon is not None and not self._solver.precon.built():
            self._solver.precon.build(
                flags=flags, build_pressure_matrix_fcn=self._build_pressure_matrix
            )
        pressure = self._solver.solve_linear_system(
            rhs=rhs, tol=self._conjugate_gradient_accuracy
        )
        # add channel dim needs to be added
        return Real3dGrid(data=pressure[:, None, :, :, :])

    @staticmethod
    def _correct_velocity(
        velocity: Staggered3dGrid, pressure: Real3dGrid
    ) -> Staggered3dGrid:
        """Correct the velocity by subtracting the pressure gradient

        Args:
            velocity (Staggered3dGrid): The current velocity
            pressure (Real3dGrid): The current pressure

        Returns:
            Staggered3dGrid: The corrected velocity
        """
        pressure_grad = pressure.get_spatial_gradient()
        vel_corrected = velocity - pressure_grad
        return vel_corrected

    @staticmethod
    def _build_pressure_matrix(
        flags: Flag3dGrid, fractions: Staggered3dGrid, system_perturb: float = 0.0
    ) -> torch.Tensor:
        """convert fractions into a sparse pressure matrix with the use of the a flag grid

        Args:
            flags (Flag3dGrid): Defining the type of the cells
            fractions (Staggered3dGrid): Fractions of the domain
            system_perturb (float): small perturbation to Adiag to pin down the null space

        Returns:
            torch.Tensor: Sparse Pressure matrix of the shape B x 3 x H x W
        """
        dx = 1.0  # / max(fractions.size())
        (
            fractions_data_x_tensor,
            fractions_data_y_tensor,
            fractions_data_z_tensor,
        ) = fractions.get_tensor()

        Adiag_fluid = (
            fractions_data_x_tensor[..., :-1]
            + fractions_data_x_tensor[..., 1:]
            + fractions_data_y_tensor[..., :-1, :]
            + fractions_data_y_tensor[..., 1:, :]
            + fractions_data_z_tensor[..., :-1, :, :]
            + fractions_data_z_tensor[..., 1:, :, :]
        )
        Aplusx_fluid = -fractions_data_x_tensor[..., 1:]
        Aplusy_fluid = -fractions_data_y_tensor[..., 1:, :]
        Aplusz_fluid = -fractions_data_z_tensor[..., 1:, :, :]

        Adiag = torch.where(
            flags.get_data() == int(CellType.FLUID),
            Adiag_fluid,
            torch.ones_like(Adiag_fluid),
        )

        Adiag = Adiag + system_perturb

        Aplusx = torch.where(
            flags.get_data() == int(CellType.FLUID),
            Aplusx_fluid,
            torch.zeros_like(Aplusx_fluid),
        )

        Aplusy = torch.where(
            flags.get_data() == int(CellType.FLUID),
            Aplusy_fluid,
            torch.zeros_like(Aplusy_fluid),
        )
        Aplusz = torch.where(
            flags.get_data() == int(CellType.FLUID),
            Aplusz_fluid,
            torch.zeros_like(Aplusz_fluid),
        )
        # using the channel dim which is 1, use save the other matrices
        A = torch.cat([Adiag, Aplusx, Aplusy, Aplusz], dim=1)

        A /= dx * dx * dx
        return A
