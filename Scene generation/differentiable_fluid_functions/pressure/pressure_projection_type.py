from enum import Enum


class PressureProjectionType(Enum):
    FRACTIONAL = "FRACTIONAL"
    VOXELIZED = "VOXELIZED"
