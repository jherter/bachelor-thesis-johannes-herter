from .abstract_pressure_projection import AbstractPressureProjection
import torch
import torch.nn.functional as F
from differentiable_fluid_functions.grid import (
    Real3dGrid,
    Level3dGrid,
    Staggered3dGrid,
    CellType,
    Flag3dGrid,
    Abstract3dGrid,
)
from differentiable_fluid_functions.solver import AbstractSolver
from typing import List, TypeVar
from differentiable_fluid_functions.external_forces import ExternalForces3d

T = TypeVar("T", bound="PressureProjection3dVoxelized")


class PressureProjection3dVoxelized(AbstractPressureProjection):
    rank = 3

    def __init__(
        self,
        solver: AbstractSolver,
        dtype,
        simulation_size: List[int],
        conjugate_gradient_accuracy: float = 1e-6,
        system_perturb: float = 0.0,
        device: torch.device = torch.device("cpu"),
    ):
        super(PressureProjection3dVoxelized, self).__init__()
        self._device = device
        self._conjugate_gradient_accuracy = conjugate_gradient_accuracy
        self._system_perturb = system_perturb
        self._solver = solver
        # inititialize external forces
        self.ext = ExternalForces3d(
            simulation_size=simulation_size, device=device, dtype=dtype
        )

    def solve_pressure(
        self, flags: Flag3dGrid, phi_obs: Level3dGrid, velocity: Staggered3dGrid
    ) -> List[Abstract3dGrid]:
        """Solve position equation for pressure.

        Args:
            flags (Flag3dGrid): Flags defining the type of each cell
            phi_obs (Level3dGrid): Domain levelset used for boundary condition corrections
            velocity (Staggered3dGrid): Current velocity of the simulation

        Returns:
            List[Abstract3dGrid]: New velocity as a staggered grid as well as resulting pressure grid as RealGrid
        """
        # Ap = Nabla dot u*
        # compute the right hand side of the linear system
        right_hand_side = self._compute_pressure_right_hand_side(flags, velocity)
        pressure = self._solve_poisson_system(flags, right_hand_side)
        velocity = self._correct_velocity(velocity, pressure)

        velocity = self.ext.set_wall_boundary_conditions(phi_obs, velocity)
        return velocity, pressure

    @staticmethod
    def _compute_pressure_right_hand_side(
        flags: Flag3dGrid,
        velocity: Staggered3dGrid,
        apply_obstacle_correction: bool = False,
    ) -> torch.Tensor:
        """Compute the right hand site of the pressure equation

        Args:
            flags (Flag3dGrid):  Flags defining the type of each cell
            velocity (Staggered3dGrid): Current velocity of the simulation
            apply_obstacle_correction (bool, optional):
                Reduce the rhs if next to obstacle as described in the book
                (Fluid Simulation for Computer Graphics by Robert Bridson). However, not used by manta. Defaults to False.

        Returns:
            torch.Tensor: rhs of the equation
        """
        (
            velocity_data_x_tensor,
            velocity_data_y_tensor,
            velocity_data_z_tensor,
        ) = velocity.get_tensor()

        # nabla u right hand side
        # delta x and delta y is assumed to be one
        dx = 1.0  # / max(velocity.size())
        rhs = -(
            # x comp
            (velocity_data_x_tensor[..., 1:] - velocity_data_x_tensor[..., :-1]) / dx
            # y comp
            + (velocity_data_y_tensor[..., 1:, :] - velocity_data_y_tensor[..., :-1, :])
            / dx
            # z comp
            + (
                velocity_data_z_tensor[..., 1:, :, :]
                - velocity_data_z_tensor[..., :-1, :, :]
            )
            / dx
        )

        # obstacle correction
        fluid_index = flags.get_data() != int(CellType.FLUID)

        # only count where there is fluid
        rhs = torch.where(
            flags.get_data() == int(CellType.FLUID), rhs, torch.zeros_like(rhs)
        )

        # correct the rhs if the fluid is next to a wall
        if apply_obstacle_correction:
            flags_tensor = flags.get_data()
            count_left = PressureProjection3dVoxelized._count_with_kernel(
                flags=flags_tensor,
                value=int(CellType.OBSTACLE),
                kernel=torch.tensor(
                    [
                        [
                            [
                                [[0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]],
                                [[0.0, 0.0, 0.0], [1.0, 0.0, 0.0], [0.0, 0.0, 0.0]],
                                [[0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]],
                            ]
                        ]
                    ],
                    device=flags_tensor.device,
                ),
            )
            count_left[fluid_index] = 0
            count_left = F.pad(
                input=count_left, pad=(0, 1, 0, 0, 0, 0), mode="constant", value=0
            )

            # create velocity index
            rhs_left_corr = torch.where(
                count_left != 0,
                velocity_data_x_tensor,
                torch.zeros_like(velocity_data_x_tensor),
            )

            count_right = PressureProjection3dVoxelized._count_with_kernel(
                flags=flags_tensor,
                value=int(CellType.OBSTACLE),
                kernel=torch.tensor(
                    [
                        [
                            [
                                [[0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]],
                                [[0.0, 0.0, 0.0], [0.0, 0.0, 1.0], [0.0, 0.0, 0.0]],
                                [[0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]],
                            ]
                        ]
                    ],
                    device=flags_tensor.device,
                ),
            )
            count_right[fluid_index] = 0
            count_right = F.pad(
                input=count_right, pad=(1, 0, 0, 0, 0, 0), mode="constant", value=0
            )

            # create velocity index
            rhs_right_corr = torch.where(
                count_right != 0,
                velocity_data_x_tensor,
                torch.zeros_like(velocity_data_x_tensor),
            )

            count_up = PressureProjection3dVoxelized._count_with_kernel(
                flags=flags_tensor,
                value=int(CellType.OBSTACLE),
                kernel=torch.tensor(
                    [
                        [
                            [
                                [[0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]],
                                [[0.0, 1.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]],
                                [[0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]],
                            ]
                        ]
                    ],
                    device=flags_tensor.device,
                ),
            )
            count_up[fluid_index] = 0
            count_up = F.pad(
                input=count_up, pad=(0, 0, 0, 1, 0, 0), mode="constant", value=0
            )

            # create velocity index
            rhs_up_corr = torch.where(
                count_up != 0,
                velocity_data_y_tensor,
                torch.zeros_like(velocity_data_y_tensor),
            )

            count_down = PressureProjection3dVoxelized._count_with_kernel(
                flags=flags_tensor,
                value=int(CellType.OBSTACLE),
                kernel=torch.tensor(
                    [
                        [
                            [
                                [[0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]],
                                [[0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 1.0, 0.0]],
                                [[0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]],
                            ]
                        ]
                    ],
                    device=flags_tensor.device,
                ),
            )
            count_down[fluid_index] = 0
            count_down = F.pad(
                input=count_down, pad=(0, 0, 1, 0, 0, 0), mode="constant", value=0
            )

            # create velocity index
            rhs_down_corr = torch.where(
                count_down != 0,
                velocity_data_y_tensor,
                torch.zeros_like(velocity_data_y_tensor),
            )

            count_front = PressureProjection3dVoxelized._count_with_kernel(
                flags=flags_tensor,
                value=int(CellType.OBSTACLE),
                kernel=torch.tensor(
                    [
                        [
                            [
                                [[0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]],
                                [[0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]],
                                [[0.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 0.0]],
                            ]
                        ]
                    ],
                    device=flags_tensor.device,
                ),
            )
            count_front[fluid_index] = 0
            count_front = F.pad(
                input=count_front, pad=(0, 0, 0, 0, 1, 0), mode="constant", value=0
            )

            # create velocity index
            rhs_front_corr = torch.where(
                count_front != 0,
                velocity_data_z_tensor,
                torch.zeros_like(velocity_data_z_tensor),
            )

            count_back = PressureProjection3dVoxelized._count_with_kernel(
                flags=flags_tensor,
                value=int(CellType.OBSTACLE),
                kernel=torch.tensor(
                    [
                        [
                            [
                                [[0.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 0.0]],
                                [[0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]],
                                [[0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]],
                            ]
                        ]
                    ],
                    device=flags_tensor.device,
                ),
            )
            count_back[fluid_index] = 0
            count_back = F.pad(
                input=count_back, pad=(0, 0, 0, 0, 0, 1), mode="constant", value=0
            )

            # create velocity index
            rhs_back_corr = torch.where(
                count_back != 0,
                velocity_data_z_tensor,
                torch.zeros_like(velocity_data_z_tensor),
            )

            rhs -= rhs_up_corr[..., :-1, :]
            rhs += rhs_down_corr[..., 1:, :]
            rhs -= rhs_left_corr[..., :-1]
            rhs += rhs_right_corr[..., 1:]
            rhs -= rhs_back_corr[..., :-1, :, :]
            rhs += rhs_front_corr[..., 1:, :, :]

        # remove unused channel dim
        rhs = rhs[:, 0, ...]
        return rhs

    @staticmethod
    def _count_with_kernel(
        flags: torch.Tensor, value: torch.Tensor, kernel: torch.Tensor
    ) -> torch.Tensor:
        """Static method to count occurrence of a value with a given kernel

        Args:
            flags (torch.Tensor): flags (Flag3dGrid):  Flags defining the type of each cell
            value (torch.Tensor): For which type of cell should the count be done
            kernel (torch.Tensor): kernel describing the neighbourhood

        Returns:
            torch.Tensor: count for each cell.
        """
        matching_flags = (flags == value).to(torch.float32)
        # first pad the tensor with zeros
        matching_flags_pad = F.pad(
            input=matching_flags, pad=(1, 1, 1, 1, 1, 1), mode="constant", value=0
        )

        count = F.conv3d(matching_flags_pad, kernel)

        # reset the count on all other spots beside the ones where it was matched
        return count

    @staticmethod
    def _build_pressure_matrix(
        flags: Flag3dGrid,
        fractions: Staggered3dGrid = None,
        system_perturb: float = 0.0,
    ) -> torch.Tensor:
        """Build the pressure matrix (A matrix)

        Args:
            flags (torch.Tensor): flags (Flag3dGrid):  Flags defining the type of each cell
            system_perturb (float): small perturbation to Adiag to pin down the null space

        Returns:
            torch.Tensor: Sparse A matrix in format (B x 4 x D x H x W)
        """
        assert fractions is None
        flags_tensor = flags.get_data()
        dx = 1.0  # / max(flags_tensor.size())
        Adiag = 6.0 - PressureProjection3dVoxelized._count_with_kernel(
            flags=flags_tensor,
            value=int(CellType.OBSTACLE),
            kernel=torch.tensor(
                [
                    [
                        [
                            [[0.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 0.0]],
                            [[0.0, 1.0, 0.0], [1.0, 0.0, 1.0], [0.0, 1.0, 0.0]],
                            [[0.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 0.0]],
                        ]
                    ]
                ],
                device=flags_tensor.device,
            ),
        )
        Adiag = torch.where(
            flags_tensor == int(CellType.FLUID),
            Adiag,
            torch.ones_like(Adiag),
        )

        Adiag = Adiag + system_perturb

        Aplusx_fluid = -PressureProjection3dVoxelized._count_with_kernel(
            flags=flags_tensor,
            value=int(CellType.FLUID),
            kernel=torch.tensor(
                [
                    [
                        [
                            [[0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]],
                            [[0.0, 0.0, 0.0], [0.0, 0.0, 1.0], [0.0, 0.0, 0.0]],
                            [[0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]],
                        ]
                    ]
                ],
                device=flags_tensor.device,
            ),
        )
        Aplusx_fluid[flags_tensor != int(CellType.FLUID)] = 0

        Aplusy_fluid = -PressureProjection3dVoxelized._count_with_kernel(
            flags=flags_tensor,
            value=int(CellType.FLUID),
            kernel=torch.tensor(
                [
                    [
                        [
                            [[0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]],
                            [[0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 1.0, 0.0]],
                            [[0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]],
                        ]
                    ]
                ],
                device=flags_tensor.device,
            ),
        )
        Aplusy_fluid[flags_tensor != int(CellType.FLUID)] = 0

        Aplusz_fluid = -PressureProjection3dVoxelized._count_with_kernel(
            flags=flags_tensor,
            value=int(CellType.FLUID),
            kernel=torch.tensor(
                [
                    [
                        [
                            [[0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]],
                            [[0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]],
                            [[0.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 0.0]],
                        ]
                    ]
                ],
                device=flags_tensor.device,
            ),
        )
        Aplusz_fluid[flags_tensor != int(CellType.FLUID)] = 0

        # using the channel dim which is 1, use save the other matrices
        A = torch.cat([Adiag, Aplusx_fluid, Aplusy_fluid, Aplusz_fluid], dim=1)
        A /= dx * dx * dx
        return A

    def _solve_poisson_system(self, flags: Flag3dGrid, rhs: torch.Tensor) -> Real3dGrid:
        """Solve the Poisson system

        Args:
            flags (torch.Tensor): flags (Flag3dGrid):  Flags defining the type of each cell
            rhs (torch.Tensor): The rhs of the equation

        Returns:
            Real3dGrid: Result
        """
        if not self._solver.pressure_matrix_built():
            self._solver.build_pressure_matrix(
                flags=flags, build_pressure_matrix_fcn=self._build_pressure_matrix
            )

        if self._solver.precon is not None and not self._solver.precon.built():
            self._solver.precon.build(
                flags=flags, build_pressure_matrix_fcn=self._build_pressure_matrix
            )

        pressure = self._solver.solve_linear_system(
            rhs=rhs, tol=self._conjugate_gradient_accuracy
        )
        # add channel dim needs to be added
        return Real3dGrid(data=pressure[:, None, :, :, :])

    @staticmethod
    def _correct_velocity(
        velocity: Staggered3dGrid, pressure: Real3dGrid
    ) -> Staggered3dGrid:
        """Correct the velocity by subtracting the pressure gradient

        Args:
            velocity (Staggered3dGrid): The current velocity
            pressure (Real3dGrid): The current pressure

        Returns:
            Staggered3dGrid: The corrected velocity
        """
        pressure_grad = pressure.get_spatial_gradient()
        vel_corrected = velocity - pressure_grad
        return vel_corrected
