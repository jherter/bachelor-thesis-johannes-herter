from abc import ABC, abstractmethod


class AbstractAdvection(ABC):
    @property
    @abstractmethod
    def rank(self) -> int:
        ...

    @abstractmethod
    def advect(self):
        ...

    @staticmethod
    def _map_sample_pos_to_function_name(sample_pos: str, rk: bool) -> str:

        # rk = runge kutta
        if sample_pos == "center":
            return "grid_sample_center" if rk else "get_centered"
        elif sample_pos == "node":
            return "grid_sample_node"
        elif sample_pos == "staggered_x":
            return "grid_sample_x" if rk else "get_staggered_x"
        elif sample_pos == "staggered_y":
            return "grid_sample_y" if rk else "get_staggered_y"
        elif sample_pos == "staggered_z":
            return "grid_sample_z" if rk else "get_staggered_z"
        else:
            raise ValueError(f"sample_pos {sample_pos} not recognized.")
