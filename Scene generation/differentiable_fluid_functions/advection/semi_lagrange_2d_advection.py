import torch
import torch.nn.functional as F
from .abstract_2d_advection import Abstract2dAdvection
from typing import List
from differentiable_fluid_functions.grid import (
    Staggered2dGrid,
    Real2dGrid,
    Node2dGrid,
    Level2dGrid,
    Abstract2dGrid,
)
from differentiable_fluid_functions.utils import (
    create_2d_meshgrid_tensor,
    sample_grid_2d,
)
from differentiable_fluid_functions.external_forces import ExternalForces2d


class SemiLagrange2dAdvection(Abstract2dAdvection):
    def __init__(self):
        super(Abstract2dAdvection, self).__init__()
        self.ext = None

    def setup_external_forces(
        self,
        dtype,
        simulation_size: List[int],
        device: torch.device = torch.device("cpu"),
    ):
        self.ext = ExternalForces2d(
            simulation_size=simulation_size, device=device, dtype=dtype
        )

    def advect(
        self,
        dt: float,
        velocity: Staggered2dGrid,
        rho: Abstract2dGrid,  # quantity that should be advected
        phi_obs: Level2dGrid = None,
        order: int = 1,
        clamp_mode: int = 2,
        rk_order: int = 3,
    ) -> Abstract2dGrid:
        """Advect a real, staggered or node grid by a velocity field using the Semi-Lagrange technique

        Args:
            dt (float): timestep
            velocity (Staggered2dGrid): The velocity field used for the advection
            rho (Abstract2dGrid): data that should be advected
            order (int, optional): Advection order, 1 for plain lagrange or MacCormack. Defaults to 1.
            clamp_mode (int, optional): Clamping mode used for MacCormack advection. Defaults to 2.
            rk_order (int, optional): Order of the Runge-Kutta method. Defaults to 3.

        Raises:
            ValueError: Wrong advection order requested

        Returns:
            Abstract2dGrid: Advected grid data
        """
        if order == 1:
            rho_adv = SemiLagrange2dAdvection._advect_semi_lagrange(
                velocity=velocity, rho=rho, dt=dt, rk_order=rk_order
            )
        elif order == 2:
            rho_fwd = SemiLagrange2dAdvection._advect_semi_lagrange(
                velocity=velocity, rho=rho, dt=dt, rk_order=rk_order
            )
            rho_bwd = SemiLagrange2dAdvection._advect_semi_lagrange(
                velocity=velocity, rho=rho_fwd, dt=-dt, rk_order=rk_order
            )
            if isinstance(rho, Real2dGrid) or isinstance(rho, Node2dGrid):
                rho_corr = SemiLagrange2dAdvection._compute_rho_correction(
                    rho, rho_fwd, rho_bwd
                )
            elif isinstance(rho, Staggered2dGrid):
                rho_x, rho_y = rho.get_data()
                rho_fwd_x, rho_fwd_y = rho_fwd.get_data()
                rho_bwd_x, rho_bwd_y = rho_bwd.get_data()

                rho_corr_x = SemiLagrange2dAdvection._compute_rho_correction(
                    rho_x, rho_fwd_x, rho_bwd_x
                )
                rho_corr_y = SemiLagrange2dAdvection._compute_rho_correction(
                    rho_y, rho_fwd_y, rho_bwd_y
                )
                rho_corr = Staggered2dGrid(data_x=rho_corr_x, data_y=rho_corr_y)

            rho_adv = SemiLagrange2dAdvection._maccormack_clamp(
                velocity=velocity,
                rho_corr=rho_corr,
                rho_fwd=rho_fwd,
                rho_org=rho,
                dt=dt,
                clamp_mode=clamp_mode,
                rk_order=rk_order,
            )
        else:
            raise ValueError("Advection order must be 1 or 2.")
        if phi_obs is not None and self.ext is not None:
            # TODO: test with level
            rho_adv = self.ext.set_wall_boundary_conditions(
                phi_obs=phi_obs, grid=rho_adv
            )
        return rho_adv

    @staticmethod
    def _compute_rho_correction(
        rho: Abstract2dGrid, rho_fwd: Abstract2dGrid, rho_bwd: Abstract2dGrid
    ) -> Abstract2dGrid:
        """Compute the correct rho use for the MacCormack advection

        Args:
            rho (Abstract2dGrid): Values at current timestep
            rho_fwd (Abstract2dGrid): Values at the next timestep
            rho_bwd (Abstract2dGrid): Values at the previous timestep

        Returns:
            Abstract2dGrid: Corrected values
        """
        rho_data = rho.get_data()
        rho_fwd_data = rho_fwd.get_data()
        rho_bwd_data = rho_bwd.get_data()
        rho_corr_data = rho_fwd_data + (rho_data - rho_bwd_data) * 0.5
        if isinstance(rho, Real2dGrid):
            return Real2dGrid(rho_corr_data)
        else:
            return Node2dGrid(rho_corr_data)

    @staticmethod
    def _advect_semi_lagrange(
        dt: float,
        velocity: Staggered2dGrid,
        rho: Abstract2dGrid,
        rk_order: int = 1,
    ) -> Abstract2dGrid:
        """check which instance the data is and call required functions

        Args:
            dt (float): timestep
            velocity (Staggered2dGrid): The velocity field used for the advection
            rho (Abstract2dGrid): data that should be advected
            rk_order (int, optional): Order of the Runge-Kutta method. Defaults to 3.

        Raises:
            TypeError: Type of the data is unknown

        Returns:
            Abstract2dGrid: Advected grid data
        """
        if isinstance(rho, Real2dGrid):
            return SemiLagrange2dAdvection._advect_real_semi_lagrange(
                velocity=velocity, rho=rho, dt=dt, rk_order=rk_order
            )
        if isinstance(rho, Staggered2dGrid):
            return SemiLagrange2dAdvection._advect_staggered_semi_lagrange(
                velocity=velocity, rho=rho, dt=dt, rk_order=rk_order
            )
        if isinstance(rho, Node2dGrid):
            return SemiLagrange2dAdvection._advect_node_semi_lagrange(
                velocity=velocity, rho=rho, dt=dt, rk_order=rk_order
            )

        raise TypeError(f"Rho has the wrong type: {type(rho)}")

    @staticmethod
    def _advect_real_semi_lagrange(
        velocity: Staggered2dGrid, rho: Real2dGrid, dt: float, rk_order: int
    ) -> Real2dGrid:
        """Advect grid data of the type Real2dGrid

        Args:
            dt (float): timestep
            velocity (Staggered2dGrid): The velocity field used for the advection
            rho (Real2dGrid): data that should be advected
            rk_order (int, optional): Order of the Runge-Kutta method. Defaults to 3.

        Returns:
            Real2dGrid: Advected grid data
        """

        input_data = rho.get_data()

        result = SemiLagrange2dAdvection._advect_semi_lagrange_single_channel(
            velocity=velocity,
            rho=input_data,
            dt=dt,
            rk_order=rk_order,
            sample_pos="center",
        )
        return Real2dGrid(result)

    # TODO: test me!
    @staticmethod
    def _advect_node_semi_lagrange(
        velocity: Staggered2dGrid, rho: Node2dGrid, dt: float, rk_order: int
    ) -> Node2dGrid:
        """Advect grid data of the type Node2dGrid

        Args:
            dt (float): timestep
            velocity (Staggered2dGrid): The velocity field used for the advection
            rho (Node2dGrid): data that should be advected
            rk_order (int, optional): Order of the Runge-Kutta method. Defaults to 3.

        Returns:
            Node2dGrid: Advected grid data
        """
        input_data = rho.get_data()

        result = SemiLagrange2dAdvection._advect_semi_lagrange_single_channel(
            velocity=velocity,
            rho=input_data,
            dt=dt,
            rk_order=rk_order,
            sample_pos="node",
        )
        return Node2dGrid(result)

    @staticmethod
    def _advect_staggered_semi_lagrange(
        velocity: Staggered2dGrid, rho: Staggered2dGrid, dt: float, rk_order: int
    ) -> Staggered2dGrid:
        """Advect grid data of the type Staggered2dGrid

        Args:
            dt (float): timestep
            velocity (Staggered2dGrid): The velocity field used for the advection
            rho (Staggered2dGrid): data that should be advected
            rk_order (int, optional): Order of the Runge-Kutta method. Defaults to 3.

        Returns:
            Staggered2dGrid: Advected grid data
        """
        rho_x_real, rho_y_real = rho.get_tensor()

        # advect in x direction
        rho_adv_x = SemiLagrange2dAdvection._advect_semi_lagrange_single_channel(
            velocity=velocity,
            rho=rho_x_real,
            dt=dt,
            rk_order=rk_order,
            sample_pos="staggered_x",
        )
        # advect in y direction
        rho_adv_y = SemiLagrange2dAdvection._advect_semi_lagrange_single_channel(
            velocity=velocity,
            rho=rho_y_real,
            dt=dt,
            rk_order=rk_order,
            sample_pos="staggered_y",
        )
        data_x = Real2dGrid(rho_adv_x)
        data_y = Real2dGrid(rho_adv_y)

        return Staggered2dGrid(data_x=data_x, data_y=data_y)

    @staticmethod
    def _advect_semi_lagrange_single_channel(
        velocity: Staggered2dGrid,
        rho: torch.Tensor,
        dt: float,
        rk_order: int,
        sample_pos: str,
    ) -> torch.Tensor:
        """Advect a single tensor

        Args:
            dt (float): timestep
            velocity (Staggered2dGrid): The velocity field used for the advection
            rho (torch.Tensor): data that should be advected
            rk_order (int, optional): Order of the Runge-Kutta method. Defaults to 3.
            sample_pos (str): where to sample the velocity field
        Returns:
            torch.Tensor: Advected tensor
        """
        # create mesh grid
        mgrid = create_2d_meshgrid_tensor(
            rho.size(), device=rho.device, dtype=rho.dtype
        )

        # use Runge-Kutta or Euler method to create the back trace field
        back_trace = SemiLagrange2dAdvection._runge_kutta(
            velocity, mgrid, dt, rk_order, sample_pos
        )

        # use warped grid sample function from PyTorch to interpolate the grid
        # and bring the interpolated values to the current position
        rho_adv = sample_grid_2d(
            grid=rho,
            back_trace=back_trace,
            normalize_x=rho.shape[-1] - 1,
            normalize_y=rho.shape[-2] - 1,
        )

        return rho_adv

    @staticmethod
    def _runge_kutta(
        velocity: Staggered2dGrid,
        mgrid: torch.Tensor,
        dt: float,
        rk_order: int,
        sample_pos: str,
    ) -> torch.Tensor:
        """Runge-Kutta method for advection

        Args:
            velocity (Staggered2dGrid): The velocity field used for the advection
            mgrid (torch.Tensor): Sample points used for the computation
            dt (float): timestep
            rk_order (int, optional): Order of the Runge-Kutta method
            sample_pos (str): where to sample the velocity field

        Raises:
            ValueError: Wrong rk order given. max is 3

        Returns:
            torch.Tensor: Advected tensor
        """
        grid_sample_fcn = SemiLagrange2dAdvection._map_sample_pos_to_function_name(
            sample_pos, rk=True
        )
        if rk_order == 1:
            return SemiLagrange2dAdvection._euler(velocity, mgrid, dt, sample_pos)
        elif rk_order == 2:
            # TODO: test me
            # find the mid point position using half of time step
            # back_trace_mid = mgrid - grid_sample_fcn(vel, mgrid)*dt*0.5
            back_trace_mid = SemiLagrange2dAdvection._euler(
                velocity, mgrid, 0.5 * dt, sample_pos
            )
            # find the mid point velocity
            return mgrid - getattr(velocity, grid_sample_fcn)(back_trace_mid) * dt
        elif rk_order == 3:
            # TODO: test me
            k1 = (
                mgrid - SemiLagrange2dAdvection._euler(velocity, mgrid, dt, sample_pos)
            ) / dt
            k2 = getattr(velocity, grid_sample_fcn)(mgrid - 0.5 * dt * k1)
            k3 = getattr(velocity, grid_sample_fcn)(mgrid - 0.75 * dt * k2)
            return (
                mgrid - 2.0 / 9.0 * dt * k1 - 3.0 / 9.0 * dt * k2 - 4.0 / 9.0 * dt * k3
            )
        else:
            raise ValueError("Runge-Kutta only support order 1, 2 or 3.")

    @staticmethod
    def _euler(
        velocity: Staggered2dGrid, mgrid: torch.Tensor, dt: float, sample_pos: str
    ) -> torch.Tensor:
        """Take a Euler step

        Args:
            velocity (Staggered2dGrid): The velocity field used for the advection
            mgrid (torch.Tensor): Sample points used for the computation
            dt (float): timestep
            sample_pos (str): where to sample the velocity field

        Raises:
            NotImplementedError: Euler step not implemented for node data advection.

        Returns:
            torch.Tensor: Data after an Euler step was performed
        """
        if sample_pos == "node":
            raise NotImplementedError(
                "Euler step not implemented for node data advection."
            )
        grid_sample_fcn = SemiLagrange2dAdvection._map_sample_pos_to_function_name(
            sample_pos, rk=False
        )

        result = mgrid - getattr(velocity, grid_sample_fcn)() * dt
        return result

    @staticmethod
    def _maccormack_clamp(
        velocity: Staggered2dGrid,
        rho_corr: Abstract2dGrid,
        rho_fwd: Abstract2dGrid,
        rho_org: Abstract2dGrid,
        dt: int,
        clamp_mode: int,
        rk_order: int,
    ) -> Abstract2dGrid:
        """check which instance the data is and call required functions

        Args:
            velocity (Staggered2dGrid): The velocity field used for the advection
            rho_org (Abstract2dGrid): Values at current timestep
            rho_fwd (Abstract2dGrid): Values at the next timestep
            rho_bwd (Abstract2dGrid): Values at the previous timestep
            dt (int): Timestep
            clamp_mode (int): Clamping mode used for MacCormack advection.
            rk_order (int): Order of the Runge-Kutta method

        Returns:
            Abstract2dGrid: Clamp data using the MacCormack scheme
        """

        if isinstance(rho_org, Staggered2dGrid):  # hard code to figure out grid type
            return SemiLagrange2dAdvection._maccormack_staggered_clamp(
                velocity=velocity,
                rho_corr=rho_corr,
                rho_fwd=rho_fwd,
                rho_org=rho_org,
                dt=dt,
                clamp_mode=clamp_mode,
                rk_order=rk_order,
            )
        else:
            return SemiLagrange2dAdvection._maccormack_real_clamp(
                velocity=velocity,
                rho_corr=rho_corr,
                rho_fwd=rho_fwd,
                rho_org=rho_org,
                dt=dt,
                clamp_mode=clamp_mode,
                rk_order=rk_order,
            )

    @staticmethod
    def _maccormack_real_clamp(
        velocity: Staggered2dGrid,
        rho_corr: Abstract2dGrid,
        rho_fwd: Abstract2dGrid,
        rho_org: Abstract2dGrid,
        dt: int,
        clamp_mode: int,
        rk_order: int,
    ) -> Abstract2dGrid:
        """Call functions for single data grids

        Args:
            velocity (Staggered2dGrid): The velocity field used for the advection
            rho_org (Abstract2dGrid): Values at current timestep
            rho_fwd (Abstract2dGrid): Values at the next timestep
            rho_bwd (Abstract2dGrid): Values at the previous timestep
            dt (int): Timestep
            clamp_mode (int): Clamping mode used for MacCormack advection.
            rk_order (int): Order of the Runge-Kutta method

        Returns:
            Abstract2dGrid: Clamp data using the MacCormack scheme
        """
        rho_adv = SemiLagrange2dAdvection._maccormack_clamp_single_channel(
            velocity=velocity,
            rho_corr=rho_corr.get_data(),
            rho_fwd=rho_fwd.get_data(),
            rho_org=rho_org.get_data(),
            dt=dt,
            clamp_mode=clamp_mode,
            rk_order=rk_order,
            sample_pos="center",
        )
        if isinstance(rho_org, Node2dGrid):
            return Node2dGrid(rho_adv)
        else:
            return Real2dGrid(rho_adv)

    @staticmethod
    def _maccormack_staggered_clamp(
        velocity: Staggered2dGrid,
        rho_corr: Staggered2dGrid,
        rho_fwd: Staggered2dGrid,
        rho_org: Staggered2dGrid,
        dt: int,
        clamp_mode: int,
        rk_order: int,
    ) -> Staggered2dGrid:
        """Call functions for staggered2dgrids

        Args:
            velocity (Staggered2dGrid): The velocity field used for the advection
            rho_org (Staggered2dGrid): Values at current timestep
            rho_fwd (Staggered2dGrid): Values at the next timestep
            rho_bwd (Staggered2dGrid): Values at the previous timestep
            dt (int): Timestep
            clamp_mode (int): Clamping mode used for MacCormack advection.
            rk_order (int): Order of the Runge-Kutta method

        Returns:
            Staggered2dGrid: Clamp data using the MacCormack scheme
        """
        rho_corr_x, rho_corr_y = rho_corr.get_tensor()
        rho_fwd_x, rho_fwd_y = rho_fwd.get_tensor()
        rho_org_x, rho_org_y = rho_org.get_tensor()
        rho_adv_x = SemiLagrange2dAdvection._maccormack_clamp_single_channel(
            velocity=velocity,
            rho_corr=rho_corr_x,
            rho_fwd=rho_fwd_x,
            rho_org=rho_org_x,
            dt=dt,
            clamp_mode=clamp_mode,
            rk_order=rk_order,
            sample_pos="staggered_x",
        )
        rho_adv_y = SemiLagrange2dAdvection._maccormack_clamp_single_channel(
            velocity=velocity,
            rho_corr=rho_corr_y,
            rho_fwd=rho_fwd_y,
            rho_org=rho_org_y,
            dt=dt,
            clamp_mode=clamp_mode,
            rk_order=rk_order,
            sample_pos="staggered_y",
        )
        rho_adv = Staggered2dGrid.from_tensor(data_x=rho_adv_x, data_y=rho_adv_y)

        return rho_adv

    @staticmethod
    def _maccormack_clamp_single_channel(
        velocity: Staggered2dGrid,
        rho_corr: torch.Tensor,  # Real or node grid
        rho_fwd: torch.Tensor,
        rho_org: torch.Tensor,
        dt: int,
        clamp_mode: int,
        rk_order: int,
        sample_pos: str,
    ) -> torch.Tensor:
        """Clamp a single channel using the Mac Cormack scheme.

        Args:
            velocity (Staggered2dGrid): The velocity field used for the advection
            rho_org (torch.Tensor): Values at current timestep
            rho_fwd (torch.Tensor): Values at the next timestep
            rho_bwd (torch.Tensor): Values at the previous timestep
            dt (int): Timestep
            clamp_mode (int): Clamping mode used for MacCormack advection.
            rk_order (int): Order of the Runge-Kutta method
            sample_pos (str): where the velocity should be sampled

        Raises:
            ValueError: clamp_mode can only be 1 or 2.

        Returns:
            torch.Tensor: The clamped Tensor
        """
        # prepare min and max
        # get the simulation shape
        _, _, H, W = rho_org.size()
        max_pool = F.max_pool2d
        pad_pos = (0, 1, 0, 1)
        # create mesh grid
        mgrid = create_2d_meshgrid_tensor(
            rho_org.size(), device=rho_org.device, dtype=rho_org.dtype
        )

        rho_org_pad_pos = F.pad(rho_org, pad=pad_pos)

        rho_org_max = max_pool(rho_org_pad_pos, kernel_size=2, stride=1)
        rho_org_min = -max_pool(-rho_org_pad_pos, kernel_size=2, stride=1)

        forward_trace = SemiLagrange2dAdvection._runge_kutta(
            velocity, mgrid, dt, rk_order, sample_pos
        )

        back_trace = SemiLagrange2dAdvection._runge_kutta(
            velocity, mgrid, -dt, rk_order, sample_pos
        )

        forward_trace_x = torch.clamp(forward_trace[:, 0, ...], 0, W - 1).long()
        forward_trace_y = torch.clamp(forward_trace[:, 1, ...], 0, H - 1).long()

        back_trace_x = torch.clamp(back_trace[:, 0, ...], 0, W - 1).long()
        back_trace_y = torch.clamp(back_trace[:, 1, ...], 0, H - 1).long()

        # index for all elements in batch
        rho_org_max_forward_trace = rho_org_max[
            :, :, forward_trace_y[0], forward_trace_x[0]
        ]
        rho_org_min_forward_trace = rho_org_min[
            :, :, forward_trace_y[0], forward_trace_x[0]
        ]
        if clamp_mode == 1:
            rho_org_max_back_trace = rho_org_max[:, :, back_trace_y[0], back_trace_x[0]]
            rho_org_min_back_trace = rho_org_min[:, :, back_trace_y[0], back_trace_x[0]]

            joined_tensors_min = torch.min(
                rho_org_min_back_trace, rho_org_min_forward_trace
            )
            joined_tensors_max = torch.max(
                rho_org_max_back_trace, rho_org_max_forward_trace
            )

            rho_adv = torch.where(
                rho_corr < joined_tensors_min,
                joined_tensors_min,
                rho_corr,
            )
            rho_adv = torch.where(
                rho_adv > joined_tensors_max,
                joined_tensors_max,
                rho_adv,
            )

        elif clamp_mode == 2:
            # clamp according to min and max
            clamp_min = rho_corr < rho_org_min_forward_trace
            clamp_max = rho_corr > rho_org_max_forward_trace
            rho_adv = torch.where(clamp_min + clamp_max, rho_fwd, rho_corr)
        else:
            raise ValueError("clamp_mode can only be 1 or 2.")

        return rho_adv
