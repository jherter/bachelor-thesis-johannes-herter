from abc import ABC, abstractmethod


class AbstractExternalForces(ABC):
    type_flags = ["x", "X", "y", "Y", "z", "Z"]

    @property
    @abstractmethod
    def rank(self) -> int:
        ...

    @abstractmethod
    def init_flags(self):
        ...

    @abstractmethod
    def init_domain(self):
        ...

    @abstractmethod
    def set_inflow_boundary_conditions(self):
        ...

    @abstractmethod
    def set_wall_boundary_conditions(self):
        ...

    @abstractmethod
    def add_buoyancy(self):
        ...

    @abstractmethod
    def add_external_forces(self):
        ...

    @abstractmethod
    def add_vorticity_confinement(self):
        ...