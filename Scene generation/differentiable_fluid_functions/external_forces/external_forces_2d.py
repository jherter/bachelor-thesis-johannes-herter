from .abstract_external_forces import AbstractExternalForces
from typing import List, Union
from differentiable_fluid_functions.grid import (
    CellType,
    Level2dGrid,
    Flag2dGrid,
    Real2dGrid,
    Abstract2dGrid,
    Staggered2dGrid,
    Node2dGrid,
)
from differentiable_fluid_functions.shapes import Box2dShape
import numpy as np
import torch
import torch.nn.functional as F


class ExternalForces2d(AbstractExternalForces):
    rank = 2

    def __init__(
        self,
        simulation_size: List[int],
        device: torch.device = torch.device("cpu"),
        dtype=torch.float32,
        *args,
        **kwargs,
    ):
        self._simulation_size = simulation_size
        self._device = device
        self._dtype = dtype
        super(ExternalForces2d, self).__init__(*args, **kwargs)

    def init_flags(
        self,
        phi: bool = False,
        boundary_width: List[int] = [0],
        wall: List[str] = ["xXyY"],
        open: List[str] = ["    "],
        inflow: List[str] = ["    "],
        outflow: List[str] = ["    "],
        nonboundary_cell_type: List[CellType] = [CellType.FLUID],
    ) -> List[Abstract2dGrid]:
        """Initialize the flags depending on the domain settings for a batch

        Args:
            phi (bool, optional): Flag if the level grid should be computed as well. Defaults to False.
            boundary_width (int, optional): Width of the boundary. Defaults to 0.
            wall (str, optional): Which side is a wall. Defaults to "xXyY".
            open (str, optional): Which side is open. Defaults to "    ".b
            inflow (str, optional): Which side has inflow. Defaults to "    ".
            outflow (str, optional): Which side has outflow. Defaults to "    ".
            nonboundary_cell_type (CellType, optional): Type of non boundary cells. Defaults to CellType.FLUID.

            phi (bool, optional): Flag if the level grid should be computed as well. Defaults to False.
            boundary_width (List[int], optional): List of Widths of the boundary. Defaults to [0].
            wall (List[str], optional): List encoding which side is a wall. Defaults to ["xXyY"].
            open (List[str], optional): List encoding Which side is a open. Defaults to ["    "].
            inflow (List[str], optional): List encoding Which side is a inflow. Defaults to ["    "].
            outflow (List[str], optional): List encoding Which side is a outflow. Defaults to ["    "].
            nonboundary_cell_type (List[CellType], optional): list encoding which type the non fluid cells have. Defaults to [CellType.FLUID].

        Raises:
            ValueError: If not all parameters have the same amount of samples

        Returns:
            List[Abstract2dGrid]: [flag_grid with all flags set, level grid with the computed sdf
        """
        batch_size, channel, width, height = self._simulation_size
        flags_data = torch.zeros(
            self._simulation_size, device=self._device, dtype=torch.uint8
        )
        phi_data = (
            torch.zeros(
                [batch_size, channel, width + 1, height + 1],
                device=self._device,
                dtype=self._dtype,
            )
            if phi
            else None
        )
        # check that everything has the same dim
        all_same_dim = (
            len(boundary_width) == batch_size
            and len(wall) == batch_size
            and len(open) == batch_size
            and len(inflow) == batch_size
            and len(outflow) == batch_size
            and len(nonboundary_cell_type) == batch_size
        )
        if not all_same_dim:
            raise ValueError(
                f"All parameters are required to have the same amount of samples {batch_size}"
            )
        for sample in range(batch_size):
            cur_flags_data, cur_phi_data = self._init_flags_single(
                phi_data=phi_data,
                boundary_width=boundary_width[sample],
                wall=wall[sample],
                open=open[sample],
                inflow=inflow[sample],
                outflow=outflow[sample],
                nonboundary_cell_type=nonboundary_cell_type[sample],
            )
            flags_data[sample] = cur_flags_data[0]
            if phi:
                phi_data[sample] = cur_phi_data[0]
        return [
            Flag2dGrid(flags_data),
            Level2dGrid(phi_data) if phi_data is not None else None,
        ]

    def _init_flags_single(
        self,
        phi_data: torch.Tensor = None,
        boundary_width: int = 0,
        wall: str = "xXyY",
        open: str = "    ",
        inflow: str = "    ",
        outflow: str = "    ",
        nonboundary_cell_type: CellType = CellType.FLUID,
    ) -> List[torch.Tensor]:
        """Initialize the flags depending on the domain settings for a single element

        Args:
            phi (bool, optional): Flag if the level grid should be computed as well. Defaults to False.
            boundary_width (int, optional): Width of the boundary. Defaults to 0.
            wall (str, optional): Which side is a wall. Defaults to "xXyY".
            open (str, optional): Which side is open. Defaults to "    ".
            inflow (str, optional): Which side has inflow. Defaults to "    ".
            outflow (str, optional): Which side has outflow. Defaults to "    ".
            nonboundary_cell_type (CellType, optional): Type of non boundary cells. Defaults to CellType.FLUID.

        Raises:
            ValueError: sim dimensions are not the same as parameter dim
            ValueError: simulation size is wrong

        Returns:
            List[torch.Tensor]: [flag_data tensor and level_data tensor]
        """
        sim_dim = ExternalForces2d.rank * 2
        if (
            len(wall) < sim_dim
            or len(open) < sim_dim
            or len(inflow) < sim_dim
            or len(outflow) < sim_dim
        ):
            raise ValueError(
                "wall, open, inflow, outflow strings should have"
                "the same length as 2*rank."
            )
        if len(self._simulation_size) != 4:
            raise ValueError(
                "For 2d array the 4 dim data is required eg. batch x channel x Height x Width"
            )

        # the width of the fluid is
        nonfluid_width = boundary_width + 1
        # domain as twice as many sides as dimensions

        # types of the sides of the simulation
        types = [0] * sim_dim
        # has this side been handled
        done = [False] * sim_dim

        for i in range(sim_dim):
            for j in range(sim_dim):
                type_flag = ExternalForces2d.type_flags[j]
                if not done[i]:
                    if open[i] == type_flag:
                        types[i] = CellType.OPEN
                        done[i] = True
                    elif inflow[i] == type_flag:
                        types[i] = CellType.INFLOW
                        done[i] = True
                    elif outflow[i] == type_flag:
                        types[i] = CellType.OUTFLOW
                        done[i] = True
                    elif wall[i] == type_flag:
                        types[i] = CellType.OBSTACLE
                        done[i] = True

        # init flags
        flags_data = torch.zeros(
            self._simulation_size, device=self._device, dtype=torch.uint8
        )
        flags_data[..., 0:nonfluid_width].fill_(int(types[0]))
        flags_data[..., -nonfluid_width:].fill_(int(types[1]))
        flags_data[..., 0:nonfluid_width, :].fill_(int(types[2]))
        flags_data[..., -nonfluid_width:, :].fill_(int(types[3]))
        # fill out the rest of the domain
        flags_data[
            ..., nonfluid_width:-nonfluid_width, nonfluid_width:-nonfluid_width
        ].fill_(int(nonboundary_cell_type))

        if phi_data is not None:
            # Assume the scene distance is scaled into [-1, 1]
            phi_walls = []
            box = Box2dShape(
                simulation_size=self._simulation_size,
                dtype=self._dtype,
                device=self._device,
            )
            _, _, height, width = self._simulation_size

            nonfluid_width_normed_x = (boundary_width + 1) / width * 2
            nonfluid_width_normed_y = (boundary_width + 1) / height * 2
            if types[0] == CellType.OBSTACLE:
                levelSet = box.compute_level_set(
                    torch.tensor([[-1.0, 0.0]], device=self._device),
                    torch.tensor(
                        [[nonfluid_width_normed_x, 10.0]], device=self._device
                    ),
                )

                phi_walls.append(levelSet)
            if types[1] == CellType.OBSTACLE:
                levelSet = box.compute_level_set(
                    torch.tensor([[1.0, 0.0]], device=self._device),
                    torch.tensor(
                        [[nonfluid_width_normed_x, 10.0]], device=self._device
                    ),
                )

                phi_walls.append(levelSet)
            if types[2] == CellType.OBSTACLE:
                levelSet = box.compute_level_set(
                    torch.tensor([[0.0, -1.0]], device=self._device),
                    torch.tensor(
                        [[10.0, nonfluid_width_normed_y]], device=self._device
                    ),
                )

                phi_walls.append(levelSet)

            if types[3] == CellType.OBSTACLE:
                levelSet = box.compute_level_set(
                    torch.tensor([[0.0, 1.0]], device=self._device),
                    torch.tensor(
                        [[10.0, nonfluid_width_normed_y]], device=self._device
                    ),
                )

                phi_walls.append(levelSet)
            if len(phi_walls) > 0:
                phi = phi_walls[0]
                for phi_wall in phi_walls:
                    phi.union(phi_wall, alpha=1.0)
                # hot fix: set the values close to 0 to be 0

                phi_data = phi.get_data()

            phi_data = torch.where(
                phi_data < 1e-6,
                torch.zeros(phi_data.size(), device=self._device, dtype=phi_data.dtype),
                phi_data,
            )
        return flags_data, phi_data

    def init_domain(self, boundary_width: List[int] = [0]) -> Level2dGrid:
        """Initialize the domain (Level2dGrid) with a fixed boundary size

        Args:
            boundary_width (List[int}, optional): List with sizes of the boundary. Defaults to [0].

        Returns:
            Level2dGrid: Resulting level set
        """
        # WARNING: only consider the boundaries to be solids
        box = Box2dShape(simulation_size=self._simulation_size, dtype=self._dtype)
        batch_size = self._simulation_size[0]
        dx = 1.0 / max(self._simulation_size)

        nonfluid_width_normed = 1.0 - (np.array(boundary_width) + 1) * dx * 2

        box_size = torch.tensor(
            [nonfluid_width_normed, nonfluid_width_normed],
            device=self._device,
            dtype=self._dtype,
        ).view(batch_size, 2)

        box_center = torch.tensor(
            [[0.0, 0.0]], device=self._device, dtype=self._dtype
        ).repeat(batch_size, 1)

        phi = box.compute_level_set(box_center, box_size)
        # do invert
        phi.invert()

        return phi

    def set_inflow_boundary_conditions(
        self,
        flags: Flag2dGrid,
        velocity: Staggered2dGrid,
        velocity_inflow_value: torch.Tensor,
    ) -> Staggered2dGrid:
        """Set inflow velocity value at cells labelled as inflow.

        Args:
            flags (Flag2dGrid): Grid defining the type of each cell
            velocity (Staggered2dGrid): Velocity of the simulation
            velocity_inflow_value (torch.Tensor): Value to set at cells labeled as inflow

        Returns:
            Staggered2dGrid: Velocity with inflow values updated
        """
        velocity_inflow = Staggered2dGrid.get_constant_grid(
            value_x=velocity_inflow_value,
            value_y=velocity_inflow_value,
            simulation_size=self._simulation_size,
        )

        velocity_inflow_x, velocity_inflow_y = velocity_inflow.get_tensor()
        velocity_x, velocity_y = velocity.get_tensor()

        # x compo
        pad_x_neg = (1, 0, 0, 0)
        pad_x_pos = (0, 1, 0, 0)
        flags_pad_x_neg = F.pad(flags.get_data(), pad=pad_x_neg)
        flags_pad_x_pos = F.pad(flags.get_data(), pad=pad_x_pos)

        vel_inflow_bcs_x = torch.where(
            (
                (flags_pad_x_neg == int(CellType.INFLOW))
                | (flags_pad_x_pos == int(CellType.INFLOW))
            ),
            velocity_inflow_x,
            velocity_x,
        )

        # y compo
        pad_y_neg = (0, 0, 1, 0)
        pad_y_pos = (0, 0, 0, 1)
        flags_pad_y_neg = F.pad(flags.get_data(), pad=pad_y_neg)
        flags_pad_y_pos = F.pad(flags.get_data(), pad=pad_y_pos)

        vel_inflow_bcs_y = torch.where(
            (
                (flags_pad_y_neg == int(CellType.INFLOW))
                | (flags_pad_y_pos == int(CellType.INFLOW))
            ),
            velocity_inflow_y,
            velocity_y,
        )
        return Staggered2dGrid.from_tensor(vel_inflow_bcs_x, vel_inflow_bcs_y)

    def set_wall_boundary_conditions(
        self, phi_obs: Level2dGrid, grid: Union[Real2dGrid, Staggered2dGrid, Node2dGrid]
    ) -> Union[Real2dGrid, Staggered2dGrid, Node2dGrid]:
        """Set the boundary condition for walls to zero at a given grid

        Args:
            phi_obs (Level2dGrid): LGrid describing the domain
            grid (Union[Real2dGrid, Staggered2dGrid, Node2dGrid]): Grid where the
            boundary conditions should be applied

        Raises:
            ValueError: Wrong format for grid was given

        Returns:
            Union[Real2dGrid, Staggered2dGrid, Node2dGrid]: [description]
        """
        result = None
        if isinstance(grid, Real2dGrid):
            grid_data = grid.get_data()
            is_obs = phi_obs.get_obstacle().get_data()
            result = torch.where(
                is_obs == int(CellType.OBSTACLE), torch.zeros_like(grid_data), grid_data
            )
            result = Real2dGrid(result)
        elif isinstance(grid, Staggered2dGrid):
            grid_x, grid_y = grid.get_tensor()

            fractions = phi_obs.get_fractions()
            fractions_x, fractions_y = fractions.get_tensor()

            result_x = torch.where(fractions_x < 1e-6, torch.zeros_like(grid_x), grid_x)
            result_y = torch.where(fractions_y < 1e-6, torch.zeros_like(grid_y), grid_y)
            return Staggered2dGrid.from_tensor(data_x=result_x, data_y=result_y)
        elif isinstance(grid, Node2dGrid):
            # has same dim the phi
            grid_data = grid.get_data()
            is_obs = phi_obs.get_data() < 1e-6
            result = torch.where(is_obs, torch.zeros_like(grid_data), grid_data)
            result = Node2dGrid(result)
        else:
            raise TypeError(f"Invalid grid type: {type(grid)}")

        return result

    def add_buoyancy(
        self,
        phi_obs: Level2dGrid,
        velocity: Staggered2dGrid,
        density: Real2dGrid,
        gravity: List[float] = [0, -4e-3],
        dt: float = 1.0,
    ) -> Staggered2dGrid:
        """Add buoyancy to the velocity with a given gravity

        Args:
            phi_obs (Level2dGrid): Grid describing the domain
            velocity (Staggered2dGrid): Velocity of the current simulation
            density (Real2dGrid): Density of the simulation
            gravity (List[float], optional): Gravity vector to use [x, y]. Defaults to [0, -4e-3].
            dt (float, optional): Timestep of the simulation. Defaults to 1.0.

        Raises:
            ValueError: Gravity needs to be 2 dimensional

        Returns:
            Staggered2dGrid: Result after the Buoyancy was applied
        """
        if len(gravity) != 2:
            raise ValueError("Gravity needs to be 2 dimensional")
        gravity = torch.tensor(gravity, device=self._device, dtype=self._dtype)
        dx = 1.0 / max(self._simulation_size)

        density_staggered = Staggered2dGrid.from_real_2d_grid_single_channel(density)
        scaled_gravity = -gravity * dt / dx

        velocity_buo = velocity + (density_staggered * scaled_gravity)

        velocity_buo = self.set_wall_boundary_conditions(phi_obs, velocity_buo)

        return velocity_buo

    def add_external_forces(
        self,
        phi_obs: Level2dGrid,
        velocity: Staggered2dGrid,
        force: Staggered2dGrid,
        dt: float = 1.0,
    ) -> Staggered2dGrid:
        """Add an external force to the velocity

        Args:
            phi_obs (Level2dGrid): Grid describing the domain
            velocity (Staggered2dGrid): Velocity where the force should be applied
            force (Staggered2dGrid): The force that should applied
            dt (float, optional): Timestep of the simulation. Defaults to 1.0.

        Raises:
            ValueError: Force field and velocity field need to have same dimensions

        Returns:
            Staggered2dGrid: Result after the force was applied
        """
        if force.size() != velocity.size():
            raise ValueError(
                "Force field and velocity field need to have same dimensions"
                f"{force.size()} != {velocity.size()}"
            )
        dx = 1.0 / max(self._simulation_size)

        velocity = velocity + (force * (dt / dx))
        velocity = self.set_wall_boundary_conditions(phi_obs, velocity)
        return velocity

    def add_vorticity_confinement(
        self,
        phi_obs: Level2dGrid,
        velocity: Staggered2dGrid,
        strength: float = 0.0,
        dt: float = 1.0,
    ):
        """Add vorticity confinement force to the velocity

        Args:
            phi_obs (Level2dGrid): Grid describing the domain
            velocity (Staggered2dGrid): Velocity where the force should be applied
            dt (float, optional): Timestep of the simulation. Defaults to 1.0.
        Returns:
            Staggered2dGrid: Result after the force was applied

        """
        vorticity = velocity.get_curl()
        vorticity_center = vorticity.get_centered()
        vorticity_magnitude = torch.norm(vorticity_center, dim=1, keepdim=True)
        vorticity_grad = (
            Real2dGrid(vorticity_magnitude).get_spatial_gradient().get_centered()
        )
        vorticity_grad_normed = vorticity_grad / (
            torch.norm(vorticity_grad, dim=1, keepdim=True) + 1e-20 / dt
        )
        # F = epsilon * dx * (N x w) (cross product). But dx is omitted here.
        confinement_force_x = (
            strength * vorticity_grad_normed[:, 1:2] * vorticity_magnitude
        )  # Ny * wz
        confinement_force_y = (
            strength * -vorticity_grad_normed[:, 0:1] * vorticity_magnitude
        )  # Nx * wz
        confinement_force = Staggered2dGrid(
            Real2dGrid(Real2dGrid(confinement_force_x).get_staggered_x()),
            Real2dGrid(Real2dGrid(confinement_force_y).get_staggered_y()),
        )
        velocity = velocity + confinement_force * dt
        velocity = self.set_wall_boundary_conditions(phi_obs, velocity)
        return velocity
