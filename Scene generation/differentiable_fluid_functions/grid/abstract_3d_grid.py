from .abstract_grid import AbstractGrid
import torch
import numpy as np
from differentiable_fluid_functions.utils import save_npy
from typing import TypeVar

T = TypeVar("T", bound="Abstract3dGrid")


class Abstract3dGrid(AbstractGrid):
    # class variables
    tensor_to_npy_transformation_sequence = (0, 2, 3, 4, 1)
    npy_to_tensor_transformation_sequence = (3, 0, 1, 2)
    npy_to_tensor_transformation_sequence_with_batch = (0, 4, 1, 2, 3)
    rank = 2

    def __init__(self, data, **kwargs):
        if data is not None and len(data.shape) != 5:
            raise ValueError(
                "For 3d array the 5 dim data is required eg. batch x channel x Depth x Height x Width"
            )
        super(Abstract3dGrid, self).__init__(data, **kwargs)

    @classmethod
    def from_array(
        cls,
        array: np.ndarray,
        dtype: torch.float32,
        device: torch.device = torch.device("cpu"),
    ) -> T:
        """Create a grid from an numpy array

        Args:
            array (np.ndarray): the array containing the data with dim (batch) x channel x Depth x Height x Width
            dtype ([type], optional): The underlaying data type for PyTorch
            device (torch.device, optional): On which device to store the grid. Defaults to torch.device("cpu").

        Raises:
            ValueError: The dimension of array is wrong

        Returns:
            (Abstract3dGrid): an instance of the grid
        """
        if not len(array.shape) in [4, 5]:
            raise ValueError(
                "For a 3d grid 4/5 dimensional data is required eg. (batch) x channel x Depth x Height x Width"
            )

        if len(array.shape) == 4:
            # add a batch dim to the array
            array = array[np.newaxis, :]
        tensor = Abstract3dGrid._array2tensor(array, dtype, device)
        return cls(data=tensor)

    @staticmethod
    def _array2tensor(
        array: np.ndarray,
        dtype=torch.float32,
        device: torch.device = torch.device("cpu"),
    ) -> torch.Tensor:
        """convert an array to a tensor

        Args:
            array (np.ndarray): the array which should be converted
            dtype ([type], optional): data type of the tensor. Defaults to torch.float32.
            device (torch.device, optional): device of the tensor. Defaults to torch.device("cpu").

        Returns:
            torch.Tensor: the tensor
        """
        tensor = torch.from_numpy(array).to(dtype).to(device)
        return tensor

    def grid2array(self) -> np.ndarray:
        """transform the grid to an array with dim batch x Depth x height x width x channel

        Returns:
            np.ndarray: data of the grid
        """
        tensor = self._data.permute(
            Abstract3dGrid.tensor_to_npy_transformation_sequence
        )
        array = tensor.detach().cpu().numpy()
        return array

    def save_npy(self, filename: str):
        """Save the grid as an npy file

        Args:
            filename (str): Filename of the npy file
        """
        array = self.grid2array()
        save_npy(array, filename)

    def trim_boundary(self, bwidth: int = 1):
        """Trim the boundaries from the grid

        Args:
            bwidth (int, optional): how much should be trimmed. Defaults to 1.
        """
        self._data = self._data[:, :, bwidth:-bwidth, bwidth:-bwidth, bwidth:-bwidth]
