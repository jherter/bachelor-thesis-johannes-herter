from .abstract_3d_grid import Abstract3dGrid
import torch

import numpy as np
from typing import List, TypeVar
from differentiable_fluid_functions.utils import (
    load_npy,
    load_npz,
    check_torch_float,
    check_numpy_float,
)
from differentiable_fluid_functions.grid import (
    Staggered3dGrid,
    Flag3dGrid,
    CellType,
)

T = TypeVar("T", bound="Level3dGrid")


class Level3dGrid(Abstract3dGrid):
    def __init__(self, data: torch.Tensor, *args, **kwargs):
        if not check_torch_float(data.dtype):
            raise TypeError(
                f"Data must me of type torch.float<16, 32, 64> currently: {data.dtype}"
            )
        super(Level3dGrid, self).__init__(data, *args, **kwargs)

    @classmethod
    def from_npy(cls, filename: str, **kwargs) -> T:
        """Create a Level3dGrid from a npy file

        Args:
            filename (str): path to the file

        Raises:
            TypeError: if the content of the file is not float32 or float64

        Returns:
            T: An instance of Level3dGrid
        """
        array = load_npy(filename)
        if not check_numpy_float(array.dtype):
            raise TypeError(
                f"{filename} has data with the wrong format: {array.dtype} required np.float<32, 64>"
            )
        dtype = torch.float32 if array.dtype == np.float32 else torch.float64
        return cls.from_array(array, dtype=dtype, **kwargs)

    @classmethod
    def from_npz(cls, filename: str, key: str, **kwargs) -> T:
        """Create a Level3dGrid from a npz file

        Args:
            filename (str): path to the file

        Raises:
            TypeError: if the content of the file is not float32 or float64

        Returns:
            T: An instance of Level3dGrid
        """
        array = load_npz(filename, key)
        if not check_numpy_float(array.dtype):
            raise TypeError(
                f"{filename} has data with the wrong format: {array.dtype} required np.float<32, 64>"
            )
        dtype = torch.float32 if array.dtype == np.float32 else torch.float64
        return cls.from_array(array, dtype=dtype, **kwargs)

    @classmethod
    def get_constant_grid(
        cls,
        value: torch.Tensor,
        simulation_size: List[int],
        **kwargs,
    ) -> T:
        """Creates a constant level 3d grid

        Args:
            value (torch.Tensor): Which value the grid should have
            simulation_size (List[int]): The dimensions of the grid

        Raises:
            ValueError: If the dimension are not correct
            TypeError: if the type of the value is not correct

        Returns:
            T: An instance of Level3dGrid
        """
        if len(simulation_size) != 5:
            raise ValueError(
                "For a 3d array the 5 dim data is required eg. batch x channel x Depth x Height x Width"
            )
        if not check_torch_float(value.dtype) or len(value.shape) != 0:
            raise TypeError(
                f"Value must me of type one of [torch.float16, torch.float32, torch.float64] current {value.dtype}\n"
                f"Values dim must be [] currently {value.shape}"
            )
        dim = np.array(simulation_size)
        dim[-3:] += 1
        tensor = (
            torch.ones(dim.tolist(), dtype=value.dtype, device=value.device) * value
        )
        return cls(tensor)

    def _get_line_fractions(self, direction: str = "z") -> List[torch.Tensor]:
        """return the fractions of the boundary at the grid lines
            if the boundary does not pass through the cell
            return 0 (inside) or 1 outside

        Args:
            direction (str, optional): From which direction the fractions should be computed. Defaults to "z".

        Returns:
            List[torch.Tensor]: horizontal and vertical fractions
        """

        # data is sim size + 1
        phi = self.get_data()
        if direction == "x":
            left = phi[..., :-1, :]
            right = phi[..., 1:, :]
            bottom = phi[..., :-1, :, :]
            up = phi[..., 1:, :, :]
        elif direction == "y":
            left = phi[..., :-1]
            right = phi[..., 1:]
            bottom = phi[..., :-1, :, :]
            up = phi[..., 1:, :, :]
        elif direction == "z":
            left = phi[..., :-1]
            right = phi[..., 1:]
            bottom = phi[..., :-1, :]
            up = phi[..., 1:, :]

        # TODO: need to review differentiabilities (@Jingwei)
        max_ub = torch.max(up, bottom)
        min_ub = torch.min(up, bottom)
        fraction_horizontal = max_ub / (max_ub - min_ub + 1e-7)
        fraction_horizontal = torch.clamp(fraction_horizontal, 0.0, 1.0)

        max_lr = torch.max(left, right)
        min_lr = torch.min(left, right)
        fraction_vertical = max_lr / (max_lr - min_lr + 1e-7)
        fraction_vertical = torch.clamp(fraction_vertical, 0.0, 1.0)

        return [fraction_horizontal, fraction_vertical]

    def get_fractions(self) -> Staggered3dGrid:
        """
            return the fractions of the boundary at the grid face
            if the boundary does not pass through the cell
            return 0 (inside) or 1 outside
        Returns:
            Staggered3dGrid: The fractional values on the grid faces
        """

        line_fractions_x = self._get_line_fractions(direction="x")
        line_fractions_y = self._get_line_fractions(direction="y")
        line_fractions_z = self._get_line_fractions(direction="z")

        face_fraction_x = self._get_face_fractions(line_fractions_x, direction="x")
        face_fraction_y = self._get_face_fractions(line_fractions_y, direction="y")
        face_fraction_z = self._get_face_fractions(line_fractions_z, direction="z")

        face_fraction = Staggered3dGrid.from_tensor(
            data_x=face_fraction_x, data_y=face_fraction_y, data_z=face_fraction_z
        )
        return face_fraction

    def _get_face_fractions(
        self, line_fractions: List[torch.Tensor], direction: str
    ) -> torch.Tensor:
        """Computes face fractions given line fractions for a plane.

        Args:
            line_fractions(List[torch.Tensor]): horizontal and vertical compoents of line fractions
            direction (str): indicates the surface normal of the line_fractions
        Returns:
            (torch.Tensor): facefractions of a given directions
        """
        fractions_data_x_tensor, fractions_data_y_tensor = line_fractions
        phi = self.get_data()

        if direction == "x":
            left = fractions_data_x_tensor[..., :, :-1, :]
            right = fractions_data_x_tensor[..., :, 1:, :]
            bottom = fractions_data_y_tensor[..., :-1, :, :]
            up = fractions_data_y_tensor[..., 1:, :, :]
            phi_00 = phi[..., :-1, :-1, :]
            phi_01 = phi[..., 1:, :-1, :]
            phi_11 = phi[..., 1:, 1:, :]

        elif direction == "y":
            left = fractions_data_x_tensor[..., :, :, :-1]
            right = fractions_data_x_tensor[..., :, :, 1:]
            bottom = fractions_data_y_tensor[..., :-1, :, :]
            up = fractions_data_y_tensor[..., 1:, :, :]
            phi_00 = phi[..., :-1, :, :-1]
            phi_01 = phi[..., 1:, :, :-1]
            phi_11 = phi[..., 1:, :, 1:]

        elif direction == "z":
            left = fractions_data_x_tensor[..., :, :, :-1]
            right = fractions_data_x_tensor[..., :, :, 1:]
            bottom = fractions_data_y_tensor[..., :, :-1, :]
            up = fractions_data_y_tensor[..., :, 1:, :]
            phi_00 = phi[..., :, :-1, :-1]
            phi_01 = phi[..., :, 1:, :-1]
            phi_11 = phi[..., :, 1:, 1:]

        face_fraction = torch.where(
            phi_01 < 1e-7,
            0.5 * ((left + right) * bottom + (right - left) * up),
            torch.zeros_like(phi_01),
        )
        face_fraction = torch.where(
            (phi_01 >= 1e-7) * (phi_11 < 1e-7),
            0.5 * ((left + right) * bottom + (left - right) * up),
            face_fraction,
        )
        face_fraction = torch.where(
            (phi_01 >= 1e-7) * (phi_11 >= 1e-7) * (phi_00 < 1e-7),
            0.5 * ((left + right) * up + (right - left) * bottom),
            face_fraction,
        )
        face_fraction = torch.where(
            (phi_01 >= 1e-7) * (phi_11 >= 1e-7) * (phi_00 >= 1e-7),
            0.5 * ((left + right) * up + (left - right) * bottom),
            face_fraction,
        )
        return face_fraction

    def get_obstacle(self) -> Flag3dGrid:
        """Convert a level grid to an flaggrid indicating where an obstacle is.
            A cell of a flaggrid is considered an obstacle if all 4 corners of the
            fractions are basically zero.
            true if obstacle false, is fluid

        Returns:
            Flag3dGrid: Resulting flaggrid indicating which cell is an obstacle
        """
        fractions = self.get_fractions()
        (
            fractions_data_x_tensor,
            fractions_data_y_tensor,
            fractions_data_z_tensor,
        ) = fractions.get_tensor()

        is_obstacle = (
            (fractions_data_x_tensor[..., :-1] < 1e-6)
            * (fractions_data_x_tensor[..., 1:] < 1e-6)
            * (fractions_data_y_tensor[..., :-1, :] < 1e-6)
            * (fractions_data_y_tensor[..., 1:, :] < 1e-6)
            * (fractions_data_z_tensor[..., :-1, :, :] < 1e-6)
            * (fractions_data_z_tensor[..., 1:, :, :] < 1e-6)
        ).to(torch.uint8)
        is_obstacle[is_obstacle > 0] = int(CellType.OBSTACLE)

        return Flag3dGrid(is_obstacle)

    def get_fluid(self) -> Flag3dGrid:
        """Return a flaggrid indicating which cells are fluid cells

        Returns:
            Flag3dGrid: True if a cell is a fluid cell, false otherwise, eg. obstacle.
        """
        fluid = self.get_obstacle()
        fluid.invert()
        return fluid

    def get_flags(self) -> Flag3dGrid:
        """Return a flaggrid indicating which cells are fluid cells
                and which cells are obstacles

        Returns:
            Flag3dGrid: indicating fluid and obstacles.
        """
        is_obstacle = self.get_obstacle().get_data()
        flags_data = torch.where(
            is_obstacle == int(CellType.OBSTACLE),
            int(CellType.OBSTACLE) * torch.ones_like(is_obstacle),
            int(CellType.FLUID) * torch.ones_like(is_obstacle),
        )
        return Flag3dGrid(flags_data)

    def union(self, lvl3dGrid: T, alpha: float = 0.0):
        """Performs union operator: self {cup} lvl3dGrid,

        Args:
            lvl3dGrid (Level3dGrid): The grid to perform the union with
            alpha (float): parameter to control blending,
                       should be in the range [0,1], alpha=1.0 is equivalent
                       to boolean blending, i.e. torch.min(f, g)
        """
        f = self.get_data()
        g = lvl3dGrid.get_data()
        if alpha < 0.0 or alpha > 1.0:
            raise ValueError("alpha should be in the range [0,1].")
        if alpha == 1.0:
            result = torch.min(f, g)
        else:
            result = (
                1.0
                / (1.0 + alpha)
                * (f + g - torch.sqrt(f * f + g * g - 2 * alpha * f * g))
            )
        self.set_data(result)

    def intersection(self, lvl3dGrid: T, alpha: float = 0.0):
        """Performs intersection operator: self {cap} lvl3dGrid,

        Args:
            lvl3dGrid (Level3dGrid): The grid to perform the union with
            alpha (float, optional): parameter to control blending,
                       should be in the range [0,1], alpha=1.0 is equivalent
                       to boolean blending, i.e. torch.max(f, g). Defaults to 0.0.
        """
        f = self.get_data()
        g = lvl3dGrid.get_data()
        if alpha < 0.0 or alpha > 1.0:
            raise ValueError("alpha should be in the range [0,1].")
        if alpha == 1.0:
            result = torch.max(f, g)
        else:
            result = (
                1.0
                / (1.0 + alpha)
                * (f + g + torch.sqrt(f * f + g * g - 2 * alpha * f * g))
            )
        self.set_data(result)

    def subtraction(self, lvl3dGrid: T, alpha: float = 0.0):
        """Performs subtraction operator: self - lvl3dGrid,

        Args:
            lvl3dGrid (T): Grid which should be subtracted
            alpha (float, optional):  parameter to control blending,
                       should be in the range [0,1]. Defaults to 0.0.

        Raises:
            ValueError: [description]

        Returns:
            [type]: [description]
        """
        if alpha < 0.0 or alpha > 1.0:
            raise ValueError("alpha should be in the range [0,1].")
        data = lvl3dGrid.get_data()
        lvl3dGrid.set_data(-data)
        self.intersection(lvl3dGrid, alpha)

    def invert(self):
        """Inverts the level set,"""
        data = self.get_data()
        self.set_data(-data)
