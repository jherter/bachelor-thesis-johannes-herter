from .abstract_2d_grid import Abstract2dGrid
import torch
import numpy as np
from typing import List, TypeVar
from differentiable_fluid_functions.utils import load_npz, load_npy

T = TypeVar("T", bound="Flag2dGrid")


class Flag2dGrid(Abstract2dGrid):
    def __init__(self, data, **kwargs):
        if data.dtype != torch.uint8:
            raise TypeError(f"Data must me of type torch.uint8 currently: {data.dtype}")
        super(Flag2dGrid, self).__init__(data, **kwargs)

    @classmethod
    def get_constant_grid(cls, value, simulation_size: List[int]) -> T:
        """Creates a constant flag 2d grid

        Args:
            value (torch.Tensor): Which value the grid should have
            simulation_size (List[int]): The dimensions of the grid

        Raises:
            ValueError: If the dimension are not correct
            TypeError: if the type of the value is not correct

        Returns:
            T: An instance of Flag2dGrid
        """
        if len(simulation_size) != 4:
            raise ValueError(
                "For 2d array the 4 dim data is required eg. batch x channel x Height x Width"
            )

        if value.dtype != torch.uint8 or len(value.shape) != 0:
            raise TypeError(
                f"Value must be single value of type torch.uint8, currently: {value.dtype}\n"
                f"As well as have a shape length of zero. currently: {len(value.shape)}"
            )

        tensor = (
            torch.ones(simulation_size, dtype=torch.uint8, device=value.device) * value
        )
        return cls(tensor)

    @classmethod
    def from_npy(cls, filename: str, device: torch.device = torch.device("cpu")) -> T:
        """Create a Real 2d grid from a npy file

        Args:
            filename (str): path to the file

        Raises:
            TypeError: if the content of the file is not uint8

        Returns:
            T: An instance of Flag2dGrid
        """
        array = load_npy(filename)
        if array.dtype != np.uint8:
            raise TypeError(
                f"{filename} has data with the wrong format: {array.dtype} required np.uint8"
            )
        return cls.from_array(array, device=device)

    @classmethod
    def from_npz(
        cls, filename: str, key: str, device: torch.device = torch.device("cpu")
    ) -> T:
        """Create a flag 2d grid from a npz file

        Args:
            filename (str): path to the file

        Raises:
            TypeError: if the content of the file is not uint8

        Returns:
            T: An instance of Flag2dGrid
        """
        array = load_npz(filename, key)
        if array.dtype != np.uint8:
            raise TypeError(
                f"{filename} has data with the wrong format: {array.dtype} required np.uint8"
            )
        return cls.from_array(array, device=device)

    # overwrite and dont allow use of dtype input
    @classmethod
    def from_array(
        cls,
        array: np.ndarray,
        device: torch.device = torch.device("cpu"),
    ) -> T:
        """Create a grid from an numpy array

        Args:
            array (np.ndarray): the array containing the data with dim (batch) x channel x Height x Width
            device (torch.device, optional): On which device to store the grid. Defaults to torch.device("cpu").

        Raises:
            ValueError: The dimension of array is wrong

        Returns:
            (Abstract2dGrid): an instance of the grid
        """
        if not len(array.shape) in [3, 4]:
            raise ValueError(
                "For a 2d grid 3/4 dimensional data is required eg. (batch) x channel x Height x Width"
            )

        if len(array.shape) == 3:
            # add a batch dim to the array
            array = array[np.newaxis, :]
        tensor = Abstract2dGrid._array2tensor(array, torch.uint8, device)
        return cls(data=tensor)

    def invert(self):
        """Invert the flags of each cell in the grid"""
        self._data = ~self._data
