from .abstract_3d_grid import Abstract3dGrid
import torch
import torch.nn.functional as F

import numpy as np
from typing import List, TypeVar, TYPE_CHECKING
from differentiable_fluid_functions.types import GridType
from differentiable_fluid_functions.utils import (
    load_npy,
    load_npz,
    check_torch_float,
    check_numpy_float,
    particle_to_grid,
    create_3d_gaussian_kernel,
)
from differentiable_fluid_functions.grid import Flag3dGrid, CellType

if TYPE_CHECKING:
    from differentiable_fluid_functions.particles import (  # pragma: no cover
        ParticleDatabase,
    )
    from differentiable_fluid_functions.kernels import (  # pragma: no cover
        AbstractKernel,
    )
    from differentiable_fluid_functions.grid import Staggered3dGrid

T = TypeVar("T", bound="Real3dGrid")


class Real3dGrid(Abstract3dGrid):
    def __init__(self, data):
        if not check_torch_float(data.dtype):
            raise TypeError(
                f"Data must me of type torch.float<16, 32, 64> currently: {data.dtype}"
            )
        super(Real3dGrid, self).__init__(data)

    @classmethod
    def from_npy(cls, filename: str, device: torch.device = torch.device("cpu")) -> T:
        """Create a Real 3d grid from a npy file

        Args:
            filename (str): path to the file

        Raises:
            TypeError: if the content of the file is not float32 or float64

        Returns:
            T: An instance of Real3dGrid
        """
        array = load_npy(filename)
        if not check_numpy_float(array.dtype):
            raise TypeError(
                f"{filename} has data with the wrong format: {array.dtype} required np.float<32, 64>"
            )
        dtype = torch.float32 if array.dtype == np.float32 else torch.float64
        return cls.from_array(array, dtype=dtype, device=device)

    @classmethod
    def from_npz(
        cls, filename: str, key: str, device: torch.device = torch.device("cpu")
    ) -> T:
        """Create a Real 3d grid from a npz file

        Args:
            filename (str): path to the file

        Raises:
            TypeError: if the content of the file is not float32 or float64

        Returns:
            T: An instance of Real3dGrid
        """
        array = load_npz(filename, key)
        if not check_numpy_float(array.dtype):
            raise TypeError(
                f"{filename} has data with the wrong format: {array.dtype} required np.float<32, 64>"
            )
        dtype = torch.float32 if array.dtype == np.float32 else torch.float64
        return cls.from_array(array, dtype=dtype, device=device)

    @classmethod
    def get_constant_grid(cls, value: torch.Tensor, simulation_size: List[int]) -> T:
        """Creates a constant real 3d grid

        Args:
            value (torch.Tensor): Which value the grid should have
            simulation_size (List[int]): The dimensions of the grid

        Raises:
            ValueError: If the dimension are not correct
            TypeError: if the type of the value is not correct

        Returns:
            T: An instance of Real3dGrid
        """
        if len(simulation_size) != 5:
            raise ValueError(
                "For 3d array the 5 dim data is required eg. batch x channel x Depth x Height x Width"
            )
        if not check_torch_float(value.dtype) or len(value.shape) != 0:
            raise TypeError(
                f"Value must me of type one of [torch.float16, torch.float32, torch.float64] current {value.dtype}\n"
                f"Values dim must be [] currently {value.shape}"
            )
        tensor = (
            torch.ones(simulation_size, dtype=value.dtype, device=value.device) * value
        )
        return cls(tensor)

    @classmethod
    def from_flag_3d_grid(
        cls,
        flag3dGrid: Flag3dGrid,
        value: torch.Tensor,
        flag: CellType = CellType.OBSTACLE,
    ) -> T:
        """Create a Real 3d grid from a Flag3dGrid with a given value

        Args:
            flag3dGrid (Flag3dGrid): Grid indicating where the value should be placed
            value (torch.Tensor): Value that should be used at FlagGrid positions


        Returns:
            T: An instance of Real3dGrid
        """
        if not check_torch_float(value.dtype) or len(value.shape) != 0:
            raise TypeError(
                f"Value must me of type one of [torch.float16, torch.float32, torch.float64] current {value.dtype}\n"
                f"Values dim must be [] currently {value.shape}"
            )
        # convert to torch.bool due to warning
        flags = flag3dGrid.get_data() == int(flag)
        data = torch.zeros_like(flags, dtype=value.dtype, device=value.device)
        data[flags] = value
        return cls(data)

    def get_spatial_gradient(self) -> "Staggered3dGrid":
        """Compute the spatial gradient of the vector field returns components for a staggered grid

        Returns:
            Staggered3dGrid: the saptial gradient as a staggered grid
        """
        from differentiable_fluid_functions.grid import Staggered3dGrid

        grid_pad = F.pad(self._data, pad=(1, 1, 1, 1, 1, 1), mode="replicate")

        grad_x = grid_pad[..., 1:-1, 1:-1, 1:] - grid_pad[..., 1:-1, 1:-1, :-1]
        grad_y = grid_pad[..., 1:-1, 1:, 1:-1] - grid_pad[..., 1:-1, :-1, 1:-1]
        grad_z = grid_pad[..., 1:, 1:-1, 1:-1] - grid_pad[..., :-1, 1:-1, 1:-1]
        return Staggered3dGrid.from_tensor(grad_x, grad_y, grad_z)

    def get_gaussian_blur(self, size: int, sigma: float):
        """Apply a cubic kernel to the data"""
        kernel = create_3d_gaussian_kernel(size, sigma, 1, self.device(), self.dtype())
        pad_size = int((size - 1) / 2)
        data_pad = F.pad(
            self.get_data(),
            pad=(pad_size, pad_size, pad_size, pad_size, pad_size, pad_size),
            mode="replicate",
        )
        return Real3dGrid(F.conv3d(data_pad, kernel))

    def get_staggered_x(self) -> torch.Tensor:
        """Interpolate real grid at the mac-x position.

        Returns:
            torch.Tensor: Data sampled at the mac-y position [B, C, D, H, W + 1]
        """
        grid_pad_x = F.pad(self._data, pad=(1, 1, 0, 0, 0, 0), mode="replicate")
        grid_staggered_x = (grid_pad_x[..., 1:] + grid_pad_x[..., :-1]) * 0.5
        return grid_staggered_x

    def get_staggered_y(self) -> torch.Tensor:
        """Interpolate real grid at the mac-y position.

        Returns:
            torch.Tensor: Data sampled at the mac-y position [B, C, D, H + 1, W]
        """
        grid_pad_y = F.pad(self._data, pad=(0, 0, 1, 1, 0, 0), mode="replicate")
        grid_staggered_y = (grid_pad_y[..., 1:, :] + grid_pad_y[..., :-1, :]) * 0.5
        return grid_staggered_y

    def get_staggered_z(self) -> torch.Tensor:
        """Interpolate real grid at the mac-y position.

        Returns:
            torch.Tensor: Data sampled at the mac-y position [B, C, D + 1, H, W]
        """
        grid_pad_z = F.pad(self._data, pad=(0, 0, 0, 0, 1, 1), mode="replicate")
        grid_staggered_z = (
            grid_pad_z[..., 1:, :, :] + grid_pad_z[..., :-1, :, :]
        ) * 0.5
        return grid_staggered_z

    def __sub__(self, other):
        return Real3dGrid(data=self._data - other._data)

    def __add__(self, other):
        return Real3dGrid(data=self._data + other._data)

    def __mul__(self, other):
        return Real3dGrid(data=self._data * other)

    def __rmul__(self, other):
        return self.__mul__(other)

    @classmethod
    def from_particle_data(
        cls,
        data: "ParticleDatabase",
        attribute_name: str,
        simulation_size: torch.Tensor,
        kernel: "AbstractKernel",
        normalization: bool,
        filter_particles_out_of_bounds: bool = False,
    ) -> T:
        """Create a realGrid from particle data

        Args:
            data (ParticleDatabase): A Particle database where the data is stored
            attribute_name (str): under which key the data is stored
            simulation_size (torch.Tensor): size of the simulation [d, h, w]
            kernel (AbstractKernel): Which kernel should be used to splat the particle data to the grid

        Raises:
            ValueError: Wrong simulation size
            ValueError: The database does not contain 2d data

        Returns:
            Real2dGrid: With the data from the particles
        """
        if len(simulation_size) != 3:
            raise ValueError(
                "Dimensions must be set to [d, h, w] (used for every element in the batch)"
            )
        # particle_to_nodegrid
        particle_data = data.get_tensor(attribute_name)
        particle_postions = data.get_tensor(data.POSITIONS_KEY)
        if particle_postions.shape[-1] != 3:
            raise ValueError("A Real2dGrid requires 3d particle positions")

        values, _ = particle_to_grid(
            particle_data=particle_data,
            particle_positions=particle_postions,
            simulation_size=simulation_size,
            domain_bounding_box=data.get_domain_bounding_box(),
            kernel=kernel,
            grid_type=GridType.REAL,
            normalization=normalization,
            filter_particles_out_of_bounds=filter_particles_out_of_bounds,
        )
        return cls(values)
