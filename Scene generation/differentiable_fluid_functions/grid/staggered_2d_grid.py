from .abstract_2d_grid import Abstract2dGrid
import torch
import torch.nn.functional as F

import numpy as np
from typing import List, TypeVar, TYPE_CHECKING
from differentiable_fluid_functions.utils import (
    load_npz_raw,
    save_npz,
    create_2d_meshgrid_tensor,
    sample_grid_2d,
)
from differentiable_fluid_functions.grid import Real2dGrid

if TYPE_CHECKING:
    from differentiable_fluid_functions.grid import Node2dGrid
T = TypeVar("T", bound="Staggered2dGrid")
DATA_X_KEY = "DATA_X"
DATA_Y_KEY = "DATA_Y"


class Staggered2dGrid(Abstract2dGrid):
    def __init__(self, data_x: Real2dGrid, data_y: Real2dGrid):
        super(Staggered2dGrid, self).__init__(data=None)

        # check that the shape is valid
        shape_x = list(data_x.get_data().shape)
        shape_y = list(data_y.get_data().shape)
        shape_x[-1] -= 1
        shape_y[-2] -= 1
        if not np.array_equal(shape_x, shape_y):
            raise ValueError(
                "The provided npy files have different simulation grids\n"
                f"data_x should be B x C x H x W + 1 currently: {list(data_x.get_data().shape)}\n"
                f"data_y should be B x C x H + 1 x W currently: {list(data_y.get_data().shape)}\n"
            )

        # save the data in separate tensors to avoid having dummy channels
        self._data_x = data_x
        data_x_raw = data_x.get_data()
        self.shape = list(data_x_raw.shape)
        # reduce the width by one to get the simulation shape
        self.shape[-1] -= 1
        self._data_y = data_y

    @classmethod
    def from_tensor(cls, data_x: torch.tensor, data_y: torch.tensor) -> T:
        """Create a Staggered grid from 2 tensors

        Args:
            data_x (torch.tensor): Tensor of the x components
            data_y (torch.tensor): Tensor of the y components


        Returns:
            Staggered2dGrid: Instance of the class
        """

        _data_x = Real2dGrid(data_x)
        _data_y = Real2dGrid(data_y)

        return cls(_data_x, _data_y)

    @classmethod
    def from_npy(
        cls,
        filename_x: str,
        filename_y: str,
        device: torch.device = torch.device("cpu"),
    ) -> T:
        """Create a Staggered grid from 2 npy files

        Args:
            filename_x (str): Path to x components
            filename_y (str): Path to y components


        Returns:
            Staggered2dGrid: Instance of the class
        """

        data_x = Real2dGrid.from_npy(filename_x, device=device)
        data_y = Real2dGrid.from_npy(filename_y, device=device)

        return cls(data_x, data_y)

    @classmethod
    def from_npz(
        cls,
        filename: str,
        key_x: str = DATA_X_KEY,
        key_y: str = DATA_Y_KEY,
        device: torch.device = torch.device("cpu"),
    ) -> T:
        """Create a Staggered grid from 2 npz files


        Args:
            filename (str): Path to npz file
            key_x (str): key for x component in the npz file
            key_y (str): key for y component in the npz file

        Returns:
            Staggered2dGrid: Instance of the class
        """
        data_x = Real2dGrid.from_npz(filename, key_x, device=device)
        data_y = Real2dGrid.from_npz(filename, key_y, device=device)

        return cls(data_x, data_y)

    @classmethod
    def from_array(
        cls,
        array_x: np.ndarray,
        array_y: np.ndarray,
        dtype=torch.float32,
        device: torch.device = torch.device("cpu"),
    ) -> T:
        """Create a staggered 2d grid from

        Args:
            array_x (np.ndarray): x data
            array_y (np.ndarray): y data
            dtype ([type], optional): Required type of the tensor. Defaults to torch.float32.
            device (torch.device, optional): On which device to store the tensors. Defaults to torch.device("cpu").


        Returns:
            Staggered2dGrid: Instance of the class
        """
        data_x = Real2dGrid.from_array(array_x, dtype, device)
        data_y = Real2dGrid.from_array(array_y, dtype, device)

        return cls(data_x, data_y)

    @classmethod
    def get_constant_grid(
        cls,
        value_x: torch.Tensor,
        value_y: torch.Tensor,
        simulation_size: List[int],
    ) -> T:
        """Create a constant staggered 2d grid

        Args:
            value_x (torch.Tensor): value for the x data
            value_y (torch.Tensor): value for the x data
            simulation_size (List[int]): The required simulation dimensions

        Returns:
            Staggered2dGrid: Instance of the class
        """
        dim_x = list(simulation_size).copy()
        dim_x[-1] += 1
        data_x = Real2dGrid.get_constant_grid(
            value=value_x,
            simulation_size=dim_x,
        )
        dim_y = list(simulation_size).copy()
        dim_y[-2] += 1
        data_y = Real2dGrid.get_constant_grid(
            value=value_y,
            simulation_size=dim_y,
        )
        return cls(data_x, data_y)

    @classmethod
    def from_real_2d_grid(cls, grid: Real2dGrid) -> T:
        """Create a staggered grid from a real grid

        Args:
            grid (Real2dGrid): holding 2d data for the staggered grid

        Raises:
            ValueError: The provided Real 2d grid does not contain enough channels

        Returns:
            Staggered2dGrid: Instance of the class
        """
        grid_data = grid.get_data()
        if grid_data.shape[1] < 2:
            raise ValueError(
                "At least 2 channels are required on the grid. Please use from_real_2d_grid_single_channel"
            )

        u_centered = grid_data[:, 0:1, ...]
        v_centered = grid_data[:, 1:2, ...]

        u_mac_middle = (u_centered[..., :-1] + u_centered[..., 1:]) * 0.5
        u_mac_neg_x = u_centered[..., 0:1] * 2 - u_mac_middle[..., 0:1]
        u_mac_pos_x = u_centered[..., -1:] * 2 - u_mac_middle[..., -1:]
        u_mac = torch.cat((u_mac_neg_x, u_mac_middle, u_mac_pos_x), dim=-1)

        v_mac_middle = (v_centered[..., :-1, :] + v_centered[..., 1:, :]) * 0.5
        v_mac_neg_y = v_centered[..., 0:1, :] * 2 - v_mac_middle[..., 0:1, :]
        v_mac_pos_y = v_centered[..., -1:, :] * 2 - v_mac_middle[..., -1:, :]
        v_mac = torch.cat((v_mac_neg_y, v_mac_middle, v_mac_pos_y), dim=-2)

        data_x = Real2dGrid(u_mac)
        data_y = Real2dGrid(v_mac)
        return cls(data_x, data_y)

    @classmethod
    def from_real_2d_grid_single_channel(cls, grid: Real2dGrid) -> T:
        """Create a staggered grid from a real grid with a single dim using the same dim for x and y

        Args:
            grid (Real2dGrid): holding 2d data for the staggered grid

        Raises:
            ValueError: The provided Real 2d grid does not contain enough channels

        Returns:
            Staggered2dGrid: Instance of the class
        """
        grid_data = grid.get_data()
        if grid_data.shape[1] != 1:
            raise ValueError(
                "Only 1 channel is required on the grid. Otherwise please use from_real_2d_grid"
            )

        u_centered = grid_data[:, 0:1, ...]
        v_centered = grid_data[:, 0:1, ...]

        u_mac_middle = (u_centered[..., :-1] + u_centered[..., 1:]) * 0.5
        u_mac_neg_x = u_centered[..., 0:1] * 2 - u_mac_middle[..., 0:1]
        u_mac_pos_x = u_centered[..., -1:] * 2 - u_mac_middle[..., -1:]
        u_mac = torch.cat((u_mac_neg_x, u_mac_middle, u_mac_pos_x), dim=-1)

        v_mac_middle = (v_centered[..., :-1, :] + v_centered[..., 1:, :]) * 0.5
        v_mac_neg_y = v_centered[..., 0:1, :] * 2 - v_mac_middle[..., 0:1, :]
        v_mac_pos_y = v_centered[..., -1:, :] * 2 - v_mac_middle[..., -1:, :]
        v_mac = torch.cat((v_mac_neg_y, v_mac_middle, v_mac_pos_y), dim=-2)

        data_x = Real2dGrid(u_mac)
        data_y = Real2dGrid(v_mac)
        return cls(data_x, data_y)

    def to_real_2d_grid(self) -> Real2dGrid:
        """Converted a staggered grid to a realgrid

        Returns:
            Real2dGrid: Instance of the real grid
        """
        grid_x = self._data_x.get_data()
        grid_y = self._data_y.get_data()
        u_centered = (grid_x[..., :-1] + grid_x[..., 1:]) * 0.5
        v_centered = (grid_y[..., :-1, :] + grid_y[..., 1:, :]) * 0.5
        vel_centered = torch.cat((u_centered, v_centered), dim=1)
        return Real2dGrid(vel_centered)

    def save_npy(self, filename_x: str, filename_y: str):
        """Save the components as an npy file

        Args:
            filename_x (str): Filename of the data x npy file
            filename_y (str): Filename of the data y npy file
        """
        self._data_x.save_npy(filename_x)
        self._data_y.save_npy(filename_y)

    def get_data(self) -> List[Real2dGrid]:
        """Return the raw tensor data of the grid as a list

        Returns:
            List[Real2dGrid]: contains data x and y
        """
        return [self._data_x, self._data_y]

    def get_numpy(self) -> List[np.ndarray]:
        """Return the raw tensor data of the grid as a list

        Returns:
            List[Real2dGrid]: contains data x and y as np arrays
        """

        return [self._data_x.get_numpy(), self._data_y.get_numpy()]

    def get_tensor(self) -> List[torch.tensor]:
        """Return the raw tensor data of the grid as a list

        Returns:
            List[torch.tensor]: contains data x and y tensors
        """
        return [self._data_x.get_data(), self._data_y.get_data()]

    def set_gradient_required(self, value: bool):
        """Set that this grid type requires a gradient

        Args:
            value (bool): flag if the gradient is required.
        """
        self._data_x.set_gradient_required(value)
        self._data_y.set_gradient_required(value)

    def grid2array(self) -> List[np.ndarray]:
        """Transform the grid components
            to an array with dimensions batch x height x width x channel

        Returns:
            List[np.ndarray]: data of the grid
        """
        return [self._data_x.grid2array(), self._data_y.grid2array()]

    def save_single_npz(self, filename: str):
        """Save the staggered 2d grid a single npz file

        Args:
            filename (str): filename to save the npz file
        """
        save_dict = {}
        save_dict[DATA_X_KEY] = self._data_x.grid2array()
        save_dict[DATA_Y_KEY] = self._data_y.grid2array()

        save_npz(filename, save_dict)

    def trim_boundary(self, bwidth: int = 1):
        """Trim the boundaries from the grid

        Args:
            bwidth (int, optional): how much should be trimmed. Defaults to 1.
        """
        self._data_x.trim_boundary(bwidth)
        self._data_y.trim_boundary(bwidth)

    def get_gradient(self) -> List[torch.Tensor]:
        """Get the gradients of the grid components as a list

        Returns:
            List[torch.Tensor]: The gradient
        """
        return [self._data_x.get_gradient(), self._data_y.get_gradient()]

    def get_divergence(self) -> Real2dGrid:
        """Compute the divergence of the staggered 2d grid

        Returns:
            Real2dGrid: divergence of a the simulation points
        """
        u = self._data_x.get_data()
        v = self._data_y.get_data()
        div = u[..., 1:] - u[..., :-1] + v[..., 1:, :] - v[..., :-1, :]
        return Real2dGrid(div)

    def grid_sample_center(self, back_trace: torch.Tensor = None) -> torch.Tensor:
        """Interpolate velocity field at the cell center position (simulation points).

        Args:
            back_trace (torch.Tensor, optional): Tensor defining which cells should be sampled. Defaults to None.

        Returns:
            torch.Tensor: Data sampled at the center [B, C, H, W]
        """
        if back_trace is None:
            back_trace = create_2d_meshgrid_tensor(
                self.shape,
                device=self.device(),
                dtype=self.dtype(),
            )

        grid_x = self._data_x.get_data()
        [_, _, height, width] = back_trace.shape
        vel_x = sample_grid_2d(
            grid_x,
            torch.cat((back_trace[:, 0:1, ...] + 0.5, back_trace[:, 1:2, ...]), dim=1),
            width,
            height - 1,
        )

        grid_y = self._data_y.get_data()
        vel_y = sample_grid_2d(
            grid_y,
            torch.cat(
                (back_trace[:, 0:1, ...], back_trace[:, 1:2, ...] + 0.5),
                dim=1,
            ),
            width - 1,
            height,
        )
        vel = torch.cat((vel_x, vel_y), dim=1)

        return vel

    def grid_sample_node(self, back_trace: torch.Tensor = None) -> torch.Tensor:
        """Interpolate velocity field at the cell nodes (corner) position.

        Args:
            back_trace (torch.Tensor, optional): Tensor defining which cells should be sampled. Defaults to None.

        Returns:
            torch.Tensor: Data sampled at the center [B, C, H + 1, W + 1]
        """
        if back_trace is None:
            [batch, channel, height, width] = self.shape
            back_trace = create_2d_meshgrid_tensor(
                [batch, channel, height + 1, width + 1],
                device=self.device(),
                dtype=self.dtype(),
            )

        grid_x = self._data_x.get_data()
        [_, _, height, width] = back_trace.shape

        vel_x = sample_grid_2d(
            grid_x,
            torch.cat((back_trace[:, 0:1, ...], back_trace[:, 1:2, ...] - 0.5), dim=1),
            width - 1,
            height - 2,
        )

        grid_y = self._data_y.get_data()
        vel_y = sample_grid_2d(
            grid_y,
            torch.cat(
                (back_trace[:, 0:1, ...] - 0.5, back_trace[:, 1:2, ...]),
                dim=1,
            ),
            width - 2,
            height - 1,
        )
        vel = torch.cat((vel_x, vel_y), dim=1)

        return vel

    def grid_sample_x(self, back_trace: torch.Tensor = None):
        """Interpolate velocity field at the mac-x position.

        Args:
            back_trace (torch.Tensor, optional): Tensor defining which cells should be sampled. Defaults to None.

        Returns:
            torch.Tensor: Data sampled at the mac-x position [B, C, H, W + 1]
        """

        if back_trace is None:
            [batch, channel, height, width] = self.shape
            back_trace = create_2d_meshgrid_tensor(
                [batch, channel, height, width + 1],
                device=self.device(),
                dtype=self.dtype(),
            )
        [batch, channel, height, width] = back_trace.shape
        grid_x = self._data_x.get_data()
        vel_x = sample_grid_2d(grid_x, back_trace, width - 1, height - 1)
        vel_y = self._get_sample_x(back_trace)
        vel = torch.cat((vel_x, vel_y), dim=1)

        return vel

    def _get_sample_x(self, back_trace: torch.Tensor = None):
        """Private function sampling the y data at the mac-x data positions

        Args:
            back_trace (torch.Tensor, optional): Tensor defining which cells should be sampled. Defaults to None.

        Returns:
            torch.Tensor: Data sampled at the mac-x position [B, C, H, W + 1]
        """
        grid_y = self._data_y.get_data()

        if back_trace is None:
            [batch, channel, height, width] = self.shape
            back_trace = create_2d_meshgrid_tensor(
                [batch, channel, height, width + 1],
                device=self.device(),
                dtype=self.dtype(),
            )
        back_trace = torch.cat(
            (back_trace[:, 0:1, ...] - 0.5, back_trace[:, 1:2, ...] + 0.5), dim=1
        )
        [batch, channel, height, width] = back_trace.shape
        grid_mac_x = sample_grid_2d(grid_y, back_trace, width - 2, height)

        return grid_mac_x

    def grid_sample_y(self, back_trace: torch.Tensor = None) -> torch.Tensor:
        """Interpolate velocity field at the mac-y position.

        Args:
            back_trace (torch.Tensor, optional): Tensor defining which cells should be sampled. Defaults to None.

        Returns:
            torch.Tensor: Data sampled at the mac-y position [B, C, H + 1, W]
        """
        if back_trace is None:
            [batch, channel, height, width] = self.shape
            back_trace = create_2d_meshgrid_tensor(
                [batch, channel, height + 1, width],
                device=self.device(),
                dtype=self.dtype(),
            )
        [batch, channel, height, width] = back_trace.shape
        vel_x = self._get_sample_y(back_trace)
        grid_y = self._data_y.get_data()
        vel_y = sample_grid_2d(grid_y, back_trace, width - 1, height - 1)
        vel = torch.cat((vel_x, vel_y), dim=1)
        return vel

    def _get_sample_y(self, back_trace: torch.Tensor = None) -> torch.Tensor:
        """Private function sampling the X data at the mac-y data positions

        Args:
            back_trace (torch.Tensor, optional): Tensor defining which cells should be sampled. Defaults to None.

        Returns:
            torch.Tensor: Data sampled at the mac-y position [B, C, H + 1, W]
        """
        grid_x = self._data_x.get_data()

        if back_trace is None:
            [batch, channel, height, width] = self.shape

            back_trace = create_2d_meshgrid_tensor(
                [batch, channel, height + 1, width],
                device=self.device(),
                dtype=self.dtype(),
            )

        back_trace = torch.cat(
            (back_trace[:, 0:1, ...] + 0.5, back_trace[:, 1:2, ...] - 0.5), dim=1
        )
        [batch, channel, height, width] = back_trace.shape
        grid_mac_y = sample_grid_2d(grid_x, back_trace, width, height - 2)

        return grid_mac_y

    def get_energy(self) -> torch.Tensor:
        """Get the energy of the staggered grid at the center positions

        Returns:
            torch.Tensor: Tensor holding the energies [B X 1 X H X W]
        """
        grid_center = self.grid_sample_center()
        energy = grid_center[:, 0:1, ...] ** 2 + grid_center[:, 1:2, ...] ** 2
        return energy

    def get_staggered_x(self) -> torch.Tensor:
        """Interpolate velocity field at the mac-x position.
            Faster since positions are fixed
        Returns:
            torch.Tensor: Data sampled at the mac-y position [B, C, H, W + 1]
        """
        u_mac_x = self._data_x.get_data()
        v_mac_y = self._data_y.get_data()
        pad_x_neg = (1, 1, 0, 0)
        v_mac_y_pad_x = F.pad(v_mac_y, pad=pad_x_neg, mode="replicate")

        v_centered = (v_mac_y_pad_x[..., 1:] + v_mac_y_pad_x[..., :-1]) * 0.5
        v_mac_x = (v_centered[..., 1:, :] + v_centered[..., :-1, :]) * 0.5

        vel_mac_x = torch.cat((u_mac_x, v_mac_x), dim=1)
        return vel_mac_x

    def get_centered(self) -> torch.Tensor:
        """Interpolate velocity field at the center position.
            Faster since positions are fixed
        Returns:
            torch.Tensor: Data sampled at the cell center position [B, C, H, W]
        """
        u_mac = self._data_x.get_data()
        v_mac = self._data_y.get_data()
        u_centered = (u_mac[..., :-1] + u_mac[..., 1:]) * 0.5
        v_centered = (v_mac[..., :-1, :] + v_mac[..., 1:, :]) * 0.5
        vel_centered = torch.cat((u_centered, v_centered), dim=1)
        return vel_centered

    def get_staggered_y(self) -> torch.Tensor:
        """Interpolate velocity field at the mac-y position.
            Faster since positions are fixed

        Returns:
            torch.Tensor: Data sampled at the mac-y position [B, C, H + 1, W]
        """
        u_mac_x = self._data_x.get_data()
        v_mac_y = self._data_y.get_data()
        pad_y_neg = (0, 0, 1, 1)
        u_mac_x_pad_y = F.pad(u_mac_x, pad=pad_y_neg, mode="replicate")
        u_centered = (u_mac_x_pad_y[..., 1:, :] + u_mac_x_pad_y[..., :-1, :]) * 0.5
        u_mac_y = (u_centered[..., 1:] + u_centered[..., :-1]) * 0.5

        vel_mac_y = torch.cat((u_mac_y, v_mac_y), dim=1)
        return vel_mac_y

    def get_curl(self) -> "Node2dGrid":
        """Compute the curl of the staggered grid by padding zeros
        v(x,y) = (u(x, y), v(x, y))
        2d-curl(v(x,y)) = dv/dx -  du/dy

        Returns:
            Node2dGrid: The curl of the 2d grid
        """
        from differentiable_fluid_functions.grid import Node2dGrid

        u = self._data_x.get_data()
        v = self._data_y.get_data()

        padx = (1, 1, 0, 0)
        pady = (0, 0, 1, 1)

        u_pady = F.pad(u, pad=pady)
        v_padx = F.pad(v, pad=padx)

        dudy = u_pady[..., 1:, :] - u_pady[..., :-1, :]
        dvdx = v_padx[..., 1:] - v_padx[..., :-1]

        vort = dvdx - dudy

        return Node2dGrid(vort)

    def get_jacobian(self):
        vel_mac_x = self.get_staggered_x()
        vel_mac_y = self.get_staggered_y()
        u_mac_x = vel_mac_x[:, 0:1, :, :]
        v_mac_x = vel_mac_x[:, 1:2, :, :]
        u_mac_y = vel_mac_y[:, 0:1, :, :]
        v_mac_y = vel_mac_y[:, 1:2, :, :]
        du_dx = u_mac_x[:, :, :, 1:] - u_mac_x[:, :, :, :-1]
        dv_dx = v_mac_x[:, :, :, 1:] - v_mac_x[:, :, :, :-1]
        du_dy = u_mac_y[:, :, 1:, :] - u_mac_y[:, :, :-1, :]
        dv_dy = v_mac_y[:, :, 1:, :] - v_mac_y[:, :, :-1, :]
        dvel_dx = torch.cat((du_dx, dv_dx), dim=1)
        dvel_dy = torch.cat((du_dy, dv_dy), dim=1)
        jacobian = torch.cat((dvel_dx.unsqueeze(2), dvel_dy.unsqueeze(2)), dim=2)
        return jacobian

    def get_q_criterion(self):
        jacobian = self.get_jacobian().permute((0, 3, 4, 1, 2))
        q = torch.linalg.det(jacobian).unsqueeze(1)
        return Real2dGrid(q)

    def get_taylor_vortices(
        self,
        phi_obs,
        a=0.08,
        U=0.06,
        center_list=[torch.tensor([-0.15, 0]), torch.tensor([0.15, 0])],
    ):
        raise NotImplementedError("IMPL: get_taylor_vortices")

    def get_galilean_ambient(self, grid, kernel_size=3, sigma=1):
        raise NotImplementedError("IMPL: get_galilean_ambient")

    def size(self):
        return self.shape

    def __sub__(self, other):
        return Staggered2dGrid(
            data_x=self._data_x - other._data_x, data_y=self._data_y - other._data_y
        )

    def __add__(self, other):
        return Staggered2dGrid(
            data_x=self._data_x + other._data_x, data_y=self._data_y + other._data_y
        )

    def __mul__(self, other):
        if not hasattr(other, "__len__"):
            return Staggered2dGrid(
                data_x=self._data_x * other, data_y=self._data_y * other
            )
        if len(other) == 2:
            return Staggered2dGrid(
                data_x=self._data_x * other[0], data_y=self._data_y * other[1]
            )
        raise ValueError(f"Mult with type {type(other)} is not supported")

    def __rmul__(self, other):
        return self.__mul__(other)

    def dtype(self):
        """Return the dtype of the data

        Returns:
            :
        """
        return self._data_x.dtype()

    def device(self) -> torch.device:
        """Return the device one which the data is stored

        Returns:
            torch.device: the device of the data
        """
        return self._data_x.device()
