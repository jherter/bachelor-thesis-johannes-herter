from .abstract_3d_grid import Abstract3dGrid
import torch
import torch.nn.functional as F

import numpy as np
from typing import List, TypeVar, TYPE_CHECKING
from differentiable_fluid_functions.utils import (
    save_npz,
    load_npz_raw,
)

from differentiable_fluid_functions.grid import Real3dGrid

if TYPE_CHECKING:
    from differentiable_fluid_functions.particles import (  # pragma: no cover
        ParticleDatabase,
    )
    from differentiable_fluid_functions.kernels import (  # pragma: no cover
        AbstractKernel,
    )
    from differentiable_fluid_functions.grid import Staggered3dGrid
T = TypeVar("T", bound="Node3dGrid")
DATA_X_KEY = "DATA_X"
DATA_Y_KEY = "DATA_Y"
DATA_Z_KEY = "DATA_Z"


class Node3dGrid(Abstract3dGrid):
    def __init__(
        self,
        data_x: Real3dGrid,
        data_y: Real3dGrid,
        data_z: Real3dGrid,
        *args,
        **kwargs,
    ):
        super(Node3dGrid, self).__init__(data=None, *args, **kwargs)
        # check that the shape is valid
        shape_x = list(data_x.get_data().shape)
        shape_y = list(data_y.get_data().shape)
        shape_z = list(data_z.get_data().shape)
        # x data has one more entry in the width and depth
        shape_x[-2] -= 1
        shape_x[-3] -= 1

        shape_y[-1] -= 1
        shape_y[-3] -= 1

        shape_z[-1] -= 1
        shape_z[-2] -= 1
        if not np.array_equal(shape_x, shape_y) or not np.array_equal(shape_x, shape_z):
            raise ValueError(
                "The provided npy files have different simulation grids\n"
                f"data_x should be B x C x D + 1 x H + 1 x W     currently: {list(data_x.get_data().shape)}\n"
                f"data_y should be B x C x D + 1 x H     x W + 1 currently: {list(data_y.get_data().shape)}\n"
                f"data_z should be B x C x D     x H + 1 x W + 1 currently: {list(data_z.get_data().shape)}\n"
            )

        # save the data in separate tensors to avoid having dummy channels

        self.shape = shape_x
        # reduce the width by one to get the simulation shape
        self._data_x = data_x
        self._data_y = data_y
        self._data_z = data_z

    @classmethod
    def from_tensor(
        cls, data_x: torch.tensor, data_y: torch.tensor, data_z: torch.tensor
    ) -> T:
        """Create a Node grid from 3 tensors

        Args:
            data_x (torch.tensor): Tensor of the x components
            data_y (torch.tensor): Tensor of the y components
            data_z (torch.tensor): Tensor of the y components

        Raises:
            ValueError: if the shape of the x/y/z does not match

        Returns:
            Staggered3dGrid: Instance of the class
        """

        _data_x = Real3dGrid(data_x)
        _data_y = Real3dGrid(data_y)
        _data_z = Real3dGrid(data_z)

        return cls(_data_x, _data_y, _data_z)

    @classmethod
    def from_array(
        cls,
        array_x: np.ndarray,
        array_y: np.ndarray,
        array_z: np.ndarray,
        dtype=torch.float32,
        device: torch.device = torch.device("cpu"),
    ) -> T:
        """Create a Node3dGrid from

        Args:
            array_x (np.ndarray): x data
            array_y (np.ndarray): y data
            array_z (np.ndarray): z data
            dtype ([type], optional): Required type of the tensor. Defaults to torch.float32.
            device (torch.device, optional): On which device to store the tensors. Defaults to torch.device("cpu").

        Returns:
            Staggered3dGrid: Instance of the class
        """
        data_x = Real3dGrid.from_array(array_x, dtype, device)
        data_y = Real3dGrid.from_array(array_y, dtype, device)
        data_z = Real3dGrid.from_array(array_z, dtype, device)

        return cls(data_x, data_y, data_z)

    @classmethod
    def from_npy(
        cls,
        filename_x: str,
        filename_y: str,
        filename_z: str,
        device: torch.device = torch.device("cpu"),
    ) -> T:
        """Create a Node grid from 3 npy files

        Args:
            filename_x (str): Path to x components
            filename_y (str): Path to y components
            filename_z (str): Path to z components


        Returns:
            Node3dGrid: Instance of the class
        """

        data_x = Real3dGrid.from_npy(filename_x, device=device)
        data_y = Real3dGrid.from_npy(filename_y, device=device)
        data_z = Real3dGrid.from_npy(filename_z, device=device)

        return cls(data_x, data_y, data_z)

    @classmethod
    def from_npz(
        cls,
        filename: str,
        key_x: str = DATA_X_KEY,
        key_y: str = DATA_Y_KEY,
        key_z: str = DATA_Z_KEY,
        device: torch.device = torch.device("cpu"),
    ) -> T:
        """Create a Node grid from 3 npz files


        Args:
            filename (str): Path to the npz file
            key_x (str): key for x component in the x npz file
            key_y (str): key for y component in the y npz file
            key_z (str): key for z component in the z npz file
        Returns:
            Node3dGrid: Instance of the class
        """
        data_x = Real3dGrid.from_npz(filename, key_x, device=device)
        data_y = Real3dGrid.from_npz(filename, key_y, device=device)
        data_z = Real3dGrid.from_npz(filename, key_z, device=device)

        return cls(data_x, data_y, data_z)

    @classmethod
    def get_constant_grid(
        cls,
        value_x: torch.Tensor,
        value_y: torch.Tensor,
        value_z: torch.Tensor,
        simulation_size: List[int],
    ) -> T:
        """Create a constant node 2d grid

        Args:
            value_x (torch.Tensor): value for the x data
            value_y (torch.Tensor): value for the y data
            value_z (torch.Tensor): value for the z data
            simulation_size (List[int]): The required simulation dimensions

        Returns:
            Node3dGrid: Instance of the class
        """
        dim_x = list(simulation_size).copy()
        dim_x[-2] += 1
        dim_x[-3] += 1
        data_x = Real3dGrid.get_constant_grid(
            value=value_x,
            simulation_size=dim_x,
        )

        dim_y = list(simulation_size).copy()
        dim_y[-1] += 1
        dim_y[-3] += 1
        data_y = Real3dGrid.get_constant_grid(
            value=value_y,
            simulation_size=dim_y,
        )

        dim_z = list(simulation_size).copy()
        dim_z[-1] += 1
        dim_z[-2] += 1
        data_z = Real3dGrid.get_constant_grid(value=value_z, simulation_size=dim_z)
        return cls(data_x, data_y, data_z)

    def save_npy(self, filename_x: str, filename_y: str, filename_z: str):
        """Save the components as an npy file

        Args:
            filename_x (str): Filename of the data x npy file
            filename_y (str): Filename of the data y npy file
            filename_z (str): Filename of the data z npy file
        """
        self._data_x.save_npy(filename_x)
        self._data_y.save_npy(filename_y)
        self._data_z.save_npy(filename_z)

    def get_data(self) -> List[Real3dGrid]:
        """Return the raw tensor data of the grid as a list

        Returns:
            List[Real3dGrid]: contains data x, y and z
        """
        return [self._data_x, self._data_y, self._data_z]

    def get_numpy(self) -> List[np.ndarray]:
        """Return the raw tensor data of the grid as a list

        Returns:
            List[Real3dGrid]: contains data  x, y and z as np arrays
        """

        return [
            self._data_x.get_numpy(),
            self._data_y.get_numpy(),
            self._data_z.get_numpy(),
        ]

    def get_tensor(self) -> List[torch.tensor]:
        """Return the raw tensor data of the grid as a list

        Returns:
            List[torch.tensor]: contains data  x, y and z tensors
        """
        return [
            self._data_x.get_data(),
            self._data_y.get_data(),
            self._data_z.get_data(),
        ]

    def set_gradient_required(self, value: bool):
        """Set that this grid type requires a gradient

        Args:
            value (bool): flag if the gradient is required.
        """
        self._data_x.set_gradient_required(value)
        self._data_y.set_gradient_required(value)
        self._data_z.set_gradient_required(value)

    def grid2array(self) -> List[np.ndarray]:
        """Transform the grid components
            to an array with dimensions batch x depth x height x width x channel

        Returns:
            List[np.ndarray]: data of the grid
        """
        return [
            self._data_x.grid2array(),
            self._data_y.grid2array(),
            self._data_z.grid2array(),
        ]

    def save_single_npz(self, filename: str):
        """Save the staggered 2d grid a single npz file

        Args:
            filename (str): filename to save the npz file
        """
        save_dict = {}
        save_dict[DATA_X_KEY] = self._data_x.grid2array()
        save_dict[DATA_Y_KEY] = self._data_y.grid2array()
        save_dict[DATA_Z_KEY] = self._data_z.grid2array()

        save_npz(filename, save_dict)

    def get_gradient(self) -> List[torch.Tensor]:
        """Get the gradients of the grid components as a list

        Returns:
            List[torch.Tensor]: The gradient
        """
        return [
            self._data_x.get_gradient(),
            self._data_y.get_gradient(),
            self._data_z.get_gradient(),
        ]

    @classmethod
    def get_gaussian(
        cls,
        mu: torch.Tensor,
        sig: torch.Tensor,
        amp: torch.Tensor,
        simulation_size: List[int],
    ) -> T:
        raise NotImplementedError("IMPL: get_gaussian")

    @classmethod
    def from_particle_data(
        cls,
        data: "ParticleDatabase",
        attribute_name: str,
        simulation_size: torch.Tensor,
        kernel: "AbstractKernel",
        normalization: bool,
        filter_particles_out_of_bounds: bool = False,
    ) -> T:
        raise NotImplementedError("IMPL: from_particle_data")

    def get_spatial_gradient(self) -> List[torch.Tensor]:
        """Return the spatial gradient of a node grid.
           The gradient is defined on the node positions.

        Returns:
            List[torch.Tensor]: x/y/z components of the gradient
        """
        grid_x = self._data_x.get_data()
        grid_x_pad = F.pad(grid_x, pad=(1, 1, 0, 0, 0, 0), mode="replicate")

        grid_y = self._data_y.get_data()
        grid_y_pad = F.pad(grid_y, pad=(0, 0, 1, 1, 0, 0), mode="replicate")

        grid_z = self._data_z.get_data()
        grid_z_pad = F.pad(grid_z, pad=(0, 0, 0, 0, 1, 1), mode="replicate")

        grad_x = grid_x_pad[..., 1:] - grid_x_pad[..., :-1]
        grad_y = grid_y_pad[..., 1:, :] - grid_y_pad[..., :-1, :]
        grad_z = grid_z_pad[..., 1:, :, :] - grid_z_pad[..., :-1, :, :]
        return [grad_x, grad_y, grad_z]

    def get_divergence(self) -> torch.Tensor:
        """Compute the divergence of the node grid

        Returns:
            torch.Tensor: divergence defined on the nodes
        """
        [grad_x, grad_y, grad_z] = self.get_spatial_gradient()
        return grad_x + grad_y + grad_z

    def get_curl(self) -> "Staggered3dGrid":
        """Compute the curl of the grid

        Returns:
            Staggered3dGrid: The xy data component of the curl of the node grid
        """
        from differentiable_fluid_functions.grid import Staggered3dGrid

        grid_x = self._data_x.get_data()
        grid_y = self._data_y.get_data()
        grid_z = self._data_z.get_data()

        # compute finite difference gradients
        dgridx_dz = grid_x[..., 1:, :, :] - grid_x[..., :-1, :, :]
        dgridx_dy = grid_x[..., 1:, :] - grid_x[..., :-1, :]
        dgridy_dx = grid_y[..., 1:] - grid_y[..., :-1]
        dgridy_dz = grid_y[..., 1:, :, :] - grid_y[..., :-1, :, :]
        dgridz_dx = grid_z[..., 1:] - grid_z[..., :-1]
        dgridz_dy = grid_z[..., 1:, :] - grid_z[..., :-1, :]

        curl_x = dgridz_dy - dgridy_dz  # B x C x D     x H     x W + 1
        curl_y = dgridx_dz - dgridz_dx  # B x C x D     x H + 1 x W
        curl_z = dgridy_dx - dgridx_dy  # B x C x D + 1 x H     x W

        return Staggered3dGrid.from_tensor(curl_x, curl_y, curl_z)

    def get_centered(self) -> torch.Tensor:
        """Interpolate node grid at the center position.
        Returns:
            torch.Tensor: Data sampled at the cell center position [B, C, D, H, W]
        """
        grid_x = self._data_x.get_data()
        grid_y = self._data_y.get_data()
        grid_z = self._data_z.get_data()
        grid_x_centered = (
            grid_x[..., 1:, 1:, :]
            + grid_x[..., :-1, 1:, :]
            + grid_x[..., 1:, :-1, :]
            + grid_x[..., :-1, :-1, :]
        ) * 0.25
        grid_y_centered = (
            grid_y[..., 1:, :, 1:]
            + grid_y[..., :-1, :, 1:]
            + grid_y[..., 1:, :, :-1]
            + grid_y[..., :-1, :, :-1]
        ) * 0.25
        grid_z_centered = (
            grid_z[..., :, 1:, 1:]
            + grid_z[..., :, :-1, 1:]
            + grid_z[..., :, 1:, :-1]
            + grid_z[..., :, :-1, :-1]
        ) * 0.25
        grid_centered = torch.cat(
            (grid_x_centered, grid_y_centered, grid_z_centered), dim=1
        )
        return grid_centered

    def get_inverse_curl(self, phi_obs, vort, cg_accu=1e-10):
        raise NotImplementedError("IMPL: get_inverse_curl")

    def size(self) -> torch.Size:
        """Return the simulation size

        Returns:
            [torch.Size]: Size of the simulation
        """
        return self.shape

    def dtype(self):
        """Return the dtype of the data

        Returns:
            : the type
        """
        return self._data_x.dtype()

    def device(self) -> torch.device:
        """Return the device one which the data is stored

        Returns:
            torch.device: the device of the data
        """
        return self._data_x.device()
