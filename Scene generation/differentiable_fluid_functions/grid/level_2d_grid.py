from .abstract_2d_grid import Abstract2dGrid
import torch

import numpy as np
from typing import List, TypeVar
from differentiable_fluid_functions.utils import (
    load_npy,
    load_npz,
    check_torch_float,
    check_numpy_float,
)
from differentiable_fluid_functions.grid import (
    Staggered2dGrid,
    Flag2dGrid,
    CellType,
)

T = TypeVar("T", bound="Level2dGrid")


class Level2dGrid(Abstract2dGrid):
    def __init__(self, data: torch.Tensor, *args, **kwargs):
        if not check_torch_float(data.dtype):
            raise TypeError(
                f"Data must me of type torch.float<16, 32, 64> currently: {data.dtype}"
            )
        super(Level2dGrid, self).__init__(data, *args, **kwargs)

    @classmethod
    def from_npy(cls, filename: str, **kwargs) -> T:
        """Create a Level2dGrid from a npy file

        Args:
            filename (str): path to the file

        Raises:
            TypeError: if the content of the file is not float32 or float64

        Returns:
            T: An instance of Level2dGrid
        """
        array = load_npy(filename)
        if not check_numpy_float(array.dtype):
            raise TypeError(
                f"{filename} has data with the wrong format: {array.dtype} required np.float<32, 64>"
            )
        dtype = torch.float32 if array.dtype == np.float32 else torch.float64
        return cls.from_array(array, dtype=dtype, **kwargs)

    @classmethod
    def from_npz(cls, filename: str, key: str, **kwargs) -> T:
        """Create a Level2dGrid from a npz file

        Args:
            filename (str): path to the file

        Raises:
            TypeError: if the content of the file is not float32 or float64

        Returns:
            T: An instance of Level2dGrid
        """
        array = load_npz(filename, key)
        if not check_numpy_float(array.dtype):
            raise TypeError(
                f"{filename} has data with the wrong format: {array.dtype} required np.float<32, 64>"
            )
        dtype = torch.float32 if array.dtype == np.float32 else torch.float64
        return cls.from_array(array, dtype=dtype, **kwargs)

    @classmethod
    def get_constant_grid(
        cls,
        value: torch.Tensor,
        simulation_size: List[int],
        **kwargs,
    ) -> T:
        """Creates a constant level 2d grid

        Args:
            value (torch.Tensor): Which value the grid should have
            simulation_size (List[int]): The dimensions of the grid

        Raises:
            ValueError: If the dimension are not correct
            TypeError: if the type of the value is not correct

        Returns:
            T: An instance of Level2dGrid
        """
        if len(simulation_size) != 4:
            raise ValueError(
                "For 2d array the 4 dim data is required eg. batch x channel x Height x Width"
            )
        if not check_torch_float(value.dtype) or len(value.shape) != 0:
            raise TypeError(
                f"Value must me of type one of [torch.float16, torch.float32, torch.float64] current {value.dtype}\n"
                f"Values dim must be [] currently {value.shape}"
            )
        dim = list(simulation_size)
        dim[-1] += 1
        dim[-2] += 1
        tensor = torch.ones(dim, dtype=value.dtype, device=value.device) * value
        return cls(tensor)

    def _get_line_fractions(self) -> Staggered2dGrid:
        """return the fractions of the boundary at the grid lines
            if the boundary does not pass through the cell
            return 0 (inside) or 1 outside
        Returns:
            Staggered2dGrid: The fractional values on the grid lines
        """
        # data is sim size + 1
        phi = self.get_data()

        left = phi[..., :-1]
        right = phi[..., 1:]
        bottom = phi[..., :-1, :]
        up = phi[..., 1:, :]

        # TODO: need to review differentiabilities (@Jingwei)
        max_ub = torch.max(up, bottom)
        min_ub = torch.min(up, bottom)
        fraction_horizontal = max_ub / (max_ub - min_ub + 1e-7)
        fraction_horizontal = torch.clamp(fraction_horizontal, 0.0, 1.0)

        max_lr = torch.max(left, right)
        min_lr = torch.min(left, right)
        fraction_vertical = max_lr / (max_lr - min_lr + 1e-7)
        fraction_vertical = torch.clamp(fraction_vertical, 0.0, 1.0)

        fraction = Staggered2dGrid.from_tensor(
            data_x=fraction_horizontal, data_y=fraction_vertical
        )
        return fraction

    def get_fractions(self) -> Staggered2dGrid:
        """return the fractions of the boundary at the grid lines
            if the boundary does not pass through the cell
            return 0 (inside) or 1 outside
        Returns:
            Staggered2dGrid: The fractional values on the grid lines
        """
        return self._get_line_fractions()

    def get_obstacle(self) -> Flag2dGrid:
        """Convert a level grid to an flaggrid indicating where an obstacle is.
            A cell of a flaggrid is considered an obstacle if all 4 corners of the
            fractions are basically zero.
            true if obstacle false, is fluid

        Returns:
            Flag2dGrid: Resulting flaggrid indicating which cell is an obstacle
        """
        fractions = self.get_fractions()
        fractions_data_x_tensor, fractions_data_y_tensor = fractions.get_tensor()

        is_obstacle = (
            (fractions_data_x_tensor[..., :-1] < 1e-6)
            * (fractions_data_x_tensor[..., 1:] < 1e-6)
            * (fractions_data_y_tensor[..., :-1, :] < 1e-6)
            * (fractions_data_y_tensor[..., 1:, :] < 1e-6)
        ).to(torch.uint8)
        is_obstacle[is_obstacle > 0] = int(CellType.OBSTACLE)

        return Flag2dGrid(is_obstacle)

    def get_fluid(self) -> Flag2dGrid:
        """Return a flaggrid indicating which cells are fluid cells

        Returns:
            Flag2dGrid: True if a cell is a fluid cell, false otherwise, eg. obstacle.
        """
        fluid = self.get_obstacle()
        fluid.invert()
        return fluid

    def get_flags(self) -> Flag2dGrid:
        """Return a flaggrid indicating which cells are fluid cells
                and which cells are obstacles

        Returns:
            Flag2dGrid: indicating fluid and obstacles.
        """
        is_obstacle = self.get_obstacle().get_data()
        flags_data = torch.where(
            is_obstacle == int(CellType.OBSTACLE),
            int(CellType.OBSTACLE) * torch.ones_like(is_obstacle),
            int(CellType.FLUID) * torch.ones_like(is_obstacle),
        )
        return Flag2dGrid(flags_data)

    def union(self, lvl2dGrid: T, alpha: float = 0.0):
        """Performs union operator: self {cup} lvl2dGrid,

        Args:
            lvl2dGrid (Level2dGrid): The grid to perform the union with
            alpha (float): parameter to control blending,
                       should be in the range [0,1], alpha=1.0 is equivalent
                       to boolean blending, i.e. torch.min(f, g)
        """
        f = self.get_data()
        g = lvl2dGrid.get_data()
        if alpha < 0.0 or alpha > 1.0:
            raise ValueError("alpha should be in the range [0,1].")
        if alpha == 1.0:
            result = torch.min(f, g)
        else:
            result = (
                1.0
                / (1.0 + alpha)
                * (f + g - torch.sqrt(f * f + g * g - 2 * alpha * f * g))
            )
        self.set_data(result)

    def intersection(self, lvl2dGrid: T, alpha: float = 0.0):
        """Performs intersection operator: self {cap} lvl2dGrid,

        Args:
            lvl2dGrid (Level2dGrid): The grid to perform the union with
            alpha (float, optional): parameter to control blending,
                       should be in the range [0,1], alpha=1.0 is equivalent
                       to boolean blending, i.e. torch.max(f, g). Defaults to 0.0.
        """
        f = self.get_data()
        g = lvl2dGrid.get_data()
        if alpha < 0.0 or alpha > 1.0:
            raise ValueError("alpha should be in the range [0,1].")
        if alpha == 1.0:
            result = torch.max(f, g)
        else:
            result = (
                1.0
                / (1.0 + alpha)
                * (f + g + torch.sqrt(f * f + g * g - 2 * alpha * f * g))
            )
        self.set_data(result)

    def subtraction(self, lvl2dGrid: T, alpha: float = 0.0):
        """Performs subtraction operator: self - lvl2dGrid,

        Args:
            lvl2dGrid (T): Grid which should be subtracted
            alpha (float, optional):  parameter to control blending,
                       should be in the range [0,1]. Defaults to 0.0.

        Raises:
            ValueError: [description]

        Returns:
            [type]: [description]
        """
        if alpha < 0.0 or alpha > 1.0:
            raise ValueError("alpha should be in the range [0,1].")
        data = lvl2dGrid.get_data()
        lvl2dGrid.set_data(-data)
        self.intersection(lvl2dGrid, alpha)

    def invert(self):
        """Inverts the level set,"""
        data = self.get_data()
        self.set_data(-data)
