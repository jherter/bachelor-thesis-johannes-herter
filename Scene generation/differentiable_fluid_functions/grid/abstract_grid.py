from abc import ABC, abstractmethod
import torch
import numpy as np
from typing import List, TypeVar

T = TypeVar("T", bound="AbstractGrid")


class AbstractGrid(ABC):
    """
    Abstract class defining all the methods required for grids

    """

    def __init__(
        self,
        data: torch.Tensor = None,
    ):
        # internal data
        self._data = data
        # on which device is the data stored

    @classmethod
    @abstractmethod
    def from_npy(
        cls,
        filename: str,
        device: torch.device = torch.device("cpu"),
    ):
        ...

    @classmethod
    @abstractmethod
    def from_npz(
        cls,
        filename: str,
        key: str,
        device: torch.device = torch.device("cpu"),
    ) -> T:
        ...

    @classmethod
    @abstractmethod
    def from_array(
        cls,
        array: np.ndarray,
        dtype=torch.float32,
        device: torch.device = torch.device("cpu"),
    ) -> T:
        ...

    @classmethod
    @abstractmethod
    def get_constant_grid(cls, value, simulation_size: List[int]) -> T:
        ...

    @classmethod
    def from_particle_data(cls) -> T:
        raise NotImplementedError(
            "This function is not implemented for this type of grid"
        )

    @property
    @abstractmethod
    def rank(self) -> int:
        ...

    @property
    @abstractmethod
    def tensor_to_npy_transformation_sequence(self) -> List[int]:
        ...

    @property
    @abstractmethod
    def npy_to_tensor_transformation_sequence(self) -> List[int]:
        ...

    # private methods
    @staticmethod
    @abstractmethod
    def _array2tensor(self, array: np.ndarray, dtype=torch.float32) -> torch.Tensor:
        ...

    # public methods
    @abstractmethod
    def grid2array(self) -> np.ndarray:
        ...

    @abstractmethod
    def save_npy(self, filename: str):
        ...

    @abstractmethod
    def trim_boundary(self, bwidth: int = 1):
        ...

    def get_data(self) -> torch.Tensor:
        """Return the raw tensor data of the grid

        Returns:
            torch.Tensor: the data
        """
        return self._data

    def get_numpy(self) -> np.ndarray:
        """Return the raw tensor data of the grid as an np nd array

        Returns:
            torch.Tensor: the data
        """
        return self._data.detach().cpu().numpy()

    def size(self) -> List[int]:
        """Return the size of the data

        Returns:
            List[int]: the shape of the grid
        """
        return list(self._data.shape)

    def dtype(self):
        """Return the dtype of the data

        Returns:
            :
        """
        return self._data.dtype

    def device(self) -> torch.device:
        """Return the device one which the data is stored

        Returns:
            torch.device: the device of the data
        """
        return self._data.device

    def set_data(self, data: torch.Tensor):
        """Update the raw tensor data of the grid

        Returns:
            torch.Tensor: the data
        """
        self._data = data

    def set_gradient_required(self, value: bool):
        """Set that this grid type requires a gradient

        Args:
            value (bool): flag if the gradient is required.
        """
        self._data.requires_grad = value

    def get_gradient(self) -> torch.Tensor:
        """Get the gradient of the grid

        Raises:
            ValueError: The gradient was set to not required

        Returns:
            torch.Tensor: The gradient
        """
        if not self._data.requires_grad:
            raise ValueError("The grid does not have a gradient")
        return self._data.grad
