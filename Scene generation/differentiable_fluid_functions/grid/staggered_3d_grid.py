from .abstract_3d_grid import Abstract3dGrid
import torch
import torch.nn.functional as F

import numpy as np
from typing import List, TypeVar, TYPE_CHECKING
from differentiable_fluid_functions.utils import (
    load_npz_raw,
    save_npz,
    create_3d_meshgrid_tensor,
    sample_grid_3d,
)
from differentiable_fluid_functions.grid import Real3dGrid

if TYPE_CHECKING:
    from differentiable_fluid_functions.grid import Node3dGrid

T = TypeVar("T", bound="Staggered3dGrid")
DATA_X_KEY = "DATA_X"
DATA_Y_KEY = "DATA_Y"
DATA_Z_KEY = "DATA_Z"


class Staggered3dGrid(Abstract3dGrid):
    def __init__(self, data_x: Real3dGrid, data_y: Real3dGrid, data_z: Real3dGrid):
        super(Staggered3dGrid, self).__init__(data=None)

        # check that the shape is valid
        shape_x = list(data_x.get_data().shape)
        shape_y = list(data_y.get_data().shape)
        shape_z = list(data_z.get_data().shape)
        shape_x[-1] -= 1
        shape_y[-2] -= 1
        shape_z[-3] -= 1
        if not np.array_equal(shape_x, shape_y) or not np.array_equal(shape_x, shape_z):
            raise ValueError(
                "The provided npy files have different simulation grids\n"
                f"data_x should be B x C x D     x H     x W + 1 currently: {list(data_x.get_data().shape)}\n"
                f"data_y should be B x C x D     x H + 1 x W currently: {list(data_y.get_data().shape)}\n"
                f"data_z should be B x C x D + 1 x H     x W currently: {list(data_z.get_data().shape)}\n"
            )

        # reduce the width by one to get the simulation shape
        self.shape = shape_x
        # save the data in separate tensors to avoid having dummy channels
        self._data_x = data_x
        self._data_y = data_y
        self._data_z = data_z

    @classmethod
    def from_tensor(
        cls, data_x: torch.tensor, data_y: torch.tensor, data_z: torch.tensor
    ) -> T:
        """Create a Staggered grid from 3 tensors

        Args:
            data_x (torch.tensor): Tensor of the x components
            data_y (torch.tensor): Tensor of the y components
            data_z (torch.tensor): Tensor of the z components


        Returns:
            Staggered3dGrid: Instance of the class
        """

        _data_x = Real3dGrid(data_x)
        _data_y = Real3dGrid(data_y)
        _data_z = Real3dGrid(data_z)

        return cls(_data_x, _data_y, _data_z)

    @classmethod
    def from_npy(
        cls,
        filename_x: str,
        filename_y: str,
        filename_z: str,
        device: torch.device = torch.device("cpu"),
    ) -> T:
        """Create a Staggered grid from 3 npy files

        Args:
            filename_x (str): Path to x components
            filename_y (str): Path to y components
            filename_z (str): Path to z components

        Returns:
            Staggered3dGrid: Instance of the class
        """

        data_x = Real3dGrid.from_npy(filename_x, device=device)
        data_y = Real3dGrid.from_npy(filename_y, device=device)
        data_z = Real3dGrid.from_npy(filename_z, device=device)

        return cls(data_x, data_y, data_z)

    @classmethod
    def from_npz(
        cls,
        filename: str,
        key_x: str = DATA_X_KEY,
        key_y: str = DATA_Y_KEY,
        key_z: str = DATA_Z_KEY,
        device: torch.device = torch.device("cpu"),
    ) -> T:
        """Create a Staggered grid from 3 npz files


        Args:
            filename (str): Path to npz file
            key_x (str): key for x component in the npz file
            key_y (str): key for y component in the npz file
            key_z (str): key for z component in the npz file

        Returns:
            Staggered3dGrid: Instance of the class
        """
        data_x = Real3dGrid.from_npz(filename, key_x, device=device)
        data_y = Real3dGrid.from_npz(filename, key_y, device=device)
        data_z = Real3dGrid.from_npz(filename, key_z, device=device)

        return cls(data_x, data_y, data_z)

    @classmethod
    def from_array(
        cls,
        array_x: np.ndarray,
        array_y: np.ndarray,
        array_z: np.ndarray,
        dtype=torch.float32,
        device: torch.device = torch.device("cpu"),
    ) -> T:
        """Create a staggered 3d grid from

        Args:
            array_x (np.ndarray): x data
            array_y (np.ndarray): y data
            array_z (np.ndarray): z data
            dtype ([type], optional): Required type of the tensor. Defaults to torch.float32.
            device (torch.device, optional): On which device to store the tensors. Defaults to torch.device("cpu").


        Returns:
            Staggered3dGrid: Instance of the class
        """
        data_x = Real3dGrid.from_array(array_x, dtype, device)
        data_y = Real3dGrid.from_array(array_y, dtype, device)
        data_z = Real3dGrid.from_array(array_z, dtype, device)

        return cls(data_x, data_y, data_z)

    @classmethod
    def get_constant_grid(
        cls,
        value_x: torch.Tensor,
        value_y: torch.Tensor,
        value_z: torch.Tensor,
        simulation_size: List[int],
    ) -> T:
        """Create a constant staggered 3d grid

        Args:
            value_x (torch.Tensor): value for the x data
            value_y (torch.Tensor): value for the y data
            value_z (torch.Tensor): value for the z data
            simulation_size (List[int]): The required simulation dimensions

        Returns:
            Staggered3dGrid: Instance of the class
        """
        dim_x = list(simulation_size).copy()
        dim_x[-1] += 1
        data_x = Real3dGrid.get_constant_grid(
            value=value_x,
            simulation_size=dim_x,
        )
        dim_y = list(simulation_size).copy()
        dim_y[-2] += 1
        data_y = Real3dGrid.get_constant_grid(
            value=value_y,
            simulation_size=dim_y,
        )
        dim_z = list(simulation_size).copy()
        dim_z[-3] += 1
        data_z = Real3dGrid.get_constant_grid(value=value_z, simulation_size=dim_z)
        return cls(data_x, data_y, data_z)

    @classmethod
    def from_real_3d_grid(cls, grid: Real3dGrid) -> T:
        """Create a staggered grid from a real grid

        Args:
            grid (Real3dGrid): holding 3d data for the staggered grid

        Raises:
            ValueError: The provided Real 3d grid does not contain enough channels

        Returns:
            Staggered3dGrid: Instance of the class
        """
        grid_data = grid.get_data()
        if grid_data.shape[1] < 3:
            raise ValueError(
                "At least 3 channels are required on the grid. Please use from_real_3d_grid_single_channel"
            )

        u_centered = grid_data[:, 0:1, ...]
        v_centered = grid_data[:, 1:2, ...]
        w_centered = grid_data[:, 2:3, ...]

        u_mac_middle = (u_centered[..., :-1] + u_centered[..., 1:]) * 0.5
        u_mac_neg_x = u_centered[..., 0:1] * 2 - u_mac_middle[..., 0:1]
        u_mac_pos_x = u_centered[..., -1:] * 2 - u_mac_middle[..., -1:]
        u_mac = torch.cat((u_mac_neg_x, u_mac_middle, u_mac_pos_x), dim=-1)

        v_mac_middle = (v_centered[..., :-1, :] + v_centered[..., 1:, :]) * 0.5
        v_mac_neg_y = v_centered[..., 0:1, :] * 2 - v_mac_middle[..., 0:1, :]
        v_mac_pos_y = v_centered[..., -1:, :] * 2 - v_mac_middle[..., -1:, :]
        v_mac = torch.cat((v_mac_neg_y, v_mac_middle, v_mac_pos_y), dim=-2)

        w_mac_middle = (w_centered[..., :-1, :, :] + w_centered[..., 1:, :, :]) * 0.5
        w_mac_neg_z = w_centered[..., 0:1, :, :] * 2 - w_mac_middle[..., 0:1, :, :]
        w_mac_pos_z = w_centered[..., -1:, :, :] * 2 - w_mac_middle[..., -1:, :, :]
        w_mac = torch.cat((w_mac_neg_z, w_mac_middle, w_mac_pos_z), dim=-3)

        data_x = Real3dGrid(u_mac)
        data_y = Real3dGrid(v_mac)
        data_z = Real3dGrid(w_mac)
        return cls(data_x, data_y, data_z)

    @classmethod
    def from_real_3d_grid_single_channel(cls, grid: Real3dGrid) -> T:
        """Create a staggered grid from a real grid with a single dim using the same dim for x,y and z

        Args:
            grid (Real3dGrid): holding 3d data for the staggered grid

        Raises:
            ValueError: The provided Real3dGrid does not contain wrong amount channels

        Returns:
            Staggered3dGrid: Instance of the class
        """
        grid_data = grid.get_data()
        if grid_data.shape[1] != 1:
            raise ValueError(
                "Only 1 channel is required on the grid. Otherwise please use from_real_3d_grid"
            )

        u_centered = grid_data[:, 0:1, ...]
        v_centered = grid_data[:, 0:1, ...]
        w_centered = grid_data[:, 0:1, ...]

        u_mac_middle = (u_centered[..., :-1] + u_centered[..., 1:]) * 0.5
        u_mac_neg_x = u_centered[..., 0:1] * 2 - u_mac_middle[..., 0:1]
        u_mac_pos_x = u_centered[..., -1:] * 2 - u_mac_middle[..., -1:]
        u_mac = torch.cat((u_mac_neg_x, u_mac_middle, u_mac_pos_x), dim=-1)

        v_mac_middle = (v_centered[..., :-1, :] + v_centered[..., 1:, :]) * 0.5
        v_mac_neg_y = v_centered[..., 0:1, :] * 2 - v_mac_middle[..., 0:1, :]
        v_mac_pos_y = v_centered[..., -1:, :] * 2 - v_mac_middle[..., -1:, :]
        v_mac = torch.cat((v_mac_neg_y, v_mac_middle, v_mac_pos_y), dim=-2)

        w_mac_middle = (w_centered[..., :-1, :, :] + w_centered[..., 1:, :, :]) * 0.5
        w_mac_neg_z = w_centered[..., 0:1, :, :] * 2 - w_mac_middle[..., 0:1, :, :]
        w_mac_pos_z = w_centered[..., -1:, :, :] * 2 - w_mac_middle[..., -1:, :, :]
        w_mac = torch.cat((w_mac_neg_z, w_mac_middle, w_mac_pos_z), dim=-3)

        data_x = Real3dGrid(u_mac)
        data_y = Real3dGrid(v_mac)
        data_z = Real3dGrid(w_mac)
        return cls(data_x, data_y, data_z)

    def to_real_3d_grid(self) -> Real3dGrid:
        """Converted a staggered grid to a realgrid

        Returns:
            Real3dGrid: Instance of the real grid
        """
        grid_x = self._data_x.get_data()
        grid_y = self._data_y.get_data()
        grid_z = self._data_z.get_data()
        u_centered = (grid_x[..., :-1] + grid_x[..., 1:]) * 0.5
        v_centered = (grid_y[..., :-1, :] + grid_y[..., 1:, :]) * 0.5
        w_centered = (grid_z[..., :-1, :, :] + grid_z[..., 1:, :, :]) * 0.5
        vel_centered = torch.cat((u_centered, v_centered, w_centered), dim=1)
        return Real3dGrid(vel_centered)

    def save_npy(self, filename_x: str, filename_y: str, filename_z: str):
        """Save the components as an npy file

        Args:
            filename_x (str): Filename of the data x npy file
            filename_y (str): Filename of the data y npy file
            filename_z (str): Filename of the data z npy file
        """
        self._data_x.save_npy(filename_x)
        self._data_y.save_npy(filename_y)
        self._data_z.save_npy(filename_z)

    def get_data(self) -> List[Real3dGrid]:
        """Return the raw tensor data of the grid as a list

        Returns:
            List[Real3dGrid]: contains data x, y and z
        """
        return [self._data_x, self._data_y, self._data_z]

    def get_numpy(self) -> List[np.ndarray]:
        """Return the raw tensor data of the grid as a list

        Returns:
            List[Real3dGrid]: contains data  x, y and z as np arrays
        """

        return [
            self._data_x.get_numpy(),
            self._data_y.get_numpy(),
            self._data_z.get_numpy(),
        ]

    def get_tensor(self) -> List[torch.tensor]:
        """Return the raw tensor data of the grid as a list

        Returns:
            List[torch.tensor]: contains data  x, y and z tensors
        """
        return [
            self._data_x.get_data(),
            self._data_y.get_data(),
            self._data_z.get_data(),
        ]

    def set_gradient_required(self, value: bool):
        """Set that this grid type requires a gradient

        Args:
            value (bool): flag if the gradient is required.
        """
        self._data_x.set_gradient_required(value)
        self._data_y.set_gradient_required(value)
        self._data_z.set_gradient_required(value)

    def grid2array(self) -> List[np.ndarray]:
        """Transform the grid components
            to an array with dimensions batch x depth x height x width x channel

        Returns:
            List[np.ndarray]: data of the grid
        """
        return [
            self._data_x.grid2array(),
            self._data_y.grid2array(),
            self._data_z.grid2array(),
        ]

    def save_single_npz(self, filename: str):
        """Save the staggered 3d grid a single npz file

        Args:
            filename (str): filename to save the npz file
        """
        save_dict = {}
        save_dict[DATA_X_KEY] = self._data_x.grid2array()
        save_dict[DATA_Y_KEY] = self._data_y.grid2array()
        save_dict[DATA_Z_KEY] = self._data_z.grid2array()

        save_npz(filename, save_dict)

    def trim_boundary(self, bwidth: int = 1):
        """Trim the boundaries from the grid

        Args:
            bwidth (int, optional): how much should be trimmed. Defaults to 1.
        """
        self._data_x.trim_boundary(bwidth)
        self._data_y.trim_boundary(bwidth)
        self._data_z.trim_boundary(bwidth)

    def get_gradient(self) -> List[torch.Tensor]:
        """Get the gradients of the grid components as a list

        Returns:
            List[torch.Tensor]: The gradient
        """
        return [
            self._data_x.get_gradient(),
            self._data_y.get_gradient(),
            self._data_z.get_gradient(),
        ]

    def get_divergence(self) -> Real3dGrid:
        """Compute the divergence of the staggered 3d grid

        Returns:
            Real3dGrid: divergence of a the simulation points
        """
        u = self._data_x.get_data()
        v = self._data_y.get_data()
        w = self._data_z.get_data()
        div = (
            u[..., 1:]
            - u[..., :-1]
            + v[..., 1:, :]
            - v[..., :-1, :]
            + w[..., 1:, :, :]
            - w[..., :-1, :, :]
        )
        return Real3dGrid(div)

    def grid_sample_center(self, back_trace: torch.Tensor = None) -> torch.Tensor:
        """Interpolate velocity field at the cell center position (simulation points).

        Args:
            back_trace (torch.Tensor, optional): Tensor defining which cells should be sampled. Defaults to None.

        Returns:
            torch.Tensor: Data sampled at the center [B, C, D, H, W]
        """
        if back_trace is None:
            back_trace = create_3d_meshgrid_tensor(
                self.shape,
                device=self.device(),
                dtype=self.dtype(),
            )

        grid_x = self._data_x.get_data()
        [_, _, depth, height, width] = back_trace.shape
        vel_x = sample_grid_3d(
            grid_x,
            torch.cat(
                (
                    back_trace[:, 0:1, ...] + 0.5,
                    back_trace[:, 1:2, ...],
                    back_trace[:, 2:3, ...],
                ),
                dim=1,
            ),
            width,
            height - 1,
            depth - 1,
        )

        grid_y = self._data_y.get_data()
        vel_y = sample_grid_3d(
            grid_y,
            torch.cat(
                (
                    back_trace[:, 0:1, ...],
                    back_trace[:, 1:2, ...] + 0.5,
                    back_trace[:, 2:3, ...],
                ),
                dim=1,
            ),
            width - 1,
            height,
            depth - 1,
        )

        grid_z = self._data_z.get_data()
        vel_z = sample_grid_3d(
            grid_z,
            torch.cat(
                (
                    back_trace[:, 0:1, ...],
                    back_trace[:, 1:2, ...],
                    back_trace[:, 2:3, ...] + 0.5,
                ),
                dim=1,
            ),
            width - 1,
            height - 1,
            depth,
        )
        vel = torch.cat((vel_x, vel_y, vel_z), dim=1)

        return vel

    def grid_sample_node(self, back_trace: torch.Tensor = None) -> torch.Tensor:
        raise NotImplementedError("grid_sample_node")

    def grid_sample_x(self, back_trace: torch.Tensor = None):
        """Interpolate velocity field at the mac-x position.

        Args:
            back_trace (torch.Tensor, optional): Tensor defining which cells should be sampled. Defaults to None.

        Returns:
            torch.Tensor: Data sampled at the mac-x position [B, C, D, H, W + 1]
        """

        if back_trace is None:
            [batch, channel, depth, height, width] = self.shape
            back_trace = create_3d_meshgrid_tensor(
                [batch, channel, depth, height, width + 1],
                device=self.device(),
                dtype=self.dtype(),
            )
        [batch, channel, depth, height, width] = back_trace.shape
        grid_x = self._data_x.get_data()
        vel_x = sample_grid_3d(grid_x, back_trace, width - 1, height - 1, depth - 1)

        vel_y = self._get_sample_x(back_trace)

        grid_z = self._data_z.get_data()
        vel_z = sample_grid_3d(
            grid_z,
            torch.cat(
                (
                    back_trace[:, 0:1, ...] - 0.5,
                    back_trace[:, 1:2, ...],
                    back_trace[:, 2:3, ...] + 0.5,
                ),
                dim=1,
            ),
            width - 2,
            height - 1,
            depth,
        )
        vel = torch.cat((vel_x, vel_y, vel_z), dim=1)

        return vel

    def _get_sample_x(self, back_trace: torch.Tensor = None):
        """Private function sampling the y data at the mac x data positions

        Args:
            back_trace (torch.Tensor, optional): Tensor defining which cells should be sampled. Defaults to None.

        Returns:
            torch.Tensor: Data sampled at the mac-x position [B, C, D, H, W + 1]
        """
        grid_y = self._data_y.get_data()

        if back_trace is None:
            [batch, channel, depth, height, width] = self.shape
            back_trace = create_3d_meshgrid_tensor(
                [batch, channel, depth, height, width + 1],
                device=self.device(),
                dtype=self.dtype(),
            )
        back_trace = torch.cat(
            (
                back_trace[:, 0:1, ...] - 0.5,
                back_trace[:, 1:2, ...] + 0.5,
                back_trace[:, 2:3, ...],
            ),
            dim=1,
        )
        [batch, channel, depth, height, width] = back_trace.shape
        grid_mac_x = sample_grid_3d(grid_y, back_trace, width - 2, height, depth - 1)

        return grid_mac_x

    def grid_sample_y(self, back_trace: torch.Tensor = None) -> torch.Tensor:
        """Interpolate velocity field at the mac-y position.

        Args:
            back_trace (torch.Tensor, optional): Tensor defining which cells should be sampled. Defaults to None.

        Returns:
            torch.Tensor: Data sampled at the mac-y position [B, C, D, H + 1, W]
        """
        if back_trace is None:
            [batch, channel, depth, height, width] = self.shape
            back_trace = create_3d_meshgrid_tensor(
                [batch, channel, depth, height + 1, width],
                device=self.device(),
                dtype=self.dtype(),
            )
        [batch, channel, depth, height, width] = back_trace.shape
        vel_x = self._get_sample_y(back_trace)

        grid_y = self._data_y.get_data()
        vel_y = sample_grid_3d(grid_y, back_trace, width - 1, height - 1, depth - 1)

        grid_z = self._data_z.get_data()
        vel_z = sample_grid_3d(
            grid_z,
            torch.cat(
                (
                    back_trace[:, 0:1, ...],
                    back_trace[:, 1:2, ...] - 0.5,
                    back_trace[:, 2:3, ...] + 0.5,
                ),
                dim=1,
            ),
            width - 1,
            height - 2,
            depth,
        )
        vel = torch.cat((vel_x, vel_y, vel_z), dim=1)
        return vel

    def grid_sample_z(self, back_trace=None):
        """Interpolate velocity field at the mac-z position.

        Args:
            back_trace (torch.Tensor, optional): Tensor defining which cells should be sampled. Defaults to None.

        Returns:
            torch.Tensor: Data sampled at the mac-z position [B, C, D + 1, H , W]
        """
        if back_trace is None:
            [batch, channel, depth, height, width] = self.shape
            back_trace = create_3d_meshgrid_tensor(
                [batch, channel, depth + 1, height, width],
                device=self.device(),
                dtype=self.dtype(),
            )
        [batch, channel, depth, height, width] = back_trace.shape
        vel_x = sample_grid_3d(
            self._data_x.get_data(),
            torch.cat(
                (
                    back_trace[:, 0:1, ...] + 0.5,
                    back_trace[:, 1:2, ...],
                    back_trace[:, 2:3, ...] - 0.5,
                ),
                dim=1,
            ),
            width,
            height - 1,
            depth - 2,
        )
        vel_y = sample_grid_3d(
            self._data_y.get_data(),
            torch.cat(
                (
                    back_trace[:, 0:1, ...],
                    back_trace[:, 1:2, ...] + 0.5,
                    back_trace[:, 2:3, ...] - 0.5,
                ),
                dim=1,
            ),
            width - 1,
            height,
            depth - 2,
        )
        vel_z = sample_grid_3d(
            self._data_z.get_data(), back_trace, width - 1, height - 1, depth - 1
        )
        vel = torch.cat((vel_x, vel_y, vel_z), dim=1)
        return vel

    def _get_sample_y(self, back_trace: torch.Tensor = None) -> torch.Tensor:
        """Private function sampling the X data at the mac-y data positions

        Args:
            back_trace (torch.Tensor, optional): Tensor defining which cells should be sampled. Defaults to None.

        Returns:
            torch.Tensor: Data sampled at the mac-y position [B, C, D, H + 1, W]
        """
        grid_x = self._data_x.get_data()

        if back_trace is None:
            [batch, channel, depth, height, width] = self.shape

            back_trace = create_3d_meshgrid_tensor(
                [batch, channel, depth, height + 1, width],
                device=self.device(),
                dtype=self.dtype(),
            )

        back_trace = torch.cat(
            (
                back_trace[:, 0:1, ...] + 0.5,
                back_trace[:, 1:2, ...] - 0.5,
                back_trace[:, 2:3, ...],
            ),
            dim=1,
        )
        [batch, channel, depth, height, width] = back_trace.shape
        grid_mac_y = sample_grid_3d(grid_x, back_trace, width, height - 2, depth - 1)

        return grid_mac_y

    def get_energy(self) -> torch.Tensor:
        """Get the energy of the staggered grid at the center positions

        Returns:
            torch.Tensor: Tensor holding the energies [B X 1 X D X H X W]
        """
        grid_center = self.grid_sample_center()
        energy = (
            grid_center[:, 0:1, ...] ** 2
            + grid_center[:, 1:2, ...] ** 2
            + grid_center[:, 2:3, ...] ** 2
        )
        return energy

    def get_staggered_x(self) -> torch.Tensor:
        """Interpolate velocity field at the mac-x position.
            Faster since positions are fixed
        Returns:
            torch.Tensor: Data sampled at the mac-y position [B, C, D, H, W + 1]
        """
        u_mac_x = self._data_x.get_data()
        v_mac_y = self._data_y.get_data()
        w_mac_z = self._data_z.get_data()
        pad_x_neg = (1, 1, 0, 0, 0, 0)

        v_mac_y_pad_x = F.pad(v_mac_y, pad=pad_x_neg, mode="replicate")
        v_centered = (v_mac_y_pad_x[..., 1:] + v_mac_y_pad_x[..., :-1]) * 0.5
        v_mac_x = (v_centered[..., 1:, :] + v_centered[..., :-1, :]) * 0.5

        w_mac_z_pad_x = F.pad(w_mac_z, pad=pad_x_neg, mode="replicate")
        w_centered = (w_mac_z_pad_x[..., 1:] + w_mac_z_pad_x[..., :-1]) * 0.5
        w_mac_x = (w_centered[..., 1:, :, :] + w_centered[..., :-1, :, :]) * 0.5

        vel_mac_x = torch.cat((u_mac_x, v_mac_x, w_mac_x), dim=1)
        return vel_mac_x

    def get_staggered_y(self) -> torch.Tensor:
        """Interpolate velocity field at the mac-y position.
            Faster since positions are fixed

        Returns:
            torch.Tensor: Data sampled at the mac-y position [B, C, D, H + 1, W]
        """
        u_mac_x = self._data_x.get_data()
        v_mac_y = self._data_y.get_data()
        w_mac_z = self._data_z.get_data()

        pad_y_neg = (0, 0, 1, 1, 0, 0)

        u_mac_x_pad_y = F.pad(u_mac_x, pad=pad_y_neg, mode="replicate")
        u_centered = (u_mac_x_pad_y[..., 1:, :] + u_mac_x_pad_y[..., :-1, :]) * 0.5
        u_mac_y = (u_centered[..., 1:] + u_centered[..., :-1]) * 0.5

        w_mac_z_pad_y = F.pad(w_mac_z, pad=pad_y_neg, mode="replicate")
        w_centered = (w_mac_z_pad_y[..., 1:, :] + w_mac_z_pad_y[..., :-1, :]) * 0.5
        w_mac_y = (w_centered[..., 1:, :, :] + w_centered[..., :-1, :, :]) * 0.5

        vel_mac_y = torch.cat((u_mac_y, v_mac_y, w_mac_y), dim=1)
        return vel_mac_y

    def get_staggered_z(self) -> torch.Tensor:
        """Interpolate velocity field at the mac-y position.
            Faster since positions are fixed

        Returns:
            torch.Tensor: Data sampled at the mac-y position [B, C, D + 1, H, W]
        """
        u_mac_x = self._data_x.get_data()
        v_mac_y = self._data_y.get_data()
        w_mac_z = self._data_z.get_data()

        pad_z_neg = (0, 0, 0, 0, 1, 1)

        u_mac_x_pad_z = F.pad(u_mac_x, pad=pad_z_neg, mode="replicate")

        u_centered = (
            u_mac_x_pad_z[..., 1:, :, :] + u_mac_x_pad_z[..., :-1, :, :]
        ) * 0.5
        u_mac_z = (u_centered[..., 1:] + u_centered[..., :-1]) * 0.5

        v_mac_y_pad_z = F.pad(v_mac_y, pad=pad_z_neg, mode="replicate")
        v_centered = (
            v_mac_y_pad_z[..., 1:, :, :] + v_mac_y_pad_z[..., :-1, :, :]
        ) * 0.5
        v_mac_z = (v_centered[..., 1:, :] + v_centered[..., :-1, :]) * 0.5

        vel_mac_y = torch.cat((u_mac_z, v_mac_z, w_mac_z), dim=1)
        return vel_mac_y

    def get_centered(self) -> torch.Tensor:
        """Interpolate velocity field at the center position.
            Faster since positions are fixed
        Returns:
            torch.Tensor: Data sampled at the mac-y position [B, C, D, H, W]
        """
        u_mac = self._data_x.get_data()
        v_mac = self._data_y.get_data()
        w_mac = self._data_z.get_data()
        u_centered = (u_mac[..., :-1] + u_mac[..., 1:]) * 0.5
        v_centered = (v_mac[..., :-1, :] + v_mac[..., 1:, :]) * 0.5
        w_centered = (w_mac[..., :-1, :, :] + w_mac[..., 1:, :, :]) * 0.5
        vel_centered = torch.cat((u_centered, v_centered, w_centered), dim=1)
        return vel_centered

    def get_curl(self) -> "Node3dGrid":
        """Compute the curl of a staggered grid


        Returns:
            Node3dGrid: xyz components of the curl
        """
        from differentiable_fluid_functions.grid import Node3dGrid

        u = self._data_x.get_data()
        v = self._data_y.get_data()
        w = self._data_z.get_data()

        padx = (1, 1, 0, 0, 0, 0)
        pady = (0, 0, 1, 1, 0, 0)
        padz = (0, 0, 0, 0, 1, 1)

        u_pady = F.pad(u, pad=pady, mode="replicate")
        u_padz = F.pad(u, pad=padz, mode="replicate")
        v_padx = F.pad(v, pad=padx, mode="replicate")
        v_padz = F.pad(v, pad=padz, mode="replicate")
        w_padx = F.pad(w, pad=padx, mode="replicate")
        w_pady = F.pad(w, pad=pady, mode="replicate")

        # vort_x = dw/dy - dv/dz
        dw_dy = w_pady[..., 1:, :] - w_pady[..., :-1, :]
        dv_dz = v_padz[..., 1:, :, :] - v_padz[..., :-1, :, :]
        vort_x = dw_dy - dv_dz

        # vort_y = du/dz - dw/dx
        du_dz = u_padz[..., 1:, :, :] - u_padz[..., :-1, :, :]
        dw_dx = w_padx[..., 1:] - w_padx[..., :-1]
        vort_y = du_dz - dw_dx

        dv_dx = v_padx[..., 1:] - v_padx[..., :-1]
        du_dy = u_pady[..., 1:, :] - u_pady[..., :-1, :]
        vort_z = dv_dx - du_dy

        return Node3dGrid.from_tensor(data_x=vort_x, data_y=vort_y, data_z=vort_z)

    def get_jacobian(self):
        vel_mac_x = self.get_staggered_x()
        vel_mac_y = self.get_staggered_y()
        vel_mac_z = self.get_staggered_z()
        u_mac_x = vel_mac_x[:, 0:1, :, :]
        v_mac_x = vel_mac_x[:, 1:2, :, :]
        w_mac_x = vel_mac_x[:, 2:3, :, :]
        u_mac_y = vel_mac_y[:, 0:1, :, :]
        v_mac_y = vel_mac_y[:, 1:2, :, :]
        w_mac_y = vel_mac_y[:, 2:3, :, :]
        u_mac_z = vel_mac_z[:, 0:1, :, :]
        v_mac_z = vel_mac_z[:, 1:2, :, :]
        w_mac_z = vel_mac_z[:, 2:3, :, :]
        du_dx = u_mac_x[:, :, :, :, 1:] - u_mac_x[:, :, :, :, :-1]
        dv_dx = v_mac_x[:, :, :, :, 1:] - v_mac_x[:, :, :, :, :-1]
        dw_dx = w_mac_x[:, :, :, :, 1:] - w_mac_x[:, :, :, :, :-1]
        du_dy = u_mac_y[:, :, :, 1:, :] - u_mac_y[:, :, :, :-1, :]
        dv_dy = v_mac_y[:, :, :, 1:, :] - v_mac_y[:, :, :, :-1, :]
        dw_dy = w_mac_y[:, :, :, 1:, :] - w_mac_y[:, :, :, :-1, :]
        du_dz = u_mac_z[:, :, 1:, :, :] - u_mac_z[:, :, :-1, :, :]
        dv_dz = v_mac_z[:, :, 1:, :, :] - v_mac_z[:, :, :-1, :, :]
        dw_dz = w_mac_z[:, :, 1:, :, :] - w_mac_z[:, :, :-1, :, :]
        dvel_dx = torch.cat((du_dx, dv_dx, dw_dx), dim=1)
        dvel_dy = torch.cat((du_dy, dv_dy, dw_dy), dim=1)
        dvel_dz = torch.cat((du_dz, dv_dz, dw_dz), dim=1)
        jacobian = torch.cat(
            (dvel_dx.unsqueeze(2), dvel_dy.unsqueeze(2), dvel_dz.unsqueeze(2)), dim=2
        )
        return jacobian

    def get_q_criterion(self):
        jacobian = self.get_jacobian()
        jacobian_t = jacobian.permute((0, 2, 1, 3, 4, 5))
        s = 0.5 * (jacobian + jacobian_t)
        omega = 0.5 * (jacobian - jacobian_t)
        q = 0.5 * (
            torch.linalg.norm(omega, dim=(1, 2)) - torch.linalg.norm(s, dim=(1, 2))
        ).unsqueeze(1)
        return Real3dGrid(q)

    def get_taylor_vortices(
        self,
        phi_obs,
        a=0.08,
        U=0.06,
        center_list=[torch.tensor([-0.15, 0]), torch.tensor([0.15, 0])],
    ):
        raise NotImplementedError("IMPL: get_taylor_vortices")

    def get_galilean_ambient(self, grid, kernel_size=3, sigma=1):
        raise NotImplementedError("IMPL: get_galilean_ambient")

    def size(self):
        return self.shape

    def __sub__(self, other):
        return Staggered3dGrid(
            data_x=self._data_x - other._data_x,
            data_y=self._data_y - other._data_y,
            data_z=self._data_z - other._data_z,
        )

    def __add__(self, other):
        return Staggered3dGrid(
            data_x=self._data_x + other._data_x,
            data_y=self._data_y + other._data_y,
            data_z=self._data_z + other._data_z,
        )

    def __mul__(self, other):
        if not hasattr(other, "__len__"):
            return Staggered3dGrid(
                data_x=self._data_x * other,
                data_y=self._data_y * other,
                data_z=self._data_z * other,
            )
        if len(other) == 3:
            return Staggered3dGrid(
                data_x=self._data_x * other[0],
                data_y=self._data_y * other[1],
                data_z=self._data_z * other[2],
            )
        raise ValueError(f"Mult with type {type(other)} is not supported")

    def __rmul__(self, other):
        return self.__mul__(other)

    def dtype(self):
        """Return the dtype of the data

        Returns:
            :
        """
        return self._data_x.dtype()

    def device(self) -> torch.device:
        """Return the device one which the data is stored

        Returns:
            torch.device: the device of the data
        """
        return self._data_x.device()
