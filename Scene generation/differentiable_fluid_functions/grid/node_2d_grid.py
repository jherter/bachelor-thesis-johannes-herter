from .abstract_2d_grid import Abstract2dGrid
import torch

import numpy as np
from typing import List, TypeVar, TYPE_CHECKING
from differentiable_fluid_functions.utils import (
    load_npy,
    load_npz,
    check_numpy_float,
    check_torch_float,
    create_2d_meshgrid_tensor,
    normalize_2d_meshgrid_tensor,
    particle_to_grid,
)

from differentiable_fluid_functions.types import GridType

if TYPE_CHECKING:
    from differentiable_fluid_functions.particles import (  # pragma: no cover
        ParticleDatabase,
    )
    from differentiable_fluid_functions.kernels import (  # pragma: no cover
        AbstractKernel,
    )
    from differentiable_fluid_functions.grid import Staggered2dGrid
T = TypeVar("T", bound="Node2dGrid")


class Node2dGrid(Abstract2dGrid):
    def __init__(self, data: torch.Tensor, *args, **kwargs):
        if not check_torch_float(data.dtype):
            raise TypeError(
                f"Data must me of type torch.float<16, 32, 64> currently: {data.dtype}"
            )
        super(Node2dGrid, self).__init__(data, *args, **kwargs)

    @classmethod
    def from_npy(cls, filename: str, device: torch.device = torch.device("cpu")) -> T:
        """Create a node 2d grid from a npy file

        Args:
            filename (str): path to the file

        Raises:
            TypeError: if the content of the file is not float32 or float64

        Returns:
            T: An instance of Node2dGrid
        """
        array = load_npy(filename)
        if not check_numpy_float(array.dtype):
            raise TypeError(
                f"{filename} has data with the wrong format: {array.dtype} required np.float<32, 64>"
            )
        dtype = torch.float32 if array.dtype == np.float32 else torch.float64
        return cls.from_array(array, dtype=dtype, device=device)

    @classmethod
    def from_npz(
        cls, filename: str, key: str, device: torch.device = torch.device("cpu")
    ) -> T:
        """Create a node 2d grid from a npz file

        Args:
            filename (str): path to the file

        Raises:
            TypeError: if the content of the file is not float32 or float64

        Returns:
            T: An instance of Node2dGrid
        """
        array = load_npz(filename, key)
        if not check_numpy_float(array.dtype):
            raise TypeError(
                f"{filename} has data with the wrong format: {array.dtype} required np.float<32, 64>"
            )
        dtype = torch.float32 if array.dtype == np.float32 else torch.float64
        return cls.from_array(array, dtype=dtype, device=device)

    @classmethod
    def get_constant_grid(
        cls,
        value: torch.Tensor,
        simulation_size: List[int],
        **kwargs,
    ) -> T:
        """Creates a constant node 2d grid

        Args:
            value (torch.Tensor): Which value the grid should have
            simulation_size (List[int]): The dimensions of the simulation

        Raises:
            ValueError: If the dimension are not correct
            TypeError: if the type of the value is not correct

        Returns:
            T: An instance of Node2dGrid
        """
        if len(simulation_size) != 4:
            raise ValueError(
                "For 2d array the 4 dim data is required eg. batch x channel x Height + 1 x Width + 1"
            )
        if not check_torch_float(value.dtype) or len(value.shape) != 0:
            raise TypeError(
                f"Value must me of type one of [torch.float16, torch.float32, torch.float64] current {value.dtype}\n"
                f"Values dim must be [] currently {value.shape}"
            )
        b, c, h, w = simulation_size
        tensor = (
            torch.ones([b, c, h + 1, w + 1], dtype=value.dtype, device=value.device)
            * value
        )
        return cls(tensor)

    @classmethod
    def get_gaussian(
        cls,
        mu: torch.Tensor,
        sig: torch.Tensor,
        amp: torch.Tensor,
        simulation_size: List[int],
    ) -> T:
        """Create a node grid with a given gaussian distribution

        Args:
            mu (torch.Tensor): Mean of the 2-dimensional distribution.
            sig (torch.Tensor): Covariance matrix of the distribution. It must be symmetric and positive-semidefinite for proper sampling.
            amp (torch.Tensor): Amplifications factor
            simulation_size (List[int]): dimensions of the grid

        Raises:
            ValueError: Mu, sigma or amp have the wrong dimensions
            ValueError: Provided Dimensions are wrong

        Returns:
            Node2dGrid: A grid with a given gaussian distribution
        """
        if not (
            mu.size(0) == 2
            and sig.size(0) == 2
            and sig.size(1) == 2
            and len(amp.shape) == 0
        ):
            raise ValueError(
                "Dimensions of mu/sig/amp do match"
                "mu = [x, y], sig = [[x, y], [x, y]], amp = value"
                f"currently: {mu.shape}, {sig.shape} amd {amp.shape}"
            )

        if len(simulation_size) != 4:
            raise ValueError("Dimensions must be set to [bxcxhxw]")

        sig_inv = torch.inverse(sig)
        # inver data type and device from mu
        device = mu.device
        dtype = mu.dtype

        mgrid = create_2d_meshgrid_tensor(
            simulation_size,
            device=device,
            dtype=dtype,
        )

        mgrid = normalize_2d_meshgrid_tensor(
            mgrid,
            simulation_size[-1],
            simulation_size[-2],
        )
        x0 = mgrid[:, 0:1, ...]
        x1 = mgrid[:, 1:2, ...]
        mahalanobis_dist_2 = (
            sig_inv[0, 0] * (x0 - mu[0]) * (x0 - mu[0])
            + sig_inv[0, 1] * (x0 - mu[0]) * (x1 - mu[1])
            + sig_inv[1, 0] * (x1 - mu[1]) * (x0 - mu[0])
            + sig_inv[1, 1] * (x1 - mu[1]) * (x1 - mu[1])
        )
        gauss = amp * torch.exp(-0.5 * mahalanobis_dist_2)
        return cls(gauss)

    @classmethod
    def from_particle_data(
        cls,
        data: "ParticleDatabase",
        attribute_name: str,
        simulation_size: torch.Tensor,
        kernel: "AbstractKernel",
        normalization: bool,
        filter_particles_out_of_bounds: bool = False,
    ) -> T:
        """Create a nodegrid from particle data

        Args:
            data (ParticleDatabase): A Particle database where the data with attribute_name is stored
            attribute_name (str): under which key the data is stored
            simulation_size (torch.Tensor): size of the simulation
            kernel (AbstractKernel): Which kernel should be used to splat the particle data to the grid

        Raises:
            ValueError: Wrong simulation size
            ValueError: The database does not contain 2d data

        Returns:
            Node2dGrid: With the data from the particles
        """
        if len(simulation_size) != 2:
            raise ValueError(
                "Dimensions must be set to [h, w] (used for every element in the batch)"
            )
        # particle_to_nodegrid
        particle_data = data.get_tensor(attribute_name)
        particle_postions = data.get_tensor(data.POSITIONS_KEY)
        if particle_postions.shape[-1] != 2:
            raise ValueError("A 2d Nodegrid requires 2d particle positions")

        values, _ = particle_to_grid(
            particle_data=particle_data,
            particle_positions=particle_postions,
            simulation_size=simulation_size,
            domain_bounding_box=data.get_domain_bounding_box(),
            kernel=kernel,
            grid_type=GridType.NODE,
            normalization=normalization,
            filter_particles_out_of_bounds=filter_particles_out_of_bounds,
        )
        return cls(values)

    def get_spatial_gradient(self) -> List[torch.Tensor]:
        """Compute the spatial gradient of the grid

        Returns:
            List[torch.Tensor]:
        """
        grid = self._data
        grad_x = grid[..., 1:] - grid[..., :-1]
        grad_y = grid[..., 1:, :] - grid[..., :-1, :]

        return [grad_x, grad_y]

    def get_curl(self) -> "Staggered2dGrid":
        """Compute the curl of the grid

        Returns:
            Staggered2dGrid: The xy data component of the curl of the node grid
        """
        from differentiable_fluid_functions.grid import Staggered2dGrid

        grid = self._data
        grad_x = -(grid[..., 1:] - grid[..., :-1])
        grad_y = grid[..., 1:, :] - grid[..., :-1, :]

        return Staggered2dGrid.from_tensor(
            data_x=grad_y,
            data_y=grad_x,
        )

    def get_centered(self) -> torch.Tensor:
        """Interpolate node grid at the center position.
        Returns:
            torch.Tensor: Data sampled at the cell center position [B, C, H, W]
        """
        return (
            self._data[..., 1:, 1:]
            + self._data[..., :-1, 1:]
            + self._data[..., 1:, :-1]
            + self._data[..., :-1, :-1]
        ) * 0.25

    def get_inverse_curl(self, phi_obs, vort, cg_accu=1e-10):
        raise NotImplementedError("IMPL: get_inverse_curl")
