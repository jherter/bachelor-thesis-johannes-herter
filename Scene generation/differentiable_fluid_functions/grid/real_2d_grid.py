from .abstract_2d_grid import Abstract2dGrid
import torch
import torch.nn.functional as F

import numpy as np
from typing import List, TypeVar, TYPE_CHECKING
from differentiable_fluid_functions.utils import (
    load_npy,
    load_npz,
    check_torch_float,
    check_numpy_float,
    particle_to_grid,
    create_2d_gaussian_kernel,
)
from differentiable_fluid_functions.grid import Flag2dGrid, CellType

from differentiable_fluid_functions.types import GridType

if TYPE_CHECKING:
    from differentiable_fluid_functions.particles import (  # pragma: no cover
        ParticleDatabase,
    )
    from differentiable_fluid_functions.kernels import (  # pragma: no cover
        AbstractKernel,
    )
    from differentiable_fluid_functions.grid import Staggered2dGrid

T = TypeVar("T", bound="Real2dGrid")


class Real2dGrid(Abstract2dGrid):
    def __init__(self, data):
        if not check_torch_float(data.dtype):
            raise TypeError(
                f"Data must me of type torch.float<16, 32, 64> currently: {data.dtype}"
            )
        super(Real2dGrid, self).__init__(data)

    @classmethod
    def from_npy(cls, filename: str, device: torch.device = torch.device("cpu")) -> T:
        """Create a Real 2d grid from a npy file

        Args:
            filename (str): path to the file

        Raises:
            TypeError: if the content of the file is not float32 or float64

        Returns:
            T: An instance of Real2dGrid
        """
        array = load_npy(filename)
        if not check_numpy_float(array.dtype):
            raise TypeError(
                f"{filename} has data with the wrong format: {array.dtype} required np.float<32, 64>"
            )
        dtype = torch.float32 if array.dtype == np.float32 else torch.float64
        return cls.from_array(array, dtype=dtype, device=device)

    @classmethod
    def from_npz(
        cls, filename: str, key: str, device: torch.device = torch.device("cpu")
    ) -> T:
        """Create a Real 2d grid from a npz file

        Args:
            filename (str): path to the file

        Raises:
            TypeError: if the content of the file is not float32 or float64

        Returns:
            T: An instance of Real2dGrid
        """
        array = load_npz(filename, key)
        if not check_numpy_float(array.dtype):
            raise TypeError(
                f"{filename} has data with the wrong format: {array.dtype} required np.float<32, 64>"
            )
        dtype = torch.float32 if array.dtype == np.float32 else torch.float64
        return cls.from_array(array, dtype=dtype, device=device)

    @classmethod
    def get_constant_grid(cls, value: torch.Tensor, simulation_size: List[int]) -> T:
        """Creates a constant real 2d grid

        Args:
            value (torch.Tensor): Which value the grid should have
            simulation_size (List[int]): The dimensions of the grid

        Raises:
            ValueError: If the dimension are not correct
            TypeError: if the type of the value is not correct

        Returns:
            T: An instance of Real2dGrid
        """
        if len(simulation_size) != 4:
            raise ValueError(
                "For 2d array the 4 dim data is required eg. batch x channel x Height x Width"
            )
        if not check_torch_float(value.dtype) or len(value.shape) != 0:
            raise TypeError(
                f"Value must me of type one of [torch.float16, torch.float32, torch.float64] current {value.dtype}\n"
                f"Values dim must be [] currently {value.shape}"
            )
        tensor = (
            torch.ones(simulation_size, dtype=value.dtype, device=value.device) * value
        )
        return cls(tensor)

    @classmethod
    def from_flag_2d_grid(
        cls,
        flag2dGrid: Flag2dGrid,
        value: torch.Tensor,
        flag: CellType = CellType.OBSTACLE,
    ) -> T:
        """Create a Real 2d grid from a Flag2dGrid with a given value

        Args:
            flag2dGrid (Flag2dGrid): Grid indicating where the value should be placed
            value (torch.Tensor): Value that should be used at FlagGrid positions


        Returns:
            T: An instance of Real2dGrid
        """
        if not check_torch_float(value.dtype) or len(value.shape) != 0:
            raise TypeError(
                f"Value must me of type one of [torch.float16, torch.float32, torch.float64] current {value.dtype}\n"
                f"Values dim must be [] currently {value.shape}"
            )
        # convert to torch.bool due to warning
        flags = flag2dGrid.get_data() == int(flag)
        data = torch.zeros_like(flags, dtype=value.dtype, device=value.device)
        data[flags] = value
        return cls(data)

    @classmethod
    def from_particle_data(
        cls,
        data: "ParticleDatabase",
        attribute_name: str,
        simulation_size: torch.Tensor,
        kernel: "AbstractKernel",
        normalization: bool = False,
        filter_particles_out_of_bounds: bool = False,
    ) -> T:
        """Create a realGrid from particle data

        Args:
            data (ParticleDatabase): A Particle database where the data is stored
            attribute_name (str): under which key the data is stored
            simulation_size (torch.Tensor): size of the simulation [h, w]
            kernel (AbstractKernel): Which kernel should be used to splat the particle data to the grid

        Raises:
            ValueError: Wrong simulation size
            ValueError: The database does not contain 2d data

        Returns:
            Real2dGrid: With the data from the particles
        """
        if len(simulation_size) != 2:
            raise ValueError(
                "Dimensions must be set to [h, w] (used for every element in the batch)"
            )
        # particle_to_nodegrid
        particle_data = data.get_tensor(attribute_name)
        particle_postions = data.get_tensor(data.POSITIONS_KEY)
        if particle_postions.shape[-1] != 2:
            raise ValueError("A Real2dGrid requires 2d particle positions")

        values, _ = particle_to_grid(
            particle_data=particle_data,
            particle_positions=particle_postions,
            simulation_size=simulation_size,
            domain_bounding_box=data.get_domain_bounding_box(),
            kernel=kernel,
            grid_type=GridType.REAL,
            normalization=normalization,
            filter_particles_out_of_bounds=filter_particles_out_of_bounds,
        )
        return cls(values)

    def get_spatial_gradient(self) -> "Staggered2dGrid":

        """Compute the spatial gradient of the vector field returns components for a staggered grid

        Returns:
            Staggered2dGrid:
                Spatial gradient
        """
        from differentiable_fluid_functions.grid import Staggered2dGrid

        grid_pad = F.pad(self._data, pad=(1, 1, 1, 1), mode="replicate")

        grad_x = grid_pad[:, :, 1:-1, 1:] - grid_pad[:, :, 1:-1, :-1]
        grad_y = grid_pad[:, :, 1:, 1:-1] - grid_pad[:, :, :-1, 1:-1]
        return Staggered2dGrid.from_tensor(data_x=grad_x, data_y=grad_y)

    def get_curl(self) -> torch.Tensor:
        """Compute the curl of the grid
            In 2D case, the data is a scalar field, equivalently on z direction
            curl(phi) = (dphi/dy, -dphi/dx)
        Returns:
            torch.Tensor: The curl of the grid
        """
        # TODO: this probably needs to return staggered grid compos as well (refactor me)
        grid_pad = F.pad(self._data, pad=(1, 1, 1, 1), mode="replicate")
        #
        #
        grad_x = grid_pad[:, :, 1:, 1:] - grid_pad[:, :, 1:, :-1]
        grad_y = grid_pad[:, :, 1:, 1:] - grid_pad[:, :, :-1, 1:]
        curl = torch.cat((grad_y, -grad_x), dim=1)
        return curl

    def get_staggered_x(self) -> torch.Tensor:
        """Interpolate real grid at the mac-x position.

        Returns:
            torch.Tensor: Data sampled at the mac-y position [B, C, H, W + 1]
        """
        grid_pad_x = F.pad(self._data, pad=(1, 1, 0, 0), mode="replicate")
        grid_staggered_x = (grid_pad_x[..., 1:] + grid_pad_x[..., :-1]) * 0.5
        return grid_staggered_x

    def get_staggered_y(self) -> torch.Tensor:
        """Interpolate real grid at the mac-y position.

        Returns:
            torch.Tensor: Data sampled at the mac-y position [B, C, H + 1, W]
        """
        grid_pad_y = F.pad(self._data, pad=(0, 0, 1, 1), mode="replicate")
        grid_staggered_y = (grid_pad_y[..., 1:, :] + grid_pad_y[..., :-1, :]) * 0.5
        return grid_staggered_y

    def get_gaussian_blur(self, size: int, sigma: float):
        """Apply a square kernel to the data"""
        kernel = create_2d_gaussian_kernel(size, sigma, 1, self.device(), self.dtype())
        pad_size = int((size - 1) / 2)
        data_pad = F.pad(
            self.get_data(),
            pad=(pad_size, pad_size, pad_size, pad_size),
            mode="replicate",
        )
        return Real2dGrid(F.conv2d(data_pad, kernel))

    def __sub__(self, other):
        return Real2dGrid(data=self._data - other._data)

    def __add__(self, other):
        return Real2dGrid(data=self._data + other._data)

    def __mul__(self, other):
        return Real2dGrid(data=self._data * other)

    def __rmul__(self, other):
        return self.__mul__(other)
