import torch
import torch.nn.functional as F
from torch.autograd import Function


class SolveLinearSystemOverwriter2d(Function):
    @staticmethod
    def forward(ctx, rhs, A, solve_linear_system_fcn, tol, limit):
        p = solve_linear_system_fcn(rhs, A, tol, limit)
        ctx.save_for_backward(A, p)
        ctx.limit = limit
        ctx.solve_linear_system_fcn = solve_linear_system_fcn
        return p

    @staticmethod
    def backward(ctx, grad_output):
        # TODO: how to test this
        # use the fact that transpose(A)=A
        A, p, *args = ctx.saved_tensors
        # use a smaller tol than usual forward step for more accurate gradients
        grad_rhs = ctx.solve_linear_system_fcn(
            grad_output, A, tol=1e-10, limit=ctx.limit
        )
        grad_Adiag = -grad_rhs * p

        padx = (0, 1, 0, 0)
        pady = (0, 0, 0, 1)

        p_padx = F.pad(p, pad=padx)
        grad_rhs_padx = F.pad(grad_rhs, pad=padx)
        p_pady = F.pad(p, pad=pady)

        grad_rhs_pady = F.pad(grad_rhs, pad=pady)
        grad_Aplusx = -grad_rhs * p_padx[..., 1:] - grad_rhs_padx[..., 1:] * p
        grad_Aplusy = -grad_rhs * p_pady[..., 1:, :] - grad_rhs_pady[..., 1:, :] * p

        grad_A = torch.stack([grad_Adiag, grad_Aplusx, grad_Aplusy])

        return grad_rhs, grad_A, None, None, None
