import torch
import torch.nn.functional as F
import numpy as np

from .solve_linear_system_overwriter_3d import SolveLinearSystemOverwriter3d
from .abstract_solver_3d import AbstractSolver3d
from differentiable_fluid_functions.grid import Staggered3dGrid, Flag3dGrid


class Jacobi3d(AbstractSolver3d):
    def __init__(self, norm_typ=np.inf, **kwargs):
        super(Jacobi3d, self).__init__()
        self._norm_typ = norm_typ

    def build_pressure_matrix(
        self,
        build_pressure_matrix_fcn,
        flags: Flag3dGrid,
        fractions: Staggered3dGrid = None,
    ):
        """Use the `build_pressure_matrix_fcn` to build the pressure matrix.

        Args:
            build_pressure_matrix_fcn: function to build pressure matrix.
            flags (Flag3dGrid): flags representing voxelized boundary conditions.
            fractions (Staggered3dGrid): fractions of fluid cell to
                represent fractional boundary conditions.
        """
        self.A = build_pressure_matrix_fcn(flags=flags, fractions=fractions)
        self._A_exists = True

    def solve_linear_system(
        self, rhs: torch.Tensor, tol: float = 1e-6, limit: int = 6000
    ) -> torch.Tensor:
        """Solve a linear system Ax=rhs

        Args:
            A (torch.Tensor): A coefficient matrix
            rhs (torch.Tensor): right hand side of the equation
            tol (float, optional): Break condition. Defaults to 1e-6.
            limit (int, optional): max iterations to use for the optimization. Defaults to 6000.

        Returns:
            torch.Tensor: Result of linear system
        """
        assert self._A_exists
        A = self.A[..., 1:-1, 1:-1, 1:-1]  # ignore ghost cells
        rhs_ = rhs[..., 1:-1, 1:-1, 1:-1]  # ignore ghost cells
        solve = SolveLinearSystemOverwriter3d.apply
        p = solve(rhs_, A, self._solve_linear_system, tol, limit)
        # pad ghost cells' values from rhs to p
        p = self._pad_values(p, rhs)
        return p

    def _solve_linear_system(
        self, rhs: torch.Tensor, A: torch.Tensor, tol: float = 1e-6, limit: int = 6000
    ) -> torch.Tensor:
        """Solve linear system function

        Args:
            rhs (torch.Tensor): right hand side of the equation
            A (torch.Tensor): A coefficient matrix
            tol (float, optional): Break condition. Defaults to 1e-6.
            limit (int, optional): max iterations to use for the optimization. Defaults to 6000.

        Raises:
            RuntimeError: Preconditioner flag is run, but preconditioner is not build yet.
            ValueError: Dimension of the rhs and A do not match.

        Returns:
            torch.Tensor: Result of linear system
        """
        if rhs.shape[0] != A.shape[0]:
            raise ValueError(
                "rhs and A matrices have different amount of samples."
                f"{rhs.shape[0]} != {A.shape[0]}"
            )
        # initialization
        p = torch.zeros(rhs.size(), device=rhs.device, dtype=rhs.dtype)
        for sample in range(rhs.shape[0]):
            p[sample] = self._run_jacobi(
                b=rhs[sample],
                A=A[sample],
                tol=tol,
                limit=limit,
            )
        return p

    def _run_jacobi(
        self,
        b: torch.Tensor,
        A: torch.Tensor,
        p: torch.Tensor = None,
        tol: float = 1e-6,
        limit: int = 6000,
    ) -> torch.Tensor:
        """
            Jacobi method, see https://en.wikipedia.org/wiki/Jacobi_method

        Args:
            b (torch.Tensor): The right hand side of the equation H x W
            A (torch.Tensor): A Matrix of shape 3 x H x W
            tol (float, optional): tolerance when to stop the iteration. Defaults to 1e-6.
            limit (int, optional): Number of max iterations. Defaults to 6000.

        Returns:
            torch.Tensor: Result of equation of shape H x W
        """
        if b.shape != A[0].shape:
            raise ValueError(
                "b and A matrices have different sizes." f"{b.shape} != {A[0].shape}"
            )
        # initial guess
        if p is None:
            p = torch.zeros_like(b)
        assert p.shape == b.shape

        # if the rhs is basically zero return zero
        if self._norm(b, self._norm_typ) <= tol:
            return p

        Adiag = A[0, ...]
        # Pseudo Invert Adiag
        AdiagInv = torch.where(
            Adiag == 0,
            torch.zeros(Adiag.size(), device=Adiag.device, dtype=Adiag.dtype),
            1.0 / Adiag,
        )

        iteration = 0
        while iteration < limit:
            p_new = AdiagInv * (b - self._applyLplusU(A, p))
            p = p_new

            r = self._applyA(A, p) - b
            if self._norm(r, self._norm_typ) <= tol:
                return p

            iteration += 1

        # residual = self._norm(r, self._norm_typ)
        # print(f"Jacobi done. Iterations: {iteration}, residual: {residual}")
        return p
