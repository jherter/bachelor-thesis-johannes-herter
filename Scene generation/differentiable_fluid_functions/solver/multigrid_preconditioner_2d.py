import torch
from .abstract_preconditioner import AbstractPreconditioner
from .multigrid_2d import Multigrid2d

from differentiable_fluid_functions.grid import Staggered2dGrid, Flag2dGrid


class MultigridPreconditioner2d(AbstractPreconditioner):
    rank = 2

    def __init__(
        self,
        pre_relax_steps=2,
        coarsest_relax_steps=4,
        post_relax_steps=2,
        jacobi_weight=2.0 / 3.0,
        min_size=8,
        **kwargs,
    ):
        super(MultigridPreconditioner2d, self).__init__()
        self.pre_relax_steps = pre_relax_steps
        self.coarsest_relax_steps = coarsest_relax_steps
        self.post_relax_steps = post_relax_steps
        self.jacobi_weight = jacobi_weight
        self.min_size = min_size

    def build(
        self,
        build_pressure_matrix_fcn,
        flags: Flag2dGrid,
        fractions: Staggered2dGrid = None,
    ):
        """Builds a multigrid solver as the preconditioner

        Args:
            build_pressure_matrix_fcn: function to build pressure matrix.
            flags (Flag2dGrid): flags representing voxelized boundary conditions.
            fractions (Staggered2dGrid): fractions of fluid cell to
                represent fractional boundary conditions.
        """
        self.mg = Multigrid2d(
            pre_relax_steps=self.pre_relax_steps,
            coarsest_relax_steps=self.coarsest_relax_steps,
            post_relax_steps=self.post_relax_steps,
            jacobi_weight=self.jacobi_weight,
            min_size=self.min_size,
        )
        self.mg.build_pressure_matrix(build_pressure_matrix_fcn, flags, fractions)
        self._precon_exists = True

    def apply(self, r: torch.Tensor, sample: int):
        """Apply preconditioner on input vector r (single sample from a mini-batch)
            by running one multigrid v-cycle: z = v_cycle(r)

        Args:
            r (torch.Tensor): The vector which should be transformed
            of shape H x W
            sample (int): batch idx

        Returns:
            torch.Tensor: The transformed vector
        """
        assert self.built()
        if self.mg.A_list[0].shape[-2:] != r.shape[-2:]:
            A_list = [A[sample, :, 1:-1, 1:-1] for A in self.mg.A_list]
        else:
            A_list = [A[sample] for A in self.mg.A_list]
        assert A_list[0].shape[-2:] == r.shape[-2:]
        PInv_r = self.mg._run_v_cycle(p=torch.zeros_like(r), b=r, A_list=A_list)
        return PInv_r
