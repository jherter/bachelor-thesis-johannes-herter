from abc import ABC, abstractmethod


class AbstractSolver(ABC):
    def __init__(self):
        self._A_exists = False

    @property
    @abstractmethod
    def rank(self) -> int:
        ...

    @abstractmethod
    def solve_linear_system(self, A, rhs):
        ...

    @abstractmethod
    def build_pressure_matrix(self, build_pressure_matrix_fcn, flags, fractions):
        ...

    def pressure_matrix_built(self):
        return self._A_exists
