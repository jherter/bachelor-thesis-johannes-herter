import torch
import numpy as np

from differentiable_fluid_functions.grid import Staggered3dGrid, Flag3dGrid
from .solve_linear_system_overwriter_3d import SolveLinearSystemOverwriter3d
from .abstract_preconditioner import AbstractPreconditioner
from .abstract_solver_3d import AbstractSolver3d


class ConjugateGradient3d(AbstractSolver3d):
    def __init__(
        self,
        precon: AbstractPreconditioner = None,
        norm_typ=np.inf,
        **kwargs,
    ):
        super(ConjugateGradient3d, self).__init__()
        self.precon = precon
        self.A = None
        self._norm_typ = norm_typ

    def build_pressure_matrix(
        self,
        build_pressure_matrix_fcn,
        flags: Flag3dGrid,
        fractions: Staggered3dGrid = None,
    ):
        """Use the `build_pressure_matrix_fcn` to build the pressure matrix.
            Also build the preconditioner if it is not built.

        Args:
            build_pressure_matrix_fcn: function to build pressure matrix.
            flags (Flag3dGrid): flags representing voxelized boundary conditions.
            fractions (Staggered3dGrid): fractions of fluid cell to
                represent fractional boundary conditions.
        """
        self.A = build_pressure_matrix_fcn(flags=flags, fractions=fractions)
        self._A_exists = True
        if self.precon is not None and not self.precon.built():
            self.precon.build(
                flags=flags, build_pressure_matrix_fcn=build_pressure_matrix_fcn
            )

    def solve_linear_system(
        self, rhs: torch.Tensor, tol: float = 1e-6, limit: int = 6000
    ) -> torch.Tensor:
        """Solve a linear system Ax=rhs

        Args:
            A (torch.Tensor): A coefficient matrix
            rhs (torch.Tensor): right hand side of the equation
            tol (float, optional): Break condition. Defaults to 1e-6.
            limit (int, optional): max iterations to use for the optimization. Defaults to 6000.

        Returns:
            torch.Tensor: Result of linear system
        """
        assert self._A_exists
        A = self.A[..., 1:-1, 1:-1, 1:-1]  # ignore ghost cells
        rhs_ = rhs[..., 1:-1, 1:-1, 1:-1]  # ignore ghost cells
        solve = SolveLinearSystemOverwriter3d.apply
        p = solve(rhs_, A, self._solve_linear_system, tol, limit)
        # pad ghost cells' values from rhs to p
        p = self._pad_values(p, rhs)
        return p

    def _solve_linear_system(
        self, rhs: torch.Tensor, A: torch.Tensor, tol: float = 1e-6, limit: int = 6000
    ) -> torch.Tensor:
        """Solve linear system function

        Args:
            rhs (torch.Tensor): right hand side of the equation
            A (torch.Tensor): A coefficient matrix
            tol (float, optional): Break condition. Defaults to 1e-6.
            limit (int, optional): max iterations to use for the optimization. Defaults to 6000.

        Raises:
            ValueError: Dimension of the rhs and A do not match.

        Returns:
            torch.Tensor: Result of linear system
        """
        if rhs.shape[0] != A.shape[0]:
            raise ValueError(
                "rhs and A matrices have different amount of samples."
                f"{rhs.shape[0]} != {A.shape[0]}"
            )
        # initialization
        p = torch.zeros(rhs.size(), device=rhs.device, dtype=rhs.dtype)
        for sample in range(rhs.shape[0]):
            p[sample] = self._run_preconditioned_conjugate_gradient(
                b=rhs[sample],
                A=A[sample],
                sample=sample,
                tol=tol,
                limit=limit,
            )
        return p

    def _run_preconditioned_conjugate_gradient(
        self,
        b: torch.Tensor,
        A: torch.Tensor,
        p: torch.Tensor = None,
        sample: int = 0,
        tol: float = 1e-6,
        limit: int = 6000,
    ) -> torch.Tensor:
        """
            The Preconditioned Conjugate Gradient (PCG) algorithm for solving Ap = b.
            More details: https://dl.acm.org/doi/pdf/10.1145/1281500.1281681 page 34

        Args:
            b (torch.Tensor): The right hand side of the equation D x H x W
            A (torch.Tensor): A Matrix of shape 4 x D x H x W
            M (torch.Tensor): preconditioner of shape 4 x D x H x W
            tol (float, optional): tolerance when to stop the iteration. Defaults to 1e-6.
            limit (int, optional): Number of max iterations. Defaults to 6000.

        Returns:
            torch.Tensor: Result of equation of shape H x W
        """
        if b.shape != A[0].shape:
            raise ValueError(
                "b and A matrices have different sizes." f"{b.shape} != {A[0].shape}"
            )
        # initial guess
        if p is None:
            p = torch.zeros_like(b)
        assert p.shape == b.shape

        # residual vector, initialized as rhs
        r = b.clone()

        # if the rhs is basically zero return zero
        if self._norm(r, self._norm_typ) <= tol:
            return p

        # Set auxiliary vector
        if self.precon is not None:
            z = self.precon.apply(r, sample)
        else:
            z = r

        # set search vector
        s = z.clone()

        sigma = self._dot_product(z, r)
        iteration = 0

        while iteration < limit:
            # Set auxiliary vector
            z = self._applyA(A, s)

            # error in the docs
            dp = self._dot_product(z, s)

            alpha = 0.0
            if abs(dp) > 0.0:
                alpha = sigma / dp

            # update
            p += alpha * s
            r -= alpha * z
            res_norm = self._norm(r, self._norm_typ)

            if res_norm <= tol:
                break

            # Set auxiliary vector
            if self.precon is not None:
                z = self.precon.apply(r, sample)
            else:
                z = r

            sigma_new = self._dot_product(z, r)
            beta = sigma_new / sigma

            # Set search vector
            s = z + beta * s

            iteration += 1
            # update sigma and iterations
            sigma = sigma_new

        # residual = self._norm(r, self._norm_typ)
        # print(f"CG done. Iterations: {iteration}, residual: {residual}")
        return p

    def build_streamfunction_matrix(self):
        raise NotImplementedError("IMPL me!")
