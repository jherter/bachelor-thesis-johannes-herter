from abc import ABC, abstractmethod


class AbstractPreconditioner(ABC):
    def __init__(self):
        self._precon_exists = False

    def built(self):
        return self._precon_exists

    @property
    @abstractmethod
    def rank(self) -> int:
        ...

    @abstractmethod
    def build(self):
        ...

    @abstractmethod
    def apply(self):
        ...
