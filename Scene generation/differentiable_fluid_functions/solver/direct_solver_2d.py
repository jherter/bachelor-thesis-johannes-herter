import torch
import numpy as np
from differentiable_fluid_functions.grid import Staggered2dGrid, Flag2dGrid

from .abstract_solver_2d import AbstractSolver2d


class DirectSolver2d(AbstractSolver2d):
    def __init__(self, *args, **kwargs):
        super(DirectSolver2d, self).__init__()

    def build_pressure_matrix(
        self,
        build_pressure_matrix_fcn,
        flags: Flag2dGrid,
        fractions: Staggered2dGrid = None,
    ):
        self.A = build_pressure_matrix_fcn(flags=flags, fractions=fractions)
        self._A_exists = True

    def solve_linear_system(
        self, rhs: torch.Tensor, convert_to_dense: bool = True
    ) -> torch.Tensor:
        """Solve the linear system Ax=b directly using LU decomposition.

        Args:
            A (torch.Tensor): Sparse batched A Matrix of shape B x 3 x H x W or dense matrix B x H*W x H*W
            rhs (torch.Tensor): right hand site of the equation of shape B x H x W
            convert_to_dense (bool, optional): If the sparse matrix needs to be transformed into a dense one. Defaults to True.

        Returns:
            torch.Tensor: The result x of the equation
        """
        assert self._A_exists
        A = self.A[..., 1:-1, 1:-1]  # ignore ghost cells
        rhs_ = rhs[..., 1:-1, 1:-1]  # ignore ghost cells
        p = self._solve_linear_system(rhs_, A, convert_to_dense)
        # pad ghost cells' values from rhs to p
        p = self._pad_values(p, rhs)
        return p

    def _solve_linear_system(
        self, rhs: torch.Tensor, A: torch.Tensor, convert_to_dense: bool = True
    ) -> torch.Tensor:
        """Solve the linear system Ax=b directly using LU decomposition.

        Args:
            A (torch.Tensor): Sparse batched A Matrix of shape B x 3 x H x W or dense matrix B x H*W x H*W
            rhs (torch.Tensor): right hand site of the equation of shape B x H x W
            convert_to_dense (bool, optional): If the sparse matrix needs to be transformed into a dense one. Defaults to True.

            If a dense matrix is supplied it is assumed that this matrix is positive semi-definite.

        Returns:
            torch.Tensor: The result x of the equation
        """
        B, H, W = rhs.shape
        if convert_to_dense:
            if rhs.shape[0] != A.shape[0]:
                raise ValueError(
                    "rhs and A matrices have different amount of samples."
                    f"{rhs.shape[0]} != {A.shape[0]}"
                )
            if rhs[0].shape != A[0, 0].shape:
                raise ValueError(
                    "rhs and A matrices have different sizes."
                    f"{rhs[0].shape} != {A[0, 0].shape}"
                )

            A = self._convert_sparse_to_dense(A)
            pressure = torch.zeros((B, H, W), device=rhs.device, dtype=rhs.dtype)

            # solve each on individually
            # This can not be batched since it is not given that all elements in the batch have the same non zero rows
            for sample in range(B):
                A_cur = A[sample]  # shape H x W
                b_cur = rhs[sample]
                nonzero_rows = torch.nonzero((A_cur != 0).any(dim=-2), as_tuple=True)[0]

                A_nonzero_rows = A_cur[nonzero_rows, :]
                A_nonzero_rows = A_nonzero_rows[:, nonzero_rows]

                # put RHS into vector form
                rhs_vec = b_cur.contiguous().view(-1, 1)
                b = rhs_vec[nonzero_rows, :]

                # direct LU solver to compute pressure
                p, _ = torch.solve(b, A_nonzero_rows)

                # put pressure back into matrix form
                nonzero_idx = np.unravel_index(nonzero_rows.cpu().numpy(), [H, W])
                pressure[sample, nonzero_idx[0], nonzero_idx[1]] = p.permute(1, 0)
        else:
            # NOTE: ASSUMES THAT A DOES NOT CONTAIN SINGULAR ENTRIES
            if np.prod(rhs[0].shape) != A.shape[-1]:
                raise ValueError(
                    "rhs and dense A have unmatched sizes. "
                    f"rhs: {rhs[0].shape}, A: {A[0, 0].shape}."
                )
            b = rhs.contiguous().view(B, H * W, 1)
            p, _ = torch.solve(b, A)
            pressure = p.view(B, H, W)

        return pressure
