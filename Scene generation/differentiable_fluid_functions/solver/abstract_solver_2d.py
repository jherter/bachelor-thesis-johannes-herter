import torch
import torch.nn.functional as F
import numpy as np

from .abstract_solver import AbstractSolver


class AbstractSolver2d(AbstractSolver):
    rank = 2

    @staticmethod
    def _convert_sparse_to_dense(A: torch.Tensor) -> torch.Tensor:
        """Convert a Pressure matrix to dense version

        Args:
            A (torch.Tensor): Pressure matrix of shape B x 3 x H x W

        Returns:
            torch.Tensor: Dense pressure matrix of shape B x H*W x H*W
        """
        Adiag, Aplusx, Aplusy = A[:, 0, ...], A[:, 1, ...], A[:, 2, ...]
        B, H, W = Adiag.shape
        n = H * W
        A = torch.zeros(B, n, n, device=Adiag.device, dtype=Adiag.dtype)

        y = np.repeat(np.arange(H), W)
        x = np.tile(np.arange(W), H)
        idx_x_y = np.ravel_multi_index([[y], [x]], dims=(H, W))

        side_y1 = np.repeat(np.arange(H), W - 1)
        side_x1 = np.tile(np.arange(W - 1), H)
        side_idx_x1_y = np.ravel_multi_index([[side_y1], [side_x1]], dims=(H, W))
        idx_x1_y = np.ravel_multi_index([[side_y1], [side_x1 + 1]], dims=(H, W))

        side_y2 = np.repeat(np.arange(H - 1), W)
        side_x2 = np.tile(np.arange(W), H - 1)
        side_idx_x2_y = np.ravel_multi_index([[side_y2], [side_x2]], dims=(H, W))
        idx_x_y1 = np.ravel_multi_index([[side_y2 + 1], [side_x2]], dims=(H, W))

        for i in range(B):
            # main diagonal
            A[i, idx_x_y, idx_x_y] = Adiag[i].contiguous().view(n)
            # right inner diagonal
            A[i, side_idx_x1_y, idx_x1_y] = (
                Aplusx[i, :, :-1].contiguous().view(H * (W - 1))
            )
            # left inner diagonal
            A[i, idx_x1_y, side_idx_x1_y] = (
                Aplusx[i, :, :-1].contiguous().view(H * (W - 1))
            )

            A[i, side_idx_x2_y, idx_x_y1] = (
                Aplusy[i, :-1, :].contiguous().view((H - 1) * W)
            )
            A[i, idx_x_y1, side_idx_x2_y] = (
                Aplusy[i, :-1, :].contiguous().view((H - 1) * W)
            )
        return A

    @staticmethod
    def _norm(x: torch.Tensor, p=np.inf) -> torch.Tensor:
        """defines the norm of a grid-represented vector.

        Args:
            x(torch.Tensor): input grid-represented vector shape H x W
            p: order of norm, can be int, float, inf, -inf, 'fro', 'nuc'
        """
        x_vec = x.contiguous().view(-1)
        norm = torch.norm(x_vec, p=p)
        return norm

    @staticmethod
    def _dot_product(x: torch.Tensor, y: torch.Tensor) -> torch.Tensor:
        """dot product of two grid-represented vectors x and y.

        Args:
            x (torch.Tensor): shape H x W
            y (torch.Tensor): shape H x W

        Returns:
            torch.Tensor: The dot product between the two vectors
        """
        return torch.sum(x * y)

    @staticmethod
    def _applyLpU(A: torch.Tensor, b: torch.Tensor) -> torch.Tensor:
        """Computes matrix vector product (L+U) @ b, where L and U are
            lower and upper triangular matrices of A matrix (5-point matrix in 2-D)

        Args:
            A (torch.Tensor): coefficient matrix A of shape 3 x H x W
                (5-point Laplacian) matrix with Adiag, Aplusx and Aplusy
            b (torch.Tensor): grid-represented vector of shape H x W


        Returns:
            torch.Tensor: Matrix vector product (L+U)@b
        """
        Aplusx = A[1, ...]
        Aplusy = A[2, ...]
        if Aplusx.size() != b.size():
            raise ValueError(
                f"Mismatch of sizes between matrix {Aplusx.size()} and vector {b.size()}."
            )

        # pad to get Aplusx(i-1,j,k), Aplusy(i,j-1,k) and Aplusz(i,j,k-1)
        pad_x_neg = (1, 0, 0, 0)
        pad_y_neg = (0, 0, 1, 0)

        Aplusx_pad_neg = F.pad(Aplusx, pad=pad_x_neg)
        Aplusy_pad_neg = F.pad(Aplusy, pad=pad_y_neg)

        # pad to get b(i-1, j, k), b(i+1, j, k) and so on
        pad_x = (1, 1, 0, 0)
        pad_y = (0, 0, 1, 1)
        b_pad_x = F.pad(b, pad=pad_x)
        b_pad_y = F.pad(b, pad=pad_y)

        # 'Matrix' (L+U) multiplies 'vector' b
        LpUb = (
            # Aplusx(i,j,k)*b(i+1,j,k)
            Aplusx * b_pad_x[..., 2:]
            # Aplusx(i-1,j,k)*b(i-1,j,k)
            + Aplusx_pad_neg[..., :-1] * b_pad_x[..., :-2]
            # Aplusy(i, j, k) * b(i, j + 1, k)
            + Aplusy * b_pad_y[..., 2:, :]
            # Aplusy(i,j-1,k)*b(i,j-1,k)
            + Aplusy_pad_neg[..., :-1, :] * b_pad_y[..., :-2, :]
        )

        return LpUb

    @staticmethod
    def _applyA(A: torch.Tensor, b: torch.Tensor) -> torch.Tensor:
        """Computes matrix vector product A @ b. A is a 5-point matrix in 2-D.

        Args:
            A (torch.Tensor): coefficient matrix A of shape 3 x H x W
                (5-point Laplacian) matrix with Adiag, Aplusx and Aplusy
            b (torch.Tensor): grid-represented vector of shape H x W


        Returns:
            torch.Tensor: Matrix vector product A and b
        """
        Adiag = A[0, ...]
        if Adiag.size() != b.size():
            raise ValueError(
                f"Mismatch of sizes between matrix {Adiag.size()} and vector {b.size()}."
            )
        LpUb = AbstractSolver2d._applyLpU(A, b)
        Ab = LpUb + Adiag * b
        return Ab

    @staticmethod
    def _pad_values(data: torch.Tensor, pad_ref: torch.Tensor) -> torch.Tensor:
        """Pads cells to `data`, according to `pad_ref`.

        Args:
            data (torch.Tensor): data to be padded, of shape [B, 1, H, W] or [1, H, W] or [H, W]
            pad_ref (torch.Tensor): reference data to figure out the padded values,
                of shape [B, 1, h+2, w+2] or [1, h+2, w+2] or [h+2, w+2]

        Returns:
            data_padded (torch.Tensor): padded data
        """
        pad_ref = pad_ref.clone().detach()
        if len(pad_ref.shape) == 3:
            pad_ref = pad_ref.unsqueeze(0)
        if len(pad_ref.shape) == 2:
            pad_ref = pad_ref.unsqueeze(0).unsqueeze(0)
        if len(data.shape) == 3:
            data_ = data.unsqueeze(0)
        elif len(data.shape) == 2:
            data_ = data.unsqueeze(0).unsqueeze(0)
        else:
            data_ = data
        assert len(pad_ref.shape) == 4
        assert len(data_.shape) == 4
        b, c, h, w = pad_ref.shape
        B, C, _, _ = data_.shape
        assert B == b and C == c
        assert c == 1
        data_padded_list = []
        for sample in range(b):
            data_sample = data_[sample]
            data_sample = F.pad(
                data_sample,
                pad=(1, 0, 0, 0),
                mode="constant",
                value=pad_ref[sample, 0, h // 2, 0],
            )
            data_sample = F.pad(
                data_sample,
                pad=(0, 1, 0, 0),
                mode="constant",
                value=pad_ref[sample, 0, h // 2, -1],
            )
            data_sample = F.pad(
                data_sample,
                pad=(0, 0, 1, 0),
                mode="constant",
                value=pad_ref[sample, 0, 0, w // 2],
            )
            data_sample = F.pad(
                data_sample,
                pad=(0, 0, 0, 1),
                mode="constant",
                value=pad_ref[sample, 0, -1, w // 2],
            )
            data_sample[0, 0, 0] = pad_ref[sample, 0, 0, 0]
            data_sample[0, 0, -1] = pad_ref[sample, 0, 0, -1]
            data_sample[0, -1, 0] = pad_ref[sample, 0, -1, 0]
            data_sample[0, -1, -1] = pad_ref[sample, 0, -1, -1]
            data_padded_list.append(data_sample)
        data_padded = torch.stack(data_padded_list, dim=0)
        if len(data.shape) == 3:
            data_padded = data_padded.squeeze(0)
        if len(data.shape) == 2:
            data_padded = data_padded.squeeze(0).squeeze(0)
        return data_padded
