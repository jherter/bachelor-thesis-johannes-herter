import torch
import torch.nn.functional as F
from .abstract_preconditioner import AbstractPreconditioner

from differentiable_fluid_functions.grid import Staggered2dGrid, Flag2dGrid
from .abstract_solver_3d import AbstractSolver3d


class IncompletePoissonPreconditioner3d(AbstractPreconditioner):
    rank = 2

    def __init__(self, **kwargs):
        super(IncompletePoissonPreconditioner3d, self).__init__()

    def build(
        self,
        build_pressure_matrix_fcn,
        flags: Flag2dGrid,
        fractions: Staggered2dGrid = None,
    ):
        """Build 7-point Incomplete Poisson Preconditioner from A matrices
                with the format B x 4 x D x H x W

        Args:
            build_pressure_matrix_fcn: function to build pressure matrix.
            flags (Flag3dGrid): flags representing voxelized boundary conditions.
            fractions (Staggered3dGrid): fractions of fluid cell to
                represent fractional boundary conditions.
        """

        A = build_pressure_matrix_fcn(flags=flags, fractions=fractions)
        self._build_precon_from_system_matrix(A)

    def _build_precon_from_system_matrix(self, A):
        # batch version B x 4 x D x H x W
        Adiag = A[:, 0, ...]
        Aplusx = A[:, 1, ...]
        Aplusy = A[:, 2, ...]
        Aplusz = A[:, 3, ...]

        # Pseudo Invert A
        AdiagInv = torch.where(
            Adiag == 0,
            torch.zeros(Adiag.size(), device=Adiag.device, dtype=Adiag.dtype),
            1.0 / Adiag,
        )

        PInvplusx = -Aplusx * AdiagInv
        PInvplusy = -Aplusy * AdiagInv
        PInvplusz = -Aplusz * AdiagInv

        pad_x_neg = (1, 0, 0, 0, 0, 0)
        pad_y_neg = (0, 0, 1, 0, 0, 0)
        pad_z_neg = (0, 0, 0, 0, 1, 0)

        PInvplusx_pad_neg = F.pad(PInvplusx, pad=pad_x_neg)
        PInvplusy_pad_neg = F.pad(PInvplusy, pad=pad_y_neg)
        PInvplusz_pad_neg = F.pad(PInvplusz, pad=pad_z_neg)

        PInvdiag = (
            1.0
            + PInvplusx_pad_neg[..., :-1] * PInvplusx_pad_neg[..., :-1]
            + PInvplusy_pad_neg[..., :-1, :] * PInvplusy_pad_neg[..., :-1, :]
            + PInvplusz_pad_neg[..., :-1, :, :] * PInvplusz_pad_neg[..., :-1, :, :]
        )
        self.PInv = torch.stack([PInvdiag, PInvplusx, PInvplusy, PInvplusz], dim=1)
        self._precon_exists = True

    def apply(self, r: torch.Tensor, sample: int):
        """Apply preconditioner on input vector r (single sample from a mini-batch):
                z = PInv * r

        Args:
            r (torch.Tensor): The vector which should be transformed
            of shape H x W
            sample (int): batch idx

        Returns:
            torch.Tensor: The transformed vector
        """
        assert self.built()
        if self.PInv.shape[-3:] != r.shape[-3:]:
            PInv_sample = self.PInv[sample, :, 1:-1, 1:-1, 1:-1]
        else:
            PInv_sample = self.PInv[sample]
        assert PInv_sample.shape[-3:] == r.shape[-3:]
        PInv_r = AbstractSolver3d._applyA(PInv_sample, r)
        return PInv_r
