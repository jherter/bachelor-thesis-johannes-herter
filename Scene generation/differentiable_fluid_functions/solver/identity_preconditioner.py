import torch
from .abstract_preconditioner import AbstractPreconditioner

from differentiable_fluid_functions.grid import Staggered2dGrid, Flag2dGrid


class IdentityPreconditioner(AbstractPreconditioner):
    """Dummy preconditioner, does nothing on the input vector"""

    rank = 0

    def build(
        self,
        build_pressure_matrix_fcn,
        flags: Flag2dGrid,
        fractions: Staggered2dGrid = None,
    ):
        """Builds nothing."""
        self._precon_exists = True

    def apply(self, r: torch.Tensor, sample: int):
        """Returns the original input `r`"""
        return r
