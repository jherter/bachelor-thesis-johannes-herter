import torch
import torch.nn.functional as F
from .abstract_preconditioner import AbstractPreconditioner

from differentiable_fluid_functions.grid import Staggered2dGrid, Flag2dGrid
from .abstract_solver_2d import AbstractSolver2d


class IncompletePoissonPreconditioner2d(AbstractPreconditioner):
    rank = 2

    def __init__(self, **kwargs):
        super(IncompletePoissonPreconditioner2d, self).__init__()

    def build(
        self,
        build_pressure_matrix_fcn,
        flags: Flag2dGrid,
        fractions: Staggered2dGrid = None,
    ):
        """Builds a 5-point incomplete poisson precondition matrix.

        Args:
            build_pressure_matrix_fcn: function to build pressure matrix.
            flags (Flag2dGrid): flags representing voxelized boundary conditions.
            fractions (Staggered2dGrid): fractions of fluid cell to
                represent fractional boundary conditions.
        """
        A = build_pressure_matrix_fcn(flags=flags, fractions=fractions)
        self._build_precon_from_system_matrix(A)

    def _build_precon_from_system_matrix(self, A):
        # batch version B x 3 x H x W
        Adiag = A[:, 0, ...]
        Aplusx = A[:, 1, ...]
        Aplusy = A[:, 2, ...]

        # Pseudo Invert A
        AdiagInv = torch.where(
            Adiag == 0,
            torch.zeros(Adiag.size(), device=Adiag.device, dtype=Adiag.dtype),
            1.0 / Adiag,
        )

        PInvplusx = -Aplusx * AdiagInv
        PInvplusy = -Aplusy * AdiagInv

        pad_x_neg = (1, 0, 0, 0)
        pad_y_neg = (0, 0, 1, 0)

        PInvplusx_pad_neg = F.pad(PInvplusx, pad=pad_x_neg)
        PInvplusy_pad_neg = F.pad(PInvplusy, pad=pad_y_neg)

        PInvdiag = (
            1.0
            + PInvplusx_pad_neg[..., :-1] * PInvplusx_pad_neg[..., :-1]
            + PInvplusy_pad_neg[..., :-1, :] * PInvplusy_pad_neg[..., :-1, :]
        )
        self.PInv = torch.stack([PInvdiag, PInvplusx, PInvplusy], dim=1)
        self._precon_exists = True

    def apply(self, r: torch.Tensor, sample: int):
        """Apply preconditioner on input vector r (single sample from a mini-batch):
                z = PInv * r

        Args:
            r (torch.Tensor): The vector which should be transformed
            of shape H x W
            sample (int): batch idx

        Returns:
            torch.Tensor: The transformed vector
        """
        assert self.built()
        if self.PInv.shape[-2:] != r.shape[-2:]:
            PInv_sample = self.PInv[sample, :, 1:-1, 1:-1]
        else:
            PInv_sample = self.PInv[sample]
        assert PInv_sample.shape[-2:] == r.shape[-2:]
        PInv_r = AbstractSolver2d._applyA(PInv_sample, r)
        return PInv_r
