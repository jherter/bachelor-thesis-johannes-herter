import torch
import numpy as np
from differentiable_fluid_functions.particles import ParticleStatus
from typing import TypeVar
from differentiable_fluid_functions.utils import load_npy, load_npz, save_npy, save_npz

T = TypeVar("T", bound="BasicParticleData")


class BasicParticleData(object):
    NPZ_KEY = "BASIC_PARTICLE_DATA"

    def __init__(self, data: torch.Tensor):
        if not len(data.shape) in [2, 3]:
            raise ValueError(
                f"BasicParticleData requires a tensor of shape (B) x N x C. currently {data.shape}"
            )

        self._data = data if len(data.shape) == 3 else data.unsqueeze(0)
        self._status = np.ones(self._data.shape[1]) * int(ParticleStatus.NEW)

    @classmethod
    def from_array(
        cls,
        array: np.ndarray,
        device: torch.device = torch.device("cpu"),
    ) -> T:
        """Create basic particle data from a numpy array

        Args:
            array (np.ndarray): The array holding the data
            device (torch.device, optional): On which device the tensor should be stored. Defaults to torch.device("cpu").

        Raises:
            ValueError: The np array has the wrong dimensions

        Returns:
            BasicParticleData: An instance of BasicParticleData
        """
        if not len(array.shape) in [2, 3]:
            raise ValueError(
                f"BasicParticleData requires a tensor of shape (B) x N x C. currently {array.shape}"
            )

        data = torch.tensor(array, device=device)
        return cls(data=data)

    @classmethod
    def from_npy(cls, filename: str, device: torch.device = torch.device("cpu")) -> T:
        """Create BasicParticleData from a npy file

        Args:
            filename (str): path to the file

        Returns:
            T: An instance of BasicParticleData
        """
        array = load_npy(filename)

        return cls.from_array(array, device=device)

    @classmethod
    def from_npz(
        cls,
        filename: str,
        key: str = NPZ_KEY,
        device: torch.device = torch.device("cpu"),
    ) -> T:
        """Create a BasicParticleData from a npz file

        Args:
            filename (str): path to the file


        Returns:
            T: An instance of BasicParticleData
        """
        array = load_npz(filename, key)
        return cls.from_array(array, device=device)

    def get_numpy(self) -> np.ndarray:
        """Get the data as a numpy array

        Returns:
            np.ndarray: The data
        """
        return self._data.detach().cpu().numpy()

    def get_data(self) -> torch.tensor:
        """Return the raw data

        Returns:
            torch.tensor: Raw data as tensor
        """
        return self._data

    def save_npy(self, filename: str):
        """Save as an npy file

        Args:
            filename (str): Filename of the npy file
        """
        array = self.get_numpy()
        save_npy(array, filename)

    def save_npz(self, filename: str):
        """Save  to an npz file

        Args:
            filename (str): name of the file
        """
        array = self.get_numpy()
        save_dict = {}
        save_dict[self.NPZ_KEY] = array
        save_npz(filename, save_dict)

    def set_gradient_required(self, value: bool):
        """Set that this particleData requires a gradient

        Args:
            value (bool): flag if the gradient is required.
        """
        self._data.requires_grad = value

    def get_gradient(self) -> torch.Tensor:
        """Get the gradient of the Particle data

        Raises:
            ValueError: The gradient was set to not required

        Returns:
            torch.Tensor: The gradient
        """
        if not self._data.requires_grad:
            raise ValueError("The BasicParticleData does not have a gradient")
        return self._data.grad

    def get_number_of_particles(self) -> int:
        """Get the number of particles

        Returns:
            int: How many particles are stored
        """
        return self._data.shape[1]
