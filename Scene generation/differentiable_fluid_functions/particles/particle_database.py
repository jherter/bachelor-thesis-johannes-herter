import torch
import numpy as np

# otherwise pylance complains
from differentiable_fluid_functions.particles.basic_particle_data import (
    BasicParticleData,
)
from typing import TYPE_CHECKING, Dict, Optional, TypeVar, List
from differentiable_fluid_functions.utils.sample import grid_to_particle
from differentiable_fluid_functions.types import GridType

if TYPE_CHECKING:  # pragma: no cover
    from differentiable_fluid_functions.grid import Node2dGrid, Real2dGrid
    from differentiable_fluid_functions.kernels import AbstractKernel

T = TypeVar("T", bound="ParticleDatabase")


class ParticleDatabase(object):
    def __init__(
        self,
        positions: BasicParticleData,
        domain_bounding_box: Optional[torch.tensor] = None,
        attributes: Optional[Dict[str, BasicParticleData]] = None,
    ):
        self._positions = positions
        self._attributes = {} if attributes is None else attributes
        self.POSITIONS_KEY = "positions"
        if domain_bounding_box is None:
            postion_tensor = positions.get_data()
            batch_size, _, dim = postion_tensor.shape

            self._domain_bounding_box = torch.zeros(
                (batch_size, dim, 2),
                dtype=postion_tensor.dtype,
                device=postion_tensor.device,
            )
            min_t = torch.min(postion_tensor, dim=1, keepdim=True)[0][:, 0]
            max_t = torch.max(postion_tensor, dim=1, keepdim=True)[0][:, 0]

            self._domain_bounding_box[..., 0] = min_t
            self._domain_bounding_box[..., 1] = max_t
        else:
            self._domain_bounding_box = domain_bounding_box

    @classmethod
    def init_from_flaggrid(cls) -> T:
        raise NotImplementedError()

    @classmethod
    def init_with_lerp(
        cls,
        start: T,
        stop: T,
        weight: float,
        domain_bounding_box: Optional[torch.tensor] = None,
        interpolation_attributes: Optional[List[str]] = None,
        positions: Optional[BasicParticleData] = None,
    ) -> T:
        """Create a particle db in-between two particle dbs (start stop)

        Args:
            start (T): Particles db used as a start for interpolation
            stop (T): Particles db used as a stop for interpolation
            weight (float): 0-1 weight for the linear interpolation
            domain_bounding_box (torch.tensor, optional): New domain bounding box. Defaults to None.
            interpolation_attributes (List[str], optional): Which attributes should be interpolated. Defaults to [].
            positions (BasicParticleData, optional): the positions of the interpolated particle db. Defaults to None.

        Raises:
            ValueError: if the position are not given and not in the list of interpolation attributes
            ValueError: if two interpolation tensors have different shapes

        Returns:
            T: the linearly interpolated particle database
        """
        interpolation_attributes = (
            [] if interpolation_attributes is None else interpolation_attributes
        )
        if start.POSITIONS_KEY not in interpolation_attributes and positions is None:
            raise ValueError(
                "Please specify either the position or add the positions to the list of values that should be interpolated"
            )

        resulting_attributes = {start.POSITIONS_KEY: positions}
        for attribute in interpolation_attributes:
            start_value = start.get_tensor(attribute)
            stop_value = stop.get_tensor(attribute)
            if start_value.shape != stop_value.shape:
                raise ValueError(
                    f"The tensor that should be interpolated need to have the same size {start_value.shape} != {stop_value.shape}"
                )

            resulting_attributes[attribute] = BasicParticleData(
                data=torch.lerp(input=start_value, end=stop_value, weight=weight)
            )

        # get the positions
        positions = resulting_attributes[start.POSITIONS_KEY]

        # remove the positions from the dict
        del resulting_attributes[start.POSITIONS_KEY]

        return cls(
            positions=positions,
            domain_bounding_box=domain_bounding_box,
            attributes=resulting_attributes,
        )

    @classmethod
    def init_from_particle_database_with_mvp(
        cls,
        particle_database: T,
        mvp_matrix: torch.tensor,
    ) -> T:
        """
           Create a new particle db with a model view projection matrix

        Args:
            particle_database (T): Pd with the attributes and data
            mvp_matrix (torch.tensor): current mvp matrix of shape: (B) x 4 x 4 .

        Raises:
            ValueError: The Particle db has different batch dim then the mvp_matrix use their 1 or the same
            RuntimeError:  matrix clamps the particles of the different samples

        Returns:
            T: New pdb with transformed particle positions
        """
        # get the positions
        p_pos = particle_database.get_tensor(particle_database.POSITIONS_KEY)

        batch_size, n_particles, dim = p_pos.shape

        if (
            len(mvp_matrix.shape) != 3
            or mvp_matrix.shape[1] != 4
            or mvp_matrix.shape[2] != 4
        ):
            raise ValueError(
                f"MVP matrix needs to have shape B x 4 x 4: Currently: {mvp_matrix.shape}"
            )

        if (mvp_matrix.shape[0] or batch_size) > 1 and batch_size != mvp_matrix.shape[
            0
        ]:
            raise ValueError(
                "The Particle db has different batch dim then the mvp_matrix use their 1 or the same"
            )

        # make positions homogenous
        ones = torch.ones(
            batch_size, n_particles, 1, dtype=p_pos.dtype, device=p_pos.device
        )
        p_pos = torch.cat((p_pos, ones), dim=2)

        # get the data type
        data_type = p_pos.dtype

        # transform
        transformed_postions = p_pos.bmm(mvp_matrix.to(data_type))

        # remove 4 dim
        denom = transformed_postions[..., 3:]  # denominator
        transformed_postions = transformed_postions[..., :3] / denom
        # print(f"p_res: {transformed_positions}")

        nr_selected_particles = None
        clamped_particles = []
        # map to raw tensors
        new_attributes = {}
        for key in particle_database._attributes.keys():
            new_attributes[key] = []

        # remove particles outside the view spectrum
        for sample_index in range(batch_size):
            # shape: n
            mask = (
                (transformed_postions[sample_index, :, 0] >= -1.0)
                & (transformed_postions[sample_index, :, 0] <= 1.0)
                & (transformed_postions[sample_index, :, 1] >= -1.0)
                & (transformed_postions[sample_index, :, 1] <= 1.0)
            )
            if dim == 3:
                mask &= (
                    transformed_postions[sample_index, :, 2] >= 0.0
                ) & (  # see get_perspective_4d why value is zero and not -1
                    transformed_postions[sample_index, :, 2] <= 1.0
                )

            included_particles = transformed_postions[sample_index][mask]

            # included_particles = transformed_positions[sample_index]
            if nr_selected_particles is None:
                nr_selected_particles = included_particles.shape[0]
            elif nr_selected_particles != included_particles.shape[0]:
                raise RuntimeError(
                    "The given mvp matrix clamps the particles of the different samples, resulting in not the same number of particles per sample"
                    + " resulting in problems storing the data in a tensor. To solve this problem change the mvp matrix or use a batch size of 1"
                )
            clamped_particles.append(included_particles)

            # filter attributes
            for key in particle_database._attributes.keys():
                new_attributes[key].append(
                    particle_database._attributes[key].get_data()[sample_index][mask]
                )

        # stack together to get back batch dim
        transformed_postions = torch.stack(clamped_particles)
        for key in particle_database._attributes.keys():
            new_attributes[key] = BasicParticleData(torch.stack(new_attributes[key]))

        # domain bounding box from -1 to 1
        domain_bounding_box = torch.ones(
            (batch_size, 3, 2),
            device=particle_database._domain_bounding_box.device,
            dtype=particle_database._domain_bounding_box.dtype,
        )
        domain_bounding_box[:, :, 0] = -1
        domain_bounding_box[:, 2, 0] = 0

        return cls(
            positions=BasicParticleData(transformed_postions),
            domain_bounding_box=domain_bounding_box,
            attributes=new_attributes,
        )

    @classmethod
    def init_from_particle_database_with_transform(
        cls,
        particle_database: T,
        transformation_matrix: torch.tensor,
        centroid: torch.tensor,
        recompute_bounding_box: bool = True,
    ) -> T:
        """Create a new particle database from a existing one using the position
            and linking the attributes of the original.
            If the new data has a bigger bounding box the new bounding box is extended.


        Args:
            particle_database (ParticleDatabase): original data that should be transformed
            transformation_matrix (torch.tensor): Transformation matrix that should be used b x 2/3 x 2/3
            centroid (torch.tensor): positions which is subtracted and added to the data
            to be able to rotated around the center shape b x 1 x 2/3

        Returns:
            ParticleDatabase: [description]
        """

        if centroid.shape[0] != transformation_matrix.shape[0]:
            raise ValueError(
                "The centroid and the transformation matrix need to have the same batch dim"
            )

        data_type = particle_database.get_tensor(particle_database.POSITIONS_KEY).dtype

        p_pos = particle_database.get_tensor(particle_database.POSITIONS_KEY)
        if p_pos.shape[0] > 1 and p_pos.shape[0] != transformation_matrix.shape[0]:
            raise ValueError(
                "The Particle db has different batch dim then the transformation matrix use their 1 or the same"
            )

        # rotated and move back
        rotated_postions = torch.matmul(
            p_pos - centroid.to(data_type), transformation_matrix.to(data_type)
        ) + centroid.to(data_type)

        # increase bounding box for
        batch_size, _, dim = rotated_postions.shape

        if recompute_bounding_box:
            new_domain = torch.zeros(
                (batch_size, 3, 2),
                device=particle_database._domain_bounding_box.device,
                dtype=particle_database._domain_bounding_box.dtype,
            )

            domain_bounding_box = torch.zeros(
                (batch_size, 3, 2),
                device=particle_database._domain_bounding_box.device,
                dtype=particle_database._domain_bounding_box.dtype,
            )
            min_t = torch.min(rotated_postions, dim=1, keepdim=False)[0]
            max_t = torch.max(rotated_postions, dim=1, keepdim=False)[0]
            new_domain[..., 0] = min_t
            new_domain[..., 1] = max_t

            domain_bounding_box[..., 0] = torch.min(
                torch.stack(
                    (
                        new_domain[..., 0],
                        particle_database._domain_bounding_box[..., 0].repeat(
                            batch_size, 1
                        ),
                    )
                ),
                keepdim=False,
                dim=0,
            )[0]

            domain_bounding_box[..., 1] = torch.max(
                torch.stack(
                    (
                        new_domain[..., 1],
                        particle_database._domain_bounding_box[..., 1].repeat(
                            batch_size, 1
                        ),
                    )
                ),
                keepdim=False,
                dim=0,
            )[0]
        else:
            domain_bounding_box = particle_database._domain_bounding_box

        # new matrices where create batch the attributes as well
        if batch_size > p_pos.shape[0] and p_pos.shape[0] == 1:
            attributes = {}
            for key, value in particle_database._attributes.items():
                current_shape = np.ones(len(value.get_data().shape)).astype(np.int64)
                current_shape[0] *= batch_size
                attributes[key] = BasicParticleData(
                    value.get_data().repeat(*current_shape)
                )
            # the domain bounding box was not updated but the batch size changed so use
            # the old one and just adjust the batch dim
            if not recompute_bounding_box:
                # update the domain bounding box as well
                bb_shape = np.ones(len(domain_bounding_box.shape)).astype(np.int64)
                bb_shape[0] *= batch_size
                domain_bounding_box = domain_bounding_box.repeat(*bb_shape)
        else:
            attributes = particle_database._attributes

        return cls(
            positions=BasicParticleData(rotated_postions),
            domain_bounding_box=domain_bounding_box,
            attributes=attributes,
        )

    @classmethod
    def init_from_particle_database(
        cls,
        particle_database: T,
        positions: BasicParticleData = None,
        domain_bounding_box: torch.tensor = None,
        attributes: Optional[Dict[str, BasicParticleData]] = None,
    ) -> T:
        """Create a new particle database from a existing one
            All attributes which are not set are taken from the passed pdb
            If the new data has a bigger bounding box the new bounding box is extended.


        Args:
            particle_database (ParticleDatabase): pdb used as a source
            positions (BasicParticleData): New positions
            domain_bounding_box (torch.tensor): new domain bounding box
            attributes (dict): dictionary with new attributes

        Returns:
            ParticleDatabase: new pdb
        """

        new_postions = (
            positions
            if positions is not None
            else particle_database.get_data(particle_database.POSITIONS_KEY)
        )
        _, nr_particles, _ = new_postions.get_data().shape
        new_attributes = {} if attributes is None else attributes.copy()
        for (key, value) in particle_database._attributes.items():
            if key not in new_attributes.keys():
                new_attributes[key] = value
            _, cur_nr_particle, _ = new_attributes[key].get_data().shape
            if cur_nr_particle != nr_particles:
                raise ValueError(
                    f"Every attribute should have the same number of particles as there are positions. {key}: {cur_nr_particle} != {nr_particles}"
                )

        return cls(
            positions=new_postions,
            domain_bounding_box=domain_bounding_box
            if domain_bounding_box is not None
            else particle_database._domain_bounding_box,
            attributes=new_attributes,
        )

    def add_attribute_from_node_grid(
        self,
        key: str,
        grid: "Node2dGrid",
        kernel: "AbstractKernel",
        overwrite: bool = False,
    ):
        """Add an attribute to the database from a NodeGrid

        Args:
            key (str): Key where the attribute should be stored
            grid (Node2dGrid): NodeGrid holding the attribute
            kernel (AbstractKernel): Kernel used for splatting
            overwrite (bool, optional): If the key already exists, should it be overwritten. Defaults to False.

        """
        particle_data_tensor = grid_to_particle(
            particle_positions=self._positions.get_data(),
            grid_data=grid.get_data(),
            domain_bounding_box=self._domain_bounding_box,
            kernel=kernel,
            grid_type=GridType.NODE,
        )
        self.add_attribute(key, BasicParticleData(data=particle_data_tensor), overwrite)

    def add_attribute_from_real_grid(
        self,
        key: str,
        grid: "Real2dGrid",
        kernel: "AbstractKernel",
        overwrite: bool = False,
    ):
        """Add an attribute to the database from a RealGrid

        Args:
            key (str): Key where the attribute should be stored
            grid (Real2dGrid): Real2dGrid holding the attribute
            kernel (AbstractKernel): Kernel used for splatting
            overwrite (bool, optional): If the key already exists, should it be overwritten. Defaults to False.

        """
        particle_data_tensor = grid_to_particle(
            particle_positions=self._positions.get_data(),
            grid_data=grid.get_data(),
            domain_bounding_box=self._domain_bounding_box,
            kernel=kernel,
            grid_type=GridType.REAL,
        )
        self.add_attribute(key, BasicParticleData(data=particle_data_tensor), overwrite)

    def get_domain_bounding_box(self) -> torch.Tensor:
        """Get the domain bounding box of the particles

        Returns:
            torch.Tensor: bounding box of shape B x 2/3 x 2
        """
        return self._domain_bounding_box

    def set_domain_bounding_box(self, domain_bounding_box) -> torch.Tensor:
        """set the domain bounding box of the particles  shape B x 2/3 x 2"""

        self._domain_bounding_box = domain_bounding_box

    def add_attribute(self, key: str, data: BasicParticleData, overwrite: bool = False):
        """Add an attribute to the db or update the positions of each particle

        Args:
            key (str): Key where the attribute should be stored
            data (BasicParticleData): The data that should be stored
            overwrite (bool, optional): If the key already exists, should it be overwritten. Defaults to False.

        Raises:
            ValueError: key already exists and overwrite is false
            ValueError: The number of particles do not match
        """
        if (key in self._attributes and not overwrite) or (
            key == self.POSITIONS_KEY and not overwrite
        ):
            raise ValueError(
                f"{key} already exists in attributes, if you want to overwrite it, set the flag."
            )
        else:
            if (
                data.get_number_of_particles()
                != self._positions.get_number_of_particles()
            ):
                raise ValueError(
                    f"The size of the data does not match the database: {data.get_number_of_particles()} != {self._positions.get_number_of_particles()}"
                )
            if key == self.POSITIONS_KEY:
                self._positions = data
            else:
                self._attributes[key] = data

    def get_data(self, key: str) -> BasicParticleData:
        """Get the raw data

        Args:
            key (str): key of the data

        Raises:
            ValueError: key does not exist in db

        Returns:
            BasicParticleData: The data under that key
        """
        if key == self.POSITIONS_KEY:
            return self._positions
        if key not in self._attributes:
            raise ValueError(f"{key} does not exist in database")
        return self._attributes[key]

    def get_tensor(self, key: str) -> torch.Tensor:
        """Get the data as tensor

        Args:
            key (str): key of the data

        Raises:
            ValueError: key does not exist in db

        Returns:
            torch.Tensor: The data under that key
        """
        if key == self.POSITIONS_KEY:
            return self._positions.get_data()
        if key not in self._attributes:
            raise ValueError(f"{key} does not exist in database")
        return self._attributes[key].get_data()

    def get_numpy(self, key: str) -> np.ndarray:
        """Get the data as numpy array

        Args:
            key (str): key of the data

        Raises:
            ValueError: key does not exist in db

        Returns:
            np.ndarray: The data under that key
        """
        if key == self.POSITIONS_KEY:
            return self._positions.get_numpy()
        if key not in self._attributes:
            raise ValueError(f"{key} does not exist in database")
        return self._attributes[key].get_numpy()

    def get_number_of_particles(self) -> int:
        """Return the number of particles in the db

        Returns:
            int: how many particles are stored
        """
        return self._positions.get_number_of_particles()
