from enum import Enum


class ParticleStatus(Enum):
    NEW = 0
    ALIVE = 1
    DEAD = 2
    INVALID = 3

    def __int__(self):
        return self.value
