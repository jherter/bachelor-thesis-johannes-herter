import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from licplot import lic_internal
from PIL import Image


def show_2d_image(array: np.array, colormap: str = "RGB"):  # pragma: no cover
    """Show a 2d array as an image the array should have H X W X C

    Args:
        array (np.array): data to be visualized
        colormap (str, optional): Which colormap to use. Defaults to "RGB".
    """
    if colormap in ["ARROW", "ARROW_COLOR", "LIC", "STREAM", "STREAM_COLOR"]:
        vector_plot(array, mode="show", plot_type=colormap)
    elif colormap == "RDBU_BAR":
        rdbu_bar_plot(array, mode="show")
    else:
        array = array[::-1]
        array = _process_img_2d(array, colormap)
        plt.figure(figsize=(10, 10))
        plt.imshow(array)
        plt.xticks([])
        plt.yticks([])
        plt.show()


def rdbu_bar_plot(
    array: np.array, filename: str = None, mode: str = "show", dpi: int = 600
):
    """Create the red blue bar plot

    Args:
        array (np.array): data, of shape H X W X C
        filename (str, optional): filename to save the data. Defaults to None.
        save (bool, optional): flag if the image should be saved or shown. Defaults to False.
        dpi (int, optional): DPI of the image. Defaults to 600.
    """
    assert len(array.shape) == 3 and array.shape[2] == 1
    image = array[:, :, 0]
    fig = plt.figure()
    canvas = FigureCanvas(fig)
    plot = plt.imshow(image, cmap="RdBu")
    plt.axis("off")
    if mode == "save":
        plt.colorbar(plot, shrink=0.8)
        assert filename is not None
        plt.savefig(filename, dpi=dpi)
        plt.close()
    elif mode == "show":
        plt.colorbar(plot, shrink=0.8)
        plt.show()  # pragma: no cover
        plt.close()
    else:
        width, height = fig.get_size_inches() * fig.get_dpi()
        img = np.fromstring(canvas.to_string_rgb(), dtype="uint8").reshape(
            height, width, 3
        )
        return img


def vector_plot(
    array: np.ndarray,
    filename: str = None,
    mode: str = "show",
    plot_type: str = "ARROW",
    dpi: int = 600,
    kernel_len: int = 100,
    upsample: bool = True,
    upsample_short_size: int = 1200,
    noise_factor: float = 4.0,
):
    """save a 2d velocity field as a vector plot

    Args:
        array (np.ndarray):  2D velocity field, in shape [H, W, C]
        filename (str, optional): The filename to save the plot. Defaults to None.
        mode (str, optional): Can be "show", "save" or "return". Defaults to "show".
        plot_type (str, optional): Type of the vector plot. Defaults to "ARROW".
        dpi (int, optional): The dpi of the save image. Defaults to 600.
    """
    assert len(array.shape) == 3
    assert array.shape[-1] >= 2
    assert mode in ["show", "save", "return"]
    res_y, res_x, _ = array.shape
    X = np.arange(0, res_x, 1)
    Y = np.arange(0, res_y, 1)
    X, Y = np.meshgrid(X, Y)
    U = array[:, :, 0]
    V = array[:, :, 1]
    if plot_type == "ARROW":
        r = int(res_x / 32) if res_x >= 64 and res_y >= 64 else 1
        fig = plt.figure()
        fig.add_axes([0, 0, 1, 1])
        plt.quiver(X[::r, ::r], Y[::r, ::r], U[::r, ::r], V[::r, ::r], width=0.001)
        plt.axis("off")
    elif plot_type == "ARROW_COLOR":
        r = int(res_x / 32) if res_x >= 64 and res_y >= 64 else 1
        speed = np.sqrt(U ** 2 + V ** 2)
        plot = plt.quiver(
            X[::r, ::r],
            Y[::r, ::r],
            U[::r, ::r],
            V[::r, ::r],
            speed[::r, ::r],
            width=0.001,
            cmap="gist_heat",
        )
        plt.colorbar(plot)
        plt.axis("off")
    elif plot_type == "STREAM":
        plt.streamplot(
            X,
            Y,
            U,
            V,
            density=2,
            linewidth=1,
            arrowsize=1,
            arrowstyle="->",
            color="k",
        )
        plt.axis("off")
    elif plot_type == "STREAM_COLOR":
        speed = np.sqrt(U ** 2 + V ** 2)
        plot = plt.streamplot(
            X,
            Y,
            U,
            V,
            color=speed,
            cmap="gist_heat",
            density=2,
            linewidth=1,
            arrowsize=1,
            arrowstyle="->",
        )
        plt.colorbar(plot.lines)
        plt.axis("off")
    elif plot_type == "LIC":
        if upsample:
            ratio = upsample_short_size / min(res_x, res_y)
            res_x = int(ratio * res_x)
            res_y = int(ratio * res_y)

            U = np.array(Image.fromarray(U).resize((res_x, res_y)))
            V = np.array(Image.fromarray(V).resize((res_x, res_y)))
        kernel = np.sin(np.arange(kernel_len) * np.pi / kernel_len)
        kernel = kernel.astype(np.float32)
        texture = np.random.randn(
            int(res_y / noise_factor), int(res_x / noise_factor)
        ).astype(np.float32)
        texture = np.array(
            Image.fromarray(texture).resize((res_x, res_y)), dtype=np.float32
        )

        img = lic_internal.line_integral_convolution(U, V, texture, kernel)
        plt.clf()
        plt.axis("off")
        plt.figimage(img[::-1], cmap="hot", vmin=np.min(img) / 3, vmax=np.max(img) / 3)
        plt.gcf().set_size_inches((res_x / float(dpi), res_y / float(dpi)))
        if mode == "return":
            return img
    else:
        raise ValueError(
            f"vector_plot: plot_type {plot_type} not recognized."
        )  # pragma: no cover
    if mode == "save":
        assert filename is not None
        plt.savefig(filename, dpi=dpi)
    elif mode == "show":
        plt.show()  # pragma: no cover
    plt.close()


def _process_img_2d(array: np.array, colormap="RGB") -> np.ndarray:
    """Transform data to valid image range data

    Args:
        array (np.array): data to be transformed
        colormap (str, optional): Which colormap to use. Defaults to "RGB".

    Raises:
        ValueError: If the colormap is not supported

    Returns:
        np.ndarray: The image
    """
    # remove batch dim
    array = np.squeeze(array)
    # deal with velocity
    if len(array.shape) == 3:
        # add a zero channel to the 2d velocity
        if array.shape[2] == 2:
            array = np.concatenate(
                (array, np.expand_dims(np.zeros_like(array[..., 0]), axis=2)),
                axis=2,
            )
    max_ = np.absolute(array.max())
    min_ = array.min() if array.min() >= 0 else -max_

    # scale from [0, 1]
    array = (array - min_) / (max_ - min_)
    if colormap == "RGB":
        array = array * 255.0
    elif colormap == "OrRd":
        cmap = plt.cm.get_cmap("OrRd")
        array = np.minimum(1, cmap(array) / cmap(0)) * 255.0
    elif colormap in plt.colormaps():
        cmap = plt.get_cmap(colormap)
        array = cmap(array) * 255.0
    else:
        raise ValueError("Colormap {} not supported.".format(colormap))
    array = np.uint8(array)
    return array
