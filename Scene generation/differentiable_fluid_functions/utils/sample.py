# Copyright © 2021 Disney | Copyright © 2021 ETH Zurich.  All rights reserved

import torch
import math
from typing import List, Optional
import torch.nn.functional as F
from differentiable_fluid_functions.kernels import AbstractKernel

from differentiable_fluid_functions.types import GridType
from .splatter import Splatter
import warnings


def normalize_2d_meshgrid_tensor(
    grid: torch.Tensor,
    normalize_x: float = 1,
    normalize_y: float = 1,
) -> torch.Tensor:

    # make dependent on x or y
    mgrid_normed_x = 2.0 * grid[:, 0:1, ...] / normalize_x - 1.0
    mgrid_normed_y = 2.0 * grid[:, 1:2, ...] / normalize_y - 1.0

    mgrid_normed = torch.cat((mgrid_normed_x, mgrid_normed_y), dim=1)
    return mgrid_normed


def normalize_3d_meshgrid_tensor(
    grid: torch.Tensor,
    normalize_x: float = 1,
    normalize_y: float = 1,
    normalize_z: float = 1,
) -> torch.Tensor:

    # make dependent on x or y
    mgrid_normed_x = 2.0 * grid[:, 0:1, ...] / normalize_x - 1.0
    mgrid_normed_y = 2.0 * grid[:, 1:2, ...] / normalize_y - 1.0
    mgrid_normed_z = 2.0 * grid[:, 2:3, ...] / normalize_z - 1.0

    mgrid_normed = torch.cat((mgrid_normed_x, mgrid_normed_y, mgrid_normed_z), dim=1)
    return mgrid_normed


def create_2d_meshgrid_tensor(
    size: List[int],
    device: torch.device = torch.device("cpu"),
    dtype=torch.float32,
) -> torch.Tensor:
    [batch, _, height, width] = size
    y_pos, x_pos = torch.meshgrid(
        [
            torch.arange(0, height, device=device, dtype=dtype),
            torch.arange(0, width, device=device, dtype=dtype),
        ]
    )
    mgrid = torch.stack([x_pos, y_pos], dim=0)  # [C, H, W]
    mgrid = mgrid.unsqueeze(0)  # [B, C, H, W]
    mgrid = mgrid.repeat(batch, 1, 1, 1)
    return mgrid


def create_3d_meshgrid_tensor(
    size: List[int],
    device: torch.device = torch.device("cpu"),
    dtype=torch.float32,
) -> torch.Tensor:
    [batch, _, depth, height, width] = size
    z_pos, y_pos, x_pos = torch.meshgrid(
        [
            torch.arange(0, depth, device=device, dtype=dtype),
            torch.arange(0, height, device=device, dtype=dtype),
            torch.arange(0, width, device=device, dtype=dtype),
        ]
    )

    mgrid = torch.stack([x_pos, y_pos, z_pos], dim=0)  # [C, D, H, W]
    mgrid = mgrid.unsqueeze(0)  # [B, C, D, H, W]
    mgrid = mgrid.repeat(batch, 1, 1, 1, 1)
    return mgrid


def create_2d_gaussian_kernel(
    size: int,
    sigma: float,
    out_channels: int = 1,
    device: torch.device = torch.device("cpu"),
    dtype=torch.float32,
) -> torch.Tensor:
    grid = create_2d_meshgrid_tensor([1, 1, size, size], device, dtype)
    grid = grid - (size - 1.0) / 2.0
    grid_x = grid[:, 0:1, ...]
    grid_y = grid[:, 1:2, ...]
    kernel = (1.0 / (2.0 * math.pi * sigma * sigma)) * torch.exp(
        -(grid_x * grid_x + grid_y * grid_y) / sigma / sigma / 2.0
    )
    kernel = kernel / torch.sum(kernel)
    kernel.repeat(out_channels, 1, 1, 1)
    return kernel


def create_3d_gaussian_kernel(
    size: int,
    sigma: float,
    out_channels: int = 1,
    device: torch.device = torch.device("cpu"),
    dtype=torch.float32,
) -> torch.Tensor:
    grid = create_3d_meshgrid_tensor([1, 1, size, size, size], device, dtype)
    grid = grid - (size - 1.0) / 2.0
    grid_x = grid[:, 0:1, ...]
    grid_y = grid[:, 1:2, ...]
    grid_z = grid[:, 2:3, ...]
    kernel = (1.0 / (math.pow(2.0 * math.pi, 1.5) * sigma * sigma * sigma)) * torch.exp(
        -(grid_x * grid_x + grid_y * grid_y + grid_z * grid_z) / sigma / sigma / 2.0
    )
    kernel = kernel / torch.sum(kernel)
    kernel.repeat(out_channels, 1, 1, 1, 1)
    return kernel


def sample_grid_2d(
    grid: torch.Tensor,
    back_trace: torch.Tensor,
    normalize_x: float,
    normalize_y: float,
) -> torch.Tensor:
    # grid:  [B, C, H, W]
    # normalize the field to [-1, 1]
    back_trace_normed = normalize_2d_meshgrid_tensor(
        back_trace, normalize_x, normalize_y
    )
    permutation = (0, 2, 3, 1)
    # [B, H, W, C]
    back_trace_normed = back_trace_normed.permute(permutation)

    # go back in time and interpolate to get updated velocity
    grid_sampled = F.grid_sample(
        grid,
        back_trace_normed,
        mode="bilinear",
        padding_mode="border",
        align_corners=True,
    )

    return grid_sampled


def sample_grid_3d(
    grid: torch.Tensor,
    back_trace: torch.Tensor,
    normalize_x: float,
    normalize_y: float,
    normalize_z: float,
) -> torch.Tensor:
    # grid:  [B, C, D, H, W]
    # normalize the field to [-1, 1]
    back_trace_normed = normalize_3d_meshgrid_tensor(
        back_trace, normalize_x, normalize_y, normalize_z
    )
    permutation = (0, 2, 3, 4, 1)
    # [B, D, H, W, C]
    back_trace_normed = back_trace_normed.permute(permutation)

    # go back in time and interpolate to get updated velocity
    grid_sampled = F.grid_sample(
        grid,
        back_trace_normed,
        mode="bilinear",
        padding_mode="border",
        align_corners=True,
    )

    return grid_sampled


def get_uniform_noise(
    dimension: List[int],
    device: torch.device = torch.device("cpu"),
    min=0.0,
    max=1.0,
    dtype=torch.float32,
):
    if max < min:
        raise ValueError("max must be greater than min")
    noise_grid = torch.rand(dimension, dtype=dtype, device=device)  # [0, 1]
    noise_grid = noise_grid * (max - min) + min
    return noise_grid


def get_loop_range(kernel: AbstractKernel) -> List:
    """Get how many cell are affected by a kernel

    Args:
        kernel (AbstractKernel): The kernel which is used

    Returns:
        List: Returns a list with a loop range and the size of the ring
    """
    cell_range = torch.tensor(float(kernel.support_radius))
    ring = torch.ceil(cell_range).to(torch.int64).item()
    loop_range = list(range(-ring, ring + 1))
    return loop_range, ring


def get_base_coordinates_for_grid(positions: torch.Tensor) -> torch.Tensor:
    """Transform positions to indices

    Args:
        positions (torch.Tensor): The positions (floats) that should be transformed
        to NodeGrid indices (int). shape B x nPoints x 2/3

    Returns:
        torch.Tensor: index for every position in the grid
    """
    return torch.round(positions).to(torch.int64)


def _p2g_g2p_common_process(
    positions: torch.tensor,
    domain_bounding_box: torch.tensor,
    simulation_size: torch.tensor,
    kernel: AbstractKernel,
    grid_type: "GridType",
    p2g_flag: bool = True,
) -> List[torch.Tensor]:
    """Common process used by the p2g and g2p function

    Args:
        positions (torch.tensor): Shape: B x nPoints x 2 or 3
        domain_bounding_box (torch.tensor): B X 2 X 2 or 3
        simulation_size (torch.tensor): Shape: 2 or 3
        kernel (AbstractKernel): kernel used for splatting

    Returns:
        List[torch.Tensor]:
            grid_resolution, (shape of the new grid) int xyz
            base_coordinates, (grid coordinates of the particles)
            weights (weights for each diagonal element of a particles grid cell)
            cell_size, the cell_size of the grid
    """

    is_2d = positions.shape[-1] != 3
    # for every sample in the batch compute the grid size
    batch_size = positions.shape[0]
    # flip the order to have the format xy
    simulation_size_flipped = torch.flip(torch.clone(simulation_size), dims=[0])
    simulation_size_batched = simulation_size_flipped.unsqueeze(0).repeat(batch_size, 1)
    # B x 2 or 3
    grid_sizes = domain_bounding_box[..., 1] - domain_bounding_box[..., 0]
    # compute the size of each cell
    cell_size = grid_sizes / simulation_size_batched
    if not torch.allclose(cell_size, cell_size[0, 0], atol=1e-4):
        raise RuntimeError(
            f"anisotropic cell size detected: {cell_size}"
            f"\nPlease fix the simulation_size: {simulation_size_batched}"
            f"\nor domain_bounding_box: {domain_bounding_box}."
        )

    # dx = 1/cell_size
    cell_size_inverse = simulation_size_batched / grid_sizes

    # get from kernel
    loop_range, ring = get_loop_range(kernel)

    grid_resolution = simulation_size + 1 + ring * 2
    # an additional border is required for all kernels
    offset = cell_size * ring
    if grid_type == GridType.REAL and p2g_flag:
        grid_resolution += 1
        offset += cell_size / 2

    # adjust domain boundaries to ghost boundaries
    new_domain_min = domain_bounding_box[..., 0] - offset
    new_domain_max = domain_bounding_box[..., 1] + offset
    new_domain = torch.stack((new_domain_min, new_domain_max), dim=-1)

    # get nearest node position: idx_x = (x-x0)/dx,
    # Shape B x 1 x 2/3 min point in the space of the new domain
    x0s = new_domain[..., 0][:, None, :]

    # float numbers representing closest grid cell
    positions_grid_coords = (positions - x0s) * cell_size_inverse[:, None, :]

    # indices in the grid
    base_coordinates = get_base_coordinates_for_grid(positions_grid_coords)

    weights = []
    if kernel.dyadic:
        for a in loop_range:
            # the position of the node in space
            distance = positions_grid_coords - (base_coordinates + a)
            weights.append(kernel(distance))
    else:
        loop_range_z = [0] if is_2d else loop_range
        for i, x in enumerate(loop_range):
            weights_y = []
            for j, y in enumerate(loop_range):
                weights_z = []
                for k, z in enumerate(loop_range_z):
                    grid_offset = [x, y]
                    if not is_2d:
                        grid_offset.append(z)
                    a = torch.tensor(grid_offset, device=positions.device)[
                        None, None, :
                    ]
                    distance = positions_grid_coords - (base_coordinates + a)
                    weight = kernel(distance)
                    weights_z.append(weight)
                weights_y.append(weights_z)
            weights.append(weights_y)

    return grid_resolution, base_coordinates, weights, cell_size


def grid_to_particle(
    particle_positions: torch.tensor,
    grid_data: torch.tensor,
    domain_bounding_box: torch.tensor,
    kernel: AbstractKernel,
    grid_type: "GridType",
    filter_particles_out_of_bounds: bool = False,
) -> torch.Tensor:
    """G2P function for a grid

    Args:
        particle_positions (torch.tensor): Shape: B x nPoints x 2 or 3
        grid_data (torch.tensor): Data of the grid in shape B x NChannels X height x width
        domain_bounding_box (torch.tensor): of the particles [h, w]
        kernel (AbstractKernel): Kernel to use for the splatting
        grid_type (GridType): The type of the given grid

    Raises:
        ValueError: Particle positions have the wrong shape
        ValueError: grid dim does not match particle dim
        ValueError: Bounding box dim does not match particle dim

    Returns:
        torch.Tensor: Properties splatted to the particles B x nPoints x NChannels
    """
    if len(particle_positions.shape) != 3:
        raise ValueError(
            "Particle positions requires: B X nPoints x nChannels\n"
            f"Currently: {particle_positions.shape}"
        )

    if len(grid_data.shape) != particle_positions.shape[-1] + 2:
        raise ValueError(
            "The grid data dimension does not match the particle positions\n"
            f"Currently: {grid_data.shape} and {particle_positions.shape}"
        )

    if domain_bounding_box.shape[1] != particle_positions.shape[-1]:
        raise ValueError(
            "bounding box require 2 2d points in 2d and 2 3d points in 3d. \n"
            f"Currently: the data is {particle_positions.shape[-1]}D and the bounding box is {domain_bounding_box.shape[1]}"
        )
    if filter_particles_out_of_bounds and particle_positions.shape[0] != 1:
        raise ValueError(
            "Filtering to the bounding box extent can only be done for a single sample. \n"
            f"Currently: batch size is {particle_positions.shape[0]}"
        )
    # filter the positions which are outside of the bounding box
    if filter_particles_out_of_bounds:
        # use dummy data for the data
        filter_particle_positions, _ = filter_by_bounding_box(
            particle_data=torch.zeros_like(particle_positions),
            particle_positions=particle_positions,
            domain_bounding_box=domain_bounding_box,
        )
    else:
        filter_particle_positions = particle_positions

    is_2d = filter_particle_positions.size(-1) == 2
    if is_2d:
        batch_size, n_features, res_y, res_x = grid_data.size()
        simulation_size = torch.tensor(
            [res_y - 1, res_x - 1], device=grid_data.device, dtype=torch.int32
        )
    else:
        batch_size, n_features, res_z, res_y, res_x = grid_data.size()
        simulation_size = torch.tensor(
            [res_z - 1, res_y - 1, res_x - 1],
            device=grid_data.device,
            dtype=torch.int32,
        )
    # computes weights and base_coor
    grid_resolution, base_coordinates, weights, cell_size = _p2g_g2p_common_process(
        positions=filter_particle_positions,
        domain_bounding_box=domain_bounding_box,
        simulation_size=simulation_size,
        kernel=kernel,
        grid_type=grid_type,
        p2g_flag=False,
    )

    # gather grid values to particles
    loop_range, ring = get_loop_range(kernel)
    if ring > 0:
        pad = (
            (ring, ring, ring, ring) if is_2d else (ring, ring, ring, ring, ring, ring)
        )

        grid_data = torch.nn.functional.pad(grid_data, pad=pad)

    n_grid_nodes = torch.cumprod(grid_resolution, dim=0)[-1].item()
    grid_data = grid_data.view(batch_size, n_features, n_grid_nodes)
    grid_data = grid_data.permute((0, 2, 1))
    value = torch.zeros(
        [batch_size, particle_positions.size(1), n_features],
        device=particle_positions.device,
        dtype=grid_data.dtype,
    )
    gathered_weight = torch.zeros(
        [batch_size, particle_positions.size(1), 1],
        device=particle_positions.device,
        dtype=grid_data.dtype,
    )

    loop_range_z = [0] if is_2d else loop_range

    for i, x in enumerate(loop_range):
        coor_x = base_coordinates[:, :, 0:1] + x
        for j, y in enumerate(loop_range):
            coor_y = base_coordinates[:, :, 1:2] + y
            for k, z in enumerate(loop_range_z):
                # compute weights
                if kernel.dyadic:
                    weight = weights[i][..., 0:1] * weights[j][..., 1:2]
                    if not is_2d:
                        weight = weight * weights[k][..., 2:3]
                else:
                    weight = weights[i][j][k]

                # compute the flattened index of given node
                grid_idx = coor_y * grid_resolution[-1] + coor_x
                if not is_2d:
                    coor_z = base_coordinates[:, :, 2:3] + z
                    grid_idx = (
                        coor_z * grid_resolution[-2] * grid_resolution[-1] + grid_idx
                    )
                grid_idx = grid_idx.to(
                    torch.int64
                )  # int64 is required by torch.Tensor.scatter_add
                # gather particle values and weights onto grid
                gathered_weight += (
                    torch.gather(
                        torch.ones_like(grid_data[..., 0:1]), dim=1, index=grid_idx
                    )
                    * weight
                )
                grid_idx = grid_idx.repeat(1, 1, n_features)
                value += torch.gather(grid_data, dim=1, index=grid_idx) * weight
    value = torch.where(gathered_weight > 0, value / (gathered_weight + 1e-9), value)
    return value


def filter_by_bounding_box(
    particle_data: torch.tensor,
    particle_positions: torch.tensor,
    domain_bounding_box: torch.tensor,
) -> List[torch.Tensor]:
    """remove all particles and data points which are outside the bounding box

    Args:
        particle_data (torch.tensor): data of each particle B x nPoints x nChannels
        particle_positions (torch.tensor): position of each particle B x nPoints x 2/3
        domain_bounding_box (torch.tensor): B x 2/3 x 2

    Returns:
        List[torch.Tensor]: the filtered data points and data
    """
    batch, nParticles, dim = particle_positions.shape
    if batch > 1:
        # otherwise there could be different amount of particles per batch
        raise ValueError(
            "filtering by bounding box can only be done if the batch size is 1"
        )

    bbox = domain_bounding_box[0].permute((1, 0))
    mask = (
        (particle_positions[0] >= bbox[0:1]) & (particle_positions[0] <= bbox[1:2])
    ).all(dim=-1)
    index = torch.nonzero(mask.to(torch.int64), as_tuple=False).squeeze()

    filtered_particle_positions = torch.index_select(
        particle_positions,
        dim=1,
        index=index,
    )
    filtered_particle_data = torch.index_select(
        particle_data,
        dim=1,
        index=index,
    )
    return filtered_particle_positions, filtered_particle_data


def particle_to_grid(
    particle_data: torch.tensor,
    particle_positions: torch.tensor,
    simulation_size: torch.tensor,
    domain_bounding_box: torch.tensor,
    kernel: AbstractKernel,
    grid_type: "GridType",
    normalization: bool = False,
    filter_particles_out_of_bounds: bool = False,
    grad_ckpt: str = "off",
    chunk_size: Optional[int] = 2 ** 14,
    use_splatter: bool = True,
) -> List[torch.tensor]:
    """P2G function that splats particles to a grid

    Args:
        particle_data (torch.tensor): Data which should be splatted to the grid shape: B x nPoints x nChannels
        particle_positions (torch.tensor): Locations of the particles : B x nPoints x 2/3
        simulation_size (torch.tensor): The resolution of the grid [h, w]
        domain_bounding_box (torch.tensor): The bounding box of the simulation shape: B x 2 x 2/3
        kernel (AbstractKernel): Kernel used for the splatting
        grid_type (GridType): The type of the given grid
        normalization (bool): Flag if the data should be normalized
        filter_particles_out_of_bounds (bool): Flag to filter particles outside of the bounding box
        grad_ckpt: Whether gradient checkpointing should be used to save memory (increases computation time).
            Options are ["off", "kernel", "scatter"], scatter enables checkpoint for both, kernels computation and
                scattering onto the grid.
        chunk_size: How many points are processed at once.
        filter_out_of_bound_ids: partially filter kernel positions that are out of bound
        use_splatter (bool): flag if the new splatter should be used
    Raises:
        ValueError: wrong particle data shape
        ValueError: particle data does not match particle positions
        ValueError: simulation size does not match particle dimensions
        ValueError: domain bounding box does not match particle dim

    Returns:
        List[torch.tensor]: values of the grid and weights
    """

    if len(particle_data.shape) != 3 or len(particle_positions.shape) != 3:
        raise ValueError(
            "particle_Data requires data with shape B x nrPoints x 2/3 and particle positions requires: B X npoints x nChannels\n"
            f"Currently: {particle_data.shape} and {particle_positions.shape}"
        )

    if particle_data.shape[:2] != particle_positions.shape[:2]:
        raise ValueError(
            "particle_Data particle positions require same amount of samples and particles\n"
            f"Currently: {particle_data.shape} and {particle_positions.shape}"
        )

    if simulation_size.shape[0] != particle_positions.shape[-1]:
        raise ValueError(
            "simulation_size should be 2d is particle positions are 2d\n"
            f"Currently: {simulation_size.shape[0]} and {particle_positions.shape[-1]}"
        )
    if domain_bounding_box.shape[1] != particle_positions.shape[-1]:
        raise ValueError(
            "bounding box require 2 2d points in 2d and 3 3d points in 3d. \n"
            f"Currently: {domain_bounding_box.shape} and data is {particle_positions.shape[-1]}D"
        )

    if filter_particles_out_of_bounds and particle_positions.shape[0] != 1:
        raise ValueError(
            "Filtering to the bounding box extent can only be done for a single sample. \n"
            f"Currently: batch size is {particle_positions.shape[0]}"
        )
    if filter_particles_out_of_bounds:
        particle_positions, particle_data = filter_by_bounding_box(
            particle_data=particle_data,
            particle_positions=particle_positions,
            domain_bounding_box=domain_bounding_box,
        )

    if use_splatter:
        ndim = particle_positions.shape[-1]

        # recover cell_size
        bbox_diff = domain_bounding_box[..., 1] - domain_bounding_box[..., 0]

        # bounding box and simulation_size specified in different order?
        cell_size = bbox_diff / torch.flip(simulation_size.clone(), dims=[0])
        if not torch.allclose(cell_size, cell_size[0, 0], atol=1e-4):
            raise RuntimeError(
                f"anisotropic cell size detected: {cell_size}"
                f"\nPlease fix the simulation_size: {simulation_size}"
                f"\nor domain_bounding_box: {domain_bounding_box}."
            )

        cell_size = cell_size[0, 0]

        if not grid_type == GridType.REAL:
            domain_bounding_box = domain_bounding_box.clone()
            domain_bounding_box[:, :, 0] -= 0.5 * cell_size
            domain_bounding_box[:, :, 1] += 0.5 * cell_size

        splatter = Splatter(
            kernel,
            cell_size,
            normalization,
            ndim,
            grad_ckpt,
            chunk_size,
        )

        return splatter(
            particle_positions,
            particle_data,
            domain_bounding_box,
        )
    else:
        warnings.warn(
            "use_splatter=False is deprecated, use use_splatter=True instead",
            DeprecationWarning,
        )
        return particle_to_grid_deprecated(
            particle_data,
            particle_positions,
            simulation_size,
            domain_bounding_box,
            kernel,
            grid_type,
            normalization,
        )


def particle_to_grid_deprecated(
    filtered_particle_data: torch.tensor,
    filter_particle_positions: torch.tensor,
    simulation_size: torch.tensor,
    domain_bounding_box: torch.tensor,
    kernel: AbstractKernel,
    grid_type: "GridType",
    normalization: bool = False,
):
    is_2d = filter_particle_positions.shape[-1] != 3
    grid_resolution, base_coordinates, weights, cell_size = _p2g_g2p_common_process(
        positions=filter_particle_positions,
        domain_bounding_box=domain_bounding_box,
        simulation_size=simulation_size,
        kernel=kernel,
        grid_type=grid_type,
    )
    # extract the number of feature and the size of the batch
    batch_size, _, n_features = filtered_particle_data.size()
    # get the number of nodes in the grid
    n_nodes = torch.cumprod(grid_resolution, dim=0)[-1].to(torch.int64).item()
    weight_dtype = weights[0].dtype if kernel.dyadic else weights[0][0][0].dtype

    # create a linear array to compute the values of the grid
    weight_grid = torch.zeros(
        [batch_size, n_nodes, 1],
        device=filtered_particle_data.device,
        dtype=weight_dtype,
    )
    value_grid = torch.zeros(
        [batch_size, n_nodes, n_features],
        device=filtered_particle_data.device,
        dtype=weight_dtype,
    )

    loop_range, ring = get_loop_range(kernel)
    loop_range_z = [0] if is_2d else loop_range

    # scatter particle values to grid
    for i, x in enumerate(loop_range):
        coor_x = base_coordinates[:, :, 0:1] + x
        for j, y in enumerate(loop_range):
            coor_y = base_coordinates[:, :, 1:2] + y
            for k, z in enumerate(loop_range_z):
                # compute weights
                if kernel.dyadic:
                    weight = weights[i][..., 0:1] * weights[j][..., 1:2]
                    if not is_2d:
                        weight = weight * weights[k][..., 2:3]
                else:
                    weight = weights[i][j][k]

                # compute the flattened index of given node
                grid_idx = coor_y * grid_resolution[-1] + coor_x

                if not is_2d:
                    coor_z = base_coordinates[:, :, 2:3] + z
                    grid_idx = (
                        coor_z * grid_resolution[-2] * grid_resolution[-1] + grid_idx
                    )

                #
                grid_idx = grid_idx.to(torch.int64)  # int64 is required by scatter

                # scatter particle values and weights onto grid
                weight_grid.scatter_add_(
                    dim=1,
                    index=grid_idx,
                    src=weight,
                )

                grid_idx_ = grid_idx.repeat(1, 1, n_features)
                source = weight * filtered_particle_data
                value_grid.scatter_add_(
                    dim=1,
                    index=grid_idx_,
                    src=source,
                )

    # reshape the grid
    if normalization:
        # torch where does not backprop as expected, workaround adding a little eps to avoid
        # getting nan values in the gradient
        value_grid = torch.where(
            weight_grid > 0, value_grid / (weight_grid + 1e-9), value_grid
        )
    weight_grid = weight_grid.view([batch_size, 1, *grid_resolution])
    value_grid = value_grid.permute(0, 2, 1).contiguous()
    value_grid = value_grid.view([batch_size, n_features, *grid_resolution])
    boundary = ring

    weight_grid = _remove_grid_boundary(weight_grid, boundary, boundary)
    value_grid = _remove_grid_boundary(value_grid, boundary, boundary)

    if grid_type == GridType.REAL:
        weight_grid = _remove_grid_boundary(weight_grid, 1, 1)
        value_grid = _remove_grid_boundary(value_grid, 1, 1)

    return value_grid, weight_grid


def _remove_grid_boundary(grid: torch.tensor, lwidth: int, rwidth: int) -> torch.tensor:
    """Remove the grid boundary from a grid tensor 2/3 D

    Args:
        grid (torch.tensor): Grid tensor
        width (int): how much should be removed

    Raises:
        ValueError: The grid has the wrong dimensions

    Returns:
        torch.tensor: grid with boundary removed
    """
    if lwidth == 0 and rwidth == 0:
        return grid
    elif len(grid.size()) == 4:
        return grid[..., lwidth:-rwidth, lwidth:-rwidth]
    elif len(grid.size()) == 5:
        return grid[..., lwidth:-rwidth, lwidth:-rwidth, lwidth:-rwidth]
    else:
        raise ValueError("grid size not supported.")
