import torch
import torch.nn.functional as F
import numpy as np
from typing import List


def _deg2rad(deg: float) -> float:
    """convert degrees to radians

    Args:
        deg (float): degrees

    Returns:
        float: radians
    """
    return deg / 180.0 * np.pi


def get_persepective_4d(
    fov: float, aspect_ratio: float, znear: float, zfar: float
) -> np.ndarray:
    """Get a homogenous perspective matrix

    Args:
        fov (float): field of view
        aspect_ratio (float): aspect ration
        znear (float): near clipping plane
        zfar (float): far clipping plane

    Returns:
        np.ndarray: 4x4 projection matrix
    """

    fov = _deg2rad(fov)

    tanHalfFov = np.tan((fov / 2))
    max_y = tanHalfFov * znear
    min_y = -max_y
    max_x = max_y * aspect_ratio
    min_x = -max_x

    # NOTE: In OpenGL the projection matrix changes the handedness of the
    # coordinate frame. i.e the NDC space positive z direction is the
    # camera space negative z direction. This is because the sign of the z
    # in the projection matrix is set to -1.0.
    # In pytorch3d we maintain a right handed coordinate system throughout
    # so the so the z sign is 1.0.
    z_sign = 1.0
    K = np.zeros((4, 4))

    K[0, 0] = 2.0 * znear / (max_x - min_x)
    K[1, 1] = 2.0 * znear / (max_y - min_y)
    K[0, 2] = (max_x + min_x) / (max_x - min_x)
    K[1, 2] = (max_y + min_y) / (max_y - min_y)
    K[3, 2] = z_sign * 1.0

    # NOTE: This maps the z coordinate from [0, 1] where z = 0 if the point
    # is at the near clipping plane and z = 1 when the point is at the far
    # clipping plane.
    K[2, 2] = z_sign * zfar / (zfar - znear)
    K[2, 3] = -(zfar * znear) / (zfar - znear)
    return K.T


def look_at_view_transform(
    dist: float = 1.0,
    elev: float = 0.0,
    azim: float = 0.0,
    eye: np.array = None,
    at: np.array = np.array([0, 0, 0]),
    up: np.array = np.array([0, 1, 0]),
) -> np.ndarray:
    """
        Get look at matrix
    Args:
        dist (float, optional): distance . Defaults to 1.0.
        elev (float, optional): angle of elevation. Defaults to 0.0.
        azim (float, optional): azimuth  of elevation. Defaults to 0.0.
        eye ([np.array], optional): eye position 3d. Defaults to None.
        at ([np.array], optional): look at position. Defaults to np.array([0, 0, 0]).
        up ([np.array], optional): up vector. Defaults to np.array([0, 1, 0]).

    Returns:
        [np.array]: the look at matrix for shape 4 x4
    """

    if eye is not None:
        C = eye
    else:
        C = camera_position_from_spherical_angles(dist, elev, azim) + at

    R = look_at_rotation(C, at, up)
    T = -np.matmul(R.T, C)
    matrix = np.eye(4)
    matrix[:3, :3] = R
    matrix[3, :3] = T
    return matrix


def translation_matrix_4d(x: float, y: float, z: float) -> np.ndarray:
    """Get a 4d translation matrix

    Args:
        x (float): translation in x direction
        y (float): translation in y direction
        z (float): translation in z direction

    Returns:
        np.ndarray: translation matrix of shape 4 x 4
    """
    t_mat = np.eye(4)
    t_mat[-1, 0] = x
    t_mat[-1, 1] = y
    t_mat[-1, 2] = z
    return t_mat


def scale_matrix_4d(s: float) -> np.ndarray:
    """get a 4d scale matrix

    Args:
        s (float): scale factor

    Returns:
        np.ndarray: scale matrix of shape 4 x   4
    """
    s_mat = np.eye(4)
    s_mat[:3, :3] = scale_matrix(s)
    return s_mat


def rotation_matrix_4d(
    roll: float = 0.0, pitch: float = 0.0, yaw: float = 0.0, order: str = "rpy"
) -> np.ndarray:
    """Return a 4d rotation matrix

    Args:
        :param roll: Amount to rotate on Z axis in degrees
        :param pitch: Amount to rotate on X axis in degrees
        :param yaw: Amount to rotate on Y (up-)axis in degrees
        :param order: Order of rotations is Roll > Pitch > Yaw by default

    Returns:
        np.ndarray: 4 d rotation matrix 4 x 4
    """
    r_mat = np.eye(4)
    r_mat[:3, :3] = rotation_matrix(roll=roll, pitch=pitch, yaw=yaw, order=order)
    return r_mat


def camera_position_from_spherical_angles(
    distance: float, elevation: float, azimuth: float
) -> np.ndarray:
    """Transform spherical angle to a 3d coordinate

    Args:
        distance (float): distance value
        elevation (float): elevation in degrees
        azimuth (float): azimuth in degrees

    Returns:
        [np.ndarray]: camera position in 3d [x, y, z]
    """
    elev = _deg2rad(elevation)
    azim = _deg2rad(azimuth)
    x = distance * np.cos(elev) * np.sin(azim)
    y = distance * np.sin(elev)
    z = distance * np.cos(elev) * np.cos(azim)
    camera_position = np.array([x, y, z])

    return camera_position


def normalize(x: np.ndarray):
    """Normalize a vector to have the length 1

    Args:
        x (np.ndarray): input

    Returns:
        [np.ndarray]: normalized vector
    """
    return x / np.linalg.norm(x)


def look_at_rotation(
    camera_position: np.ndarray, at: np.ndarray, up: np.ndarray
) -> np.ndarray:
    """Get the rotation matrix from camera position and look at + up vector

    Args:
        camera_position ([np.ndarray]): 3d camera position
        at ([np.ndarray]):  3d look at position
        up ([np.ndarray]):  3d up vector

    Returns:
        [np.ndarray]: [description]
    """
    z_axis = normalize(at - camera_position)
    x_axis = normalize(np.cross(up, z_axis))
    y_axis = normalize(np.cross(z_axis, x_axis))

    R = np.stack((x_axis, y_axis, z_axis))
    return R.T


def _rz(roll: float) -> np.ndarray:
    """Get a rotation matrix around the z axis

    Args:
        roll (float): degrees for rotation

    Returns:
        np.ndarray: 3 x 3 rotation matrix
    """
    rad = _deg2rad(roll)
    c = np.cos(rad)
    s = np.sin(rad)
    rot_mat = np.array([[c, -s, 0], [s, c, 0], [0, 0, 1]])
    return rot_mat


def _ry(yaw: float) -> np.ndarray:
    """Get a rotation matrix around the y axis

    Args:
        yaw (float): degrees for rotation

    Returns:
        np.ndarray: 3 x 3 rotation matrix
    """
    rad = _deg2rad(yaw)
    c = np.cos(rad)
    s = np.sin(rad)
    rot_mat = np.array([[c, 0, s], [0, 1, 0], [-s, 0, c]])
    return rot_mat


def _rx(pitch: float) -> np.ndarray:
    """Get a rotation matrix around the x axis

    Args:
        pitch (float): degrees for rotation

    Returns:
        np.ndarray: 3 x 3 rotation matrix
    """
    rad = _deg2rad(pitch)
    c = np.cos(rad)
    s = np.sin(rad)
    rot_mat = np.array([[1, 0, 0], [0, c, s], [0, -s, c]])
    return rot_mat


def scale_matrix(s: float) -> np.ndarray:
    s_mat = np.array([[s, 0, 0], [0, s, 0], [0, 0, s]])
    return s_mat


def rotation_matrix(
    roll: float = 0.0, pitch: float = 0.0, yaw: float = 0.0, order: str = "rpy"
) -> np.ndarray:
    """
    :param roll: Amount to rotate on Z axis in degrees
    :param pitch: Amount to rotate on X axis in degrees
    :param yaw: Amount to rotate on Y (up-)axis in degrees
    :param order: Order of rotations is Roll > Pitch > Yaw by default
    :return: R = Yaw * Pitch * Roll

    Convention used in this function: Y is up axis and Z is depth axis.
    This rotation matrix can be applied to a 3D tensor [B,C,D,H,W] of shape where
    the corresponding axis for the shape are [-,-,Z,X,Y]
    """
    rz = _rz(roll)
    rx = _rx(pitch)
    ry = _ry(yaw)
    if order == "rpy":
        r = np.matmul(ry, np.matmul(rx, rz))
    elif order == "ypr":
        r = np.matmul(rz, np.matmul(rx, ry))
    else:
        raise NotImplementedError()
    return r


def get_id() -> np.ndarray:
    """return identity matrix

    Returns:
        np.ndarray: identity matrix shape 3 x3
    """
    return np.identity(3)


def get_camera_rotation_matrix(view: List[float]) -> np.ndarray:
    """Get a rotation matrix from a view

    Args:
        view (List[float]): theta (y axis rotation in degrees ), phi x-axis rotation in degrees

    Returns:
        np.ndarray: 3 x 3 rotation matrix
    """
    theta, phi = view
    rz = _ry(phi)
    ry = _rx(theta)
    rot_mat = np.matmul(rz, ry)

    return rot_mat


def get_mvp_matrix(
    projection: np.ndarray,
    view: np.ndarray,
    model: np.ndarray = np.eye(4),
) -> np.ndarray:
    """Multiply model view and projection matrix together

    Args:
        projection (np.ndarray): matrix 4 x 4
        view (np.ndarray): matrix 4 x 4
        model (np.ndarray, optional): matrix 4 x 4. Defaults to np.eye(4).

    Returns:
        np.ndarray: mvp matrix 4 x 4
    """
    return np.matmul(model, np.matmul(view, projection))
