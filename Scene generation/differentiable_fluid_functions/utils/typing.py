import torch
import numpy as np


def check_torch_float(dtype) -> bool:
    """Check if the type is one of torch float types

    Args:
        dtype ([type]): the type to check

    Returns:
        bool: true if it is one on the types
    """
    return dtype == torch.float16 or dtype == torch.float32 or dtype == torch.float64


def check_numpy_float(dtype) -> bool:
    """Check if the type is one of numpy float types

    Args:
        dtype ([type]): the type to check

    Returns:
        bool: true if it is one on the types
    """
    return dtype == np.float32 or dtype == np.float64
