import numpy as np
import os
from .plot import (
    vector_plot,
    rdbu_bar_plot,
    _process_img_2d,
)
from PIL import Image


def load_npy(filename: str, remove_redundant_channel: bool = False) -> np.ndarray:
    """Loads .npy file into a np.array

    The .npy file comes from mantaflow's saved arrays. It can be density
    or velocity in 2D or 3D.

    Args:
        filename (str): filename of the array, should end with '.npy'.
        remove_redundant_channel (bool, optional): since the npy files are saved in accordance
            with mantaflow, the 2D velocities have a third channel which is
            redundant. The flag is to specify whether this channel should
            be removed when loading the npy file.. Defaults to True.

    Raises:
        ValueError: If the filename has the wrong extension

    Returns:
        np.ndarray: the content of the npy file
    """
    if not filename.endswith(".npy"):
        raise ValueError("filename should end with .npy.")
    array = np.load(filename)
    dim = 2 if len(array.shape) == 3 else 3

    # remove dummy channel in 2D velocity
    if dim == 2 and array.shape[2] == 3 and remove_redundant_channel:
        array = array[:, :, :-1]

    return array


def load_npz(filename: str, key: str) -> np.ndarray:
    """load the content from an npz file by a given key

    Args:
        filename (str): the filename from where to load the data
        key (str): which key should be extracted

    Raises:
        ValueError: file has the wrong extension
        ValueError: key does not exist in files

    Returns:
        np.ndarray: the content found in the data under the key
    """
    if not filename.endswith(".npz"):
        raise ValueError("filename should end with .npz.")
    data = np.load(filename)
    if key not in data.files:
        raise ValueError("key {} is not in npz file".format(key))
    return data[key]


def load_npz_raw(filename: str) -> dict:
    """load the content from an npz file by a given key

    Args:
        filename (str): the filename from where to load the data

    Raises:
        ValueError: file has the wrong extension

    Returns:
        dict: the content found in the file
    """
    if not filename.endswith(".npz"):
        raise ValueError("filename should end with .npz.")
    return np.load(filename)


def save_npy(array: np.ndarray, filename: str, add_redundant_channel: bool = False):
    """save a numpy array as npy file

    Args:
        array (np.ndarray): the array which used be saved
        filename (str): name of the npy file
        add_redundant_channel (bool, optional): add an extra channel to the array Defaults to False.

    Raises:
        ValueError: wrong filename was given.
    """
    if not filename.endswith(".npy"):
        raise ValueError("filename should end with .npy.")
    if len(array.shape) == 3 and array.shape[2] == 2 and add_redundant_channel:
        array = np.concatenate(
            (array, np.zeros((array.shape[0], array.shape[1], 1))), axis=2
        )
    np.save(filename, array)


def save_npz(filename: str, kwds: dict):
    if not filename.endswith(".npz"):
        raise ValueError("filename should end with .npz.")

    np.savez(filename, **kwds)


def mkdir(path: str):
    if not os.path.exists(path):
        os.makedirs(path)


def _get_filename_with_index(filename: str, index: int) -> str:
    basename, ext = os.path.splitext(filename)
    new_file_name = basename + "_{:03}".format(index) + ext
    return new_file_name


def get_filename_on_axis(filename: str, axis: str) -> str:
    basename = os.path.basename(filename)
    dirname = os.path.dirname(filename)
    basename_axis = axis + "_" + basename
    return os.path.join(dirname, basename_axis)


# for densities
def _render_front_view(array, transmit):
    density = array[::-1]  # view density from front to back
    tau = np.exp(-transmit * np.cumsum(density, axis=0))
    rendered = np.sum(density * tau, axis=0)
    rendered = rendered / np.max(rendered)  # remap to [0, 1]
    return rendered[::-1]


def save_img(array, filename: str, colormap: str = "RGB", project=True, transmit=0.05, chan=3):
    """Saves an array into an image.

    The array can be 2D/3D density or 2D/3D velocity field.

    Args:
        array(np.array): of size [B, (D), H, W, C], input array to be saved.
            Density array should be in range [0, 1].
            Velocity array is in range (-inf, inf)
        filename(string): filename of the output file, should end with .png.
    """
    if not filename.endswith(".png"):
        raise ValueError("filename should end with .png")
    vector_plot_colormaps = ["ARROW", "ARROW_COLOR", "STREAM", "STREAM_COLOR", "LIC"]
    use_suffix = array.shape[0] != 1
    if chan == 1:
        ret = np.zeros((array.shape[0], array.shape[1], array.shape[2]))
    else:
        ret = np.zeros((array.shape[0], array.shape[1], array.shape[2], chan))
    
    for i in range(array.shape[0]):
        cur_array = array[i]
        new_filename = _get_filename_with_index(filename, i) if use_suffix else filename
        # 2d case: directly save as image
        if len(cur_array.shape) == 3:
            if colormap in vector_plot_colormaps:
                vector_plot(cur_array, new_filename, mode="save", plot_type=colormap)
            elif colormap == "RDBU_BAR":
                cur_array = cur_array[::-1]  # flip the array upside down
                rdbu_bar_plot(cur_array, new_filename, mode="save")
            else:
                cur_array = cur_array[::-1]  # flip the array upside down
                #_save_img_2d(cur_array, new_filename, colormap)
                ret[i] = _get_img_2d(cur_array, new_filename, colormap)
                
        # 3d case: save middle slice in all three axes as image
        elif len(cur_array.shape) == 4:
            D, H, W, _ = cur_array.shape

            filename_x_middle = get_filename_on_axis(new_filename, "x_middle")
            filename_y_middle = get_filename_on_axis(new_filename, "y_middle")
            filename_z_middle = get_filename_on_axis(new_filename, "z_middle")
            array_x_middle = np.transpose(cur_array[:, :, int(W / 2), :], (1, 0, 2))[
                ::-1
            ]
            array_y_middle = cur_array[:, int(H / 2), :, :]
            array_z_middle = (cur_array[int(D / 2), :, :, :])[::-1]

            if colormap in vector_plot_colormaps:
                vector_plot(
                    array_x_middle[::-1],
                    filename_x_middle,
                    mode="save",
                    plot_type=colormap,
                )
                vector_plot(
                    array_y_middle, filename_y_middle, mode="save", plot_type=colormap
                )
                vector_plot(
                    array_z_middle[::-1],
                    filename_z_middle,
                    mode="save", 
                    plot_type=colormap,
                )
            elif colormap == "RDBU_BAR":
                rdbu_bar_plot(array_x_middle, filename_x_middle, mode="save")
                rdbu_bar_plot(array_y_middle, filename_y_middle, mode="save")
                rdbu_bar_plot(array_z_middle, filename_z_middle, mode="save")
            else:
                if project:
                    filename_z_proj = get_filename_on_axis(new_filename, "z_proj")
                    array_z_proj = _render_front_view(cur_array, transmit)
                    _save_img_2d(array_z_proj, filename_z_proj, colormap)
                else:
                    _save_img_2d(array_x_middle, filename_x_middle, colormap)
                    _save_img_2d(array_y_middle, filename_y_middle, colormap)
                    _save_img_2d(array_z_middle, filename_z_middle, colormap)
        else:
            raise ValueError("input array should be in size [B, (D), H, W, C].")
    return ret

def save_img_help(array, filename: str, colormap: str = "RGB", project=True, transmit=0.05, chan=3):
    """Saves an array into an image.

    The array can be 2D/3D density or 2D/3D velocity field.

    Args:
        array(np.array): of size [B, (D), H, W, C], input array to be saved.
            Density array should be in range [0, 1].
            Velocity array is in range (-inf, inf)
        filename(string): filename of the output file, should end with .png.
    """
    if not filename.endswith(".png"):
        raise ValueError("filename should end with .png")
    vector_plot_colormaps = ["ARROW", "ARROW_COLOR", "STREAM", "STREAM_COLOR", "LIC"]
    use_suffix = array.shape[0] != 1
    
    for i in range(array.shape[0]):
        cur_array = array[i]
        new_filename = _get_filename_with_index(filename, i) if use_suffix else filename
        # 2d case: directly save as image
        if len(cur_array.shape) == 3:
            if colormap in vector_plot_colormaps:
                vector_plot(cur_array, new_filename, mode="save", plot_type=colormap)
            elif colormap == "RDBU_BAR":
                cur_array = cur_array[::-1]  # flip the array upside down
                rdbu_bar_plot(cur_array, new_filename, mode="save")
            else:
                cur_array = cur_array[::-1]  # flip the array upside down
                _save_img_2d(cur_array, new_filename, colormap)
                
        # 3d case: save middle slice in all three axes as image
        elif len(cur_array.shape) == 4:
            D, H, W, _ = cur_array.shape

            filename_x_middle = get_filename_on_axis(new_filename, "x_middle")
            filename_y_middle = get_filename_on_axis(new_filename, "y_middle")
            filename_z_middle = get_filename_on_axis(new_filename, "z_middle")
            array_x_middle = np.transpose(cur_array[:, :, int(W / 2), :], (1, 0, 2))[
                ::-1
            ]
            array_y_middle = cur_array[:, int(H / 2), :, :]
            array_z_middle = (cur_array[int(D / 2), :, :, :])[::-1]

            if colormap in vector_plot_colormaps:
                vector_plot(
                    array_x_middle[::-1],
                    filename_x_middle,
                    mode="save",
                    plot_type=colormap,
                )
                vector_plot(
                    array_y_middle, filename_y_middle, mode="save", plot_type=colormap
                )
                vector_plot(
                    array_z_middle[::-1],
                    filename_z_middle,
                    mode="save", 
                    plot_type=colormap,
                )
            elif colormap == "RDBU_BAR":
                rdbu_bar_plot(array_x_middle, filename_x_middle, mode="save")
                rdbu_bar_plot(array_y_middle, filename_y_middle, mode="save")
                rdbu_bar_plot(array_z_middle, filename_z_middle, mode="save")
            else:
                if project:
                    filename_z_proj = get_filename_on_axis(new_filename, "z_proj")
                    array_z_proj = _render_front_view(cur_array, transmit)
                    _save_img_2d(array_z_proj, filename_z_proj, colormap)
                else:
                    _save_img_2d(array_x_middle, filename_x_middle, colormap)
                    _save_img_2d(array_y_middle, filename_y_middle, colormap)
                    _save_img_2d(array_z_middle, filename_z_middle, colormap)
        else:
            raise ValueError("input array should be in size [B, (D), H, W, C].")
            
def _save_img_2d(array, filename, colormap="RGB"):
    array = _process_img_2d(array, colormap)
    img = Image.fromarray(array)
    img.save(filename)
    
def _get_img_2d(array, filename, colormap="RGB"):
    img = _process_img_2d(array, colormap)
    
    return img
