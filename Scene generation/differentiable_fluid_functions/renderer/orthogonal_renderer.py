from .abstract_renderer import AbstractRenderer
import torch


class OrthogonalRenderer(AbstractRenderer):
    def __init__(self, transmittance: float):
        """[summary]

        Args:
            transmittance (float): transmittance absorption factor
        """
        self.transmittance = transmittance

    def _render(self, density: torch.Tensor) -> torch.Tensor:

        density_flip = torch.flip(density, dims=[2])
        transmit = torch.exp(-torch.cumsum(density_flip, dim=2) * self.transmittance)
        transmit = torch.flip(transmit, dims=[2])
        d_ren = torch.sum(density * transmit, axis=2)
        return d_ren, transmit

    def render(self, density: torch.Tensor) -> torch.Tensor:
        """Render a density field using a

        Args:
            density (torch.Tensor): density field with the following

        Returns:
            torch.Tensor: [1 x 1 x  Height x Width]
        """
        if len(density.shape) != 5:
            raise ValueError(
                "Density must have shape [1 x 1 x Depth x Height x Width]. "
                f"currently is {density.shape}"
            )

        d_ren, _ = self._render(density)
        denom = d_ren.max()
        if denom < 1e-6:
            return torch.zeros_like(d_ren)
        return d_ren / denom

    def renderAdditive(
        self, density: torch.Tensor, prev_rendering: torch.Tensor
    ) -> torch.Tensor:
        """render a volume slice based

        Args:
            density (torch.Tensor): Remaining density to render [1 x 1 x Depth x Height x Width]
            prev_rendering (torch.Tensor): previous rendering [1 x 1 x  Height x Width]

        Raises:
            ValueError: density has wrong shape
            ValueError: rendering has wrong shape
            ValueError: density and prev rendering shape does not match

        Returns:
            torch.Tensor: [1 x 1 x  Height x Width]
        """
        if len(density.shape) != 5:
            raise ValueError(
                "Density must have shape [1 x 1 x Depth x Height x Width]. "
                f"currently is {density.shape}"
            )
        if len(prev_rendering.shape) != 4:
            raise ValueError(
                "prev_rendering must have shape [1 x 1 x Height x Width]. "
                f"currently is {prev_rendering.shape}"
            )

        if (
            prev_rendering.shape[-1] != density.shape[-1]
            or prev_rendering.shape[-2] != density.shape[-2]
        ):
            raise ValueError(
                "The Previous rendering and the density need to have the same width and height"
                f"currently is {prev_rendering.shape[-1]} != {density.shape[-1]} or "
                f"{prev_rendering.shape[-2]} != {density.shape[-2]}"
            )
        d_ren, transmit = self._render(density)
        d_unnormalized = prev_rendering * transmit[:, :, 0] + d_ren
        if d_unnormalized.max() > 1e-9:
            d_rendering_final = d_unnormalized / d_unnormalized.max()
        else:
            d_rendering_final = d_unnormalized
        return d_rendering_final, d_unnormalized

    __call__ = render
