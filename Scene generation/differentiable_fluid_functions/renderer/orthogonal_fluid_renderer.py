from .abstract_renderer import AbstractRenderer
import torch


class OrthogonalFluidRenderer(AbstractRenderer):
    def __init__(self, transmittance: float):
        """[summary]

        Args:
            transmittance (float): transmittance absorption factor
        """
        self.transmittance = transmittance

    def render(self, density: torch.Tensor) -> torch.Tensor:
        """Render a density field representing a fluid

        Args:
            density (torch.Tensor): density field with the following

        Returns:
            torch.Tensor: [1 x 1 x  Height x Width]
        """
        if len(density.shape) != 5:
            raise ValueError(
                "Density must have shape [1 x 1 x Depth x Height x Width]. "
                f"currently is {density.shape}"
            )

        density_flip = torch.flip(density, dims=[2])
        transmit = torch.exp(-torch.cumsum(density_flip, dim=2) * self.transmittance)
        d_ren = 1.0 - transmit[:, :, -1]
        return d_ren / d_ren.max()

    __call__ = render
