from abc import ABC, abstractmethod
import torch


class AbstractRenderer(ABC):
    @abstractmethod
    def render(self, density: torch.Tensor) -> torch.Tensor:
        ...
