import torch
from differentiable_fluid_functions.simulation import SimulationParameters
from differentiable_fluid_functions.grid import (
    Flag3dGrid,
    Level3dGrid,
    Node3dGrid,
    Real3dGrid,
    Staggered3dGrid,
    Abstract3dGrid,
    Flag2dGrid,
    Level2dGrid,
    Node2dGrid,
    Real2dGrid,
    Staggered2dGrid,
    Abstract2dGrid,
    AbstractGrid,
)
from differentiable_fluid_functions.types import (
    GridType,
    ShapeType,
    SolverType,
    PreconditionerType,
)
from differentiable_fluid_functions.external_forces import (
    ExternalForces2d,
    ExternalForces3d,
)
from differentiable_fluid_functions.advection import (
    SemiLagrange2dAdvection,
    SemiLagrange3dAdvection,
)
from differentiable_fluid_functions.pressure import (
    PressureProjection2dFractional,
    PressureProjection2dVoxelized,
    PressureProjection3dFractional,
    PressureProjection3dVoxelized,
    PressureProjectionType,
)
from differentiable_fluid_functions.solver import (
    DirectSolver2d,
    ConjugateGradient2d,
    ConjugateGradient3d,
    Jacobi2d,
    Jacobi3d,
    Multigrid2d,
    Multigrid3d,
    MultigridPreconditioner2d,
    MultigridPreconditioner3d,
    IncompletePoissonPreconditioner2d,
    IncompletePoissonPreconditioner3d,
    IdentityPreconditioner,
)

from differentiable_fluid_functions.shapes import (
    Box2dShape,
    Blob2dShape,
    Sphere2dShape,
    BoxSet2dShape,
    Box3dShape,
    Blob3dShape,
    Sphere3dShape,
    BoxSet3dShape,
)

grid_type_to_class_2d = {
    GridType.FLAG.value: Flag2dGrid,
    GridType.LEVEL.value: Level2dGrid,
    GridType.NODE.value: Node2dGrid,
    GridType.REAL.value: Real2dGrid,
    GridType.STAGGERED.value: Staggered2dGrid,
}

grid_type_to_class_3d = {
    GridType.FLAG.value: Flag3dGrid,
    GridType.LEVEL.value: Level3dGrid,
    GridType.NODE.value: Node3dGrid,
    GridType.REAL.value: Real3dGrid,
    GridType.STAGGERED.value: Staggered3dGrid,
}

pressure_projection_type_to_class_2d = {
    PressureProjectionType.FRACTIONAL.value: PressureProjection2dFractional,
    PressureProjectionType.VOXELIZED.value: PressureProjection2dVoxelized,
}

pressure_projection_type_to_class_3d = {
    PressureProjectionType.FRACTIONAL.value: PressureProjection3dFractional,
    PressureProjectionType.VOXELIZED.value: PressureProjection3dVoxelized,
}

solver_type_to_class_2d = {
    SolverType.PCG.value: ConjugateGradient2d,
    SolverType.DIRECT.value: DirectSolver2d,
    SolverType.JACOBI.value: Jacobi2d,
    SolverType.MULTIGRID.value: Multigrid2d,
}

preconditioner_type_to_class_2d = {
    PreconditionerType.NONE.value: IdentityPreconditioner,
    PreconditionerType.INCOMPLETE_POISSON.value: IncompletePoissonPreconditioner2d,
    PreconditionerType.MULTIGRID.value: MultigridPreconditioner2d,
}

preconditioner_type_to_class_3d = {
    PreconditionerType.NONE.value: IdentityPreconditioner,
    PreconditionerType.INCOMPLETE_POISSON.value: IncompletePoissonPreconditioner3d,
    PreconditionerType.MULTIGRID.value: MultigridPreconditioner3d,
}

solver_type_to_class_3d = {
    SolverType.PCG.value: ConjugateGradient3d,
    SolverType.JACOBI.value: Jacobi3d,
    SolverType.MULTIGRID.value: Multigrid3d,
}


shape_type_to_class_2d = {
    ShapeType.BOX.value: Box2dShape,
    ShapeType.BLOB.value: Blob2dShape,
    ShapeType.SPHERE.value: Sphere2dShape,
    ShapeType.BOXSET.value: BoxSet2dShape,
}

shape_type_to_class_3d = {
    ShapeType.BOX.value: Box3dShape,
    ShapeType.BLOB.value: Blob3dShape,
    ShapeType.SPHERE.value: Sphere3dShape,
    ShapeType.BOXSET.value: BoxSet3dShape,
}


class SimulationRunner(object):
    def __init__(
        self,
        parameters: SimulationParameters,
    ):
        self.parameters = parameters

    def _create_2d_grid(
        self, value: torch.Tensor, grid_type: GridType
    ) -> Abstract2dGrid:
        cur_class = grid_type_to_class_2d[grid_type.value]
        if grid_type == GridType.STAGGERED:
            return cur_class.get_constant_grid(
                value_x=value,
                value_y=value,
                simulation_size=self.parameters.simulation_size,
            )
        else:
            return cur_class.get_constant_grid(
                value=value, simulation_size=self.parameters.simulation_size
            )

    def _create_3d_grid(
        self, value: torch.Tensor, grid_type: GridType
    ) -> Abstract3dGrid:
        cur_class = grid_type_to_class_3d[grid_type.value]
        if grid_type == GridType.STAGGERED:
            return cur_class.get_constant_grid(
                value_x=value,
                value_y=value,
                value_z=value,
                simulation_size=self.parameters.simulation_size,
            )
        else:
            return cur_class.get_constant_grid(
                value=value, simulation_size=self.parameters.simulation_size
            )

    def create_grid(self, grid_type: GridType) -> AbstractGrid:
        value = torch.tensor(0).to(self.parameters.device)
        if grid_type == GridType.FLAG:
            value = value.to(torch.uint8)
        else:
            value = value.to(self.parameters.dtype)

        if self.parameters.is_2d():
            return self._create_2d_grid(value, grid_type)
        else:
            return self._create_3d_grid(value, grid_type)

    def create_extforces(self):
        if self.parameters.is_2d():
            return ExternalForces2d(
                simulation_size=self.parameters.simulation_size,
                device=self.parameters.device,
                dtype=self.parameters.dtype,
            )
        else:
            return ExternalForces3d(
                simulation_size=self.parameters.simulation_size,
                device=self.parameters.device,
                dtype=self.parameters.dtype,
            )

    def create_advection(self):
        if self.parameters.is_2d():
            return SemiLagrange2dAdvection()
        else:
            return SemiLagrange3dAdvection()

    def _create_solver(
        self, solver_type: SolverType, preconditioner_type: PreconditionerType
    ):
        if self.parameters.is_2d():
            cur_class = solver_type_to_class_2d[solver_type.value]
        else:
            cur_class = solver_type_to_class_3d[solver_type.value]
        if solver_type == SolverType.PCG:
            precon = self._create_preconditioner(preconditioner_type)
            return cur_class(precon=precon)
        return cur_class()

    def _create_preconditioner(self, preconditioner_type: PreconditionerType):
        if self.parameters.is_2d():
            cur_class = preconditioner_type_to_class_2d[preconditioner_type.value]
        else:
            cur_class = preconditioner_type_to_class_3d[preconditioner_type.value]
        return cur_class()

    def create_pressure_projection(
        self,
        pressure_projection_type: PressureProjectionType,
        solver_type: SolverType,
        preconditioner_type: PreconditionerType = PreconditionerType.NONE,
        system_perturb: float = 0.0,
    ):
        if self.parameters.is_2d():
            cur_class = pressure_projection_type_to_class_2d[
                pressure_projection_type.value
            ]
            return cur_class(
                solver=self._create_solver(solver_type, preconditioner_type),
                device=self.parameters.device,
                dtype=self.parameters.dtype,
                simulation_size=self.parameters.simulation_size,
                conjugate_gradient_accuracy=self.parameters.conjugate_gradient_accuracy,
                system_perturb=system_perturb,
            )
        else:
            cur_class = pressure_projection_type_to_class_3d[
                pressure_projection_type.value
            ]
            return cur_class(
                solver=self._create_solver(solver_type, preconditioner_type),
                device=self.parameters.device,
                dtype=self.parameters.dtype,
                simulation_size=self.parameters.simulation_size,
                conjugate_gradient_accuracy=self.parameters.conjugate_gradient_accuracy,
                system_perturb=system_perturb,
            )

    def create_shape(self, shape_type: ShapeType):
        if self.parameters.is_2d():
            cur_class = shape_type_to_class_2d[shape_type.value]
            if shape_type == ShapeType.BLOB:
                return cur_class(
                    simulation_size=self.parameters.simulation_size,
                    n_sphere=self.parameters.n_sphere,
                    dtype=self.parameters.dtype,
                    device=self.parameters.device,
                )
            return cur_class(
                simulation_size=self.parameters.simulation_size,
                dtype=self.parameters.dtype,
                device=self.parameters.device,
            )
        else:
            cur_class = shape_type_to_class_3d[shape_type.value]
            if shape_type == ShapeType.BLOB:
                return cur_class(
                    simulation_size=self.parameters.simulation_size,
                    n_sphere=self.parameters.n_sphere,
                    dtype=self.parameters.dtype,
                    device=self.parameters.device,
                )
            return cur_class(
                simulation_size=self.parameters.simulation_size,
                dtype=self.parameters.dtype,
                device=self.parameters.device,
            )

    def step(self):
        self.parameters.step()

    def create_tensor(self, data):
        return torch.tensor(
            data,
            dtype=self.parameters.dtype,
            device=self.parameters.device,
        )
