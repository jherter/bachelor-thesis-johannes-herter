import torch
from typing import List


class SimulationParameters(object):
    def __init__(
        self,
        dtype=torch.float32,
        dim: int = 2,
        dt: float = 1.0,
        device: torch.device = torch.device("cpu"),
        simulation_size: List[int] = [1, 1, 256, 256],
        conjugate_gradient_accuracy: float = 1e-6,
        n_sphere: int = 8,
        advection_order: int = 1,
        rk_order: int = 1,
    ):
        self.dim = dim
        self.dtype = dtype
        self.dt = dt
        self.device = device

        self.advection_order = advection_order
        self.rk_order = rk_order

        self.frame = 0
        self.time_per_frame = 0
        self.frame_length = 1.0
        self.time_total = 0
        self.conjugate_gradient_accuracy = conjugate_gradient_accuracy
        self.n_sphere = n_sphere

        if dim == 2 and len(simulation_size) != 4:
            raise ValueError(
                "For 2d simulation simulation size should have 4 parameters B x C x H x W"
            )

        self.simulation_size = simulation_size

    def step(self):
        """Advances the simulation one time step"""
        self.time_per_frame += self.dt
        self.time_total += self.dt

        if self.time_per_frame >= self.frame_length:
            self.frame += 1

            # re-calculate total time to prevent drift
            self.time_total = self.frame * self.frame_length
            self.time_per_frame = 0

    def get_dx(self):
        return 1.0 / max(self.simulation_size)

    def is_2d(self):
        return self.dim == 2

    def is_3d(self):
        return self.dim == 3

    def set_device(self, device: str = "cuda"):
        if device not in ["cuda", "cpu"]:
            raise ValueError(
                "Set_device: device {} must be either cuda or cpu.".format(device)
            )
        self.device = torch.device(device)
