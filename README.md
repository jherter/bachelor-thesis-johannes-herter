# Navigating the Latent Space of Simulation Data via Deep Reinforcement Learning

![](assets/title_page_image.png?raw=True)

## Abstract

Latent sequential prediction for complex equations such as predicting fluid flows from a set of reduced parameters often results in drifting and 
accumulated error in theory and practice. Over prolonged time spans, this can lead to a polynomial increase in prediction errors which in turn can 
cause instability of the underlying time integration. In the present work, we propose a general framework based on imitation learning to robustly 
navigate the latent space of time series datasets for fluid dynamics. To achieve stable predictions, a physics-informed LSTM-based time integration 
network is trained for modeling the nonlinear dynamics of the unsteady time-varying flow. To prevent compounding errors, we iteratively sample data 
from the latent space during training and add it to the training dataset with underlying ground truth labels. We can show that our algorithm learns 
a policy with good performance under the distribution of observations it induces. Experiments on 2D moving smoke source data demonstrate that our 
method exhibits sufficient temporal stability, but also that the performance of the optimal policy is upper bounded by the expressiveness of the 
latent space and thus the quality of the encoder.

----
Bachelor Thesis Johannes Herter

Supervised by: Dr. Byungsoo Kim

Overseen by: Prof. Markus Gross
